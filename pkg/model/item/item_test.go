//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package item_test

import (
	"encoding/json"
	"strings"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/item"
)

var jsonItem = []byte(`
{
  "name": "Potion",
  "stack": 1,
  "target": "team",
  "type": 7,
  "reusable": false,
  "modifier": [
    {
      "name": "absoluteHealth",
      "amount": 10
    }
  ]
}
`)

var jsonItem2 = []byte(`
{
  "name": "ReusablePotion",
  "stack": 3,
  "target": "opponent",
  "type": 11,
  "reusable": true,
  "modifier": [
    {
      "name": "absoluteHealth",
      "amount": 13
    }
  ]
}
`)

func TestUnmarshal(t *testing.T) {
	Convey("Trying to unmarshal a valid item", t, func() {
		base := &item.Base{}
		err := json.Unmarshal(jsonItem, base)
		So(err, ShouldBeNil)

		Convey("The name should be 'Potion'", func() {
			So(base.Name, ShouldEqual, "Potion")
		})
		Convey("The stack should be 1", func() {
			So(base.Stack, ShouldEqual, 1)
		})
		Convey("The target should be 'team'", func() {
			So(base.Target, ShouldEqual, "team")
		})
		Convey("The item should not be reusable", func() {
			So(base.Reusable, ShouldBeFalse)
		})
		Convey("the type should be 'battle_breeder', 'battle_wild' and 'world'", func() {
			So(base.Type, ShouldEqual, item.Type_Battle_Breeder|item.Type_Battle_Wild|item.Type_World)
		})

	})
	Convey("Trying to unmarshal a valid item", t, func() {
		base := &item.Base{}
		err := json.Unmarshal(jsonItem2, base)
		So(err, ShouldBeNil)

		Convey("The name should be 'ReusablePotion'", func() {
			So(base.Name, ShouldEqual, "ReusablePotion")
		})
		Convey("The stack should be 3", func() {
			So(base.Stack, ShouldEqual, 3)
		})
		Convey("The target should be 'opponent'", func() {
			So(base.Target, ShouldEqual, "opponent")
		})
		Convey("The item should be reusable", func() {
			So(base.Reusable, ShouldBeTrue)
		})
		Convey("the type should be 'battle_breeder', 'battle_wild' and 'holding'", func() {
			So(base.Type, ShouldEqual, item.Type_Battle_Breeder|item.Type_Battle_Wild|item.Type_Holding)
		})

	})
	Convey("Trying to unmarshal invalid items", t, func() {
		jsonArr := []string{
			`{`,
			`"name": "ReusablePotion",`,
			`"stack": 3,`,
			`"target": "opponent",`,
			`"type": 11,`,
			`"reusable": true,`,
			`"effect": {},`,
			`"x": 1`,
			`}`,
		}
		Convey("Missing the name", func() {
			jsonArr = append(jsonArr[:1], jsonArr[2:]...)
			jsonStr := []byte(strings.Join(jsonArr, "\n"))
			base := &item.Base{}
			err := json.Unmarshal(jsonStr, base)
			So(err, ShouldNotBeNil)

		})

		Convey("Missing the stack", func() {
			jsonArr = append(jsonArr[:2], jsonArr[3:]...)
			jsonStr := []byte(strings.Join(jsonArr, "\n"))
			base := &item.Base{}
			err := json.Unmarshal(jsonStr, base)
			So(err, ShouldNotBeNil)

		})
		Convey("Missing the target", func() {
			jsonArr = append(jsonArr[:3], jsonArr[4:]...)
			jsonStr := []byte(strings.Join(jsonArr, "\n"))
			base := &item.Base{}
			err := json.Unmarshal(jsonStr, base)
			So(err, ShouldNotBeNil)

		})
		Convey("Missing the type", func() {
			jsonArr = append(jsonArr[:4], jsonArr[5:]...)
			jsonStr := []byte(strings.Join(jsonArr, "\n"))
			base := &item.Base{}
			err := json.Unmarshal(jsonStr, base)
			So(err, ShouldNotBeNil)

		})

	})

}

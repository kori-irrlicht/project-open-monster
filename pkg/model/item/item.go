//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package item

import (
	"encoding/json"
	"errors"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/status"
)

type Type uint

const (
	// Type_Battle_Breeder allows the usage of the item in battles between breeders
	Type_Battle_Breeder Type = 1 << iota
	// Type_Battle_Wild allows the usage of the item in battles against wild monsters
	Type_Battle_Wild
	Type_World
	Type_Holding
)

type ModifierType string

const (
	ModifierType_AbsoluteHealth = "absoluteHealth"
	ModifierType_CureStatus     = "cureStatus"
	ModifierType_Capture        = "capture"
)

type Base struct {
	Name     string `json:"name"`
	Stack    int    `json:"stack"`
	Target   string `json:"target"`
	Type     Type   `json:"type"`
	Reusable bool   `json:"reusable"`

	Effect Effect `json:"effect"`
}

type Effect struct {
	Overworld string `json:"overworld"`
	Battle    string `json:"battle"`
}

func (b *Base) UnmarshalJSON(data []byte) error {
	var tmp struct {
		Name     string `json:"name"`
		Stack    int    `json:"stack"`
		Target   string `json:"target"`
		Type     Type   `json:"type"`
		Reusable bool   `json:"reusable"`
		Effect   Effect `json:"effect"`
	}

	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}

	b.Name = tmp.Name
	if len(tmp.Name) == 0 {
		return errors.New("Name must not be empty")
	}

	b.Stack = tmp.Stack
	if tmp.Stack <= 0 {
		return errors.New("Stack must be greater than 0")
	}
	b.Target = tmp.Target
	if len(tmp.Target) == 0 {
		return errors.New("Target must not be empty")
	}
	b.Type = tmp.Type
	if tmp.Type == 0 {
		return errors.New("Type must not be empty")
	}
	b.Reusable = tmp.Reusable

	b.Effect = tmp.Effect

	return nil

}

type Modifier interface {
	Type() ModifierType
}

type CaptureModifier struct{}

func (cm CaptureModifier) Type() ModifierType {
	return ModifierType_Capture
}

type AbsoluteHealthModifier struct {
	Amount int
}

func (ahm *AbsoluteHealthModifier) MarshalJSON() ([]byte, error) {
	data := struct {
		Type   ModifierType `json:"type"`
		Amount int          `json:"amount"`
	}{ahm.Type(), ahm.Amount}
	return json.Marshal(data)
}

func (ahm AbsoluteHealthModifier) Type() ModifierType {
	return ModifierType_AbsoluteHealth
}

type CureStatusModifier struct {
	Status status.Status
}

func (ahm *CureStatusModifier) MarshalJSON() ([]byte, error) {
	data := struct {
		Type   ModifierType  `json:"type"`
		Status status.Status `json:"status"`
	}{ahm.Type(), ahm.Status}
	return json.Marshal(data)
}

func (ahm CureStatusModifier) Type() ModifierType {
	return ModifierType_CureStatus
}

type Item struct {
	Base   *Base
	Amount uint
}

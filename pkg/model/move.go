//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package model

import (
	"encoding/json"
	"errors"
	"fmt"
)

//TODO: Move to  config file
const (
	MoveAmount int = 4
)

type Move struct {
	base     MoveBase
	cooldown int
}

func NewMove(base MoveBase) *Move {
	m := &Move{
		base: base,
	}

	return m
}

func (move *Move) ResetCooldown() {
	move.cooldown = 0
}

func (move *Move) IncreaseCooldown() {
	if move.cooldown < move.base.Cooldown() {
		move.cooldown += 1
	}
}

func (m Move) IsReady() bool {
	return m.cooldown >= m.Base().Cooldown()
}

func (m Move) Base() MoveBase {
	return m.base
}

func (m Move) String() string {
	return fmt.Sprintf(`Move{base: %v, cooldown: %v}`, m.Base(), m.cooldown)
}

func (m *Move) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`"%s"`, m.Base().Name())), nil
}

type MoveArr [MoveAmount]*Move

func NewMoveArr() MoveArr {
	var ma MoveArr
	for i := 0; i < MoveAmount; i++ {
		ma[i] = NewMove(NilMoveBase)
	}
	return ma

}

/**
Contains checks, if there is a move with the given MoveBase in the MoveArr
*/
func (ma MoveArr) Contains(mb MoveBase) bool {
	for i := 0; i < MoveAmount; i++ {
		if ma[i].Base() == mb {
			return true
		}
	}
	return false

}

/**
Add adds a move to the MoveArr if there is space left and the move is not already in the MoveArr
Returns an error, if the move was not added
*/
func (ma MoveArr) Add(m *Move) (MoveArr, error) {
	if ma.Contains(m.Base()) {
		return ma, errors.New("Already contains move")
	}
	for i := 0; i < MoveAmount; i++ {
		if ma[i].Base() == NilMoveBase {
			ma[i] = m
			return ma, nil
		}
	}
	return ma, errors.New("Could not add move. MoveArr is full")
}

/**
Remove removes the move with the given index. Also reorders the moves to remove empty spaces after the move has been removed
Returns an error, if it is the last move left or the index is out ouf bounds
*/
func (ma MoveArr) Remove(index int) (MoveArr, error) {
	if index < 0 || index >= MoveAmount {
		return ma, errors.New("Index out of bounds")
	}

	// Since the moves are reordered after a move was removed, the second position will be empty if there is only one move left
	if ma[1].Base() == NilMoveBase {
		return ma, errors.New("Can't remove the last move")
	}

	ma[index] = NewMove(NilMoveBase)
	// Bubble the nil value to the end of the MoveArr
	for i := index + 1; i < MoveAmount; i++ {
		ma[i-1], ma[i] = ma[i], ma[i-1]
	}

	return ma, nil
}

func (ma MoveArr) MarshalJSON() ([]byte, error) {
	var moves []*Move
	for _, k := range ma {
		if k.Base() != NilMoveBase {
			moves = append(moves, k)
		}
	}
	return json.Marshal(moves)
}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package model_test

import (
	"encoding/json"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/status"
)

var bspMoveBaseJson = []byte(`
{
   "name": "White Frost",
   "type": "Ice",
   "staminaUsage": 5,
   "target": "Single",
   "cooldown": 1,
   "modifier": [
      {
         "type": "damage",
         "power": 90,
         "attackStat": "SpecAttack",
         "defenseStat": "SpecDefense"
      },
      {
         "type": "status",
         "status": "Freeze",
         "power": 30
      }
   ]
}
`)

func TestMoveBase(t *testing.T) {
	Convey("Given a template in json notation and unmarshalling it", t, func() {
		var moveBase model.MoveBaseTemplate
		err := json.Unmarshal(bspMoveBaseJson, &moveBase)
		Convey("So there should be no error", func() {
			So(err, ShouldBeNil)
		})
		Convey("The name should be 'White Frost'", func() {
			So(moveBase.Name(), ShouldEqual, "White Frost")
		})
		Convey("The type should be 'Ice'", func() {
			So(moveBase.Type(), ShouldEqual, "Ice")
		})
		Convey("The staminaUsage should be '5'", func() {
			So(moveBase.StaminaUsage(), ShouldEqual, 5)
		})
		Convey("The target should be 'Single'", func() {
			So(moveBase.Target(), ShouldEqual, "Single")
		})
		Convey("The cooldown should be '1'", func() {
			So(moveBase.Cooldown(), ShouldEqual, 1)
		})
		Convey("The first modifier should be 'Damage'", func() {
			So(moveBase.Modifier(), ShouldNotBeEmpty)
			mod, ok := moveBase.Modifier()[0].(*model.DamageModifier)
			So(ok, ShouldBeTrue)
			So(mod.ModifierType(), ShouldEqual, model.ModifierType_Damage)
			So(mod.Power, ShouldEqual, 90)
			So(mod.AttackStat, ShouldEqual, "SpecAttack")
			So(mod.DefenseStat, ShouldEqual, "SpecDefense")
		})
		Convey("The second modifier should be 'status'", func() {
			So(moveBase.Modifier(), ShouldNotBeEmpty)
			mod, ok := moveBase.Modifier()[1].(*model.StatusModifier)
			So(ok, ShouldBeTrue)
			So(mod.ModifierType(), ShouldEqual, model.ModifierType_Status)
			So(mod.Status, ShouldEqual, status.Freeze)
			So(mod.Power, ShouldEqual, 30)

		})

	})
}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package habitat

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

/* #nosec */
var Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

var habitatCache = map[string][]Entry{}

func Add(name string, he Entry) error {
	exists := false

	for k := range model.SpeciesCache {
		if k == he.Species {
			exists = true
			break
		}
	}
	if !exists {
		return fmt.Errorf("Species '%s' does not exist", he.Species)
	}

	habitatCache[name] = append(habitatCache[name], he)
	return nil
}

func Get(name string) []Entry {
	return habitatCache[name]
}

func GetRandomEntry(name string) Entry {
	entries := []Entry{}
	for _, v := range Get(name) {
		for i := uint8(0); i < v.Chance; i++ {
			entries = append(entries, v)
		}
	}

	return entries[Rand.Intn(len(entries))]
}

func Clear() {
	habitatCache = map[string][]Entry{}
}

// CreateMonsterFromHabitatEntry takes an Entry and converts it into a monster.
//
// In most cases CreateRandomMonsterFromHabitat might be the preferred method.
func CreateMonsterFromHabitatEntry(entry Entry) *model.Monster {
	return model.CreateMonster(model.SpeciesCache[entry.Species], func() (model.Level, model.Level) { return entry.MinLevel, entry.MaxLevel }, model.DefaultGeneticModifierRange)
}

// CreateRandomMonsterFromHabitat returns a random monster from its habitat list
func CreateRandomMonsterFromHabitat(name string) *model.Monster {
	return CreateMonsterFromHabitatEntry(GetRandomEntry(name))
}

type Entry struct {
	Species string

	// The chance of encountering a monster.
	// A monster with a value of 2 appears twice as often as a monster with a value of 1
	Chance uint8

	// The minimal level at which a monster can be encountered
	MinLevel model.Level

	// The maximal level at which a monster can be encountered
	MaxLevel model.Level
}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package habitat_test

import (
	"math/rand"
	"testing"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/spf13/viper"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/file"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/json"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/habitat"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/status"
)

func init() {
	logrus.SetLevel(logrus.WarnLevel)
	basepath_monster := "../../assets/data/monster"
	basepath_moves := "../../assets/data/moves"
	viper.SetDefault("data.dir.monster", basepath_monster)
	viper.SetDefault("data.dir.move", basepath_moves)

	if err := LoadMoves(); err != nil {
		panic(err)
	}
	if err := LoadSpecies(); err != nil {
		panic(err)
	}
	if err := LoadStatus(); err != nil {
		panic(err)
	}
}

func TestGetHabitat(t *testing.T) {

	Convey("Requesting an unknown area", t, func() {
		e := habitat.Get("unknown")
		Convey("Nil should be returned", func() {
			So(e, ShouldBeNil)
		})
		Reset(habitat.Clear)
	})
}

func TestAddHabitat(t *testing.T) {

	Convey("Adding a valid Entry", t, func() {
		he := habitat.Entry{
			Species:  "Hoarfox",
			Chance:   50,
			MinLevel: 10,
			MaxLevel: 20,
		}
		err := habitat.Add("test", he)
		Convey("Should return no error", func() {
			So(err, ShouldBeNil)
		})
		Convey("The entry should be added", func() {
			e := habitat.Get("test")
			So(e, ShouldContain, he)
		})
		Reset(habitat.Clear)
	})

	Convey("Adding an Entry with an unknown species", t, func() {
		he := habitat.Entry{
			Species:  "Bllaaaa",
			Chance:   50,
			MinLevel: 10,
			MaxLevel: 20,
		}
		err := habitat.Add("test", he)
		Convey("Should return an error", func() {
			So(err, ShouldNotBeNil)
		})
		Convey("The entry should not be added", func() {
			e := habitat.Get("test")
			So(e, ShouldNotContain, he)
		})
		Reset(habitat.Clear)
	})

}

func TestGetRandomHabitat(t *testing.T) {

	Convey("Adding entries", t, func() {

		he := habitat.Entry{
			Species:  "Hoarfox",
			Chance:   2,
			MinLevel: 10,
			MaxLevel: 20,
		}
		err := habitat.Add("test", he)
		if err != nil {
			panic(err)
		}

		he = habitat.Entry{
			Species:  "Owlix",
			Chance:   1,
			MinLevel: 10,
			MaxLevel: 20,
		}
		err = habitat.Add("test", he)
		if err != nil {
			panic(err)
		}

		Convey("Getting 100 random entries", func() {
			habitat.Rand = rand.New(rand.NewSource(10))
			var entries []habitat.Entry
			for i := 0; i < 100; i++ {
				entries = append(entries, habitat.GetRandomEntry("test"))
			}
			count := map[string]int{"Hoarfox": 0, "Owlix": 0}
			for _, v := range entries {
				count[v.Species]++
			}

			Convey("'Hoarfox' should be at around 66 entries", func() {
				So(count["Hoarfox"], ShouldAlmostEqual, 66, 5)
			})
			Convey("'Owlix' should be at around 33 entries", func() {
				So(count["Owlix"], ShouldAlmostEqual, 33, 5)
			})

		})
		Reset(habitat.Clear)

	})
}

func TestCreateMonsterFromHabitatEntry(t *testing.T) {
	Convey("Given a habitat entry", t, func() {
		he := habitat.Entry{
			Species:  "Hoarfox",
			Chance:   2,
			MinLevel: 10,
			MaxLevel: 20,
		}
		mon := habitat.CreateMonsterFromHabitatEntry(he)
		Convey("The species should match", func() {
			So(mon.Species.Name, ShouldEqual, he.Species)
		})
		Convey("The level should be between 10 and 20", func() {
			So(mon.Level, ShouldBeBetweenOrEqual, he.MinLevel, he.MaxLevel)
		})
		Reset(habitat.Clear)
	})
}

func LoadSpecies() error {
	l := logrus.WithField("method", "LoadSpecies")
	l.Infoln("Loading")

	provider := file.Provider{}
	reader, _ := provider.ProvideFiles("../../../assets/data")
	loader := json.NewSpeciesLoader(reader["monster"])
	species, err := loader.Load()
	if err != nil {
		return errors.Wrap(err, "could not load species")
	}
	model.SpeciesCache = species
	l.Infoln("done")
	return nil
}

func LoadMoves() error {
	l := logrus.WithField("method", "LoadMoves")
	l.Infoln("Loading")

	provider := file.Provider{}
	reader, _ := provider.ProvideFiles("../../../assets/data")
	loader := json.NewMoveLoader(reader["moves"])
	moves, err := loader.Load()
	if err != nil {
		return errors.Wrap(err, "could not load moves")
	}
	model.MoveBaseCache = moves
	l.Infoln("done")
	return nil
}

func LoadStatus() error {
	l := logrus.WithField("method", "LoadStatus")
	l.Infoln("Loading")

	provider := file.Provider{}
	reader, _ := provider.ProvideFiles("../../../assets/data")
	loader := json.NewStatusLoader(reader["status"])
	stat, err := loader.Load()
	if err != nil {
		return errors.Wrap(err, "could not load status")
	}
	for _, v := range stat {
		status.Register(*v)
	}
	l.Infoln("done")
	return nil
}

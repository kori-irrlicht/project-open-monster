//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package model_test

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

func TestAttributes(t *testing.T) {
	Convey("Given some attributes", t, func() {
		attr := model.Attributes{
			Health:      10,
			PhysAttack:  20,
			PhysDefense: 30,
			SpecAttack:  40,
			SpecDefense: 50,
			Initiative:  60,
			Stamina:     70,
		}
		Convey("Adding some modifier", func() {

			Convey("To all attributes", func() {

				mod := model.Attributes{
					Health:      10,
					PhysAttack:  20,
					PhysDefense: 30,
					SpecAttack:  40,
					SpecDefense: 50,
					Initiative:  60,
					Stamina:     70,
				}

				after := attr.Add(mod)

				So(after.Health, ShouldEqual, 20)
				So(after.PhysAttack, ShouldEqual, 40)
				So(after.PhysDefense, ShouldEqual, 60)
				So(after.SpecAttack, ShouldEqual, 80)
				So(after.SpecDefense, ShouldEqual, 100)
				So(after.Initiative, ShouldEqual, 120)
				So(after.Stamina, ShouldEqual, 140)
			})
			Convey("To health", func() {

				mod := model.Attributes{
					Health:      40,
					PhysAttack:  0,
					PhysDefense: 0,
					SpecAttack:  0,
					SpecDefense: 0,
					Initiative:  0,
				}

				after := attr.Add(mod)

				So(after.Health, ShouldEqual, 50)
				So(after.PhysAttack, ShouldEqual, 20)
				So(after.PhysDefense, ShouldEqual, 30)
				So(after.SpecAttack, ShouldEqual, 40)
				So(after.SpecDefense, ShouldEqual, 50)
				So(after.Initiative, ShouldEqual, 60)
				So(after.Stamina, ShouldEqual, 70)
			})
			Convey("To PhysAttack", func() {

				mod := model.Attributes{
					Health:      0,
					PhysAttack:  40,
					PhysDefense: 0,
					SpecAttack:  0,
					SpecDefense: 0,
					Initiative:  0,
				}

				after := attr.Add(mod)

				So(after.Health, ShouldEqual, 10)
				So(after.PhysAttack, ShouldEqual, 60)
				So(after.PhysDefense, ShouldEqual, 30)
				So(after.SpecAttack, ShouldEqual, 40)
				So(after.SpecDefense, ShouldEqual, 50)
				So(after.Initiative, ShouldEqual, 60)
				So(after.Stamina, ShouldEqual, 70)
			})
			Convey("To PhysDefense", func() {

				mod := model.Attributes{
					Health:      0,
					PhysAttack:  0,
					PhysDefense: 40,
					SpecAttack:  0,
					SpecDefense: 0,
					Initiative:  0,
				}

				after := attr.Add(mod)

				So(after.Health, ShouldEqual, 10)
				So(after.PhysAttack, ShouldEqual, 20)
				So(after.PhysDefense, ShouldEqual, 70)
				So(after.SpecAttack, ShouldEqual, 40)
				So(after.SpecDefense, ShouldEqual, 50)
				So(after.Initiative, ShouldEqual, 60)
				So(after.Stamina, ShouldEqual, 70)
			})
			Convey("To SpecAttack", func() {

				mod := model.Attributes{
					Health:      0,
					PhysAttack:  0,
					PhysDefense: 0,
					SpecAttack:  40,
					SpecDefense: 0,
					Initiative:  0,
				}

				after := attr.Add(mod)

				So(after.Health, ShouldEqual, 10)
				So(after.PhysAttack, ShouldEqual, 20)
				So(after.PhysDefense, ShouldEqual, 30)
				So(after.SpecAttack, ShouldEqual, 80)
				So(after.SpecDefense, ShouldEqual, 50)
				So(after.Initiative, ShouldEqual, 60)
				So(after.Stamina, ShouldEqual, 70)
			})
			Convey("To SpecDefense", func() {

				mod := model.Attributes{
					Health:      0,
					PhysAttack:  0,
					PhysDefense: 0,
					SpecAttack:  0,
					SpecDefense: 40,
					Initiative:  0,
				}

				after := attr.Add(mod)

				So(after.Health, ShouldEqual, 10)
				So(after.PhysAttack, ShouldEqual, 20)
				So(after.PhysDefense, ShouldEqual, 30)
				So(after.SpecAttack, ShouldEqual, 40)
				So(after.SpecDefense, ShouldEqual, 90)
				So(after.Initiative, ShouldEqual, 60)
				So(after.Stamina, ShouldEqual, 70)
			})
			Convey("To Initiative", func() {

				mod := model.Attributes{
					Health:      0,
					PhysAttack:  0,
					PhysDefense: 0,
					SpecAttack:  0,
					SpecDefense: 0,
					Initiative:  40,
				}

				after := attr.Add(mod)

				So(after.Health, ShouldEqual, 10)
				So(after.PhysAttack, ShouldEqual, 20)
				So(after.PhysDefense, ShouldEqual, 30)
				So(after.SpecAttack, ShouldEqual, 40)
				So(after.SpecDefense, ShouldEqual, 50)
				So(after.Initiative, ShouldEqual, 100)
				So(after.Stamina, ShouldEqual, 70)
			})
			Convey("To Stamina", func() {

				mod := model.Attributes{
					Health:      0,
					PhysAttack:  0,
					PhysDefense: 0,
					SpecAttack:  0,
					SpecDefense: 0,
					Initiative:  00,
					Stamina:     40,
				}

				after := attr.Add(mod)

				So(after.Health, ShouldEqual, 10)
				So(after.PhysAttack, ShouldEqual, 20)
				So(after.PhysDefense, ShouldEqual, 30)
				So(after.SpecAttack, ShouldEqual, 40)
				So(after.SpecDefense, ShouldEqual, 50)
				So(after.Initiative, ShouldEqual, 60)
				So(after.Stamina, ShouldEqual, 110)
			})

		})
		Convey("Calling the Sign function", func() {
			mod := model.Attributes{
				Health:      -10,
				PhysAttack:  20,
				PhysDefense: -30,
				SpecAttack:  40,
				SpecDefense: -50,
				Initiative:  60,
				Stamina:     -70,
			}
			after := mod.Sign()
			Convey("The correct signs should be returned", func() {
				So(after.Health, ShouldEqual, -1)
				So(after.PhysAttack, ShouldEqual, 1)
				So(after.PhysDefense, ShouldEqual, -1)
				So(after.SpecAttack, ShouldEqual, 1)
				So(after.SpecDefense, ShouldEqual, -1)
				So(after.Initiative, ShouldEqual, 1)
				So(after.Stamina, ShouldEqual, -1)
			})

		})
		Convey("Multiplying some modifier", func() {

			Convey("To all attributes", func() {

				mod := model.Attributes{
					Health:      10,
					PhysAttack:  20,
					PhysDefense: 30,
					SpecAttack:  40,
					SpecDefense: 50,
					Initiative:  60,
					Stamina:     70,
				}

				after := attr.Mult(mod)

				So(after.Health, ShouldEqual, 100)
				So(after.PhysAttack, ShouldEqual, 400)
				So(after.PhysDefense, ShouldEqual, 900)
				So(after.SpecAttack, ShouldEqual, 1600)
				So(after.SpecDefense, ShouldEqual, 2500)
				So(after.Initiative, ShouldEqual, 3600)
				So(after.Stamina, ShouldEqual, 4900)
			})
			Convey("To health", func() {

				mod := model.Attributes{
					Health:      40,
					PhysAttack:  0,
					PhysDefense: 0,
					SpecAttack:  0,
					SpecDefense: 0,
					Initiative:  0,
				}

				after := attr.Mult(mod)

				So(after.Health, ShouldEqual, 400)
				So(after.PhysAttack, ShouldEqual, 0)
				So(after.PhysDefense, ShouldEqual, 0)
				So(after.SpecAttack, ShouldEqual, 0)
				So(after.SpecDefense, ShouldEqual, 0)
				So(after.Initiative, ShouldEqual, 0)
				So(after.Stamina, ShouldEqual, 0)
			})
			Convey("To PhysAttack", func() {

				mod := model.Attributes{
					Health:      0,
					PhysAttack:  40,
					PhysDefense: 0,
					SpecAttack:  0,
					SpecDefense: 0,
					Initiative:  0,
				}

				after := attr.Mult(mod)

				So(after.Health, ShouldEqual, 0)
				So(after.PhysAttack, ShouldEqual, 800)
				So(after.PhysDefense, ShouldEqual, 0)
				So(after.SpecAttack, ShouldEqual, 0)
				So(after.SpecDefense, ShouldEqual, 0)
				So(after.Initiative, ShouldEqual, 0)
				So(after.Stamina, ShouldEqual, 0)
			})
			Convey("To PhysDefense", func() {

				mod := model.Attributes{
					Health:      0,
					PhysAttack:  0,
					PhysDefense: 40,
					SpecAttack:  0,
					SpecDefense: 0,
					Initiative:  0,
				}

				after := attr.Mult(mod)

				So(after.Health, ShouldEqual, 0)
				So(after.PhysAttack, ShouldEqual, 0)
				So(after.PhysDefense, ShouldEqual, 1200)
				So(after.SpecAttack, ShouldEqual, 0)
				So(after.SpecDefense, ShouldEqual, 0)
				So(after.Initiative, ShouldEqual, 0)
				So(after.Stamina, ShouldEqual, 0)
			})
			Convey("To SpecAttack", func() {

				mod := model.Attributes{
					Health:      0,
					PhysAttack:  0,
					PhysDefense: 0,
					SpecAttack:  40,
					SpecDefense: 0,
					Initiative:  0,
				}

				after := attr.Mult(mod)

				So(after.Health, ShouldEqual, 0)
				So(after.PhysAttack, ShouldEqual, 0)
				So(after.PhysDefense, ShouldEqual, 0)
				So(after.SpecAttack, ShouldEqual, 1600)
				So(after.SpecDefense, ShouldEqual, 0)
				So(after.Initiative, ShouldEqual, 0)
				So(after.Stamina, ShouldEqual, 0)
			})
			Convey("To SpecDefense", func() {

				mod := model.Attributes{
					Health:      0,
					PhysAttack:  0,
					PhysDefense: 0,
					SpecAttack:  0,
					SpecDefense: 40,
					Initiative:  0,
				}

				after := attr.Mult(mod)

				So(after.Health, ShouldEqual, 0)
				So(after.PhysAttack, ShouldEqual, 0)
				So(after.PhysDefense, ShouldEqual, 0)
				So(after.SpecAttack, ShouldEqual, 0)
				So(after.SpecDefense, ShouldEqual, 2000)
				So(after.Initiative, ShouldEqual, 0)
				So(after.Stamina, ShouldEqual, 0)
			})
			Convey("To Initiative", func() {

				mod := model.Attributes{
					Health:      0,
					PhysAttack:  0,
					PhysDefense: 0,
					SpecAttack:  0,
					SpecDefense: 0,
					Initiative:  40,
				}

				after := attr.Mult(mod)

				So(after.Health, ShouldEqual, 0)
				So(after.PhysAttack, ShouldEqual, 0)
				So(after.PhysDefense, ShouldEqual, 0)
				So(after.SpecAttack, ShouldEqual, 0)
				So(after.SpecDefense, ShouldEqual, 0)
				So(after.Initiative, ShouldEqual, 2400)
				So(after.Stamina, ShouldEqual, 0)
			})
			Convey("To Stamina", func() {

				mod := model.Attributes{
					Health:      0,
					PhysAttack:  0,
					PhysDefense: 0,
					SpecAttack:  0,
					SpecDefense: 0,
					Initiative:  00,
					Stamina:     40,
				}

				after := attr.Mult(mod)

				So(after.Health, ShouldEqual, 0)
				So(after.PhysAttack, ShouldEqual, 0)
				So(after.PhysDefense, ShouldEqual, 0)
				So(after.SpecAttack, ShouldEqual, 0)
				So(after.SpecDefense, ShouldEqual, 0)
				So(after.Initiative, ShouldEqual, 0)
				So(after.Stamina, ShouldEqual, 2800)
			})

		})

	})

}

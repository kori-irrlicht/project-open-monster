//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package status

import (
	"encoding/json"
)

var cache = make(map[string]*Base)

type Status string

const (
	Normal Status = "normal"
	KO     Status = "ko"
	Freeze Status = "Freeze"
)

type AttributeModifier struct {
	// Fixed determines if the change to an attribute is on a fixed or a relative scale.
	Fixed bool `json:"fixed,omitempty"`

	Amount float64 `json:"amount"`

	// Repeat determines if the modifier is applied in each round or only once.
	//
	// For example, a poison status is going to reduce the HP every turn, so Repeat should be true.
	Repeat bool `json:"repeat,omitempty"`

	// Attribute determines which attribute of a monster is affected by the status condition.
	Attribute string `json:"attribute"`

	// Permanent determines if the effects of the status condition should remain after the condition is cleared.
	//
	// For example, a poison status is going to permanently reduce the targets HP.
	Permanent bool `json:"permanent,omitempty"`

	// RelativeToCurrent determines if the Amount is calculated relative to the unaffected attribute.
	//
	// For example, a poison status is going to reduce HP relative to the max HP of the monster, so this value should be false.
	RelativeToCurrent bool `json:"relativeToCurrent,omitempty"`
}

type TurnModifier struct {
	// SkipTurn if a monsters turn is skipped.
	// For example, if a monster is asleep, it should not attack.
	// Using an item or switching a monster should not be affected by SkipTurn.
	SkipTurn bool `json:"skipTurn,omitempty"`
}

// RecoverConditions contains different conditions under which a monster can recover from a given condition.
// If at least one condition is fulfilled, the monster is recovered
type RecoverConditions struct {
	// Turns is the amount of turns needed to recover from the status.
	// If Turns is 0, the monster never recovers through this condition.
	Turns uint `json:"turns,omitempty"`

	// Attacked means, if a monster is attacked while having the status condition, the monster is cured of that condition at the end of the turn.
	Attacked bool `json:"attacked,omitempty"`
}

type Base struct {
	Name              string              `json:"name"`
	AttributeModifier []AttributeModifier `json:"attributeModifier,omitempty"`
	TurnModifier      TurnModifier        `json:"turnModifier"`
	RecoverConditions RecoverConditions   `json:"recoverConditions"`
}

type Ailment struct {
	Base *Base

	// Turns is the amount of turns a monster is afflicted by the status
	Turns uint
}

type ailmentMarshalHelper struct {
	Base string `json:"base"`

	// Turns is the amount of turns a monster is afflicted by the status
	Turns uint `json:"turns"`
}

func (ailment Ailment) MarshalJSON() ([]byte, error) {
	values := ailmentMarshalHelper{
		Base:  ailment.Base.Name,
		Turns: ailment.Turns,
	}

	return json.Marshal(values)

}

func (ailment *Ailment) UnmarshalJSON(data []byte) error {
	var values ailmentMarshalHelper
	if err := json.Unmarshal(data, &values); err != nil {
		return err
	}
	ailment.Base = cache[values.Base]
	ailment.Turns = values.Turns
	return nil
}

func New(name string) Ailment {
	return Ailment{
		cache[name],
		0,
	}
}

func Register(base Base) {
	cache[base.Name] = &base
}

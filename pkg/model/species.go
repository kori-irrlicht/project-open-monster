//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package model

import (
	"encoding/json"
	"fmt"
)

type ExpCurve struct {
	Name  string          `json:"name"`
	Curve func(Exp) Level `json:"-"`
}

func (ec *ExpCurve) UnmarshalJSON(data []byte) error {
	var values string
	if err := json.Unmarshal(data, &values); err != nil {
		return err
	}
	ec.Name = values
	return nil
}

func (ec *ExpCurve) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`"%s"`, ec.Name)), nil
}

type Requirement struct {
	ID        ID        `json:"id"`
	Name      string    `json:"name"`
	Trigger   string    `json:"trigger"`
	Condition Condition `json:"condition"`
}

func (r Requirement) IsFulfilled(m Monster) bool {
	return r.Condition.IsFulfilled(m)
}

func (r *Requirement) UnmarshalJSON(data []byte) error {
	var value struct {
		ID        ID              `json:"id"`
		Name      string          `json:"name"`
		Trigger   string          `json:"trigger"`
		Condition json.RawMessage `json:"condition"`
	}

	if err := json.Unmarshal(data, &value); err != nil {
		return err
	}

	r.ID = value.ID
	r.Name = value.Name
	r.Trigger = value.Trigger

	var condition struct {
		Type string `json:"type"`
	}
	if err := json.Unmarshal(value.Condition, &condition); err != nil {
		return err
	}
	switch condition.Type {
	case "Level":
		var c LevelCondition
		if err := json.Unmarshal(value.Condition, &c); err != nil {
			return err
		}
		r.Condition = c
	}
	return nil
}

type Condition interface {
	Type() string
	IsFulfilled(m Monster) bool
}

type LevelCondition struct {
	Level Level `json:"level"`
}

func (lv LevelCondition) MarshalJSON() ([]byte, error) {
	v := struct {
		Type  string `json:"type"`
		Level Level  `json:"level"`
	}{
		Type:  lv.Type(),
		Level: lv.Level,
	}
	return json.Marshal(v)
}

func (l LevelCondition) Type() string               { return "Level" }
func (l LevelCondition) IsFulfilled(m Monster) bool { return m.Level >= l.Level }

type MoveRequirement Requirement
type EvolveRequirement Requirement

type Species struct {
	Name      string        `json:"name"`
	Base      Attributes    `json:"attributeBase"`
	Types     []Type        `json:"types"`
	BaseExp   Exp           `json:"baseExp"`
	ExpCurve  *ExpCurve     `json:"expCurve"`
	MoveSet   []Requirement `json:"moveSet"`
	Evolution []Requirement `json:"evolution"`

	// How many training modifier a monster gains, if it defeats a monster of this species
	TrainingModifier Attributes `json:"trainingModifier"`
}

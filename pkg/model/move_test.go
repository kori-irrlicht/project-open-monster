//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package model_test

import (
	"fmt"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

func TestMove(t *testing.T) {
	Convey("Given a move base", t, func() {

		name := "Name"
		desc := "Desc"
		var mt model.Type = "type"
		var staminaUsage = 10
		var power uint = 90

		mb := model.NewMoveBaseTemplate(name, desc, mt, staminaUsage, power)

		Convey("It should be properly created", func() {

			So(mb.Name(), ShouldEqual, name)
			So(mb.Description(), ShouldEqual, desc)
			So(mb.Type(), ShouldEqual, mt)
			So(mb.StaminaUsage(), ShouldEqual, staminaUsage)
			for _, mod := range mb.Modifier() {
				switch v := mod.(type) {
				case *model.DamageModifier:

					So(v.Power, ShouldEqual, power)
				}
			}
		})

		Convey("Creating a move based on it", func() {
			move := model.NewMove(mb)

			Convey("It should have the given base", func() {
				So(move.Base(), ShouldEqual, mb)
			})

			Convey("The base should not be altered", func() {

				So(move.Base().Name(), ShouldEqual, name)
				So(move.Base().Description(), ShouldEqual, desc)
				So(move.Base().Type(), ShouldEqual, mt)
				So(move.Base().StaminaUsage(), ShouldEqual, staminaUsage)
			})
		})

	})

}

func generateMove(i int) *model.Move {
	mb := model.NewMoveBaseTemplate(fmt.Sprintf("Test%d", i), "", "", 5, 90)
	return model.NewMove(mb)
}

func TestMoveArr(t *testing.T) {

	Convey("Given a MoveArr", t, func() {
		//var ma model.MoveArr
		ma := model.NewMoveArr()
		Convey("The movearr is initalised with NilMoves", func() {
			for i := 0; i < model.MoveAmount; i++ {
				So(ma[0].Base(), ShouldEqual, model.NilMoveBase)
			}

		})
		Convey("Adding a move", func() {
			m0 := generateMove(0)
			ma, err := ma.Add(m0)
			Convey("It should be the first move", func() {
				So(err, ShouldBeNil)
				So(ma[0], ShouldResemble, m0)
			})
			Convey("It should contain the move", func() {
				So(ma.Contains(m0.Base()), ShouldBeTrue)
			})

			Convey("Adding a second move", func() {
				m1 := generateMove(1)
				ma, err := ma.Add(m1)
				Convey("It should be the second move", func() {
					So(err, ShouldBeNil)
					So(ma[0], ShouldResemble, m0)
					So(ma[1], ShouldResemble, m1)
				})
				Convey("It should contain the move", func() {
					So(ma.Contains(m1.Base()), ShouldBeTrue)
				})
			})

			Convey("Filling the MoveArr", func() {
				for i := 1; i < model.MoveAmount; i++ {
					ma, _ = ma.Add(generateMove(i))
				}
				Convey("Adding another move should fail", func() {
					move := generateMove(model.MoveAmount + 1)
					ma, err := ma.Add(move)
					So(err, ShouldNotBeNil)
					So(ma.Contains(move.Base()), ShouldBeFalse)
				})

				Convey("Removing the first move", func() {
					ma, err := ma.Remove(0)
					So(err, ShouldBeNil)
					So(ma[0], ShouldNotEqual, model.NilMoveBase)
					So(ma[model.MoveAmount-1].Base(), ShouldEqual, model.NilMoveBase)
				})

				Convey("Removing index -1 should return an error", func() {
					ma, err = ma.Remove(-1)
					So(err, ShouldNotBeNil)
				})

				Convey("Removing index MoveAmount + 1 should return an error", func() {
					ma, err = ma.Remove(model.MoveAmount + 1)
					So(err, ShouldNotBeNil)
				})
			})

			Convey("Adding a move twice", func() {
				m01 := model.NewMove(m0.Base())
				ma, err := ma.Add(m01)
				Convey("It should not be added", func() {
					So(err, ShouldNotBeNil)
					So(ma[0], ShouldResemble, m0)
					So(ma[1].Base(), ShouldEqual, model.NilMoveBase)
				})
			})

			Convey("Removing the last move should fail", func() {
				ma, err = ma.Remove(0)
				So(err, ShouldNotBeNil)
			})

		})

	})

}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package model_test

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

func TestTypeEffectivity(t *testing.T) {
	Convey("Given a default configuration of types", t, func() {
		model.AddTypeMatchup("Fire", "Ice", 2)
		model.AddTypeMatchup("Fire", "Forest", 2)
		model.AddTypeMatchup("Fire", "Fire", 0.5)
		model.AddTypeMatchup("Fire", "Rock", 0.5)

		Convey("Matching unlisted combinations should result in 1", func() {
			So(model.GetTypeEffectivity("Ice", "Ice", "Normal"), ShouldEqual, 1)
			So(model.GetTypeEffectivity("Fire", "Water", "Normal"), ShouldEqual, 1)
			So(model.GetTypeEffectivity("Water", "Water", "Normal"), ShouldEqual, 1)
		})

		Convey("Attacking with \"fire\" against \"ice\" should return 2", func() {
			So(model.GetTypeEffectivity("Fire", "Ice"), ShouldEqual, 2)
		})
		Convey("Attacking with \"fire\" against \"fire\" should return 0.5", func() {
			So(model.GetTypeEffectivity("Fire", "Fire"), ShouldEqual, 0.5)
		})

		Convey("Attacking with \"fire\" against \"ice\" and \"forest\" should return 4", func() {
			So(model.GetTypeEffectivity("Fire", "Ice", "Forest"), ShouldEqual, 4)
		})

		Convey("Attacking with \"fire\" against \"fire\" and \"rock\" should return 0.25", func() {
			So(model.GetTypeEffectivity("Fire", "Fire", "Rock"), ShouldEqual, 0.25)
		})

		Convey("Attacking with \"fire\" against \"fire\" and \"ice\" should return 1", func() {
			So(model.GetTypeEffectivity("Fire", "Fire", "Ice"), ShouldEqual, 1)
		})

	})
}

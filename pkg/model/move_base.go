//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package model

import (
	"encoding/json"
	"fmt"
)

type MoveBase interface {
	Name() string
	Description() string
	Type() Type
	Modifier() []MoveModifier
	StaminaUsage() int
	Cooldown() int
}

type MoveTarget string

const (
	ModifierType_Damage = "damage"
	ModifierType_Status = "status"
)

type MoveModifier interface {
	ModifierType() string
}

type DamageModifier struct {
	Power       uint   `json:"power"`
	AttackStat  string `json:"attackStat"`
	DefenseStat string `json:"defenseStat"`
}

func (d DamageModifier) MarshalJSON() ([]byte, error) {
	data := struct {
		Type        string `json:"type"`
		Power       uint   `json:"power"`
		AttackStat  string `json:"attackStat"`
		DefenseStat string `json:"defenseStat"`
	}{
		Type:        d.ModifierType(),
		Power:       d.Power,
		AttackStat:  d.AttackStat,
		DefenseStat: d.DefenseStat,
	}
	return json.Marshal(data)
}

func (d DamageModifier) ModifierType() string {
	return ModifierType_Damage
}

type StatusModifier struct {
	Status string `json:"status"`
	Power  int    `json:"power"`
}

func (d StatusModifier) ModifierType() string {
	return ModifierType_Status
}

func (d StatusModifier) MarshalJSON() ([]byte, error) {
	data := struct {
		Type   string `json:"type"`
		Power  int    `json:"power"`
		Status string `json:"status"`
	}{
		Type:   d.ModifierType(),
		Power:  d.Power,
		Status: d.Status,
	}
	return json.Marshal(data)
}

/**
MoveBaseTemplate contains the shared information of a move.
Everything in MoveBaseTemplate is constant and should not be changed

upgradePoints defines how many points will be added to the move points after an upgrade
*/
type MoveBaseTemplate struct {
	name         string
	description  string
	moveType     Type
	target       MoveTarget
	modifier     []MoveModifier
	staminaUsage int
	cooldown     int
}

func (mbt *MoveBaseTemplate) UnmarshalJSON(msg []byte) error {
	var data map[string]interface{}
	if err := json.Unmarshal(msg, &data); err != nil {
		return err
	}
	mbt.name = data["name"].(string)
	mbt.moveType = Type(data["type"].(string))
	mbt.staminaUsage = int(data["staminaUsage"].(float64))
	mbt.cooldown = int(data["cooldown"].(float64))
	mbt.target = MoveTarget(data["target"].(string))

	mbt.modifier = []MoveModifier{}
	if modifier, ok := data["modifier"].([]interface{}); ok {

		for _, v := range modifier {
			mod := v.(map[string]interface{})
			switch mod["type"] {
			case ModifierType_Damage:
				var dmgMod DamageModifier
				mbt.modifier = append(mbt.modifier, &dmgMod)
				dmgMod.Power = uint(mod["power"].(float64))
				dmgMod.AttackStat = mod["attackStat"].(string)
				dmgMod.DefenseStat = mod["defenseStat"].(string)
			case ModifierType_Status:
				var statMod StatusModifier
				mbt.modifier = append(mbt.modifier, &statMod)
				statMod.Status = mod["status"].(string)
				statMod.Power = int(mod["power"].(float64))
			}
		}
	} else {
		return fmt.Errorf("Could not load modifier for move '%s'", mbt.name)
	}

	return nil
}

func (mbt *MoveBaseTemplate) MarshalJSON() ([]byte, error) {
	data := struct {
		Name         string         `json:"name"`
		Description  string         `json:"description"`
		MoveType     Type           `json:"type"`
		Target       MoveTarget     `json:"target"`
		Modifier     []MoveModifier `json:"modifier"`
		StaminaUsage int            `json:"staminaUsage"`
		Cooldown     int            `json:"cooldown"`
	}{Name: mbt.name, Description: mbt.description, MoveType: mbt.moveType, Target: mbt.target, Modifier: mbt.modifier, StaminaUsage: mbt.staminaUsage, Cooldown: mbt.cooldown}

	return json.MarshalIndent(data, "", "\t")
}

func NewMoveBaseTemplate(name, description string, moveType Type, staminaUsage int, power uint) *MoveBaseTemplate {
	return &MoveBaseTemplate{
		name:        name,
		description: description,
		moveType:    moveType,
		modifier: []MoveModifier{&DamageModifier{
			Power: power,
		}},
		staminaUsage: staminaUsage,
	}
}

func (mb MoveBaseTemplate) Name() string {
	return mb.name
}

func (mb MoveBaseTemplate) Description() string {
	return mb.description
}

func (mb MoveBaseTemplate) Type() Type {
	return mb.moveType
}

func (mb MoveBaseTemplate) Target() MoveTarget {
	return mb.target
}

func (mb MoveBaseTemplate) StaminaUsage() int {
	return mb.staminaUsage
}

func (mb MoveBaseTemplate) Cooldown() int {
	return mb.cooldown
}

func (mb MoveBaseTemplate) Modifier() []MoveModifier {
	return mb.modifier
}

func (mb *MoveBaseTemplate) SetName(name string) {
	mb.name = name
}

func (mb *MoveBaseTemplate) SetDescription(desc string) {
	mb.description = desc
}

func (mb *MoveBaseTemplate) SetType(t Type) {
	mb.moveType = t
}

func (mb *MoveBaseTemplate) SetTarget(t MoveTarget) {
	mb.target = t
}

func (mb *MoveBaseTemplate) SetStaminaUsage(su int) {
	mb.staminaUsage = su
}

func (mb *MoveBaseTemplate) SetCooldown(c int) {
	mb.cooldown = c
}

func (mb *MoveBaseTemplate) SetModifier(mod []MoveModifier) {
	mb.modifier = mod
}

func (mb MoveBaseTemplate) String() string {
	return fmt.Sprintf(`{name: %s, description: %s, moveType: %v, staminaUsage: %v, target: %v, modifier: %v}`,
		mb.name, mb.description, mb.moveType, mb.staminaUsage, mb.target, mb.modifier)
}

var NilMoveBase = NewMoveBaseTemplate("Nil", "Placeholder for empty moves", "", 0, 90)

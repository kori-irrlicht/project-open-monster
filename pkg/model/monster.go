//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package model

import (
	"encoding/json"
	"fmt"
	"math/rand"

	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/status"
)

const MinGeneticModifier AttributeStat = 1
const MaxGeneticModifier AttributeStat = 25

type Exp uint32
type Level uint8

type MonsterVolatile struct {
	TempModifier    Attributes
	CurrentHP       int
	CurrentStamina  int
	StatusThreshold map[string]int
}

type Monster struct {
	ID                  ID `json:"id" storm:"id,increment"`
	Nickname            string
	Level               Level
	Species             *Species `json:"species"`
	TrainingModifier    Attributes
	GeneticModifier     Attributes
	NutritionalModifier Attributes
	Gender              Gender
	Moves               MoveArr
	Exp                 Exp
	OwnerID             ID `storm:"index"`
	Status              []status.Ailment
	Trait               *Trait
	MonsterVolatile
}

type monsterMarshalHelper struct {
	ID                  ID               `json:"id"`
	Nickname            string           `json:"nickname,omitempty"`
	Level               Level            `json:"level"`
	Species             string           `json:"species"`
	TrainingModifier    Attributes       `json:"trainingModifier"`
	GeneticModifier     Attributes       `json:"geneticModifier"`
	NutritionalModifier Attributes       `json:"nutritionalModifier"`
	Gender              Gender           `json:"gender"`
	Moves               []string         `json:"moves"`
	Exp                 Exp              `json:"exp"`
	OwnerID             ID               `json:"ownerId"`
	CurrentHP           int              `json:"currentHP"`
	Status              []status.Ailment `json:"status"`
	Trait               string           `json:"trait"`
}

func (mon Monster) MarshalJSON() ([]byte, error) {
	values := monsterMarshalHelper{
		ID:                  mon.ID,
		Nickname:            mon.Nickname,
		Level:               mon.Level,
		Species:             mon.Species.Name,
		TrainingModifier:    mon.TrainingModifier,
		GeneticModifier:     mon.GeneticModifier,
		NutritionalModifier: mon.NutritionalModifier,
		Gender:              mon.Gender,
		Exp:                 mon.Exp,
		OwnerID:             mon.OwnerID,
		CurrentHP:           mon.CurrentHP,
		Status:              mon.Status,
		Moves:               []string{},
		Trait:               mon.Trait.Name,
	}

	for _, v := range mon.Moves {
		if v.Base() != NilMoveBase {
			values.Moves = append(values.Moves, v.Base().Name())
		}
	}

	return json.Marshal(values)

}

func (mon *Monster) UnmarshalJSON(data []byte) error {

	var values monsterMarshalHelper

	if err := json.Unmarshal(data, &values); err != nil {
		return err
	}

	mon.ID = values.ID
	mon.Level = values.Level
	mon.CurrentHP = values.CurrentHP
	mon.Exp = values.Exp
	mon.Gender = values.Gender
	mon.Nickname = values.Nickname
	mon.Species = SpeciesCache[values.Species]
	mon.Status = values.Status
	mon.OwnerID = values.OwnerID
	mon.Trait = TraitCache[values.Trait]

	moveArr := NewMoveArr()
	for _, moveName := range values.Moves {
		move := NewMove(MoveBaseCache[moveName])

		var err error
		moveArr, err = moveArr.Add(move)
		if err != nil {
			return err
		}
	}
	mon.Moves = moveArr

	mon.GeneticModifier = values.GeneticModifier
	mon.TrainingModifier = values.TrainingModifier
	mon.NutritionalModifier = values.NutritionalModifier

	mon.CurrentStamina = int(mon.Attributes().Stamina)
	mon.StatusThreshold = make(map[string]int)

	return nil
}

func (mon *Monster) Attributes() Attributes {

	calcTraitMod := func(mod Attributes, attr string, value float64) Attributes {
		v := 1 + (value / 100)
		switch attr {
		case "health":
			mod.Health = AttributeStat(float64(mod.Health) * v)
			if mod.Health < 1 {
				mod.Health = 1
			}
		case "stamina":
			mod.Stamina = AttributeStat(float64(mod.Stamina) * v)
			if mod.Stamina < 1 {
				mod.Stamina = 1
			}
		case "physAttack":
			mod.PhysAttack = AttributeStat(float64(mod.PhysAttack) * v)
			if mod.PhysAttack < 1 {
				mod.PhysAttack = 1
			}
		case "physDefense":
			mod.PhysDefense = AttributeStat(float64(mod.PhysDefense) * v)
			if mod.PhysDefense < 1 {
				mod.PhysDefense = 1
			}
		case "specAttack":
			mod.SpecAttack = AttributeStat(float64(mod.SpecAttack) * v)
			if mod.SpecAttack < 1 {
				mod.SpecAttack = 1
			}
		case "specDefense":
			mod.SpecDefense = AttributeStat(float64(mod.SpecDefense) * v)
			if mod.SpecDefense < 1 {
				mod.SpecDefense = 1
			}
		case "initiative":
			mod.Initiative = AttributeStat(float64(mod.Initiative) * v)
			if mod.Initiative < 1 {
				mod.Initiative = 1
			}
		}
		return mod

	}

	gm := mon.GeneticModifier.MultI(2)
	tm := mon.TrainingModifier.MultF(0.5)
	nm := mon.NutritionalModifier.Div(tm.AddI(4))

	staticBonus := Attributes{
		Health:  AttributeStat(mon.Level),
		Stamina: 10,
	}

	attr := mon.Species.Base.Add(gm).Add(tm).Add(nm)
	attr = attr.MultI(int(mon.Level))
	attr = attr.MultF(0.01)
	attr = attr.Add(staticBonus)

	attr = calcTraitMod(attr, mon.Trait.IncreasedAttribute, mon.Trait.IncreasedBy)
	attr = calcTraitMod(attr, mon.Trait.DecreasedAttribute, -mon.Trait.DecreasedBy)

	attr = attr.AddI(10)

	tempMod := mon.TempModifier.Abs().AddI(2).MultF(0.5)
	tempMod = tempMod.Pow(mon.TempModifier.Sign())
	attr = attr.Mult(tempMod)

	return attr
}

func (mon *Monster) Damage(dmg int) {
	mon.CurrentHP -= dmg
}

// ApplyStatus adds an amount of points to a status threshold.
// Returns true, if the threshold is reached and the ailment applied.
func (mon *Monster) ApplyStatus(name string, amount int) bool {
	mon.StatusThreshold[name] += amount
	if mon.StatusThreshold[name] >= 100 { // TODO: make configurable
		if len(mon.Status) == 2 { // TODO: make configurable
			mon.StatusThreshold[mon.Status[0].Base.Name] = 0
			mon.Status = mon.Status[1:]
		}
		mon.Status = append(mon.Status, status.New(name))
		return true

	}
	return false
}

func (mon *Monster) Types() []Type { return mon.Species.Types }

func (mon *Monster) CurrentHPPercent() float64 {
	return float64(mon.CurrentHP) / float64(mon.Attributes().Health) * 100
}

func (mon *Monster) CurrentStaminaPercent() float64 {
	return float64(mon.CurrentStamina) / float64(mon.Attributes().Stamina) * 100
}

func (mon *Monster) ReduceStamina(stamina int) {
	mon.CurrentStamina -= stamina
}

func (mon *Monster) IsKO() bool {
	return mon.CurrentHP <= 0
}

type IDSupplier interface {
	NextID() ID
}

func (mon *Monster) String() string {
	return fmt.Sprintf(`{id: %d, level: %d, species: %v, attributes: %v, CurrentHP: %d, moves: %v, status: %v}`,
		mon.ID, mon.Level, mon.Species, mon.Attributes(), mon.CurrentHP, mon.Moves, mon.Status)
}

type LevelRangeSupplier func() (from Level, to Level)
type GeneticModifierRangeSupplier func() (from AttributeStat, to AttributeStat)

func CreateMonster(species *Species, levelRange LevelRangeSupplier, gmRange GeneticModifierRangeSupplier) *Monster {
	if species == nil {
		panic("Can't create a monster with nil as species")
	}
	from, to := levelRange()

	// -1, because otherwise Intn will panic if to == from
	fromMin1 := from - 1
	/* #nosec */
	level := Level(rand.Intn(int(to-fromMin1))) + fromMin1

	// +1 to neutralize the -1 from before
	level += 1
	monster := &Monster{
		Species: species,
		Level:   level,
		Status:  []status.Ailment{},
	}

	randomStat := func() AttributeStat {
		from, to := gmRange()

		// -1, because otherwise Intn will panic if to == from
		fromMin1 := from - 1
		/* #nosec */
		attr := AttributeStat(rand.Intn(int(to-fromMin1))) + fromMin1

		// +1 to neutralize the -1 from before
		attr += 1

		return attr
	}

	monster.GeneticModifier = Attributes{
		Health:      randomStat(),
		Stamina:     randomStat(),
		PhysAttack:  randomStat(),
		PhysDefense: randomStat(),
		SpecAttack:  randomStat(),
		SpecDefense: randomStat(),
		Initiative:  randomStat(),
	}

	// Assign a random trait to the monster
	traitKeys := TraitCache.Keys()

	if len(traitKeys) > 0 {
		/* #nosec */
		monster.Trait = TraitCache[traitKeys[rand.Intn(len(traitKeys))]]
	}

	// TODO: Remove as soon as possible
	if monster.Trait == nil {
		monster.Trait = &Trait{Name: "Dummy"}
	}

	monster.StatusThreshold = make(map[string]int)
	monster.CurrentHP = int(monster.Attributes().Health)
	monster.CurrentStamina = int(monster.Attributes().Stamina)

	for i := 0; i < MoveAmount; i++ {
		monster.Moves[i] = NewMove(NilMoveBase)
	}

	for _, moveReq := range monster.Species.MoveSet {
		if moveReq.Trigger == "LevelUp" && moveReq.IsFulfilled(*monster) {
			moveBase := MoveBaseCache[moveReq.Name]
			move := NewMove(moveBase)
			var err error
			monster.Moves, err = monster.Moves.Add(move)
			if err != nil {
				logrus.WithFields(logrus.Fields{
					"move":    moveReq.Name,
					"species": species.Name,
				}).WithError(err).Errorln("Could not add move")
			}
		}
	}

	return monster
}

type Breeder struct {
	ID   ID        `json:"id" storm:"id,increment"`
	Name string    `json:"name"`
	Team []Monster `json:"-"`
}

type Trait struct {
	Name               string  `json:"name"`
	IncreasedAttribute string  `json:"increasedAttribute"`
	DecreasedAttribute string  `json:"decreasedAttribute"`
	IncreasedBy        float64 `json:"increasedBy"`
	DecreasedBy        float64 `json:"decreasedBy"`
}

func DefaultGeneticModifierRange() (AttributeStat, AttributeStat) {
	return MinGeneticModifier, MaxGeneticModifier
}

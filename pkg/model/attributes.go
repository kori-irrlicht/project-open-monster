//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package model

import "math"

type AttributeStat int

type Attributes struct {
	Health      AttributeStat `json:"health"`
	PhysAttack  AttributeStat `json:"physAttack"`
	PhysDefense AttributeStat `json:"physDefense"`
	SpecAttack  AttributeStat `json:"specAttack"`
	SpecDefense AttributeStat `json:"specDefense"`
	Initiative  AttributeStat `json:"initiative"`
	Stamina     AttributeStat `json:"stamina"`
}

func (a Attributes) Add(b Attributes) Attributes {
	return Attributes{
		Health:      a.Health + b.Health,
		PhysAttack:  a.PhysAttack + b.PhysAttack,
		PhysDefense: a.PhysDefense + b.PhysDefense,
		SpecAttack:  a.SpecAttack + b.SpecAttack,
		SpecDefense: a.SpecDefense + b.SpecDefense,
		Initiative:  a.Initiative + b.Initiative,
		Stamina:     a.Stamina + b.Stamina,
	}
}

func (a Attributes) Mult(b Attributes) Attributes {
	return Attributes{
		Health:      a.Health * b.Health,
		PhysAttack:  a.PhysAttack * b.PhysAttack,
		PhysDefense: a.PhysDefense * b.PhysDefense,
		SpecAttack:  a.SpecAttack * b.SpecAttack,
		SpecDefense: a.SpecDefense * b.SpecDefense,
		Initiative:  a.Initiative * b.Initiative,
		Stamina:     a.Stamina * b.Stamina,
	}
}

func (a Attributes) Div(b Attributes) Attributes {
	return Attributes{
		Health:      a.Health / b.Health,
		PhysAttack:  a.PhysAttack / b.PhysAttack,
		PhysDefense: a.PhysDefense / b.PhysDefense,
		SpecAttack:  a.SpecAttack / b.SpecAttack,
		SpecDefense: a.SpecDefense / b.SpecDefense,
		Initiative:  a.Initiative / b.Initiative,
		Stamina:     a.Stamina / b.Stamina,
	}
}

func (a Attributes) Pow(b Attributes) Attributes {
	return Attributes{
		Health:      AttributeStat(math.Pow(float64(a.Health), float64(b.Health))),
		PhysAttack:  AttributeStat(math.Pow(float64(a.PhysAttack), float64(b.PhysAttack))),
		PhysDefense: AttributeStat(math.Pow(float64(a.PhysDefense), float64(b.PhysDefense))),
		SpecAttack:  AttributeStat(math.Pow(float64(a.SpecAttack), float64(b.SpecAttack))),
		SpecDefense: AttributeStat(math.Pow(float64(a.SpecDefense), float64(b.SpecDefense))),
		Initiative:  AttributeStat(math.Pow(float64(a.Initiative), float64(b.Initiative))),
		Stamina:     AttributeStat(math.Pow(float64(a.Stamina), float64(b.Stamina))),
	}
}

func (a Attributes) Abs() Attributes {
	return Attributes{
		Health:      AttributeStat(math.Abs(float64(a.Health))),
		PhysAttack:  AttributeStat(math.Abs(float64(a.PhysAttack))),
		PhysDefense: AttributeStat(math.Abs(float64(a.PhysDefense))),
		SpecAttack:  AttributeStat(math.Abs(float64(a.SpecAttack))),
		SpecDefense: AttributeStat(math.Abs(float64(a.SpecDefense))),
		Initiative:  AttributeStat(math.Abs(float64(a.Initiative))),
		Stamina:     AttributeStat(math.Abs(float64(a.Stamina))),
	}
}

func (a Attributes) Sign() Attributes {
	sign := func(x float64) float64 {
		if math.Signbit(x) {
			return -1
		}
		return 1

	}
	return Attributes{
		Health:      AttributeStat(sign(float64(a.Health))),
		PhysAttack:  AttributeStat(sign(float64(a.PhysAttack))),
		PhysDefense: AttributeStat(sign(float64(a.PhysDefense))),
		SpecAttack:  AttributeStat(sign(float64(a.SpecAttack))),
		SpecDefense: AttributeStat(sign(float64(a.SpecDefense))),
		Initiative:  AttributeStat(sign(float64(a.Initiative))),
		Stamina:     AttributeStat(sign(float64(a.Stamina))),
	}
}

func (a Attributes) AddI(b int) Attributes {
	return Attributes{
		Health:      AttributeStat(int(a.Health) + b),
		PhysAttack:  AttributeStat(int(a.PhysAttack) + b),
		PhysDefense: AttributeStat(int(a.PhysDefense) + b),
		SpecAttack:  AttributeStat(int(a.SpecAttack) + b),
		SpecDefense: AttributeStat(int(a.SpecDefense) + b),
		Initiative:  AttributeStat(int(a.Initiative) + b),
		Stamina:     AttributeStat(int(a.Stamina) + b),
	}
}

func (a Attributes) MultI(b int) Attributes {
	return Attributes{
		Health:      AttributeStat(int(a.Health) * b),
		PhysAttack:  AttributeStat(int(a.PhysAttack) * b),
		PhysDefense: AttributeStat(int(a.PhysDefense) * b),
		SpecAttack:  AttributeStat(int(a.SpecAttack) * b),
		SpecDefense: AttributeStat(int(a.SpecDefense) * b),
		Initiative:  AttributeStat(int(a.Initiative) * b),
		Stamina:     AttributeStat(int(a.Stamina) * b),
	}
}

func (a Attributes) AddF(b float64) Attributes {
	return Attributes{
		Health:      AttributeStat(float64(a.Health) + b),
		PhysAttack:  AttributeStat(float64(a.PhysAttack) + b),
		PhysDefense: AttributeStat(float64(a.PhysDefense) + b),
		SpecAttack:  AttributeStat(float64(a.SpecAttack) + b),
		SpecDefense: AttributeStat(float64(a.SpecDefense) + b),
		Initiative:  AttributeStat(float64(a.Initiative) + b),
		Stamina:     AttributeStat(float64(a.Stamina) + b),
	}
}

func (a Attributes) MultF(b float64) Attributes {
	return Attributes{
		Health:      AttributeStat(float64(a.Health) * b),
		PhysAttack:  AttributeStat(float64(a.PhysAttack) * b),
		PhysDefense: AttributeStat(float64(a.PhysDefense) * b),
		SpecAttack:  AttributeStat(float64(a.SpecAttack) * b),
		SpecDefense: AttributeStat(float64(a.SpecDefense) * b),
		Initiative:  AttributeStat(float64(a.Initiative) * b),
		Stamina:     AttributeStat(float64(a.Stamina) * b),
	}
}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package model_test

import (
	"encoding/json"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

var bspJson = []byte(`
{
  "name": "Flamurus",
  "attributeBase": {
    "health": 100,
    "physAttack": 101,
    "physDefense": 102,
    "specAttack": 103,
    "specDefense": 104,
    "initiative": 105
  },
  "types": ["Fire", "Air"],
  "expCurve": "Normal",
  "evolution": [{
      "Name": "Infernanda",
      "Trigger": "LevelUp",
      "Condition": {
          "Type": "Level",
          "Level": 20
      }
  }],
  "moveSet": [{
      "Name": "White Frost",
      "Trigger": "LevelUp",
      "Condition": {
          "Type": "Level",
          "Level": 15
      }
  }]
}
`)

func TestSpecies(t *testing.T) {
	Convey("Given a species and a level range", t, func() {
		species := &model.Species{}
		levelRange := func() (model.Level, model.Level) {
			return 1, 10
		}
		Convey("A monster is generated from this species", func() {
			monster := model.CreateMonster(species, levelRange, model.DefaultGeneticModifierRange)
			Convey("It has the given species", func() {

				So(monster.Species, ShouldEqual, species)
			})
			Convey("Its level is in the given range", func() {
				from, to := levelRange()
				So(monster.Level, ShouldBeBetweenOrEqual, from, to)
			})
			Convey("Its genetic modifier is in the given range", func() {
				from, to := model.DefaultGeneticModifierRange()
				So(monster.GeneticModifier.Health, ShouldBeBetweenOrEqual, from, to)
				So(monster.GeneticModifier.Stamina, ShouldBeBetweenOrEqual, from, to)
				So(monster.GeneticModifier.PhysAttack, ShouldBeBetweenOrEqual, from, to)
				So(monster.GeneticModifier.PhysDefense, ShouldBeBetweenOrEqual, from, to)
				So(monster.GeneticModifier.SpecAttack, ShouldBeBetweenOrEqual, from, to)
				So(monster.GeneticModifier.SpecDefense, ShouldBeBetweenOrEqual, from, to)
				So(monster.GeneticModifier.Initiative, ShouldBeBetweenOrEqual, from, to)
			})
		})
	})
	Convey("Given a species in JSON format and unmarshalling it", t, func() {
		var species model.Species
		err := json.Unmarshal(bspJson, &species)
		Convey("The error should be nil", func() {
			So(err, ShouldBeNil)
		})
		Convey("The name should be 'Flamurus'", func() {
			So(species.Name, ShouldEqual, "Flamurus")
		})
		Convey("The attributes should all be 100", func() {
			So(species.Base.Health, ShouldEqual, 100)
			So(species.Base.PhysAttack, ShouldEqual, 101)
			So(species.Base.PhysDefense, ShouldEqual, 102)
			So(species.Base.SpecAttack, ShouldEqual, 103)
			So(species.Base.SpecDefense, ShouldEqual, 104)
			So(species.Base.Initiative, ShouldEqual, 105)
		})
		Convey("The types should be 'Fire' and 'Air'", func() {
			So(species.Types[0], ShouldEqual, "Fire")
			So(species.Types[1], ShouldEqual, "Air")
		})
		Convey("The ExpCurve should be 'normal'", func() {
			So(species.ExpCurve.Name, ShouldEqual, "Normal")
			So(species.Types[1], ShouldEqual, "Air")
		})
		Convey("The move set contains 'White Frost' at level 15", func() {
			So(species.MoveSet[0].Name, ShouldEqual, "White Frost")
			So(species.MoveSet[0].Trigger, ShouldEqual, "LevelUp")
			So(species.MoveSet[0].Condition, ShouldNotBeNil)
			So(species.MoveSet[0].Condition.Type(), ShouldEqual, "Level")

			mon := model.Monster{}
			mon.Level = (15)
			So(species.MoveSet[0].Condition.IsFulfilled(mon), ShouldBeTrue)
			//So(species.MoveSet[0].Level, ShouldEqual, 15)
		})
		Convey("Evolution contains 'Infernanda' at level 20", func() {
			So(species.Evolution[0].Name, ShouldEqual, "Infernanda")
			So(species.Evolution[0].Trigger, ShouldEqual, "LevelUp")
			So(species.Evolution[0].Condition, ShouldNotBeNil)
			So(species.Evolution[0].Condition.Type(), ShouldEqual, "Level")

			mon := model.Monster{}
			mon.Level = (20)
			So(species.Evolution[0].Condition.IsFulfilled(mon), ShouldBeTrue)
			//So(species.Evolution[0].Level, ShouldEqual, 20)
		})

	})

}

func TestRequirementFulfilled(t *testing.T) {
	Convey("Given a requirement: level = 45", t, func() {
		req := model.Requirement{
			Condition: model.LevelCondition{Level: 45},
		}
		Convey("Given a monster with level 40", func() {

			mon := model.Monster{}
			mon.Level = (40)
			Convey("The condition is not fulfilled", func() {
				So(req.IsFulfilled(mon), ShouldBeFalse)
			})
		})
		Convey("Given a monster with level 45", func() {

			mon := model.Monster{}
			mon.Level = (45)
			Convey("The condition is fulfilled", func() {
				So(req.IsFulfilled(mon), ShouldBeTrue)
			})
		})
	})

}

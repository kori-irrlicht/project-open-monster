//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package model

type Type string

var typeMatchup map[Type]map[Type]float64 = make(map[Type]map[Type]float64)

// AddTypeMatchup adds the effectivity of an attacking type against a defending type.
// If a combination already exists, it will be replaced.
func AddTypeMatchup(attacker Type, defender Type, effectivity float64) {
	matchups, ok := typeMatchup[attacker]
	if !ok {
		matchups = make(map[Type]float64)
		typeMatchup[attacker] = matchups
	}
	matchups[defender] = effectivity
}

// getTypeEffectivity returns the effectivity of an attacking type against a single defending type.
// If the combination is not listed, a neutral effectivity is assumed.
func getTypeEffectivity(attacker Type, defender Type) float64 {
	matchups := typeMatchup[attacker]
	if effectivity, ok := matchups[defender]; !ok {
		return 1
	} else {
		return effectivity
	}
}

// GetTypeEffectivity returns the effectivity of an attack type against the defending types.
// If the combination is not listed, a neutral effectivity is assumed.
// In the case of a dual-type, the individual type combinations will be used to calculate the final effectivity.
func GetTypeEffectivity(attacker Type, defender ...Type) float64 {
	if _, ok := typeMatchup[attacker]; !ok {
		return 1
	} else {
		res := 1.0
		for _, v := range defender {
			res *= getTypeEffectivity(attacker, v)
		}
		return res
	}
}

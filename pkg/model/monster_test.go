//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package model_test

import (
	"encoding/json"
	"strings"
	"testing"

	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/spf13/viper"
	. "gitlab.com/kori-irrlicht/goconvey-assertions"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/data"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/status"
)

var monsterJSON = []byte(`{
  "id": 1,
  "nickname": "Test",
  "species": "Hoarfox",
  "status": [],
  "moves": [
    "Bite",
    "Chill"
  ],
  "geneticModifier": {
    "health": 10,
    "physAttack": 20,
    "physDefense": 30,
    "specAttack": 40,
    "specDefense": 50,
    "initiative": 60,
    "stamina": 70
  },
  "trainingModifier": {
    "health": 11,
    "physAttack": 22,
    "physDefense": 33,
    "specAttack": 44,
    "specDefense": 55,
    "initiative": 66,
    "stamina": 77
  },
  "nutritionalModifier": {
    "health": 0,
    "physAttack": 0,
    "physDefense": 0,
    "specAttack": 0,
    "specDefense": 0,
    "initiative": 0,
    "stamina": 0
  },
  "gender": 1,
  "exp": 100,
  "level": 20,
  "currentHP": 20,
  "ownerId": 1,
  "trait": "testing"
}
`)

var monster2JSON = []byte(`
{
  "id": 2,
  "species": "Flamurus",
  "status": [{"base":"Freeze","turns":0}],
  "moves": [
    "White Frost"
  ],
  "geneticModifier": {
    "health": 11,
    "physAttack": 21,
    "physDefense": 31,
    "specAttack": 41,
    "specDefense": 51,
    "initiative": 61,
    "stamina": 71
  },
  "trainingModifier": {
    "health": 19,
    "physAttack": 29,
    "physDefense": 39,
    "specAttack": 49,
    "specDefense": 59,
    "initiative": 69,
    "stamina": 79
  },
  "nutritionalModifier": {
    "health": 0,
    "physAttack": 0,
    "physDefense": 0,
    "specAttack": 0,
    "specDefense": 0,
    "initiative": 0,
    "stamina": 0
  },
  "gender": 2,
  "exp": 400,
  "level": 22,
  "currentHP": 23,
  "ownerId": 1,
  "trait": "testing"
}
`)

func init() {
	logrus.SetLevel(logrus.WarnLevel)
	basepath := "../../assets/data/monster"
	viper.SetDefault("data.dir.monster", basepath)
	viper.Set("data.archive.location", "../../assets/data")
	viper.Set("data.archive.type", "file")

	err := data.Load("../../assets/data", "file")

	if err != nil {
		panic(err)
	}

}

func TestMonster(t *testing.T) {
	Convey("Given a monster in JSON format and unmarshalling it", t, func() {
		var monster model.Monster
		err := json.Unmarshal(monsterJSON, &monster)
		Convey("There should be no error", func() {
			So(err, ShouldBeNil)
		})
		Convey("The ID should be 1", func() {
			So(monster.ID, ShouldEqual, 1)
		})
		Convey("The nickname should be 'Test'", func() {
			So(monster.Nickname, ShouldEqual, "Test")
		})
		Convey("The species should be 'Hoarfox'", func() {
			So(monster.Species, ShouldEqual, model.SpeciesCache["Hoarfox"])
		})
		Convey("The level should be 20", func() {
			So(monster.Level, ShouldEqual, 20)
		})
		Convey("The currentHP should be 20", func() {
			So(monster.CurrentHP, ShouldEqual, 20)
		})
		Convey("The moves should be 'Bite' and 'Chill'", func() {

			c := 0
			var moves []model.MoveBase
			for _, m := range monster.Moves {
				So(m, ShouldNotBeNil)
				moves = append(moves, m.Base())
				if m.Base() != model.NilMoveBase {
					c++
				}
			}

			So(c, ShouldEqual, 2)
			So(moves, ShouldContain, model.MoveBaseCache["Bite"])
			So(moves, ShouldContain, model.MoveBaseCache["Chill"])
		})

		Convey("The exp should be 100", func() {
			So(monster.Exp, ShouldEqual, 100)
		})

		Convey("The gender should be 'male'", func() {
			So(monster.Gender, ShouldEqual, model.GenderMale)
		})
		Convey("The status should be empty", func() {
			So(monster.Status, ShouldBeEmpty)
		})

		Convey("The geneticModifier should be {10, 20, 30, 40, 50, 60, 70}", func() {
			So(monster.GeneticModifier.Health, ShouldEqual, 10)
			So(monster.GeneticModifier.PhysAttack, ShouldEqual, 20)
			So(monster.GeneticModifier.PhysDefense, ShouldEqual, 30)
			So(monster.GeneticModifier.SpecAttack, ShouldEqual, 40)
			So(monster.GeneticModifier.SpecDefense, ShouldEqual, 50)
			So(monster.GeneticModifier.Initiative, ShouldEqual, 60)
			So(monster.GeneticModifier.Stamina, ShouldEqual, 70)
		})

		Convey("The trainingModifier should be {11, 22, 33, 44, 55, 66, 77}", func() {
			So(monster.TrainingModifier.Health, ShouldEqual, 11)
			So(monster.TrainingModifier.PhysAttack, ShouldEqual, 22)
			So(monster.TrainingModifier.PhysDefense, ShouldEqual, 33)
			So(monster.TrainingModifier.SpecAttack, ShouldEqual, 44)
			So(monster.TrainingModifier.SpecDefense, ShouldEqual, 55)
			So(monster.TrainingModifier.Initiative, ShouldEqual, 66)
			So(monster.TrainingModifier.Stamina, ShouldEqual, 77)
		})

		Convey("Marshalling it again", func() {
			res, err := json.MarshalIndent(monster, "", "  ")
			Convey("Should return no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("Should return the initial JSON", func() {
				So(strings.ReplaceAll(string(res), "\n", ""), ShouldResembleJSON, strings.ReplaceAll(string(monsterJSON), "\n", ""))
			})
		})

		Convey("The owner should be 1", func() {
			So(monster.OwnerID, ShouldEqual, 1)
		})

	})
	Convey("Given a monster in JSON format without a nickname and unmarshalling it", t, func() {
		var monster model.Monster
		err := json.Unmarshal(monster2JSON, &monster)
		Convey("There should be no error", func() {
			So(err, ShouldBeNil)
		})
		Convey("The ID should be 2", func() {
			So(monster.ID, ShouldEqual, 2)
		})
		Convey("The nickname should be empty", func() {
			So(monster.Nickname, ShouldBeEmpty)
		})
		Convey("The species should be 'Flamurus'", func() {
			So(monster.Species, ShouldEqual, model.SpeciesCache["Flamurus"])
		})
		Convey("The level should be 22", func() {
			So(monster.Level, ShouldEqual, 22)
		})
		Convey("The currentHP should be 23", func() {
			So(monster.CurrentHP, ShouldEqual, 23)
		})
		Convey("The moves should be 'White Frost'", func() {

			c := 0
			var moves []model.MoveBase
			for _, m := range monster.Moves {
				So(m, ShouldNotBeNil)
				moves = append(moves, m.Base())
				if m.Base() != model.NilMoveBase {
					c++
				}
			}

			So(c, ShouldEqual, 1)
			So(moves, ShouldContain, model.MoveBaseCache["White Frost"])
		})

		Convey("The exp should be 400", func() {
			So(monster.Exp, ShouldEqual, 400)
		})

		Convey("The gender should be 'female'", func() {
			So(monster.Gender, ShouldEqual, model.GenderFemale)
		})
		Convey("The status should contain 'freeze'", func() {
			So(monster.Status, ShouldContain, status.New(string(status.Freeze)))
		})

		Convey("The geneticModifier should be {11, 21, 31, 41, 51, 61, 71}", func() {
			So(monster.GeneticModifier.Health, ShouldEqual, 11)
			So(monster.GeneticModifier.PhysAttack, ShouldEqual, 21)
			So(monster.GeneticModifier.PhysDefense, ShouldEqual, 31)
			So(monster.GeneticModifier.SpecAttack, ShouldEqual, 41)
			So(monster.GeneticModifier.SpecDefense, ShouldEqual, 51)
			So(monster.GeneticModifier.Initiative, ShouldEqual, 61)
			So(monster.GeneticModifier.Stamina, ShouldEqual, 71)
		})

		Convey("The trainingModifier should be {19, 29, 39, 49, 59, 69, 79}", func() {
			So(monster.TrainingModifier.Health, ShouldEqual, 19)
			So(monster.TrainingModifier.PhysAttack, ShouldEqual, 29)
			So(monster.TrainingModifier.PhysDefense, ShouldEqual, 39)
			So(monster.TrainingModifier.SpecAttack, ShouldEqual, 49)
			So(monster.TrainingModifier.SpecDefense, ShouldEqual, 59)
			So(monster.TrainingModifier.Initiative, ShouldEqual, 69)
			So(monster.TrainingModifier.Stamina, ShouldEqual, 79)
		})

		Convey("Marshalling it again", func() {
			res, err := json.MarshalIndent(monster, "", "  ")
			Convey("Should return no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("Should return the initial JSON", func() {
				So(strings.ReplaceAll(string(res), "\n", ""), ShouldResembleJSON, strings.ReplaceAll(string(monster2JSON), "\n", ""))
			})
		})

		Convey("The owner should be 1", func() {
			So(monster.OwnerID, ShouldEqual, 1)
		})
	})

	Convey("Given a new monster", t, func() {
		monster := model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 10, 10 }, func() (model.AttributeStat, model.AttributeStat) { return 0, 0 })
		Convey("Adding some status damage", func() {
			monster.ApplyStatus(string(status.Freeze), 10)
			Convey("It should receive status damage", func() {
				So(monster.StatusThreshold[string(status.Freeze)], ShouldEqual, 10)
			})
			Convey("It should not have the status ailment", func() {
				So(monster.Status, ShouldNotContain, status.Freeze)
			})
		})
		Convey("Adding enough status damage", func() {
			monster.ApplyStatus(string(status.Freeze), 100)
			Convey("It should receive status damage", func() {
				So(monster.StatusThreshold[string(status.Freeze)], ShouldEqual, 100)
			})
			Convey("It should have the status ailment", func() {
				So(monster.Status, ShouldContain, status.New(string(status.Freeze)))
			})
		})
		Convey("with status effects", func() {
			monster.ApplyStatus("Burn", 200)
			monster.ApplyStatus("Stun", 200)
			Convey("Adding another effect", func() {
				monster.ApplyStatus(string(status.Freeze), 100)
				Convey("It should have the new status ailment", func() {
					So(monster.Status, ShouldContain, status.New(string(status.Freeze)))
				})
				Convey("It should have lost the first status ailment", func() {
					So(monster.Status, ShouldNotContain, "1")
				})
				Convey("the first ailment should be reset", func() {
					So(monster.StatusThreshold["1"], ShouldEqual, 0)
				})
			})
		})

	})
}

var monster3JSON = []byte(`
{
  "id": 2,
  "species": "Hoarfox",
  "status": [{"base":"Freeze","turns":0}],
  "moves": [
    "White Frost"
  ],
  "geneticModifier": {
    "health": 100,
    "physAttack": 200,
    "physDefense": 300,
    "specAttack": 400,
    "specDefense": 500,
    "initiative": 600,
    "stamina": 700
  },
  "trainingModifier": {
    "health": 0,
    "physAttack": 0,
    "physDefense": 0,
    "specAttack": 0,
    "specDefense": 0,
    "initiative": 0,
    "stamina": 0
  },
  "nutritionalModifier": {
    "health": 0,
    "physAttack": 0,
    "physDefense": 0,
    "specAttack": 0,
    "specDefense": 0,
    "initiative": 0,
    "stamina": 0
  },
  "gender": 2,
  "exp": 400,
  "level": 100,
  "currentHP": 23,
  "ownerId": 1,
  "trait": "testing"
}
`)

func TestAttributeCalculation(t *testing.T) {
	Convey("Given a monster", t, func() {
		var monster model.Monster
		err := json.Unmarshal(monster3JSON, &monster)
		if err != nil {
			panic(err)
		}
		Convey("It has a trait increasing its 'health'", func() {
			monster.Trait = &model.Trait{
				IncreasedAttribute: "health",
				IncreasedBy:        10,
			}
			Convey("So the health is increased by 10%", func() {
				So(monster.Attributes().Health, ShouldEqual, 450) // gm + species value
			})
		})
		Convey("It has a trait increasing its 'physAttack'", func() {
			monster.Trait = &model.Trait{
				IncreasedAttribute: "physAttack",
				IncreasedBy:        10,
			}
			Convey("So the stamina is increased by 10%", func() {
				So(monster.Attributes().PhysAttack, ShouldEqual, 560) // gm + species value
			})
		})
		Convey("It has a trait increasing its 'PhysDefense'", func() {
			monster.Trait = &model.Trait{
				IncreasedAttribute: "physDefense",
				IncreasedBy:        10,
			}
			Convey("So the health is increased by 10%", func() {
				So(monster.Attributes().PhysDefense, ShouldEqual, 780) // gm + species value
			})
		})
		Convey("It has a trait increasing its 'specAttack'", func() {
			monster.Trait = &model.Trait{
				IncreasedAttribute: "specAttack",
				IncreasedBy:        10,
			}
			Convey("So the health is increased by 10%", func() {
				So(monster.Attributes().SpecAttack, ShouldEqual, 1000) // gm + species value
			})
		})
		Convey("It has a trait increasing its 'specDefense'", func() {
			monster.Trait = &model.Trait{
				IncreasedAttribute: "specDefense",
				IncreasedBy:        10,
			}
			Convey("So the health is increased by 10%", func() {
				So(monster.Attributes().SpecDefense, ShouldEqual, 1220) // gm + species value
			})
		})
		Convey("It has a trait increasing its 'initiative'", func() {
			monster.Trait = &model.Trait{
				IncreasedAttribute: "initiative",
				IncreasedBy:        10,
			}
			Convey("So the health is increased by 10%", func() {
				So(monster.Attributes().Initiative, ShouldEqual, 1440) // gm + species value
			})
		})
		Convey("It has a trait increasing its 'stamina'", func() {
			monster.Trait = &model.Trait{
				IncreasedAttribute: "stamina",
				IncreasedBy:        10,
			}
			Convey("So the health is increased by 10%", func() {
				So(monster.Attributes().Stamina, ShouldEqual, 1671) // gm + species value
			})
		})

		Convey("It has a trait decreasing its 'health'", func() {
			monster.Trait = &model.Trait{
				DecreasedAttribute: "health",
				DecreasedBy:        10,
			}
			Convey("So the health is decreased by 10%", func() {
				So(monster.Attributes().Health, ShouldEqual, 370) // gm + species value
			})
		})
		Convey("It has a trait decreasing its 'physAttack'", func() {
			monster.Trait = &model.Trait{
				DecreasedAttribute: "physAttack",
				DecreasedBy:        10,
			}
			Convey("So the stamina is decreased by 10%", func() {
				So(monster.Attributes().PhysAttack, ShouldEqual, 460) // gm + species value
			})
		})
		Convey("It has a trait decreasing its 'PhysDefense'", func() {
			monster.Trait = &model.Trait{
				DecreasedAttribute: "physDefense",
				DecreasedBy:        10,
			}
			Convey("So the health is decreased by 10%", func() {
				So(monster.Attributes().PhysDefense, ShouldEqual, 640) // gm + species value
			})
		})
		Convey("It has a trait decreasing its 'specAttack'", func() {
			monster.Trait = &model.Trait{
				DecreasedAttribute: "specAttack",
				DecreasedBy:        10,
			}
			Convey("So the health is decreased by 10%", func() {
				So(monster.Attributes().SpecAttack, ShouldEqual, 820) // gm + species value
			})
		})
		Convey("It has a trait decreasing its 'specDefense'", func() {
			monster.Trait = &model.Trait{
				DecreasedAttribute: "specDefense",
				DecreasedBy:        10,
			}
			Convey("So the health is decreased by 10%", func() {
				So(monster.Attributes().SpecDefense, ShouldEqual, 1000) // gm + species value
			})
		})
		Convey("It has a trait decreasing its 'initiative'", func() {
			monster.Trait = &model.Trait{
				DecreasedAttribute: "initiative",
				DecreasedBy:        10,
			}
			Convey("So the health is decreased by 10%", func() {
				So(monster.Attributes().Initiative, ShouldEqual, 1180) // gm + species value
			})
		})
		Convey("It has a trait decreasing its 'stamina'", func() {
			monster.Trait = &model.Trait{
				DecreasedAttribute: "stamina",
				DecreasedBy:        10,
			}
			Convey("So the health is decreased by 10%", func() {
				So(monster.Attributes().Stamina, ShouldEqual, 1369) // gm + species value
			})
		})

	})
}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package monster

import "gitlab.com/kori-irrlicht/project-open-monster/pkg/model"

//TODO: Change to data-driven approach

var (
	Hoarfox = &model.Species{
		Name: "Hoarfox",
		Base: model.Attributes{
			Health:      100,
			PhysAttack:  100,
			PhysDefense: 100,
			SpecAttack:  100,
			SpecDefense: 100,
			Initiative:  100,
			Stamina:     100,
		},
		Types: []model.Type{"Ice"},
		ExpCurve: &model.ExpCurve{
			Curve: func(exp model.Exp) model.Level {
				return model.Level(exp)
			},
		},
	}
)

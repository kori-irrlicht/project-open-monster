//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package file

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
)

type Provider struct{}

// Load loads every file in the basepath folder and tries to convert it into a monster species
// The basepath is read from the 'data.dir.monster' field in the configuration.
//
// Failed files will be listed in the returned error object
// The returned map contains every species, which could be parsed
func (tp *Provider) ProvideFiles(basepath string) (map[string]map[string]io.Reader, error) {
	m := make(map[string]map[string]io.Reader)

	err := filepath.Walk(basepath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}

		file, err := ioutil.ReadFile(filepath.Clean(path))
		if err != nil {
			return err
		}

		currentDir := filepath.Base(filepath.Dir(path))
		if _, ok := m[currentDir]; !ok {
			m[currentDir] = make(map[string]io.Reader)
		}

		reader := bytes.NewReader(file)
		m[currentDir][path] = reader

		return nil
	})
	if err != nil {
		return nil, err
	}

	return m, nil
}

var _ db.FileProvider = (*Provider)(nil)

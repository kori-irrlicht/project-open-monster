//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package bbolt

import (
	"encoding/json"
	"strconv"

	bolt "go.etcd.io/bbolt"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

var (
	breederBucket  = []byte("breeder")
	positionBucket = []byte("position")
)

type BreederDB struct {
	db *bolt.DB
}

func NewBreederDB(db *bolt.DB) *BreederDB {
	return &BreederDB{
		db: db,
	}
}

func (b *BreederDB) Create(breeder model.Breeder) (model.ID, error) {

	err := b.db.Update(func(tx *bolt.Tx) error {

		bucket, err := tx.CreateBucketIfNotExists(breederBucket)
		if err != nil {
			return err
		}

		id, _ := bucket.NextSequence()
		breeder.ID = model.ID(id)

		buf, err := json.Marshal(breeder)
		if err != nil {
			return err
		}

		return bucket.Put(itob(breeder.ID), buf)

	})
	return breeder.ID, err
}

func (b *BreederDB) Load(id model.ID) (model.Breeder, error) {

	var breeder model.Breeder
	err := b.db.View(func(tx *bolt.Tx) error {

		bucket := tx.Bucket(breederBucket)
		buf := bucket.Get(itob(id))
		return json.Unmarshal(buf, &breeder)

	})
	return breeder, err
}

func (b *BreederDB) Save(breeder model.Breeder) error {
	return b.db.Update(func(tx *bolt.Tx) error {

		bucket := tx.Bucket(breederBucket)

		buf, err := json.Marshal(breeder)
		if err != nil {
			return err
		}

		return bucket.Put(itob(breeder.ID), buf)

	})

}

func (b *BreederDB) LoadAll() ([]model.Breeder, error) { return nil, nil }

func (b *BreederDB) Delete(id model.ID) error {
	return b.db.Update(func(tx *bolt.Tx) error {

		bucket := tx.Bucket(breederBucket)

		return bucket.Delete(itob(id))
	})
}

func (b *BreederDB) LoadPosition(id model.ID) (model.Vector2D, error) {
	var pos model.Vector2D

	err := b.db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(positionBucket)
		buf := bucket.Get(itob(id))
		return json.Unmarshal(buf, &pos)

	})
	return pos, err
}

func (b *BreederDB) SavePosition(breederID model.ID, position model.Vector2D) (err error) {
	return b.db.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists(positionBucket)
		if err != nil {
			return err
		}

		buf, err := json.Marshal(position)
		if err != nil {
			return err
		}

		return bucket.Put(itob(breederID), buf)
	})

}

var _ db.BreederDB = (*BreederDB)(nil)

// itob returns an 8-byte big endian representation of v.
func itob(v model.ID) []byte {
	return []byte(strconv.Itoa(int(v)))
}

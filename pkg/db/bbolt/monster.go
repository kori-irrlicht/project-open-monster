//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package bbolt

import (
	"encoding/json"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	bolt "go.etcd.io/bbolt"
)

type monsterDB struct {
	db *bolt.DB
}

func NewMonsterDB(db *bolt.DB) db.MonsterDB {
	return &monsterDB{
		db: db,
	}
}

func (m *monsterDB) Create(monster model.Monster) (model.ID, error) {
	err := m.db.Update(func(tx *bolt.Tx) error {

		bucket, err := tx.CreateBucketIfNotExists([]byte("monster"))
		if err != nil {
			return err
		}

		id, _ := bucket.NextSequence()
		monster.ID = model.ID(id)

		buf, err := json.Marshal(monster)
		if err != nil {
			return err
		}

		return bucket.Put(itob(monster.ID), buf)

	})
	return monster.ID, err
}

func (m *monsterDB) Load(id model.ID) (model.Monster, error) {

	var monster model.Monster
	err := m.db.View(func(tx *bolt.Tx) error {

		bucket := tx.Bucket([]byte("monster"))
		buf := bucket.Get(itob(id))
		return json.Unmarshal(buf, &monster)

	})
	return monster, err
}

func (m *monsterDB) Save(monster model.Monster) error {
	return m.db.Update(func(tx *bolt.Tx) error {

		bucket := tx.Bucket([]byte("monster"))

		buf, err := json.Marshal(monster)
		if err != nil {
			return err
		}

		return bucket.Put(itob(monster.ID), buf)

	})

}
func (m *monsterDB) LoadTeamByBreederId(breederID model.ID) ([]model.Monster, error) {
	var team []model.Monster
	err := m.db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte("teams"))
		buf := bucket.Get(itob(breederID))
		var ids []model.ID
		if err := json.Unmarshal(buf, &ids); err != nil {
			return err
		}
		for _, id := range ids {
			monster, err := m.Load(id)
			if err != nil {
				return err
			}
			team = append(team, monster)

		}

		return nil
	})
	return team, err
}

func (m *monsterDB) SaveTeamByBreederId(breederID model.ID, team []model.Monster) error {
	return m.db.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte("teams"))
		if err != nil {
			return err
		}

		var ids []model.ID
		for _, mon := range team {
			ids = append(ids, mon.ID)
		}
		buf, err := json.Marshal(ids)
		if err != nil {
			return err
		}

		return bucket.Put(itob(breederID), buf)

	})
}

func (m *monsterDB) Delete(id model.ID) error {
	return m.db.Update(func(tx *bolt.Tx) error {

		bucket := tx.Bucket([]byte("monster"))

		return bucket.Delete(itob(id))
	})
}

var _ db.MonsterDB = (*monsterDB)(nil)

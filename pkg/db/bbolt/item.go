//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package bbolt

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	bolt "go.etcd.io/bbolt"
)

type itemDB struct {
	db *bolt.DB
}

func NewItemDB(db *bolt.DB) db.ItemDB {
	return &itemDB{
		db: db,
	}
}

func (i *itemDB) Add(breederID model.ID, itemName string, amount int) error {
	return i.changeItemAmount(breederID, itemName, amount)
}

func (i *itemDB) Remove(breederID model.ID, itemName string, amount int) error {
	return i.changeItemAmount(breederID, itemName, -amount)
}

func (i *itemDB) changeItemAmount(breederID model.ID, itemName string, amount int) error {
	return i.db.Update(func(tx *bolt.Tx) error {

		bucket, err := tx.CreateBucketIfNotExists([]byte("inventory"))
		if err != nil {
			return err
		}

		id := strconv.Itoa(int(breederID))
		breederBucket, err := bucket.CreateBucketIfNotExists([]byte(id))
		if err != nil {
			return err
		}

		amountByte := breederBucket.Get([]byte(itemName))
		var currentAmount int
		if err = json.Unmarshal(amountByte, &currentAmount); err != nil {
			return err
		}
		currentAmount += amount
		if currentAmount < 0 {
			return fmt.Errorf("can't reduce item '%s' below 0", itemName)
		}

		amountByte, err = json.Marshal(amountByte)

		if err != nil {
			return err
		}

		return bucket.Put([]byte(itemName), amountByte)

	})
}

func (i *itemDB) Inventory(breederID model.ID) (map[string]int, error) {
	m := make(map[string]int)
	err := i.db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte("inventory"))

		id := strconv.Itoa(int(breederID))
		breederBucket := bucket.Bucket([]byte(id))

		c := breederBucket.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			item := string(k)
			var currentAmount int
			if err := json.Unmarshal(v, &currentAmount); err != nil {
				return err
			}
			m[item] = currentAmount

		}

		return nil
	})
	return m, err
}

func (i *itemDB) HasCollected(breederID model.ID, itemID string) (bool, error) {
	var res bool
	err := i.db.View(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte("collectable"))
		if err != nil {
			return err
		}
		itemBucket, err := bucket.CreateBucketIfNotExists([]byte(itemID))
		if err != nil {
			return err
		}

		var b []byte
		binary.LittleEndian.PutUint64(b, uint64(breederID))
		result := itemBucket.Get(b)

		res = result[0] == 1

		return nil
	})

	return res, err

}

// Marks an item as collected by a breeder and adds it to their inventory
func (i *itemDB) Collect(breederID model.ID, itemID string, itemName string, amount int) error {
	return errors.New("Not implemented")

}

var _ db.ItemDB = (*itemDB)(nil)

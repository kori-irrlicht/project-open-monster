package memory

import (
	"fmt"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type ItemDB struct {
	inventories map[model.ID]map[string]int
	collected   map[model.ID]map[string]bool
}

func NewItemDB() *ItemDB {
	return &ItemDB{
		inventories: make(map[model.ID]map[string]int),
		collected:   make(map[model.ID]map[string]bool),
	}
}

func (i *ItemDB) Add(breederID model.ID, itemName string, amount int) error {
	return i.changeItemAmount(breederID, itemName, amount)
}

func (i *ItemDB) Remove(breederID model.ID, itemName string, amount int) error {
	return i.changeItemAmount(breederID, itemName, -amount)
}

func (i *ItemDB) changeItemAmount(breederID model.ID, itemName string, amount int) error {
	inventory, ok := i.inventories[breederID]
	if !ok {
		inventory = make(map[string]int)
		i.inventories[breederID] = inventory
	}

	inventory[itemName] += amount

	return nil

}

func (i *ItemDB) Inventory(breederID model.ID) (map[string]int, error) {
	inventory, ok := i.inventories[breederID]
	if !ok {
		inventory = make(map[string]int)
		i.inventories[breederID] = inventory
	}
	return inventory, nil

}

func (i *ItemDB) HasCollected(breederID model.ID, itemID string) (bool, error) {
	collectedItems, ok := i.collected[breederID]
	if !ok {
		collectedItems = make(map[string]bool)
		i.collected[breederID] = collectedItems
	}

	return collectedItems[itemID], nil

}

// Marks an item as collected by a breeder and adds it to their inventory
func (i *ItemDB) Collect(breederID model.ID, itemID string, itemName string, amount int) error {
	if ok, err := i.HasCollected(breederID, itemID); err != nil {
		return fmt.Errorf("Could not check if the item has been collected: %w", err)
	} else if ok {
		return fmt.Errorf("Item '%s' was already collected by '%d'", itemID, breederID)
	}

	i.collected[breederID][itemName] = true

	if err := i.changeItemAmount(breederID, itemName, amount); err != nil {
		return fmt.Errorf("Could not update inventory: %w", err)
	}

	return nil

}

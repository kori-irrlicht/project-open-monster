//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package memory

import (
	"errors"
	"fmt"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type MonsterDB struct {
	monster    map[model.ID]model.Monster
	team       map[model.ID][]model.Monster
	idProvider chan model.ID
}

func NewMonsterDB() *MonsterDB {
	mdb := &MonsterDB{
		monster:    make(map[model.ID]model.Monster),
		team:       make(map[model.ID][]model.Monster),
		idProvider: make(chan model.ID, 10),
	}
	go fillIdProvider(mdb.idProvider)
	return mdb
}

func (mdb *MonsterDB) Create(monster model.Monster) (model.ID, error) {
	monster.ID = <-mdb.idProvider
	mdb.monster[monster.ID] = monster
	return monster.ID, nil
}

func (mdb *MonsterDB) Load(id model.ID) (model.Monster, error) {
	if monster, ok := mdb.monster[id]; ok {
		return monster, nil
	}
	return model.Monster{}, fmt.Errorf("Could not find monster with id: %d", id)
}

func (mdb *MonsterDB) Save(monster model.Monster) error {
	if _, ok := mdb.monster[monster.ID]; !ok {
		return fmt.Errorf("Could not find monster with id: %d", monster.ID)
	}
	mdb.monster[monster.ID] = monster

	return nil
}

func (mdb *MonsterDB) LoadTeamByBreederId(id model.ID) (team []model.Monster, err error) {
	var ok bool
	if team, ok = mdb.team[id]; ok {
		return team, nil
	}
	return team, fmt.Errorf("Could not find a team for breeder id: %d", id)
}

func (mdb *MonsterDB) SaveTeamByBreederId(id model.ID, team []model.Monster) error {
	if len(team) == 0 {
		return errors.New("Team should not be empty")
	}
	mdb.team[id] = team
	return nil
}

func (mdb *MonsterDB) Delete(id model.ID) error {
	if _, ok := mdb.monster[id]; !ok {
		return fmt.Errorf("Could not find monster with id: %d", id)
	}
	delete(mdb.monster, id)

	return nil
}

var _ db.MonsterDB = (*MonsterDB)(nil)

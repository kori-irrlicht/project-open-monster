//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package memory

import (
	"fmt"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type BattleDB struct {
	requests   map[model.ID]model.Request
	idProvider chan model.ID
}

func NewBattleDB() *BattleDB {
	bdb := &BattleDB{
		requests:   make(map[model.ID]model.Request),
		idProvider: make(chan model.ID, 10),
	}
	go fillIdProvider(bdb.idProvider)

	return bdb
}

func (bdb *BattleDB) CreateRequest(req model.Request) (model.ID, error) {
	req.ID = <-bdb.idProvider
	req.Status = model.ReqStatus_Pending
	bdb.requests[req.ID] = req
	return req.ID, nil
}

func (bdb *BattleDB) LoadRequest(id model.ID) (model.Request, error) {
	if request, ok := bdb.requests[id]; ok {
		return request, nil
	}
	return model.Request{}, db.NotFoundError{Msg: fmt.Sprintf("Could not find request with id: %d", id)}
}

func (bdb *BattleDB) SaveRequest(request model.Request) error {
	if _, ok := bdb.requests[request.ID]; !ok {
		return db.NotFoundError{Msg: fmt.Sprintf("Could not find request with id: %d", request.ID)}
	}
	bdb.requests[request.ID] = request

	return nil
}

func (bdb *BattleDB) LoadRequestByBreeder(id model.ID) ([]model.Request, error) {
	result := []model.Request{}
	for _, request := range bdb.requests {
		if (request.From == id || request.To == id) && request.Status == model.ReqStatus_Pending {
			result = append(result, request)
		}
	}
	if len(result) == 0 {
		return nil, db.NotFoundError{Msg: fmt.Sprintf("Could not find request with id: %d", id)}
	}
	return result, nil
}

// Enforce implementation of the interface db.BattleDB
var _ db.BattleDB = (*BattleDB)(nil)

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package memory

import (
	"errors"
	"fmt"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type BreederDB struct {
	idProvider chan model.ID
	breeder    map[model.ID]model.Breeder
}

// Generates sequential ids and stores them in the given channel
func fillIdProvider(provider chan model.ID) {
	var id model.ID = 1
	for {
		provider <- id
		id++
	}
}

func NewBreederDB() *BreederDB {
	tdb := &BreederDB{
		idProvider: make(chan model.ID, 10),
		breeder:    make(map[model.ID]model.Breeder),
	}

	go fillIdProvider(tdb.idProvider)

	return tdb
}

func (tdb *BreederDB) Create(breeder model.Breeder) (model.ID, error) {
	if len(breeder.Name) == 0 {
		return 0, errors.New("Breeder has no name")
	}
	breeder.ID = <-tdb.idProvider
	tdb.breeder[breeder.ID] = breeder
	return breeder.ID, nil
}

func (tdb *BreederDB) Load(id model.ID) (model.Breeder, error) {
	if breeder, ok := tdb.breeder[id]; ok {
		return breeder, nil
	}
	return model.Breeder{}, db.NotFoundError{Msg: fmt.Sprintf("Could not find breeder with id: %d", id)}
}

func (tdb *BreederDB) LoadAll() ([]model.Breeder, error) {
	result := make([]model.Breeder, 0)
	for _, v := range tdb.breeder {
		result = append(result, v)
	}
	return result, nil
}

func (tdb *BreederDB) Save(breeder model.Breeder) error {
	if _, ok := tdb.breeder[breeder.ID]; !ok {
		return fmt.Errorf("Could not find breeder with id: %d", breeder.ID)
	}
	tdb.breeder[breeder.ID] = breeder

	return nil
}

func (tdb *BreederDB) Delete(id model.ID) error {
	if _, ok := tdb.breeder[id]; !ok {
		return fmt.Errorf("Could not find breeder with id: %d", id)
	}
	delete(tdb.breeder, id)

	return nil

}

func (b *BreederDB) SavePosition(breederID model.ID, position model.Vector2D) (err error) {

	return nil
}

var _ db.BreederDB = &BreederDB{}

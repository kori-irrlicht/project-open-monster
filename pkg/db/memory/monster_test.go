//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package memory_test

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/memory"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

func TestMonsterDB(t *testing.T) {
	Convey("Given a new monster db", t, func() {
		monsterDb := memory.NewMonsterDB()
		Convey("Asking to create a new monster", func() {
			monster := model.Monster{}
			id, err := monsterDb.Create(monster)
			monster.ID = id
			Convey("So the monster should be created with id 1", func() {
				So(err, ShouldBeNil)
				So(id, ShouldEqual, 1)
			})
			Convey("Asking to create a second monster", func() {
				monster := model.Monster{}
				id, err := monsterDb.Create(monster)
				Convey("So the monster should be created with id 2", func() {
					So(err, ShouldBeNil)
					So(id, ShouldEqual, 2)
				})
			})
			Convey("The created monster can be loaded", func() {
				monsterLoaded, err := monsterDb.Load(1)
				Convey("So there should be no error", func() {
					So(err, ShouldBeNil)
					So(monsterLoaded, ShouldResemble, monster)
				})
			})
			Convey("Updating the monster", func() {
				monster.Nickname = "Hello World"
				err := monsterDb.Save(monster)
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The saved monster should have an updated name", func() {
					tr, _ := monsterDb.Load(monster.ID)
					So(tr, ShouldResemble, monster)
				})
			})
			Convey("Deleting the monster", func() {
				err := monsterDb.Delete(monster.ID)
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The breeder should be deleted", func() {
					tr, err := monsterDb.Load(monster.ID)
					So(tr.ID, ShouldEqual, 0)
					So(err, ShouldNotBeNil)
				})
			})
		})
		Convey("Trying to load an unknown id", func() {
			monster, err := monsterDb.Load(10000)
			Convey("Should return an error", func() {
				So(err, ShouldNotBeNil)
				So(monster.ID, ShouldEqual, 0)
			})
		})
		Convey("Trying to save a monster with an unknown id", func() {
			monster := model.Monster{ID: 10000}
			err := monsterDb.Save(monster)
			Convey("Should return an error", func() {
				So(err, ShouldNotBeNil)
			})
			Convey("The monster should not be saved", func() {
				t, err := monsterDb.Load(monster.ID)
				So(t, ShouldResemble, model.Monster{})
				So(err, ShouldNotBeNil)
			})
		})
		Convey("Trying to delete an unknown id", func() {
			err := monsterDb.Delete(10000)
			Convey("Should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})
		Convey("Given a breeder", func() {

			breeder := model.Breeder{ID: 1}
			Convey("With an empty team", func() {
				var team []model.Monster
				Convey("Saving the team", func() {
					err := monsterDb.SaveTeamByBreederId(breeder.ID, team)
					Convey("Should fail", func() {
						So(err, ShouldNotBeNil)
						teamLoaded, err := monsterDb.LoadTeamByBreederId(breeder.ID)
						So(err, ShouldNotBeNil)
						So(teamLoaded, ShouldBeEmpty)
					})
				})
			})
			Convey("With a team", func() {
				team := []model.Monster{
					model.Monster{},
					model.Monster{},
					model.Monster{},
				}
				for _, v := range team {
					id, _ := monsterDb.Create(v)
					v.ID = id
				}
				Convey("Saving the team", func() {
					err := monsterDb.SaveTeamByBreederId(breeder.ID, team)
					Convey("Should not return an error", func() {
						So(err, ShouldBeNil)
					})
					Convey("And loading the team", func() {
						teamLoaded, err := monsterDb.LoadTeamByBreederId(breeder.ID)
						Convey("Should not return an error", func() {
							So(err, ShouldBeNil)
						})
						Convey("Should return the team", func() {
							So(teamLoaded, ShouldResemble, team)
						})
					})
				})
			})
			Convey("Trying to load a not existing team", func() {
				team, err := monsterDb.LoadTeamByBreederId(10000)
				Convey("Should fail", func() {
					So(err, ShouldNotBeNil)
					So(team, ShouldBeEmpty)
				})
			})
		})
	})
}

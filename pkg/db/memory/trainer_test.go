//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package memory_test

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/memory"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

func TestBreederDB(t *testing.T) {
	Convey("Given a new breeder db", t, func() {
		breederDb := memory.NewBreederDB()
		Convey("Asking to create a new breeder", func() {
			breeder := model.Breeder{Name: "Test"}
			id, err := breederDb.Create(breeder)
			breeder.ID = id
			Convey("So the breeder should be created with id 1", func() {
				So(err, ShouldBeNil)
				So(id, ShouldEqual, 1)
			})
			Convey("Asking to create a second breeder", func() {
				breeder2 := model.Breeder{Name: "Test2"}
				id, err := breederDb.Create(breeder2)
				breeder2.ID = id
				Convey("So the breeder should be created with id 2", func() {
					So(err, ShouldBeNil)
					So(id, ShouldEqual, 2)
				})
				Convey("Trying to load all breeders", func() {
					trList, err := breederDb.LoadAll()
					Convey("There should be 2 breeder", func() {
						So(trList, ShouldHaveLength, 2)
					})
					Convey("There should be no error", func() {
						So(err, ShouldBeNil)
					})
					Convey("The list should contain breeder 1", func() {
						So(trList, ShouldContain, breeder)
					})
					Convey("The list should contain breeder 2", func() {
						So(trList, ShouldContain, breeder2)
					})
				})
			})
			Convey("The created breeder can be loaded", func() {
				breederLoaded, err := breederDb.Load(1)
				Convey("So there should be no error", func() {
					So(err, ShouldBeNil)
					So(breederLoaded, ShouldResemble, breeder)
				})
			})
			Convey("Updating the breeder", func() {
				breeder.Name = "Hello World"
				err := breederDb.Save(breeder)
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The saved breeder should have an updated name", func() {
					tr, _ := breederDb.Load(breeder.ID)
					So(tr, ShouldResemble, breeder)
				})
			})
			Convey("Deleting the breeder", func() {
				err := breederDb.Delete(breeder.ID)
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The breeder should be deleted", func() {
					tr, err := breederDb.Load(breeder.ID)
					So(tr.ID, ShouldEqual, 0)
					So(err, ShouldNotBeNil)
				})
			})
		})
		Convey("Asking to create a new without name", func() {
			breeder := model.Breeder{}
			id, err := breederDb.Create(breeder)
			Convey("So the breeder should not be created", func() {
				So(err, ShouldNotBeNil)
				So(id, ShouldEqual, 0)
			})
		})
		Convey("Trying to load an unknown id", func() {
			breeder, err := breederDb.Load(10000)
			Convey("Should return an error", func() {
				So(err, ShouldNotBeNil)
				So(breeder.ID, ShouldEqual, 0)
			})
		})
		Convey("Trying to save a breeder with an unknown id", func() {
			breeder := model.Breeder{ID: 10000, Name: "Test"}
			err := breederDb.Save(breeder)
			Convey("Should return an error", func() {
				So(err, ShouldNotBeNil)
			})
			Convey("The breeder should not be saved", func() {
				t, err := breederDb.Load(breeder.ID)
				So(t, ShouldResemble, model.Breeder{})
				So(err, ShouldNotBeNil)
			})
		})
		Convey("Trying to delete an unknown id", func() {
			err := breederDb.Delete(10000)
			Convey("Should return an error", func() {
				So(err, ShouldNotBeNil)
			})
		})
		Convey("Trying to load all breeders", func() {
			breeder, err := breederDb.LoadAll()
			Convey("Should not return an error", func() {
				So(err, ShouldBeNil)
			})
			Convey("Should return an empty list", func() {
				So(breeder, ShouldBeEmpty)
				So(breeder, ShouldNotBeNil)
			})
		})
	})
}

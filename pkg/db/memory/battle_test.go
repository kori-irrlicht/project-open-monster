//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package memory_test

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/memory"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

func TestBattleDB(t *testing.T) {
	Convey("Given a new battle db", t, func() {
		battleDb := memory.NewBattleDB()
		Convey("Asking to create a new request", func() {
			request := model.Request{From: 1, To: 2}
			id, err := battleDb.CreateRequest(request)
			request.ID = id
			request.Status = model.ReqStatus_Pending
			Convey("So the request should be created with id 1", func() {
				So(err, ShouldBeNil)
				So(id, ShouldEqual, 1)
			})
			Convey("Asking to create a second request", func() {
				request := model.Request{}
				id, err := battleDb.CreateRequest(request)
				Convey("So the request should be created with id 2", func() {
					So(err, ShouldBeNil)
					So(id, ShouldEqual, 2)
				})
			})
			Convey("The created request can be loaded", func() {
				requestLoaded, err := battleDb.LoadRequest(1)
				Convey("So there should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The request should have the same From value", func() {
					So(requestLoaded.From, ShouldResemble, request.From)
				})
				Convey("The request should have the same To value", func() {
					So(requestLoaded.To, ShouldResemble, request.To)
				})
				Convey("The request should have the 'pending' status", func() {
					So(requestLoaded.Status, ShouldResemble, model.ReqStatus_Pending)
				})
			})
			Convey("The created request can be loaded by breeder id", func() {
				requestLoaded, err := battleDb.LoadRequestByBreeder(1)
				Convey("So there should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The result should contain the initial request", func() {
					So(requestLoaded, ShouldContain, request)
				})
			})
			Convey("Updating the request", func() {
				request.Status = model.ReqStatus_Ok
				err := battleDb.SaveRequest(request)
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The saved request should have an updated status", func() {
					tr, _ := battleDb.LoadRequest(request.ID)
					So(tr, ShouldResemble, request)
				})
			})
		})
		Convey("Trying to load an unknown id", func() {
			request, err := battleDb.LoadRequest(10000)
			Convey("Should return an error", func() {
				So(err, ShouldNotBeNil)
				So(request.ID, ShouldEqual, 0)
			})
		})
		Convey("Trying to save a request with an unknown id", func() {
			request := model.Request{ID: 10000}
			err := battleDb.SaveRequest(request)
			Convey("Should return an error", func() {
				So(err, ShouldNotBeNil)
			})
			Convey("The request should not be saved", func() {
				t, err := battleDb.LoadRequest(request.ID)
				So(t, ShouldResemble, model.Request{})
				So(err, ShouldNotBeNil)
			})
		})
	})
}

package stormdb

import (
	"fmt"

	"github.com/asdine/storm/v3"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type ItemDB struct {
	db *storm.DB
}

func NewItemDB(db *storm.DB) *ItemDB {
	return &ItemDB{
		db: db,
	}
}

func (i *ItemDB) Add(breederID model.ID, itemName string, amount int) error {
	return i.changeItemAmount(i.db, breederID, itemName, amount)
}

func (i *ItemDB) Remove(breederID model.ID, itemName string, amount int) error {
	return i.changeItemAmount(i.db, breederID, itemName, -amount)
}

func (i *ItemDB) changeItemAmount(tx storm.Node, breederID model.ID, itemName string, amount int) error {
	var inventory inventoryEntry

	err := tx.One(
		"ID",
		itemID{
			BreederID: breederID,
			Item:      itemName,
		},
		&inventory,
	)

	if err != nil {

		if err != storm.ErrNotFound {
			return err
		} else {
			inventory.ID.BreederID = breederID
			inventory.BreederID = breederID
			inventory.ID.Item = itemName

		}
	}

	inventory.Amount += amount

	if inventory.Amount < 0 {
		return fmt.Errorf("can't reduce item '%s' below 0", itemName)
	}

	return tx.Save(&inventory)

}

type inventoryEntry struct {
	ID        itemID `storm:"id"`
	Amount    int
	BreederID model.ID `storm:"index"`
}

type itemID struct {
	BreederID model.ID
	Item      string
}

func (i *ItemDB) Inventory(breederID model.ID) (map[string]int, error) {
	var inventory []inventoryEntry

	items := make(map[string]int)

	if err := i.db.Find("BreederID", breederID, &inventory); err != nil && err != storm.ErrNotFound {
		return nil, err
	}

	for _, e := range inventory {
		items[e.ID.Item] = e.Amount
	}
	return items, nil

}

func (i *ItemDB) HasCollected(breederID model.ID, itemID string) (bool, error) {
	b, err := i.db.From("collectible").KeyExists(itemID, breederID)
	if err != nil {
		if err == storm.ErrNotFound {
			return false, nil
		}
		return false, err
	}
	return b, err

}

// Marks an item as collected by a breeder and adds it to their inventory
func (i *ItemDB) Collect(breederID model.ID, itemID string, itemName string, amount int) error {
	if ok, err := i.HasCollected(breederID, itemID); err != nil {
		return fmt.Errorf("Could not check if the item has been collected: %w", err)
	} else if ok {
		return fmt.Errorf("Item '%s' was already collected by '%d'", itemID, breederID)
	}
	tx, err := i.db.Begin(true)
	if err != nil {
		return fmt.Errorf("Could not start transaction: %w", err)
	}
	defer func() { _ = tx.Rollback() }()

	if err := tx.From("collectible").Set(itemID, breederID, true); err != nil {
		return fmt.Errorf("Could not set the item as collected: %w", err)
	}

	if err := i.changeItemAmount(tx, breederID, itemName, amount); err != nil {
		return fmt.Errorf("Could not update inventory: %w", err)
	}

	if err := tx.Commit(); err != nil {
		return fmt.Errorf("Could not commit transaction: %w", err)
	}
	return nil

}

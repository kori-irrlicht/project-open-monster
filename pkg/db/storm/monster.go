//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package stormdb

import (
	"github.com/asdine/storm/v3"
	"github.com/asdine/storm/v3/q"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type MonsterDB struct {
	db *storm.DB
}

func NewMonsterDB(db *storm.DB) *MonsterDB {
	return &MonsterDB{
		db: db,
	}
}

func (m *MonsterDB) Create(monster model.Monster) (model.ID, error) {
	err := m.db.Save(&monster)
	return monster.ID, err
}

func (m *MonsterDB) Load(id model.ID) (monster model.Monster, err error) {
	err = m.db.One("ID", id, &monster)
	return monster, err
}

func (m *MonsterDB) Save(monster model.Monster) error {
	return m.db.Update(&monster)
}

func (m *MonsterDB) LoadTeamByBreederId(breederID model.ID) ([]model.Monster, error) {
	var teamID []model.ID
	if err := m.db.Get("teams", breederID, &teamID); err != nil {
		return nil, err
	}
	query := m.db.Select(q.In("ID", teamID))

	var team []model.Monster
	err := query.Find(&team)
	return team, err
}

func (m *MonsterDB) SaveTeamByBreederId(breederID model.ID, team []model.Monster) error {
	var teamID []model.ID
	for _, m := range team {
		teamID = append(teamID, m.ID)
	}
	return m.db.Set("teams", breederID, teamID)
}

func (m *MonsterDB) Delete(id model.ID) error {
	return m.db.DeleteStruct(&model.Monster{ID: id})
}

var _ db.MonsterDB = (*MonsterDB)(nil)

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package stormdb

import (
	"github.com/asdine/storm/v3"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type BreederDB struct {
	db *storm.DB
}

func NewBreederDB(db *storm.DB) *BreederDB {
	return &BreederDB{
		db: db,
	}
}

func (b *BreederDB) Create(breeder model.Breeder) (model.ID, error) {

	err := b.db.Save(&breeder)
	return breeder.ID, err

}

func (b *BreederDB) Load(id model.ID) (breeder model.Breeder, err error) {
	err = b.db.One("ID", id, &breeder)
	return
}

func (b *BreederDB) Save(breeder model.Breeder) error {
	return b.db.Update(&breeder)

}

func (b *BreederDB) LoadAll() (breeder []model.Breeder, err error) {
	err = b.db.All(&breeder)
	return
}

func (b *BreederDB) Delete(id model.ID) error {
	return b.db.DeleteStruct(&model.Breeder{
		ID: id,
	})
}

func (b *BreederDB) LoadPosition(id model.ID) (pos model.Vector2D, err error) {
	err = b.db.Get("position", id, &pos)
	return pos, err
}

func (b *BreederDB) SavePosition(breederID model.ID, position model.Vector2D) (err error) {
	return b.db.Set("position", breederID, &position)
}

var _ db.BreederDB = (*BreederDB)(nil)

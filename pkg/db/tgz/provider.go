//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package tgz

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
)

type Provider struct{}

// Load loads every file in the basepath folder and tries to convert it into a monster species
// The basepath is read from the 'data.dir.monster' field in the configuration.
//
// Failed files will be listed in the returned error object
// The returned map contains every species, which could be parsed
func (tp *Provider) ProvideFiles(basepath string) (map[string]map[string]io.Reader, error) {
	m := make(map[string]map[string]io.Reader)

	file, err := os.Open(filepath.Clean(basepath + ".tar.gz"))

	if err != nil {
		return nil, errors.Wrap(err, "Could not open file")
	}
	defer func() {

		if errClose := file.Close(); errClose != nil {
			logrus.WithError(errClose).WithField("file", file.Name()).Errorln("Could not close file")
		}
	}()

	archive, err := gzip.NewReader(file)
	if err != nil {
		return nil, errors.Wrap(err, "Could not decompress the file")
	}
	defer func() {

		if errClose := archive.Close(); errClose != nil {
			logrus.WithError(errClose).WithField("file", file.Name()).Errorln("Could not close archive")
		}
	}()

	fileReader := tar.NewReader(archive)

	for {
		header, err := fileReader.Next()
		if err != nil {
			break
		}

		if !header.FileInfo().IsDir() {

			file, err := ioutil.ReadAll(fileReader)
			if err != nil {
				return nil, errors.Wrap(err, "Could not read the file")
			}

			currentDir := filepath.Base(filepath.Dir(header.Name))
			if _, ok := m[currentDir]; !ok {
				m[currentDir] = make(map[string]io.Reader)
			}
			readCloser := bytes.NewReader(file)
			m[currentDir][header.Name] = readCloser
		}
	}

	return m, nil
}

var _ db.FileProvider = (*Provider)(nil)

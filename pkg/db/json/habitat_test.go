//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package json_test

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/spf13/viper"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/file"
	dbJson "gitlab.com/kori-irrlicht/project-open-monster/pkg/db/json"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/habitat"
)

func TestLoadHabitat(t *testing.T) {
	basepath := "../../../assets/data/"
	viper.SetDefault("data.dir.habitat", basepath)
	Convey("Given a new HabitatLoader", t, func() {
		provider := file.Provider{}
		reader, _ := provider.ProvideFiles(basepath)

		ml := dbJson.NewHabitatLoader(reader["habitat"])
		Convey("Trying to load a valid file", func() {
			f, _ := ioutil.ReadFile(fmt.Sprintf("%s/habitat/testing.json", basepath))
			habitats, err := ml.LoadHabitat(bytes.NewReader(f))
			Convey("It should not return an error", func() {
				So(err, ShouldBeNil)
				So(habitats, ShouldNotBeNil)
			})
			Convey("The habitats should contain the 'testing' habitat", func() {
				hab, ok := habitats["testing"]
				So(ok, ShouldBeTrue)
				So(hab, ShouldNotBeNil)
				Convey("The list should contain the entry for a Hoarfox", func() {
					entry := habitat.Entry{
						Species:  "Hoarfox",
						MinLevel: 5,
						MaxLevel: 5,
						Chance:   5,
					}
					So(hab, ShouldContain, &entry)
				})
			})
		})
		Convey("Trying to load the habitat folder", func() {
			habitats, err := ml.Load()
			Convey("It should not return an error", func() {
				So(err, ShouldBeNil)
				So(habitats, ShouldNotBeEmpty)
			})

		})
	})
}

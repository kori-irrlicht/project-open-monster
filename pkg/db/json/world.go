//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package json

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"strconv"
	"strings"

	"github.com/Noofbiz/tmx"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type WorldLoader struct {
	worldFiles map[string]io.Reader
}

func NewWorldLoader(worldFiles map[string]io.Reader) *WorldLoader {
	return &WorldLoader{
		worldFiles: worldFiles,
	}
}

// Load loads every file in the basepath folder and tries to convert it into a world
// The basepath is read from the 'data.dir.world' field in the configuration.
//
// Failed files will be listed in the returned error object
// The returned map contains every species, which could be parsed
func (ml *WorldLoader) Load() (map[string]*tmx.Map, error) {
	logger := logrus.WithField("method", "WorldLoader.Load")
	failedSpecErr := NewFailedToLoadFileError()

	maps := make(map[string]*tmx.Map)

	offsets := make(map[string]model.Vector2D)

	for k, f := range ml.worldFiles {
		if k[len(k)-4:] != ".tsx" {
			continue
		}
		logger.WithField("file", k).Infoln("Loading tileset")
		file, err := ioutil.ReadAll(f)
		if err != nil {
			//failedSpecErr.files[f] = err
		} else {
			path := strings.Split(k, "/")
			fileName := path[len(path)-1]
			tmx.PreloadedTilesets[fileName] = file

		}
	}

	for k, f := range ml.worldFiles {
		if k[len(k)-6:] != ".world" {
			continue
		}
		logger.WithField("file", k).Infoln("Loading world")
		world, err := ml.LoadWorld(f)
		if err != nil {
			//failedSpecErr.files[f] = err
		} else {
			for _, m := range world.Maps {
				offsets[m.FileName] = model.Vector2D{X: float32(m.X), Y: float32(m.Y)}
			}
		}
	}

	for k, f := range ml.worldFiles {
		if k[len(k)-4:] != ".tmx" {
			continue
		}
		logger.WithField("file", k).Infoln("Loading map")
		m, err := tmx.Parse(f)
		if err != nil {
			//failedSpecErr.files[f] = err
		} else {
			path := strings.Split(k, "/")
			fileName := path[len(path)-1]
			m.Properties = append(m.Properties, tmx.Property{
				Name:  "offsetX",
				Type:  "int",
				Value: strconv.Itoa(int(offsets[fileName].X)),
			})
			m.Properties = append(m.Properties, tmx.Property{
				Name:  "offsetY",
				Type:  "int",
				Value: strconv.Itoa(int(offsets[fileName].Y)),
			})
			maps[k] = &m

		}
	}

	if len(failedSpecErr.Files) == 0 {
		return maps, nil
	}

	return maps, errors.Wrap(failedSpecErr, "failed to load all worlds")
}

// LoadWorld loads a worldbase from the given file in the basepath folder.
// The 'filename' parameter sould be relative to the basepath.
// The basepath is read from the 'data.dir.world' field in the configuration.
func (ml *WorldLoader) LoadWorld(reader io.Reader) (*tmxWorld, error) {
	logrus.Infoln("Trying to load world")
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, errors.Wrap(err, "Could not read file")
	}
	var tw tmxWorld
	err = json.Unmarshal(data, &tw)
	if err != nil {
		return nil, errors.Wrap(err, "Could not parse json")
	}
	return &tw, nil
}

type tmxWorld struct {
	Maps                 []tmxMap `json:"maps"`
	OnlyShowAdjacentMaps bool     `json:"onlyShowAdjacentMaps"`
	Type                 string   `json:"type"`
}

type tmxMap struct {
	FileName string `json:"fileName"`
	Height   int    `json:"height"`
	Width    int    `json:"width"`
	X        int    `json:"x"`
	Y        int    `json:"y"`
}

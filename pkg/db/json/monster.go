//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package json

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type FailedToLoadFileError struct {
	Files map[string]error
}

func NewFailedToLoadFileError() FailedToLoadFileError {
	return FailedToLoadFileError{
		Files: make(map[string]error),
	}

}

func (e FailedToLoadFileError) Error() string {
	return fmt.Sprintf("The following files could not be parsed: %v", e.Files)
}

type SpeciesLoader struct {
	speciesFiles map[string]io.Reader
}

func NewSpeciesLoader(speciesFiles map[string]io.Reader) db.SpeciesLoader {
	return &SpeciesLoader{
		speciesFiles: speciesFiles,
	}
}

// Load loads every file in the basepath folder and tries to convert it into a monster species
// The basepath is read from the 'data.dir.monster' field in the configuration.
//
// Failed files will be listed in the returned error object
// The returned map contains every species, which could be parsed
func (ml *SpeciesLoader) Load() (map[string]*model.Species, error) {

	failedSpecErr := NewFailedToLoadFileError()
	species := make(map[string]*model.Species)

	for _, f := range ml.speciesFiles {
		spec, err := ml.LoadSpecies(f)
		if err != nil {
			//failedSpecErr.files[f] = err
		} else {
			species[spec.Name] = spec
		}
	}

	if len(failedSpecErr.Files) == 0 {
		return species, nil
	}

	return species, errors.Wrap(failedSpecErr, "failed to load all species")
}

// LoadSpecies loads a monster species from the given file in the basepath folder.
// The 'filename' parameter sould be relative to the basepath.
// The basepath is read from the 'data.dir.monster' field in the configuration.
func (ml *SpeciesLoader) LoadSpecies(reader io.Reader) (*model.Species, error) {
	logrus.Infoln("Trying to load monster")
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, errors.Wrap(err, "Could not read file")
	}
	var species model.Species
	err = json.Unmarshal(data, &species)
	if err != nil {
		return nil, errors.Wrap(err, "Could not parse json")
	}
	return &species, nil
}

var _ db.SpeciesLoader = &SpeciesLoader{}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package json

import (
	"encoding/json"
	"io"
	"io/ioutil"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type MoveLoader struct {
	moveFiles map[string]io.Reader
}

func NewMoveLoader(moveFiles map[string]io.Reader) db.MoveLoader {
	return &MoveLoader{
		moveFiles: moveFiles,
	}
}

// Load loads every file in the basepath folder and tries to convert it into a move
// The basepath is read from the 'data.dir.move' field in the configuration.
//
// Failed files will be listed in the returned error object
// The returned map contains every species, which could be parsed
func (ml *MoveLoader) Load() (map[string]model.MoveBase, error) {
	failedSpecErr := NewFailedToLoadFileError()

	moves := make(map[string]model.MoveBase)

	for _, f := range ml.moveFiles {
		move, err := ml.LoadMove(f)
		if err != nil {
			//failedSpecErr.files[f] = err
		} else {
			moves[move.Name()] = move
		}
	}

	if len(failedSpecErr.Files) == 0 {
		return moves, nil
	}

	return moves, errors.Wrap(failedSpecErr, "failed to load all moves")
}

// LoadMove loads a movebase from the given file in the basepath folder.
// The 'filename' parameter sould be relative to the basepath.
// The basepath is read from the 'data.dir.move' field in the configuration.
func (ml *MoveLoader) LoadMove(reader io.Reader) (model.MoveBase, error) {
	logrus.Infoln("Trying to load move")
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, errors.Wrap(err, "Could not read file")
	}
	var movebase model.MoveBaseTemplate
	err = json.Unmarshal(data, &movebase)
	if err != nil {
		return nil, errors.Wrap(err, "Could not parse json")
	}
	return &movebase, nil
}

var _ db.MoveLoader = (*MoveLoader)(nil)

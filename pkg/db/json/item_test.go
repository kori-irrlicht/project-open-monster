//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package json_test

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/file"
	dbJson "gitlab.com/kori-irrlicht/project-open-monster/pkg/db/json"
)

func TestLoadItems(t *testing.T) {
	basepath := "../../../assets/data"
	Convey("Given a new ItemLoader", t, func() {
		provider := file.Provider{}
		reader, _ := provider.ProvideFiles(basepath)

		ml := dbJson.NewItemLoader(reader["items"]).(*dbJson.ItemLoader)
		Convey("Trying to load a valid file", func() {
			f, _ := ioutil.ReadFile(fmt.Sprintf("%s/items/potion.json", basepath))
			item, err := ml.LoadItem(bytes.NewReader(f))
			Convey("It should not return an error", func() {
				So(err, ShouldBeNil)
				So(item, ShouldNotBeNil)
				So(item.Name, ShouldEqual, "Potion")
			})
		})
		Convey("Trying to load the item folder", func() {
			species, err := ml.Load()
			Convey("It should not return an error", func() {
				So(err, ShouldBeNil)
				So(species, ShouldNotBeEmpty)
			})

		})
	})
}

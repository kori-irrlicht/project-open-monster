//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package json

import (
	"encoding/json"
	"io"
	"io/ioutil"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/item"
)

type ItemLoader struct {
	itemFiles map[string]io.Reader
}

func NewItemLoader(itemFiles map[string]io.Reader) db.ItemLoader {
	return &ItemLoader{
		itemFiles: itemFiles,
	}
}

// Load loads every file in the basepath folder and tries to convert it into an item
// The basepath is read from the 'data.dir.item' field in the configuration.
//
// Failed files will be listed in the returned error object
// The returned map contains every species, which could be parsed
func (ml *ItemLoader) Load() (map[string]*item.Base, error) {
	failedSpecErr := NewFailedToLoadFileError()

	items := make(map[string]*item.Base)

	for _, f := range ml.itemFiles {
		item, err := ml.LoadItem(f)
		if err != nil {
			//failedSpecErr.files[f] = err
			logrus.WithError(err).Errorln("Could not load item")
		} else {
			items[item.Name] = item
		}
	}

	if len(failedSpecErr.Files) == 0 {
		return items, nil
	}

	return items, errors.Wrap(failedSpecErr, "failed to load all items")
}

// LoadItem loads a itembase from the given file in the basepath folder.
// The 'filename' parameter sould be relative to the basepath.
// The basepath is read from the 'data.dir.item' field in the configuration.
func (ml *ItemLoader) LoadItem(reader io.Reader) (*item.Base, error) {
	logrus.Infoln("Trying to load item")
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, errors.Wrap(err, "Could not read file")
	}
	var itembase item.Base
	err = json.Unmarshal(data, &itembase)
	if err != nil {
		return nil, errors.Wrap(err, "Could not parse json")
	}
	return &itembase, nil
}

var _ db.ItemLoader = (*ItemLoader)(nil)

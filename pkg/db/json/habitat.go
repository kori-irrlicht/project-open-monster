//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package json

import (
	"encoding/json"
	"io"
	"io/ioutil"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/habitat"
)

type HabitatLoader struct {
	habitatFiles map[string]io.Reader
}

func NewHabitatLoader(habitatFiles map[string]io.Reader) *HabitatLoader {
	return &HabitatLoader{
		habitatFiles: habitatFiles,
	}
}

// Load loads every file in the basepath folder and tries to convert it into a monster habitat
// The basepath is read from the 'data.dir.monster' field in the configuration.
//
// Failed files will be listed in the returned error object
// The returned map contains every habitat, which could be parsed
func (ml *HabitatLoader) Load() (map[string][]*habitat.Entry, error) {

	failedSpecErr := NewFailedToLoadFileError()
	habitat := make(map[string][]*habitat.Entry)

	for _, f := range ml.habitatFiles {
		var err error
		habitat, err = ml.LoadHabitat(f)
		if err != nil {
			//failedSpecErr.files[f] = err
		}
	}

	if len(failedSpecErr.Files) == 0 {
		return habitat, nil
	}

	return habitat, errors.Wrap(failedSpecErr, "failed to load all habitat")
}

// LoadHabitat loads a monster habitat from the given file in the basepath folder.
// The 'filename' parameter sould be relative to the basepath.
// The basepath is read from the 'data.dir.monster' field in the configuration.
func (ml *HabitatLoader) LoadHabitat(reader io.Reader) (map[string][]*habitat.Entry, error) {
	logrus.Infoln("Trying to load habitat")
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, errors.Wrap(err, "Could not read file")
	}
	var habitat map[string][]*habitat.Entry
	err = json.Unmarshal(data, &habitat)
	if err != nil {
		return nil, errors.Wrap(err, "Could not parse json")
	}
	return habitat, nil
}

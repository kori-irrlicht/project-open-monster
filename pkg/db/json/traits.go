//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package json

import (
	"encoding/json"
	"io"
	"io/ioutil"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type TraitLoader struct {
	TraitFiles map[string]io.Reader
}

func NewTraitLoader(TraitFiles map[string]io.Reader) *TraitLoader {
	return &TraitLoader{
		TraitFiles: TraitFiles,
	}
}

// Load loads every file in the basepath folder and tries to convert it into an Trait
// The basepath is read from the 'data.dir.Trait' field in the configuration.
//
// Failed files will be listed in the returned error object
// The returned map contains every species, which could be parsed
func (ml *TraitLoader) Load() (map[string]*model.Trait, error) {
	failedSpecErr := NewFailedToLoadFileError()

	Traits := make(map[string]*model.Trait)

	for _, f := range ml.TraitFiles {
		Trait, err := ml.LoadTrait(f)
		if err != nil {
			//failedSpecErr.files[f] = err
			logrus.WithError(err).Errorln("Could not load Trait")
		} else {
			Traits[Trait.Name] = Trait
		}
	}

	if len(failedSpecErr.Files) == 0 {
		return Traits, nil
	}

	return Traits, errors.Wrap(failedSpecErr, "failed to load all Traits")
}

// LoadTrait loads a Traitbase from the given file in the basepath folder.
// The 'filename' parameter sould be relative to the basepath.
// The basepath is read from the 'data.dir.Trait' field in the configuration.
func (ml *TraitLoader) LoadTrait(reader io.Reader) (*model.Trait, error) {
	logrus.Infoln("Trying to load Trait")
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, errors.Wrap(err, "Could not read file")
	}
	var Traitbase model.Trait
	err = json.Unmarshal(data, &Traitbase)
	if err != nil {
		return nil, errors.Wrap(err, "Could not parse json")
	}
	return &Traitbase, nil
}

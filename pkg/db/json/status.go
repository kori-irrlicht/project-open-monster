//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package json

import (
	"encoding/json"
	"io"
	"io/ioutil"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/status"
)

type StatusLoader struct {
	statusFiles map[string]io.Reader
}

func NewStatusLoader(statusFiles map[string]io.Reader) db.StatusLoader {
	return &StatusLoader{
		statusFiles: statusFiles,
	}
}

// Load loads every file in the basepath folder and tries to convert it into an status
// The basepath is read from the 'data.dir.status' field in the configuration.
//
// Failed files will be listed in the returned error object
// The returned map contains every species, which could be parsed
func (ml *StatusLoader) Load() (map[string]*status.Base, error) {
	failedSpecErr := NewFailedToLoadFileError()

	status := make(map[string]*status.Base)

	for _, f := range ml.statusFiles {
		stat, err := ml.LoadStatus(f)
		if err != nil {
			//failedSpecErr.files[f] = err
			logrus.WithError(err).Errorln("Could not load status")
		} else {
			status[stat.Name] = stat
		}
	}

	if len(failedSpecErr.Files) == 0 {
		return status, nil
	}

	return status, errors.Wrap(failedSpecErr, "failed to load all status")
}

// LoadStatus loads a statusbase from the given file in the basepath folder.
// The 'filename' parameter sould be relative to the basepath.
// The basepath is read from the 'data.dir.status' field in the configuration.
func (ml *StatusLoader) LoadStatus(reader io.Reader) (*status.Base, error) {
	logrus.Infoln("Trying to load status")
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, errors.Wrap(err, "Could not read file")
	}
	var statusbase status.Base
	err = json.Unmarshal(data, &statusbase)
	if err != nil {
		return nil, errors.Wrap(err, "Could not parse json")
	}
	return &statusbase, nil
}

var _ db.StatusLoader = (*StatusLoader)(nil)

PROJECT_NAME := "project-open-monster"
PKG := "gitlab.com/kori-irrlicht/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
BUILD_TAGS := noaudio,nogl,headless

.PHONY: all dep build clean test coverage coverhtml lint generate

all: build

lint: ## Lint the files
	@golangci-lint run ./...

test: ## Run unittests
	@go test -tags ${BUILD_TAGS} -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -tags ${BUILD_TAGS} -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -tags ${BUILD_TAGS} -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	@go test -tags ${BUILD_TAGS} -short -coverprofile coverage.out ${PKG_LIST}

coverhtml: coverage ## Generate global code coverage report in HTML
	@go tool cover -html=coverage.out -o coverage.html

dep: ## Get the dependencies
	@go get -v -d ./...

build: dep ## Build the binary file
	@go build -tags ${BUILD_TAGS} -i -v $(PKG)

generate:
	@project-openmonster-message-generator -i api -l go -o internal/server/messages

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

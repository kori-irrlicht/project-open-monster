
# Prerequisites
go version go1.13.7

# Download
`go get gitlab.com/kori-irrlicht/project-open-monster/`

# Build
Enter the download location and run
`go build`

# Run
`./project-open-monster startServer`
or
`./project-open-monster startServer -p <port>`
//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package server

import (
	"context"
	"encoding/json"
	"io"

	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/battle"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/logic"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/server/messages"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/util"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/habitat"
	luar "layeh.com/gopher-luar"
)

func handleOverworldMove(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	var msg messages.OverworldMove
	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := msg.Validate(); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	session.OverworldInput <- overworld.MoveCommand{
		Direction: model.Vector2D{
			X: float32(msg.Direction.X),
			Y: float32(msg.Direction.Y),
		},
	}

	return nil
}

func handleOverworldInput(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	var msg messages.OverworldInput
	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := msg.Validate(); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	if session.BreederID != model.ID(msg.BreederID) {
		return sendError(message, writer, unauthorized)
	}

	session.OverworldInput <- overworld.MoveCommand{
		Direction: model.Vector2D{
			X: float32(msg.Direction.X),
			Y: float32(msg.Direction.Y),
		},
	}

	session.OverworldInput <- overworld.InteractionCommand{
		Interact: msg.Interaction,
	}

	return nil
}

func owHandleBattleUpdate(context context.Context, conns util.Connections, d overworld.BattleUpdate, breeder model.Breeder) {
	challenged := model.Breeder{
		ID:   0,
		Name: "WildBattle",
	}

	part := battle.Participants{[]model.Breeder{breeder}, []model.Breeder{challenged}}

	team := []model.Monster{*habitat.CreateRandomMonsterFromHabitat(d.Area)}

	if err := createBattle(context, conns, part, team); err != nil {
		logrus.WithError(err).Errorln("could not create battle")
		return
	}

}

func owHandleVelocityUpdateBatch(d overworld.VelocityUpdateBatch, writer io.Writer) {
	m := messages.OverworldUpdatePositions{
		Updates: []messages.OverworldPositionUpdate{},
	}
	for _, v := range d {
		m.Updates = append(m.Updates, messages.OverworldPositionUpdate{
			BreederId: int(v.ID),
			Position: messages.Vector2D{
				X: float64(v.Position.X),
				Y: float64(v.Position.Y),
			},
			Velocity: messages.Vector2D{
				X: float64(v.Velocity.X),
				Y: float64(v.Velocity.Y),
			},
		})
	}
	if err := sendMessage(messages.Type_OverworldUpdatePositions, m, writer, 0); err != nil {
		logrus.WithError(err).Errorln("could not send message")
		return
	}

}

func owHandleWorldUpdate(d overworld.WorldUpdate, writer io.Writer) {
	m := messages.OverworldUpdateWorldPositions{}

	for _, coord := range d.Coordinates {
		m.Updates = append(m.Updates, messages.OverworldWorldUpdate{
			World:  coord.World,
			Area:   messages.Vector2D{X: float64(coord.Area.X), Y: float64(coord.Area.Y)},
			Tiles:  coord.Tiles,
			Offset: messages.Vector2D{X: float64(coord.Offset.X), Y: float64(coord.Offset.Y)},
			Size:   messages.Vector2D{X: float64(coord.Size.X), Y: float64(coord.Size.Y)},
		})
	}
	if err := sendMessage(messages.Type_OverworldUpdateWorldPositions, m, writer, 0); err != nil {
		logrus.WithError(err).Errorln("could not send message")
		return
	}

}

func owHandleDisconnectUpdate(d overworld.DisconnectUpdate, writer io.Writer) {
	m := messages.OverworldRemove{
		WorldID: int(d.ID),
	}
	if err := sendMessage(messages.Type_OverworldRemove, m, writer, 0); err != nil {
		logrus.WithError(err).Errorln("could not send message")
		return
	}

}

func connectToOverworld(context context.Context, conns util.Connections, writer io.Writer, breeder model.Breeder, session *Session) (uint64, error) {

	go func() {
		l := logic.PrepareLua(conns)
		l.SetGlobal("notification", luar.New(l, &NotificationAPI{}))
		l.SetGlobal("breeder", luar.New(l, breeder))
		defer l.Close()

		for c := range session.OverworldOutput {
			switch d := c.(type) {
			case overworld.VelocityUpdateBatch:
				owHandleVelocityUpdateBatch(d, writer)
			case overworld.BattleUpdate:
				owHandleBattleUpdate(context, conns, d, breeder)
			case overworld.WorldUpdate:
				owHandleWorldUpdate(d, writer)

			case overworld.DisconnectUpdate:
				owHandleDisconnectUpdate(d, writer)

			case overworld.EventUpdate:
				if err := l.DoString(d.Lua); err != nil {
					logrus.WithError(err).Errorln("error while executing lua script")
				}
			}
		}
	}()

	return conns.Overworld.AddPlayer(breeder.ID, session.OverworldInput, session.OverworldOutput)

}

type NotificationAPI struct{}

func (n *NotificationAPI) SendDialog(breeder model.ID, dialog string) {
	writer := connectionPool[breeder]
	m := messages.OverworldUpdateDialogs{
		Dialog: dialog,
	}
	if err := sendMessage(messages.Type_OverworldUpdateDialogs, m, writer, 0); err != nil {
		logrus.WithError(err).Errorln("could not send message")
		return
	}
}

func (n *NotificationAPI) ReceiveItem(breeder model.ID, item string, amount int) {
	writer := connectionPool[breeder]

	m := messages.ReceiveItem{
		Item:   item,
		Amount: amount,
	}
	if err := sendMessage(messages.Type_ReceiveItem, m, writer, 0); err != nil {
		logrus.WithError(err).Errorln("could not send message")
		return
	}
}

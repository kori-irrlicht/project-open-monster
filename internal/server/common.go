//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package server

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"regexp"

	"github.com/spf13/viper"
	luar "layeh.com/gopher-luar"

	"gitlab.com/kori-irrlicht/project-open-monster/internal/logic"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/server/messages"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/util"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

var connectionPool = make(map[model.ID]io.Writer)

func handleLogin(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	var msg messages.Login
	data := message.Data
	if err := json.Unmarshal(data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}
	if msg.BreederID == 0 {
		return sendError(message, writer, invalidData)
	}

	breederDB := conns.BreederDB
	breederID := model.ID(msg.BreederID)
	breeder, err := breederDB.Load(breederID)
	if err != nil {
		if errors.Is(err, db.NotFoundError{}) {
			return sendError(message, writer, notFound)
		} else {
			handleError(message, writer, serverError)
			return fmt.Errorf("could not load breeder: %w", err)
		}
	}

	session.BreederID = breederID
	sessions.ById[breederID] = session

	logResp := messages.LoginResponse{
		Breeder: messages.BreederView{
			Id:   int(breeder.ID),
			Name: breeder.Name,
		},
	}

	overworldID, err := connectToOverworld(context, conns, writer, breeder, session)
	logResp.WorldId = int(overworldID)

	if err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not add player to the overworld: %w", err)
	}

	if err := sendMessage(messages.Type_LoginResponse, logResp, writer, message.ID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not send message: %w", err)
	}

	connectionPool[breederID] = writer
	return nil
}

// Valid names start with a non-whitespace character
// followed by more non-whitespace characters. These can be separated by whitespaces.
// Names are not allowed to start or end with a whitespace
var nameRegex = regexp.MustCompile(`(?m)^\S+( \S+)*$`)

// handleCreateBreeder creates a new breeder.
//
// Matches the MessageHandler type.
// Does not require an active session.
func handleCreateBreeder(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	//l := logrus.WithField("msgType", "createBreeder")
	var msg messages.CreateBreeder
	data := message.Data

	if err := json.Unmarshal(data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if !nameRegex.MatchString(msg.Name) {
		return sendError(message, writer, invalidData)
	}

	breeder := model.Breeder{
		Name: msg.Name,
	}

	starterSpecies := viper.GetStringSlice("game.starter")
	ok := false
	for _, species := range starterSpecies {
		ok = ok || species == msg.Species
	}
	if !ok {
		return sendError(message, writer, badRequest)
	}

	spec, ok := model.SpeciesCache[msg.Species]
	if !ok {
		return sendError(message, writer, notFound)
	}

	breederDB := conns.BreederDB
	monsterDB := conns.MonsterDB
	breederCreator := logic.NewBreederCreator(breederDB, monsterDB)

	breeder, err := breederCreator.CreateBreeder(breeder, spec)

	if err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not create breeder: %w", err)
	}

	createResp := messages.CreateBreederResponse{
		Breeder: messages.BreederView{
			Id:   int(breeder.ID),
			Name: breeder.Name,
		},
	}
	if err = sendMessage(messages.Type_CreateBreederResponse, createResp, writer, message.ID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not send message: %w", err)
	}

	return nil
}

func handleRequestTeam(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	var msg messages.RequestTeam

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	id := session.BreederID
	breederID := model.ID(msg.BreederID)
	if breederID != id {
		return sendError(message, writer, unauthorized)
	}
	monsterDB := conns.MonsterDB
	team, err := monsterDB.LoadTeamByBreederId(breederID)
	if err != nil {

		handleError(message, writer, serverError)
		return fmt.Errorf("could not load team: %w", err)
	}

	if err := sendMessage(messages.Type_RequestTeamResponse, team, writer, message.ID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not send message: %w", err)
	}

	return nil
}

func handleRequestStarter(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	var msg messages.RequestStarter

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	// TODO: Read starter from other config file
	starterSpecies := viper.GetStringSlice("game.starter")
	if err := sendMessage(messages.Type_RequestStarterResponse, messages.RequestStarterResponse{Species: starterSpecies}, writer, message.ID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not send message: %w", err)
	}

	return nil
}

func handleRequestMonsterInfo(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	var msg messages.RequestMonsterInfo

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}
	monsterDB := conns.MonsterDB
	monsterInfo := make([]messages.DetailedMonsterInfo, 0)
	for _, v := range msg.MonsterIDs {
		monster, err := monsterDB.Load(model.ID(v))
		if err != nil {
			continue
		}

		if monster.OwnerID == session.BreederID {
			info := &messages.DetailedMonsterInfo{
				Species:        monster.Species.Name,
				Level:          int(monster.Level),
				Name:           monster.Nickname,
				Status:         []string{},
				Id:             int(monster.ID),
				CurrentHP:      monster.CurrentHP,
				CurrentStamina: monster.CurrentStamina,
				Moves:          []string{},
			}
			for _, v := range monster.Status {
				info.Status = append(info.Status, v.Base.Name)
			}
			for _, move := range monster.Moves {
				if move.Base() == model.NilMoveBase {
					continue
				}
				info.Moves = append(info.Moves, move.Base().Name())
			}
			monsterInfo = append(monsterInfo, *info)
		}
	}
	if err := sendMessage(messages.Type_RequestMonsterInfoResponse, messages.RequestMonsterInfoResponse{MonsterInfo: monsterInfo}, writer, message.ID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not send message: %w", err)
	}
	return nil
}

func handleRequestInventory(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	var msg messages.RequestInventory

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	id := session.BreederID
	breederID := model.ID(msg.BreederID)
	if breederID != id {
		return sendError(message, writer, unauthorized)
	}

	inv, err := conns.ItemDB.Inventory(id)
	if err != nil {

		handleError(message, writer, serverError)
		return fmt.Errorf("could not load inventory: %w", err)
	}

	var items []messages.InventoryItem
	for k, v := range inv {
		items = append(items, messages.InventoryItem{Name: k, Amount: v})
	}

	if err := sendMessage(messages.Type_RequestInventoryResponse, messages.RequestInventoryResponse{Items: items}, writer, message.ID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not send message: %w", err)
	}
	return nil
}

func handleUseItem(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	var msg messages.UseItem

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	inv, err := conns.ItemDB.Inventory(session.BreederID)
	if err != nil {

		handleError(message, writer, serverError)
		return fmt.Errorf("could not load inventory: %w", err)
	}

	if amount, ok := inv[msg.Item]; !ok || amount == 0 {
		return sendError(message, writer, notFound)
	}

	mon, err := conns.MonsterDB.Load(model.ID(msg.Target))
	if err != nil {
		if errors.Is(err, db.NotFoundError{}) {
			return sendError(message, writer, notFound)
		}
		handleError(message, writer, serverError)
		return fmt.Errorf("could not load monster: %w", err)
	}

	if mon.OwnerID != session.BreederID {
		return sendError(message, writer, notAuthorized)
	}

	l := logic.PrepareLua(conns)
	defer l.Close()

	item := model.ItemCache[msg.Item]

	breeder, err := conns.BreederDB.Load(session.BreederID)
	if err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not load breeder: %w", err)
	}

	l.SetGlobal("breeder", luar.New(l, breeder))
	l.SetGlobal("targetID", luar.New(l, msg.Target))

	err = l.DoString(item.Effect.Overworld)
	if err != nil {

		handleError(message, writer, serverError)
		return fmt.Errorf("could not use item: %w", err)
	}

	// TODO: What happens, if the item fails to be removed?
	if err := conns.ItemDB.Remove(session.BreederID, msg.Item, 1); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not remove item: %w", err)
	}

	return nil
}

// sendMessage tries to send the data with the given messageType to the client.
// It takes care of converting the data into JSON and creating the message to send it to the client.
//
// responseTo is the ID of a previous message, if the new message is a response to the first message.
// If it is no response, responseTo should be 0.
//
// Returns an error if the data could not be converted into JSON or the message could not be send.
func sendMessage(msgType string, data interface{}, writer io.Writer, responseTo int) error {

	dataJson, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("could not marshal data: %w", err)
	}
	msg := Message{
		Type:       msgType,
		ID:         0,
		ResponseID: responseTo,
		Data:       json.RawMessage(dataJson),
	}

	result, err := json.Marshal(&msg)
	if err != nil {
		return fmt.Errorf("could not marshal message: %w", err)
	}

	if _, err := writer.Write(result); err != nil {
		return fmt.Errorf("could not write message: %w", err)
	}
	return nil
}

var messageHandlerMap = map[string]MessageHandler{
	// Login
	messages.Type_Login:              handleLogin,
	messages.Type_CreateBreeder:      handleCreateBreeder,
	messages.Type_RequestTeam:        Authenticated(handleRequestTeam),
	messages.Type_RequestStarter:     handleRequestStarter,
	messages.Type_RequestMonsterInfo: Authenticated(handleRequestMonsterInfo),
	messages.Type_RequestInventory:   Authenticated(handleRequestInventory),

	messages.Type_BattleSendMonster:   Authenticated(handleSendMonster),
	messages.Type_BattleAttack:        Authenticated(handleAttack),
	messages.Type_BattleCapture:       Authenticated(handleCapture),
	messages.Type_BattleItem:          Authenticated(handleItem),
	messages.Type_BattleRequest:       Authenticated(handleBattleRequest),
	messages.Type_BattleRequestAccept: Authenticated(handleBattleRequestAccept),
	messages.Type_BattleRequestCancel: Authenticated(handleBattleRequestCancel),
	messages.Type_BattleSurrender:     Authenticated(handleBattleSurrender),

	messages.Type_OverworldMove:  Authenticated(handleOverworldMove),
	messages.Type_OverworldInput: Authenticated(handleOverworldInput),

	messages.Type_UseItem: Authenticated(handleUseItem),

	messages.Type_RequestWildBattle: Authenticated(handleRequestWildBattle),
}

func Authenticated(handler MessageHandler) MessageHandler {
	return func(ctx context.Context, conns util.Connections, msg Message, writer io.Writer, session *Session) error {

		if !isAuthenticated(*session) {
			return sendError(msg, writer, notAuthenticated)
		}

		return handler(ctx, conns, msg, writer, session)
	}
}

func isAuthenticated(s Session) bool {
	return s.BreederID > 0
}

// handleMessage converts the message into the approprioate message struct and calls the corresponding handler method
func handleMessage(context context.Context, conns util.Connections, msg []byte, conn io.Writer, session *Session) error {
	var message Message
	if err := json.Unmarshal(msg, &message); err != nil {
		return sendError(message, conn, badRequest)
	}

	msgType := message.Type
	if handler, ok := messageHandlerMap[msgType]; !ok {
		return sendError(message, conn, badRequest)
	} else {
		return handler(context, conns, message, conn, session)
	}

}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package server

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"strings"
	"testing"

	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/spf13/viper"

	"gitlab.com/kori-irrlicht/project-open-monster/internal/data"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/logic"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/server/messages"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/util"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"

	. "gitlab.com/kori-irrlicht/goconvey-assertions"
)

func init() {
	logrus.SetLevel(logrus.WarnLevel)
	basepath_monster := "../../assets/data/monster"
	basepath_moves := "../../assets/data/moves"
	viper.SetDefault("data.dir.monster", basepath_monster)
	viper.SetDefault("data.dir.move", basepath_moves)
	viper.Set("data.archive.location", "../../assets/data")
	viper.Set("data.archive.type", "file")

	err := data.Load("../../assets/data", "file")

	if err != nil {
		panic(err)
	}

}

func TestLogin(t *testing.T) {
	Convey("Given a session and a connection", t, func() {
		ctx := context.TODO()
		session := &Session{}
		prepareSession(session)
		conn := NewTestWriter()
		conns := prepareConnections()
		breederDB := conns.BreederDB
		breeder := model.Breeder{
			Name: "test",
		}
		id, _ := breederDB.Create(breeder)
		breeder.ID = id
		Convey("Sending invalid JSON", func() {
			msg := Message{
				ID:   1,
				Data: []byte("{:}"),
			}
			err := messageHandlerMap[messages.Type_Login](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should not be logged in", func() {
				So(session.BreederID, ShouldEqual, 0)
			})
			Convey("The user should be notified", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)

			})
		})
		Convey("Leaving out the data", func() {
			msg := Message{
				ID:   1,
				Data: []byte("{}"),
			}
			err := messageHandlerMap[messages.Type_Login](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should not be logged in", func() {
				So(session.BreederID, ShouldEqual, 0)
			})
			Convey("The user should be notified", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})
		Convey("Sending valid data", func() {
			msg := Message{
				ID:   1,
				Data: []byte(fmt.Sprintf(`{"breederID": %d}`, id)),
			}
			err := messageHandlerMap[messages.Type_Login](ctx, conns, msg, conn, session)
			Convey("There should be no error and the user should be logged in", func() {
				So(err, ShouldBeNil)
				So(session.BreederID, ShouldEqual, id)
			})
			Convey("The user should be notified", func() {
				resMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":{"breeder":{"id":%d,"name":"%s"},"worldId":5}}`, messages.Type_LoginResponse, msg.ID, breeder.ID, breeder.Name)
				So(string(conn.data), ShouldResembleJSON, resMsg)

			})
			Convey("the user should be in the connection pool", func() {
				resConn, ok := connectionPool[id]
				So(ok, ShouldBeTrue)
				So(resConn, ShouldEqual, conn)
			})
		})
		Convey("The id does not exist", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1000}`),
			}
			err := messageHandlerMap[messages.Type_Login](ctx, conns, msg, conn, session)
			Convey("There should be no error and the user should not be logged in", func() {
				So(err, ShouldBeNil)
				So(session.BreederID, ShouldEqual, 0)
			})
			Convey("The user should be notified", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, notFound)
				So(string(conn.data), ShouldResembleJSON, errMsg)

			})
			Convey("the user should not be in the connection pool", func() {
				_, ok := connectionPool[1000]
				So(ok, ShouldBeFalse)
			})
		})
	})
}

func TestCreateBreeder(t *testing.T) {
	Convey("Given a session, a connection and a breeder database", t, func() {
		ctx := context.TODO()
		basepath := "../../assets/data/monster"
		viper.SetDefault("data.dir.monster", basepath)
		viper.Set("game.starter", []string{"Hoarfox", "Owlix"})
		session := &Session{}
		conn := NewTestWriter()
		conns := prepareConnections()
		breederDB := conns.BreederDB
		monsterDB := conns.MonsterDB
		Convey("Sending invalid JSON", func() {
			msg := Message{
				ID:   1,
				Data: []byte("{:}"),
			}
			err := messageHandlerMap[messages.Type_CreateBreeder](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The breeder should not be created", func() {
				trList, _ := breederDB.LoadAll()
				So(trList, ShouldBeEmpty)
			})
			Convey("The user should be receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})
		Convey("Sending invalid names", func() {
			data := map[string][]byte{
				"No name":                              []byte(`{"gender":0}`),
				"Only whitespaces":                     []byte(`{"name": "    ", "gender":0}`),
				"Starting with a whitespace":           []byte(`{"name": " name", "gender":0}`),
				"Ending with a whitespace":             []byte(`{"name": "name ", "gender":0}`),
				"Two whitespaces following each other": []byte(`{"name": "na  me", "gender":0}`),
			}
			for k, v := range data {

				Convey("Sending a request ("+k+") should fail", func() {
					msg := Message{
						ID:   1,
						Data: v,
					}

					// No separate Convey, because of
					// https://github.com/smartystreets/goconvey/issues/248
					err := messageHandlerMap[messages.Type_CreateBreeder](ctx, conns, msg, conn, session)
					So(err, ShouldBeNil)

					trList, _ := breederDB.LoadAll()
					So(trList, ShouldBeEmpty)

					errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, invalidData)
					So(string(conn.data), ShouldResembleJSON, errMsg)
				})
			}

		})
		Convey("Sending a valid name and requesting a Hoarfox", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"name": "test", "species": "Hoarfox"}`),
			}
			err := messageHandlerMap[messages.Type_CreateBreeder](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The breeder should be created", func() {
				breeder, err := breederDB.Load(1)
				So(err, ShouldBeNil)
				So(breeder.Name, ShouldEqual, "test")
				trLi, _ := breederDB.LoadAll()
				So(trLi, ShouldHaveLength, 1)
			})
			Convey("The user should receive the id", func() {
				breeder, _ := breederDB.Load(1)
				resMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":{"breeder":{"id":%d,"name":"%s"}}}`, messages.Type_CreateBreederResponse, msg.ID, breeder.ID, breeder.Name)
				So(string(conn.data), ShouldResembleJSON, resMsg)
			})
			Convey("The breeder should only have a Hoarfox", func() {
				team, _ := monsterDB.LoadTeamByBreederId(1)
				So(team, ShouldHaveLength, 1)
				So(team[0].Species.Name, ShouldEqual, "Hoarfox")
				Convey("The monster should start at level 5", func() {
					So(team[0].Level, ShouldEqual, 5)
				})
			})

		})
		Convey("Sending a valid name and requesting an Owlix", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"name": "test", "species": "Owlix"}`),
			}
			err := messageHandlerMap[messages.Type_CreateBreeder](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The breeder should be created", func() {
				breeder, err := breederDB.Load(1)
				So(err, ShouldBeNil)
				So(breeder.Name, ShouldEqual, "test")
				trLi, _ := breederDB.LoadAll()
				So(trLi, ShouldHaveLength, 1)
			})
			Convey("The user should receive the id", func() {
				breeder, _ := breederDB.Load(1)
				resMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":{"breeder":{"id":%d,"name":"%s"}}}`, messages.Type_CreateBreederResponse, msg.ID, breeder.ID, breeder.Name)
				So(string(conn.data), ShouldResembleJSON, resMsg)
			})
			Convey("The breeder should only have an Owlix", func() {
				team, _ := monsterDB.LoadTeamByBreederId(1)
				So(team, ShouldHaveLength, 1)
				So(team[0].Species.Name, ShouldEqual, "Owlix")
				Convey("The monster should start at level 5", func() {
					So(team[0].Level, ShouldEqual, 5)
				})
			})

		})
		Convey("Sending a valid name and requesting an Flamurus", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"name": "test", "species": "Flamurus"}`),
			}
			err := messageHandlerMap[messages.Type_CreateBreeder](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The breeder should not be created", func() {
				trList, _ := breederDB.LoadAll()
				So(trList, ShouldBeEmpty)
			})
			Convey("The user should be receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, badRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})

	})
}

func TestRequestTeam(t *testing.T) {
	Convey("Given an environment with a breeder and a team", t, func() {
		ctx := context.TODO()
		basepath := "../../assets/data/monster"
		viper.SetDefault("data.dir.monster", basepath)
		session := &Session{}
		conn := NewTestWriter()
		conns := prepareConnections()
		breederDB := conns.BreederDB
		monsterDB := conns.MonsterDB
		breeder := model.Breeder{
			Name: "test",
		}
		id, _ := breederDB.Create(breeder)
		breeder.ID = id

		spec := model.SpeciesCache["Hoarfox"]
		monster := *model.CreateMonster(spec, func() (model.Level, model.Level) { return 10, 10 }, func() (model.AttributeStat, model.AttributeStat) { return 0, 0 })
		id, _ = monsterDB.Create(monster)
		monster.ID = id

		monsterDB.SaveTeamByBreederId(breeder.ID, []model.Monster{monster})

		// "Login"
		session.BreederID = breeder.ID

		Convey("If the user is not logged in", func() {
			session.BreederID = 0
			msg := Message{
				ID:   1,
				Data: []byte("{:}"),
			}
			err := messageHandlerMap[messages.Type_RequestTeam](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should be receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, notAuthenticated)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})

		Convey("Sending invalid JSON", func() {
			msg := Message{
				ID:   1,
				Data: []byte("{:}"),
			}
			err := messageHandlerMap[messages.Type_RequestTeam](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should be receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})

		Convey("Sending a message with a breeder ID", func() {

			Convey("If it is the ID of the current breeder", func() {
				msg := Message{
					ID:   1,
					Data: []byte(fmt.Sprintf(`{"breederID":%d}`, id)),
				}
				err := messageHandlerMap[messages.Type_RequestTeam](ctx, conns, msg, conn, session)

				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})

				Convey("The user should receive the team", func() {
					So(string(conn.data), ShouldContainSubstring, `"species":"Hoarfox"`)
					So(string(conn.data), ShouldContainSubstring, fmt.Sprintf(`"id":%d`, monster.ID))
				})

			})

			Convey("If it is not the ID of the current breeder", func() {
				msg := Message{
					ID:   1,
					Data: []byte(fmt.Sprintf(`{"breederID":%d}`, id+1)),
				}
				err := messageHandlerMap[messages.Type_RequestTeam](ctx, conns, msg, conn, session)

				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})

				Convey("The user should receive an error", func() {
					errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, unauthorized)
					So(string(conn.data), ShouldResembleJSON, errMsg)
				})

			})

		})

	})
}

func TestRequestStarter(t *testing.T) {
	Convey("Given an environment", t, func() {
		ctx := context.TODO()
		basepath := "../../assets/data/monster"
		viper.SetDefault("data.dir.monster", basepath)
		session := &Session{}
		conn := NewTestWriter()
		conns := prepareConnections()

		Convey("Sending invalid JSON", func() {
			msg := Message{
				ID:   1,
				Data: []byte("{:}"),
			}
			err := messageHandlerMap[messages.Type_RequestStarter](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should be receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})

		Convey("Sending a request", func() {
			msg := Message{
				ID:   1,
				Data: []byte("{}"),
			}
			err := messageHandlerMap[messages.Type_RequestStarter](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})

			starterSpecies := viper.GetStringSlice("game.starter")
			Convey("The user should receive the team", func() {
				var spec []string
				for _, s := range starterSpecies {
					spec = append(spec, fmt.Sprintf(`"%s"`, s))
				}
				resMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":{"species":[%s]}}`, messages.Type_RequestStarterResponse, msg.ID, strings.Join(spec, ","))
				So(string(conn.data), ShouldResembleJSON, resMsg)
			})

		})
	})
}

func TestRequestMonsterInfo(t *testing.T) {
	Convey("Given an environment with a breeder and a team", t, func() {
		ctx := context.TODO()
		basepath := "../../assets/data/monster"
		viper.SetDefault("data.dir.monster", basepath)
		session := &Session{}
		conn := NewTestWriter()
		conns := prepareConnections()
		breederDB := conns.BreederDB
		monsterDB := conns.MonsterDB
		bc := logic.NewBreederCreator(breederDB, monsterDB)
		breeder := model.Breeder{
			Name: "test",
		}

		spec := model.SpeciesCache["Hoarfox"]

		breeder, _ = bc.CreateBreeder(breeder, spec)
		team, _ := monsterDB.LoadTeamByBreederId(breeder.ID)
		monster := team[0]

		// "Login"
		session.BreederID = breeder.ID

		Convey("If the user is not logged in", func() {
			session.BreederID = 0
			msg := Message{
				ID:   1,
				Data: []byte("{:}"),
			}
			err := messageHandlerMap[messages.Type_RequestMonsterInfo](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should be receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, notAuthenticated)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})

		Convey("Sending invalid JSON", func() {
			msg := Message{
				ID:   1,
				Data: []byte("{:}"),
			}
			err := messageHandlerMap[messages.Type_RequestMonsterInfo](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should be receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})

		Convey("Checking your own monster", func() {
			msg := Message{
				ID:   1,
				Data: []byte(fmt.Sprintf(`{"monsterIds":[%d]}`, monster.ID)),
			}
			err := messageHandlerMap[messages.Type_RequestMonsterInfo](ctx, conns, msg, conn, session)

			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})

			Convey("The user should receive the monster info", func() {
				info := messages.DetailedMonsterInfo{
					Name:           monster.Nickname,
					Species:        monster.Species.Name,
					Level:          int(monster.Level),
					Status:         []string{},
					Id:             int(monster.ID),
					CurrentHP:      monster.CurrentHP,
					CurrentStamina: monster.CurrentStamina,
					Moves:          []string{},
				}
				for _, v := range monster.Status {
					info.Status = append(info.Status, v.Base.Name)
				}
				for _, v := range monster.Moves {
					if v.Base() == model.NilMoveBase {
						continue
					}
					info.Moves = append(info.Moves, v.Base().Name())
				}
				resp := messages.RequestMonsterInfoResponse{
					MonsterInfo: []messages.DetailedMonsterInfo{info},
				}
				j, _ := json.Marshal(resp)
				So(string(conn.data), ShouldResembleJSON, fmt.Sprintf(`{
					    "type": "%s",
					    "id": 0,
					    "responseID": %d,
					    "data":
%s
                                               }`, messages.Type_RequestMonsterInfoResponse, msg.ID, string(j)))

			})

		})

		Convey("Checking your the monster of another player", func() {

			msg := Message{
				ID:   1,
				Data: []byte(fmt.Sprintf(`{"monsterIds":[%d]}`, monster.ID)),
			}
			session.BreederID = session.BreederID + 1
			err := messageHandlerMap[messages.Type_RequestMonsterInfo](ctx, conns, msg, conn, session)

			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})

			Convey("The user should not receive the monster info", func() {
				resp := messages.RequestMonsterInfoResponse{
					MonsterInfo: []messages.DetailedMonsterInfo{},
				}
				j, _ := json.Marshal(resp)
				So(string(conn.data), ShouldResembleJSON, fmt.Sprintf(`{
					    "type": "%s",
					    "id": 0,
					    "responseID": %d,
					    "data":
%s
                                               }`, messages.Type_RequestMonsterInfoResponse, msg.ID, string(j)))
			})

		})

	})
}

func TestUseItem(t *testing.T) {
	Convey("Given an environment with a breeder and an inventory", t, func() {
		ctx := context.TODO()
		session := &Session{}
		conn := NewTestWriter()
		conns := prepareConnections()
		breederDB := conns.BreederDB
		monsterDB := conns.MonsterDB
		bc := logic.NewBreederCreator(breederDB, monsterDB)
		breeder := model.Breeder{
			Name: "test",
		}

		spec := model.SpeciesCache["Hoarfox"]

		breeder, _ = bc.CreateBreeder(breeder, spec)
		breeder2, _ := bc.CreateBreeder(model.Breeder{Name: "Foo"}, spec)

		// "Login"
		session.BreederID = breeder.ID

		Convey("If the user is not logged in", func() {
			session.BreederID = 0
			msg := Message{
				ID:   1,
				Data: []byte("{:}"),
			}
			err := messageHandlerMap[messages.Type_UseItem](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should be receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, notAuthenticated)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})

		Convey("Sending invalid JSON", func() {
			msg := Message{
				ID:   1,
				Data: []byte("{:}"),
			}
			err := messageHandlerMap[messages.Type_UseItem](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})

		Convey("The user does not have the item in their inventory", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"item": "Potion", "target": 1}`),
			}
			err := messageHandlerMap[messages.Type_UseItem](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, notFound)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})

		Convey("The user does not target their own monster", func() {
			conns.ItemDB.Add(session.BreederID, "Potion", 1)
			team, _ := conns.MonsterDB.LoadTeamByBreederId(breeder2.ID)
			msg := Message{
				ID:   1,
				Data: []byte(fmt.Sprintf(`{"item": "Potion", "target": %d}`, team[0].ID)),
			}
			err := messageHandlerMap[messages.Type_UseItem](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, notAuthorized)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})

		Convey("The user does target their own monster", func() {
			conns.ItemDB.Add(session.BreederID, "Potion", 1)
			team, _ := conns.MonsterDB.LoadTeamByBreederId(breeder.ID)
			msg := Message{
				ID:   1,
				Data: []byte(fmt.Sprintf(`{"item": "Potion", "target": %d}`, team[0].ID)),
			}
			err := messageHandlerMap[messages.Type_UseItem](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should not receive an error", func() {
				So(conn.data, ShouldHaveLength, 0)
			})

			Convey("After using it, the amount in the inventory is reduced by one", func() {
				inv, _ := conns.ItemDB.Inventory(breeder.ID)
				So(inv["Potion"], ShouldEqual, 0)
			})

		})

	})
}

func TestHandleMessage(t *testing.T) {
	Convey("Given a handlermap with entry 'test'", t, func() {
		ctx := context.TODO()
		session := &Session{}
		conn := NewTestWriter()
		var called bool
		var passedData []byte
		conns := prepareConnections()
		messageHandlerMap["test"] = func(_ context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
			called = true
			passedData = message.Data
			return nil
		}
		Convey("Sending a message of type 'test'", func() {
			handleMessage(ctx, conns, []byte(`{"type":"test","data":{"test":"test"},"id":1}`), conn, session)
			Convey("The function registered with 'test' should be called", func() {
				So(called, ShouldBeTrue)
			})
			Convey("The function should get the data as json", func() {
				So(string(passedData), ShouldResembleJSON, (`{"test":"test"}`))
			})
		})
		Convey("Sending an unparsable message", func() {
			handleMessage(ctx, conns, []byte(`test`), conn, session)
			Convey("The handler should respond with an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"data":"%s"}`, messages.Type_Error, badRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})
		Convey("Sending an unknown message type", func() {
			handleMessage(ctx, conns, []byte(`{"type":"hello world","data":{"test":"test"},"id":1}`), conn, session)
			Convey("The handler should respond with an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":1,"data":"%s"}`, messages.Type_Error, badRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})

	})
}

func TestAuthenticated(t *testing.T) {
	Convey("Given a context", t, func() {
		ctx := context.TODO()
		session := &Session{}
		conn := NewTestWriter()
		conns := prepareConnections()
		breederDB := conns.BreederDB
		breeder := model.Breeder{
			Name: "test",
		}
		id, _ := breederDB.Create(breeder)
		breeder.ID = id

		Convey("The user is logged in", func() {

			// "Login"
			session.BreederID = breeder.ID

			Convey("Trying to call a method behind authentication", func() {
				called := false
				checkFunc := func(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
					called = true
					return nil
				}
				wrapped := Authenticated(checkFunc)

				msg := Message{}
				wrapped(ctx, conns, msg, conn, session)
				Convey("The method is called", func() {
					So(called, ShouldBeTrue)

				})

			})
		})
		Convey("The user is not logged in", func() {

			Convey("Trying to call a method behind authentication", func() {
				called := false
				checkFunc := func(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
					called = true
					return nil
				}
				wrapped := Authenticated(checkFunc)

				msg := Message{}
				wrapped(ctx, conns, msg, conn, session)
				Convey("The method is not called", func() {
					So(called, ShouldBeFalse)
				})

			})
		})

	})
}

type TestWriter struct {
	data        []byte
	dataChannel chan []byte
}

func NewTestWriter() *TestWriter {
	return &TestWriter{
		dataChannel: make(chan []byte, 10),
	}
}

func (tw *TestWriter) Write(data []byte) (int, error) {
	tw.data = data
	tw.dataChannel <- data
	return len(data), nil
}

func prepareSession(session *Session) {

	session.OverworldInput = make(chan overworld.Command, 5)
	session.OverworldOutput = make(chan overworld.Update, 5)
}

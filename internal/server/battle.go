//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package server

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"

	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/ai"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/battle"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/server/messages"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/util"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/habitat"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/item"
)

const pseudoBreeder = 0

func handleSendMonster(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	var msg messages.BattleSendMonster
	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := msg.Validate(); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	id := session.BreederID
	breederID := model.ID(msg.BreederID)

	if breederID != id {
		return sendError(message, writer, unauthorized)
	}

	breederDB := conns.BreederDB
	breeder, err := breederDB.Load(breederID)
	if err != nil {
		return sendError(message, writer, notFound)
	}

	bs := conns.BattleServer
	btl, err := bs.GetByBreeder(breederID)
	if err != nil {
		return sendError(message, writer, notFound)
	}

	action := &battle.SendMonsterAction{
		Breeder:   &breeder,
		TeamIndex: uint(msg.TeamIndex),
		Position:  uint(msg.Location.Position),
		Side:      uint(msg.Location.Side),
		IssuedAction: battle.IssuedAction{
			Breeder: &breeder,
		},
		LocationAction: battle.LocationAction{
			Location: battle.Location{
				Position: uint(msg.Location.Position),
				Side:     uint(msg.Location.Side),
			},
		},
		ActionType: battle.BattleActionType_Player,
	}

	if err := action.IsOK(btl); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	btl.SendAction(action)

	return nil
}

func handleAttack(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	var msg messages.BattleAttack
	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := msg.Validate(); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	if len(msg.Targets) == 0 {
		return sendError(message, writer, malformedRequest)
	}

	id := session.BreederID
	breederID := model.ID(msg.BreederID)
	if breederID != id {
		return sendError(message, writer, unauthorized)
	}

	breederDB := conns.BreederDB
	breeder, err := breederDB.Load(breederID)
	if err != nil {
		return sendError(message, writer, notFound)
	}

	bs := conns.BattleServer
	btl, err := bs.GetByBreeder(breeder.ID)
	if err != nil {
		return sendError(message, writer, notFound)
	}

	atkLocation := battle.Location{
		Side:     uint(msg.Attacker.Side),
		Position: uint(msg.Attacker.Position),
	}

	var targetLocation []battle.Location
	for _, v := range msg.Targets {
		target := battle.Location{
			Side:     uint(v.Side),
			Position: uint(v.Position),
		}
		targetLocation = append(targetLocation, target)
	}

	atkAct := &battle.AttackAction{
		Move:             uint(msg.MoveIndex),
		AttackerLocation: atkLocation,
		DefenderLocation: targetLocation[0], // TODO: Should allow multiple locations
		PlayerAction: battle.PlayerAction{
			IssuedAction: battle.IssuedAction{
				Breeder: &breeder,
			},
		},
		LocationAction: battle.LocationAction{
			Location: atkLocation,
		},
	}

	if err := atkAct.IsOK(btl); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	btl.SendAction(atkAct)

	return nil
}

// TODO: Deprecated
// Replace with handleItem
func handleCapture(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	var msg messages.BattleCapture
	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := msg.Validate(); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	id := session.BreederID
	breederID := model.ID(msg.BreederID)
	if breederID != id {
		return sendError(message, writer, unauthorized)
	}

	breederDB := conns.BreederDB
	breeder, err := breederDB.Load(breederID)
	if err != nil {
		return sendError(message, writer, notFound)
	}

	bs := conns.BattleServer
	btl, err := bs.GetByBreeder(breeder.ID)
	if err != nil {
		return sendError(message, writer, notFound)
	}

	atkLocation := battle.Location{
		Side:     uint(msg.ActionFor.Side),
		Position: uint(msg.ActionFor.Position),
	}

	action := &battle.CaptureAction{
		PlayerAction: battle.PlayerAction{
			IssuedAction: battle.IssuedAction{
				Breeder: &breeder,
			},
			LocationAction: battle.LocationAction{
				Location: atkLocation,
			},
		},
		TargetLocation: battle.Location{
			Side:     uint(msg.Target.Side),
			Position: uint(msg.Target.Position),
		},
		Item: msg.Item,
	}
	if err := action.IsOK(btl); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	btl.SendAction(action)

	return nil
}

func handleItem(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	var msg messages.BattleItem

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := msg.Validate(); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	id := session.BreederID
	breederID := model.ID(msg.BreederID)
	if breederID != id {
		return sendError(message, writer, unauthorized)
	}

	breederDB := conns.BreederDB
	breeder, err := breederDB.Load(breederID)
	if err != nil {
		return sendError(message, writer, notFound)
	}

	bs := conns.BattleServer
	btl, err := bs.GetByBreeder(breeder.ID)
	if err != nil {
		return sendError(message, writer, notFound)
	}

	atkLocation := battle.Location{
		Side:     uint(msg.ActionFor.Side),
		Position: uint(msg.ActionFor.Position),
	}

	action := &battle.ItemAction{
		PlayerAction: battle.PlayerAction{
			IssuedAction: battle.IssuedAction{
				Breeder: &breeder,
			},
			LocationAction: battle.LocationAction{
				Location: atkLocation,
			},
		},
		TargetLocation: battle.Location{
			Side:     uint(msg.Target.Side),
			Position: uint(msg.Target.Position),
		},
		Item: msg.Item,
	}
	if err := action.IsOK(btl); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	btl.SendAction(action)

	return nil
}

func handleBattleRequest(context context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {

	var msg messages.BattleRequest
	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := msg.Validate(); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	breederId := session.BreederID

	if msg.BreederID <= 0 || model.ID(msg.BreederID) == breederId {
		return sendError(message, writer, invalidData)
	}

	// Check if current player is busy
	battleDB := conns.BattleDB
	prevReq, err := battleDB.LoadRequestByBreeder(breederId)
	if err != nil {
		if !errors.Is(err, db.NotFoundError{}) {
			return sendError(message, writer, serverError)
		} else {
			// Ok
		}
	} else if len(prevReq) > 0 {
		return sendError(message, writer, playerBusy)
	}

	battleServer := conns.BattleServer
	_, err = battleServer.GetByBreeder(breederId)
	if err != nil {
		if !errors.Is(err, db.NotFoundError{}) {
			return sendError(message, writer, serverError)
		} else {
			// Ok
		}
	} else {
		return sendError(message, writer, playerBusy)
	}

	// Check if the other player is busy
	_, err = battleServer.GetByBreeder(model.ID(msg.BreederID))
	if err != nil {
		if !errors.Is(err, db.NotFoundError{}) {
			return sendError(message, writer, serverError)
		} else {
			// Ok
		}
	} else {
		return sendError(message, writer, playerBusy)
	}

	req := model.Request{
		From: breederId,
		To:   model.ID(msg.BreederID),
	}

	conn, ok := connectionPool[req.To]
	if !ok {
		return sendError(message, writer, notFound)
	}

	id, err := battleDB.CreateRequest(req)
	if err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not create battle request: %w", err)
	}

	req, err = battleDB.LoadRequest(id)
	if err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not load battle request: %w", err)
	}

	if err = sendMessage(messages.Type_BattleRequestResponse, nil, writer, message.ID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not respond to message: %w", err)
	}

	inviteMsg := messages.BattleRequestInvite{
		RequestID: int(req.ID),
		From:      int(req.From),
		To:        int(req.To),
	}

	if err = sendMessage(messages.Type_BattleRequestInvite, inviteMsg, conn, 0); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not invite other breeder: %w", err)
	}

	return nil
}

func handleBattleRequestAccept(ctx context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	logger := logrus.WithField("method", "handleBattleRequestAccept")

	var msg messages.BattleRequestAccept
	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}
	if err := msg.Validate(); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	if msg.RequestID == 0 {
		return sendError(message, writer, invalidData)
	}

	battleDB := conns.BattleDB
	if req, err := battleDB.LoadRequestByBreeder(session.BreederID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not load battle request: %w", err)

	} else if len(req) > 1 {
		return sendError(message, writer, playerBusy)
	}

	// Make sure, that the player is currently not in a battle
	battleServer := conns.BattleServer
	if _, err := battleServer.GetByBreeder(session.BreederID); err == nil {
		return sendError(message, writer, playerBusy)
	}

	req, err := battleDB.LoadRequest(model.ID(msg.RequestID))
	if err != nil {
		if errors.Is(err, db.NotFoundError{}) {
			return sendError(message, writer, notFound)
		} else {
			handleError(message, writer, serverError)
			return fmt.Errorf("could not load battle request: %w", err)
		}
	}

	if req.To != session.BreederID {
		return sendError(message, writer, notAuthorized)
	}

	// Accept request
	req.Status = model.ReqStatus_Ok
	if err = battleDB.SaveRequest(req); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not save request: %w", err)
	}

	logger.Traceln("Start creating battle")
	// Start battle
	breederDB := conns.BreederDB

	challenger, err := breederDB.Load(req.From)
	if err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not load challenging breeder: %w", err)
	}

	challenged, err := breederDB.Load(req.To)
	if err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not load challenged breeder: %w", err)
	}

	part := battle.Participants{[]model.Breeder{challenger}, []model.Breeder{challenged}}

	if err = createBattle(ctx, conns, part, nil); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not create battle: %w", err)
	}

	logger.Traceln("Finished...")

	return nil
}

func createBattle(ctx context.Context, conns util.Connections, participants battle.Participants, aiTeam []model.Monster) error {
	logger := logrus.WithField("method", "createBattle")
	battleServer := conns.BattleServer

	teams, err := loadTeamsForParticipants(ctx, conns, participants)
	if err != nil {
		return fmt.Errorf("could not load teams: %w", err)
	}
	if aiTeam != nil {
		teams[0] = aiTeam
	}

	inventory := make(map[model.ID][]item.Item)
	for _, part := range participants.Flatten() {

		var inv map[string]int
		inv, err = conns.ItemDB.Inventory(part.ID)

		if err != nil {
			return fmt.Errorf("could not load inventory: %w", err)
		}

		inventory[part.ID] = []item.Item{}
		for k, v := range inv {
			inventory[part.ID] = append(inventory[part.ID],
				item.Item{
					Base:   model.ItemCache[k],
					Amount: uint(v),
				})

		}
	}

	// TODO: Change to 2 sides
	b := battle.CreateBattle(participants, teams, 2, inventory, &resultSaver{monsterDB: conns.MonsterDB})
	logger.Traceln("Creating a battle on the battle server")
	battleServer.Create(b)
	logger.Traceln("Battle created")

	// Notify participants
	mbc := messages.BattleCreation{
		BattleID: int(b.ID()),
		Teams:    calculateMonsterViewTeams(teams),
	}

	mbc.Locations = calculateLocations(participants)

	pool := make(map[model.ID]io.Writer)
	for _, v := range participants.Flatten() {
		if v.ID == pseudoBreeder {
			// Create listener for AI
			pool[v.ID] = ai.NewSimple(b)
		} else {
			pool[v.ID] = connectionPool[v.ID]
		}
	}

	if err = informBattleCreation(participants, mbc, pool); err != nil {
		return fmt.Errorf("could not notify breeder about battle creation: %w", err)
	}

	if err = setupBattleListener(ctx, conns, participants, b.ID(), pool); err != nil {
		return fmt.Errorf("could not setup battle listener: %w", err)
	}

	if logrus.GetLevel() >= logrus.DebugLevel {
		err := addBattleListener(ctx, conns, b.ID(),
			func(ch chan battle.BattleResult) {
				for res := range ch {
					logger.WithField("result", res).Debugln("Received result")
				}
			})

		if err != nil {
			return fmt.Errorf("could not add listener to battle %d: %w", b.ID(), err)
		}
	}
	return nil

}

func informBattleCreation(participants battle.Participants, mbc messages.BattleCreation, conns map[model.ID]io.Writer) error {
	logger := logrus.WithField("method", "informBattleCreation")
	logger.Traceln("Notify participants")
	for _, participant := range participants.Flatten() {
		if err := sendMessage(messages.Type_BattleCreation, mbc, conns[participant.ID], 0); err != nil {
			return fmt.Errorf("could not notify breeder with ID %d: %w", participant.ID, err)
		}
	}
	return nil
}

func setupBattleListener(ctx context.Context, conns util.Connections, participants battle.Participants, battleId model.ID, writer map[model.ID]io.Writer) error {
	logger := logrus.WithField("method", "setupBattleListener")

	logger.Traceln("Adding listener")
	// Add notifier
	for _, participants := range participants {
		for _, participant := range participants {
			// Change scope of the variable
			participant := participant
			err := addBattleListener(ctx, conns, battleId,
				func(ch chan battle.BattleResult) {
					for res := range ch {
						if res.ResultType == battle.BattleResult_Finished && participant.ID > 0 {
							session := sessions.ById[participant.ID]
							session.OverworldInput <- overworld.BattleFinishedCommand{}
						}

						if err := sendMessage(messages.Type_BattleResult, res, writer[participant.ID], 0); err != nil {
							logger.WithError(err).Errorln("could not send the battle result to the client")
						}
					}
				})
			if err != nil {
				return err
			}

		}
	}

	return nil
}

func addBattleListener(_ context.Context, conns util.Connections, battleId model.ID, listener func(chan battle.BattleResult)) error {
	battleServer := conns.BattleServer
	ch := make(chan battle.BattleResult, 1)
	if err := battleServer.AddListener(battleId, ch); err != nil {
		return fmt.Errorf("could not add listener to battle %d: %w", battleId, err)
	}

	go listener(ch)
	return nil
}

func loadTeamsForParticipants(_ context.Context, conns util.Connections, participants battle.Participants) (map[model.ID][]model.Monster, error) {
	teams := make(map[model.ID][]model.Monster)

	logrus.Traceln("Loading teams into battle")
	monsterDB := conns.MonsterDB
	for _, partTeam := range participants {
		for _, participant := range partTeam {
			if participant.ID == pseudoBreeder {
				continue
			}
			var team []model.Monster
			team, err := monsterDB.LoadTeamByBreederId(participant.ID)
			if err != nil {
				return nil, fmt.Errorf("could not load team for breeder with ID %d: %w", participant.ID, err)
			}

			teams[participant.ID] = team
		}
	}
	return teams, nil
}

func calculateMonsterViewTeams(teams map[model.ID][]model.Monster) map[int][]messages.MonsterView {

	mvTeam := make(map[int][]messages.MonsterView)

	for breederID, team := range teams {
		for _, mon := range team {

			mv := messages.MonsterView{
				Species:        mon.Species.Name,
				Level:          int(mon.Level),
				Name:           mon.Nickname,
				Status:         []string{},
				Id:             int(mon.ID),
				CurrentHP:      mon.CurrentHPPercent(),
				CurrentStamina: mon.CurrentStaminaPercent(),
			}
			for _, v := range mon.Status {
				mv.Status = append(mv.Status, v.Base.Name)
			}
			if len(mv.Name) == 0 {
				mv.Name = mv.Species
			}

			id := int(breederID)
			mvTeam[id] = append(mvTeam[id], mv)
		}
	}
	return mvTeam
}

func calculateLocations(participants battle.Participants) map[int][]messages.BattleLocation {

	locations := make(map[int][]messages.BattleLocation)
	for k, partTeam := range participants {
		for i, participant := range partTeam {
			id := int(participant.ID)
			locations[id] = append(locations[id], messages.BattleLocation{
				Side:     k,
				Position: i,
			})
			if len(partTeam) == 1 {
				locations[id] = append(locations[id], messages.BattleLocation{
					Side:     k,
					Position: i + 1,
				})
			}
		}

	}
	return locations
}

func handleBattleRequestCancel(ctx context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {

	var msg messages.BattleRequestCancel
	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}
	if err := msg.Validate(); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	if msg.RequestID == 0 {
		return sendError(message, writer, invalidData)
	}

	battleDB := conns.BattleDB
	req, err := battleDB.LoadRequest(model.ID(msg.RequestID))
	if err != nil {
		if errors.Is(err, db.NotFoundError{}) {
			return sendError(message, writer, notFound)
		} else {
			handleError(message, writer, serverError)
			return fmt.Errorf("could not load battle request: %w", err)
		}
	}

	if req.To != session.BreederID && req.From != session.BreederID {
		return sendError(message, writer, notAuthorized)
	}

	// Cancel request
	req.Status = model.ReqStatus_Canceled
	if err = battleDB.SaveRequest(req); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not save request: %w", err)
	}

	if err = sendMessage(messages.Type_BattleRequestCancelResponse, messages.BattleRequestCancelResponse{RequestID: int(req.ID)}, writer, message.ID); err != nil {
		return sendError(message, writer, serverError)
	}

	var w io.Writer
	if req.To == session.BreederID {
		w = connectionPool[req.From]
	} else {
		w = connectionPool[req.To]
	}
	if err = sendMessage(messages.Type_BattleRequestCancelResponse, messages.BattleRequestCancelResponse{RequestID: int(req.ID)}, w, 0); err != nil {
		return sendError(message, writer, serverError)
	}

	return nil
}

func handleBattleSurrender(ctx context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {
	var msg messages.BattleSurrender
	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := msg.Validate(); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	id := session.BreederID
	breederID := model.ID(msg.BreederID)

	if breederID != id {
		return sendError(message, writer, unauthorized)
	}

	breederDB := conns.BreederDB
	breeder, err := breederDB.Load(breederID)
	if err != nil {
		return sendError(message, writer, notFound)
	}

	bs := conns.BattleServer
	btl, err := bs.GetByBreeder(breederID)
	if err != nil {
		return sendError(message, writer, notFound)
	}

	btl.SendAction(battle.NewSurrenderAction(battle.Location{
		Side:     uint(msg.Location.Side),
		Position: uint(msg.Location.Position),
	}, &breeder))

	return nil
}

func handleRequestWildBattle(ctx context.Context, conns util.Connections, message Message, writer io.Writer, session *Session) error {

	logger := logrus.WithField("method", "handleRequestWildBattle")

	var msg messages.RequestWildBattle
	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}
	if err := msg.Validate(); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	logger.Traceln("Start creating battle")
	// Start battle
	breederDB := conns.BreederDB

	challenger, err := breederDB.Load(session.BreederID)
	if err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not load challenging breeder: %w", err)
	}

	challenged := model.Breeder{
		ID:   0,
		Name: "WildBattle",
	}

	part := battle.Participants{[]model.Breeder{challenger}, []model.Breeder{challenged}}

	team := []model.Monster{*habitat.CreateRandomMonsterFromHabitat(msg.Area)}

	if err = createBattle(ctx, conns, part, team); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not create battle: %w", err)
	}
	return nil
}

type resultSaver struct {
	monsterDB util.MonsterDB
	itemDB    util.ItemDB
}

func (rs *resultSaver) CaptureMonster(breederId model.ID, monster model.Monster) {
	logger := logrus.WithField("method", "CaptureMonster")
	monster.OwnerID = breederId

	monsterDB := rs.monsterDB

	var err error

	if monster.ID, err = monsterDB.Create(monster); err != nil {
		logger.WithError(err).Errorln("Could not create monster")
		return
	}

	team, err := monsterDB.LoadTeamByBreederId(breederId)
	if err != nil {
		logger.WithError(err).Errorln("Could not load team")
		return
	}

	if len(team) < 6 {
		team = append(team, monster)
		if err := monsterDB.SaveTeamByBreederId(breederId, team); err != nil {
			logger.WithError(err).Errorln("Could not save team")
			return

		}
	}

}

func (rs *resultSaver) SaveMonster(monster model.Monster) {
	if err := rs.monsterDB.Save(monster); err != nil {
		logrus.WithError(err).WithField("monster", monster).Errorln("Could not save monster")
	}
	logrus.WithField("monster", monster).Infoln("Saved monster")
}

func (rs *resultSaver) SaveUsedItem(breeder model.ID, item string, amount int) {
	if err := rs.itemDB.Remove(breeder, item, amount); err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{
			"breeder": breeder,
			"item":    item,
			"amount":  amount,
		}).Errorln("Could not save used item")
	}

}

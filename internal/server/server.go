//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package server

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"path/filepath"
	"time"

	"github.com/asdine/storm/v3"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"gitlab.com/kori-irrlicht/hekate"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/battle"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/data"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/util"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/memory"
	stormdb "gitlab.com/kori-irrlicht/project-open-monster/pkg/db/storm"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

func handlerHekate(ctx context.Context, b []byte, conns util.Connections, conn net.Conn, session *Session) {

	if s := session.OverworldInput; s == nil {
		session.OverworldInput = make(chan overworld.Command, 50)
	}

	if s := session.OverworldOutput; s == nil {
		session.OverworldOutput = make(chan overworld.Update, 50)
	}

	data := b

	if err := handleMessage(ctx, conns, data, conn, session); err != nil {
		logrus.WithError(err).Errorln("Could not handle the message")
	}
}

func Start() {
	var level logrus.Level = logrus.InfoLevel
	switch viper.GetString("logging.level") {
	case "trace":
		level = logrus.TraceLevel
	case "debug":
		level = logrus.DebugLevel
	}
	logrus.SetLevel(level)

	basepathMonster := filepath.Clean("./assets/data/monster")
	viper.SetDefault("data.dir.monster", basepathMonster)

	basepathMove := filepath.Clean("./assets/data/moves")
	viper.SetDefault("data.dir.move", basepathMove)

	err := data.Load(viper.GetString("data.archive.location"), viper.GetString("data.archive.type"))

	if err != nil {
		panic(err)
	}

	db, err := storm.Open("my.db")
	if err != nil {
		logrus.Fatalln(err)
	}

	defer func() {
		if errClose := db.Close(); errClose != nil {
			logrus.WithError(errClose).Errorln("Could not close database")
		}
	}()

	address := fmt.Sprintf(":%d", viper.GetInt("port"))
	logrus.WithField("address", address).Infoln("Starting server")

	server, err := hekate.Listen("udp", address, 0xF001)
	if err != nil {
		panic(err)
	}
	defer func() {
		_ = server.Close()
	}()

	conns := createConnections(db)

	for {
		server.ResendLostPackages()
		server.KeepAlive()

		b := make([]byte, 1024)
		if err = server.SetReadDeadline(time.Now().Add(time.Millisecond * 15)); err != nil {
			logrus.WithError(err).Errorln("Error while setting deadline")
			continue
		}
		_, conn, err := server.ReadFromHekate(b)
		if err != nil {
			if !errors.Is(err, os.ErrDeadlineExceeded) {
				logrus.WithError(err).Errorln("Error while reading from hekate")
			}
			continue
		}
		b = bytes.Trim(b, "\x00")

		var (
			session *Session
			ok      bool
		)

		if session, ok = sessions.ByAddr[conn.RemoteAddr().String()]; !ok {
			session = &Session{}
			sessions.ByAddr[conn.RemoteAddr().String()] = session
		}

		ctx := context.Background()
		ctx = context.WithValue(ctx, contextKey("addr"), conn.RemoteAddr().String())
		go handlerHekate(ctx, b, conns, conn, session)

		handleDisconnects(conns, server.CleanUpDisconnects())
	}
}

func handleDisconnects(conns util.Connections, hconns []*hekate.Conn) {
	for _, v := range hconns {
		session := sessions.ByAddr[v.RemoteAddr().String()]
		session.OverworldInput <- overworld.DisconnectCommand{}
		b, err := conns.BattleServer.GetByBreeder(session.BreederID)
		if err != nil {
			logrus.WithError(err).WithField("id", session.BreederID).Debugln("Could not load battle")
			continue
		}
		breeder, err := conns.BreederDB.Load(session.BreederID)
		if err != nil {
			logrus.WithError(err).Errorln("Could not load breeder")
			continue
		}
		b.Surrender(&breeder)
		logrus.WithField("client", v.RemoteAddr().String()).Infoln("Disconnected")
	}
}

func createConnections(db *storm.DB) util.Connections {
	bdb := stormdb.NewBreederDB(db)
	bs := battle.NewServer()
	conns := util.Connections{
		BreederDB:    bdb,
		MonsterDB:    stormdb.NewMonsterDB(db),
		BattleDB:     memory.NewBattleDB(),
		BattleServer: bs,
		ItemDB:       stormdb.NewItemDB(db),
	}
	conns.Overworld = overworld.Start(bdb)
	bs.Start()
	go bs.Run()

	return conns
}

var sessions = sessionHandler{
	ByAddr: make(map[string]*Session),
	ById:   make(map[model.ID]*Session),
}

type sessionHandler struct {
	ByAddr map[string]*Session
	ById   map[model.ID]*Session
}

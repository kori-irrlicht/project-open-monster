//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package server

import (
	"fmt"
	"io"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/server/messages"
)

type ErrorCode string

var (
	badRequest       ErrorCode = `bad request`
	invalidData      ErrorCode = `invalid data`
	malformedRequest ErrorCode = `malformed request`
	serverError      ErrorCode = `server error`
	unauthorized     ErrorCode = `unauthorized`
	notAuthenticated ErrorCode = `not authenticated`
	notFound         ErrorCode = `notFound`
	notAuthorized    ErrorCode = `notAuthorized`
	playerBusy       ErrorCode = `player is busy`
)

// sendError is a shortcut, if the server needs to send an error message to the client.
// sendError already wraps the error, so the result can be directly returned
func sendError(message Message, writer io.Writer, errorCode ErrorCode) error {
	if err := sendMessage(messages.Type_Error, string(errorCode), writer, message.ID); err != nil {
		return errors.Wrap(err, "could not send error")
	}
	return nil
}

// sendError is a shortcut, if the server needs to send an error message to the client.
// sendError already wraps the error, so the result can be directly returned
func sendErrorWithMessage(message Message, writer io.Writer, errorCode ErrorCode, msg string) error {
	if err := sendMessage(messages.Type_Error, fmt.Sprintf("%s: %s", string(errorCode), msg), writer, message.ID); err != nil {
		return errors.Wrap(err, "could not send error")
	}
	return nil
}

// handleError tries to notify the client about the server error.
// If it could not notify the client, it will log the error. If you need the error code, use sendError instead.
//
// message is the message of the client, to which the server will respond with an error.
// writer is the client connection.
//
func handleError(message Message, writer io.Writer, errorCode ErrorCode) {
	if err := sendError(message, writer, errorCode); err != nil {
		logrus.WithError(err).Errorln("error while handling another error")
	}
}

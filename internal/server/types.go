//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package server

import (
	"context"
	"encoding/json"
	"io"

	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/util"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

// MessageHandler parse the message data and act according to the data.
//
// If an error occurs, the user should be notified. If the error was caused by the user, like referencing an unknown user,
// no error should be returned. Returns an error, if a connection problem, a database error or similiar server-side errors appear.
type MessageHandler func(context.Context, util.Connections, Message, io.Writer, *Session) error

type contextKey string

// Message is the container for messages send to or received from the client.
// It contains the type, an id and the data.
// If the Message is a response to another message, ResponseID will contain the id of the first message.
type Message struct {
	Type       string          `json:"type"`
	ID         int             `json:"id"`
	ResponseID int             `json:"responseID,omitempty"`
	Data       json.RawMessage `json:"data"`
}

// Session contains session-scoped values
type Session struct {
	BreederID model.ID

	OverworldInput  chan overworld.Command
	OverworldOutput chan overworld.Update
}

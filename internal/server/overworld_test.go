//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package server

import (
	"context"
	"fmt"
	"testing"

	"github.com/EngoEngine/engo"
	. "github.com/smartystreets/goconvey/convey"
	. "gitlab.com/kori-irrlicht/goconvey-assertions"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/data"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/server/messages"
)

func init() {

	err := data.Load("../../assets/data", "file")

	if err != nil {
		panic(err)
	}

}

func TestHandleOverworldInput(t *testing.T) {

	Convey("Given a session and a connection", t, func() {
		ctx := context.TODO()
		sessions, conns := prepare()
		session := sessions[0]
		conn := NewTestWriter()

		Convey("Sending invalid JSON", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{:}`),
			}
			err := handleOverworldInput(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("There should be no input to the overworld", func() {
				So(session.OverworldInput, ShouldHaveLength, 0)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})
		Convey("Sending a request to a non existant breeder", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 100}`),
			}
			err := handleOverworldInput(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("There should be no new input to the overworld", func() {
				So(session.OverworldInput, ShouldHaveLength, 0)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, unauthorized)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})

		Convey("Sending a request to another breeder", func() {

			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 2}`),
			}
			err := handleOverworldInput(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("There should be no new input to the overworld", func() {
				So(session.OverworldInput, ShouldHaveLength, 0)
				So(sessions[1].OverworldInput, ShouldHaveLength, 0)
			})

			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, unauthorized)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})

		Convey("The breeder ID is unparsable", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": -1}`),
			}
			err := handleOverworldInput(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("There should be no new input to the overworld", func() {
				So(session.OverworldInput, ShouldHaveLength, 0)
			})

			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})

		})

		Convey("Sending valid data", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1,"direction":{"x": 20, "y": 30}, "interaction": true}`),
			}
			err := handleOverworldInput(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("There should be new input to the overworld", func() {
				So(session.OverworldInput, ShouldHaveLength, 2)
				Convey("The first is the direction data", func() {
					d := (<-session.OverworldInput).(overworld.MoveCommand)
					So(d.Direction.X, ShouldEqual, 20)
					So(d.Direction.Y, ShouldEqual, 30)
				})
				Convey("The second is the interaction data", func() {
					<-session.OverworldInput
					d := (<-session.OverworldInput).(overworld.InteractionCommand)
					So(d.Interact, ShouldBeTrue)
				})
			})

		})

	})
}

func TestHandleDisconnectUpdate(t *testing.T) {
	Convey("Sending a DisconnectUpdate to a given connection", t, func() {
		conn := NewTestWriter()
		upd := overworld.DisconnectUpdate{ID: 4}
		owHandleDisconnectUpdate(upd, conn)
		Convey("The client is informed of the disconnect", func() {
			data := `{"worldID":4}`
			msg := fmt.Sprintf(`{"type":"%s","id":0,"data":%s}`, messages.Type_OverworldRemove, data)
			So(string(conn.data), ShouldResembleJSON, msg)
		})

	})
}

func TestHandleWorldUpdate(t *testing.T) {
	Convey("Sending a world update to a given connection", t, func() {
		conn := NewTestWriter()
		upd := overworld.WorldUpdate{Coordinates: []overworld.WorldCoordinates{
			{World: "foo", AreaName: "bar", Tiles: []int{1, 2, 3}, Area: engo.Point{X: 2, Y: 3}, Offset: engo.Point{1, 2}, Size: engo.Point{3, 4}},
		}}
		owHandleWorldUpdate(upd, conn)
		Convey("The client is informed of the update", func() {
			data := `{"updates":[{"tiles":[1,2,3], "world":"foo", "size":{"x":3,"y":4},"offset":{"x":1,"y":2},"area":{"x":2,"y":3}}]}`
			msg := fmt.Sprintf(`{"type":"%s","id":0,"data":%s}`, messages.Type_OverworldUpdateWorldPositions, data)
			So(string(conn.data), ShouldResembleJSON, msg)
		})

	})
}

func TestNotificationAPI(t *testing.T) {

	Convey("Given a new notification API", t, func() {
		conn := NewTestWriter()
		connectionPool[1] = conn
		api := NotificationAPI{}

		Convey("Sending a dialog", func() {
			api.SendDialog(1, "Test")
			data := `{"dialog":"Test"}`
			msg := fmt.Sprintf(`{"type":"%s","id":0,"data":%s}`, messages.Type_OverworldUpdateDialogs, data)
			So(string(conn.data), ShouldResembleJSON, msg)
		})
		Convey("Receiving an item", func() {
			api.ReceiveItem(1, "Test", 5)
			data := `{"item":"Test","amount":5}`
			msg := fmt.Sprintf(`{"type":"%s","id":0,"data":%s}`, messages.Type_ReceiveItem, data)
			So(string(conn.data), ShouldResembleJSON, msg)
		})
	})
}

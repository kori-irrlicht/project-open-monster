//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package server

import (
	"context"
	"fmt"
	"strings"
	"testing"

	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/spf13/viper"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/battle"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/data"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/server/messages"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/util"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"

	//	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/json"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/memory"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/habitat"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/item"

	. "gitlab.com/kori-irrlicht/goconvey-assertions"
)

func init() {
	logrus.SetLevel(logrus.WarnLevel)
	basepath := "../../assets/data/monster"
	viper.SetDefault("data.dir.monster", basepath)
	viper.Set("data.archive.location", "../../assets/data")
	viper.Set("data.archive.type", "file")

	err := data.Load("../../assets/data", "file")

	if err != nil {
		panic(err)
	}

	habitat.Add("testing", habitat.Entry{
		Species:  "Hoarfox",
		Chance:   100,
		MinLevel: 5,
		MaxLevel: 5,
	})
}

func TestRequestBattle(t *testing.T) {
	Convey("Given a session and a connection", t, func() {
		ctx := context.TODO()
		sessions, conns := prepare()
		session := sessions[0]
		conn := NewTestWriter()
		battleDB := conns.BattleDB
		conns.BattleServer = &testServer{}
		bs := conns.BattleServer
		Convey("Sending invalid JSON", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{:}`),
			}
			err := handleBattleRequest(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("There should be no new request in the db", func() {
				req, err := battleDB.LoadRequest(1)
				So(err, ShouldNotBeNil)
				So(req, ShouldResemble, model.Request{})
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})
		Convey("Leaving out the data", func() {
			msg := Message{
				ID:   1,
				Data: []byte("{}"),
			}
			err := messageHandlerMap[messages.Type_BattleRequest](ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should be notified", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})
		Convey("Sending a new valid request", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 2}`),
			}
			err := handleBattleRequest(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("There should be a new request in the db", func() {
				req, err := battleDB.LoadRequest(1)
				So(err, ShouldBeNil)
				So(req, ShouldNotBeNil)
				So(req.From, ShouldEqual, 1)
				So(req.To, ShouldEqual, 2)
			})
			Convey("The user should be notified", func() {
				resMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":null}`, messages.Type_BattleRequestResponse, msg.ID)
				So(string(conn.data), ShouldResembleJSON, resMsg)
			})
			Convey("The other breeder should be notified", func() {
				conn := connectionPool[2].(*TestWriter)
				req, _ := battleDB.LoadRequest(1)
				resMsg := fmt.Sprintf(`{"type":"%s","id":0,"data":{"from":%d,"to":%d,"requestID":%d}}`, messages.Type_BattleRequestInvite, req.From, req.To, req.ID)
				So(string(conn.data), ShouldResembleJSON, resMsg)

			})
			Convey("Sending a request, while waiting for the result of another request", func() {

				msg := Message{
					ID:   1,
					Data: []byte(`{"breederID": 2}`),
				}
				err := handleBattleRequest(ctx, conns, msg, conn, session)
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("There should be no new request in the db", func() {
					req, err := battleDB.LoadRequest(2)
					So(err, ShouldNotBeNil)
					So(req, ShouldResemble, model.Request{})
				})
				Convey("The user should receive an error", func() {
					errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, playerBusy)
					So(string(conn.data), ShouldResembleJSON, errMsg)
				})

			})

		})
		Convey("Sending a request to a non existant breeder", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 100}`),
			}
			err := handleBattleRequest(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("There should be no new request in the db", func() {
				req, err := battleDB.LoadRequest(1)
				So(err, ShouldNotBeNil)
				So(req, ShouldResemble, model.Request{})
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, notFound)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})

		Convey("Sending a request to themself", func() {

			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1}`),
			}
			err := handleBattleRequest(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("There should be no new request in the db", func() {
				req, err := battleDB.LoadRequest(1)
				So(err, ShouldNotBeNil)
				So(req, ShouldResemble, model.Request{})
			})

			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})

		Convey("Sending a request to an unknown ID", func() {

			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 100}`),
			}
			err := handleBattleRequest(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("There should be no new request in the db", func() {
				req, err := battleDB.LoadRequest(1)
				So(err, ShouldNotBeNil)
				So(req, ShouldResemble, model.Request{})
			})

			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, notFound)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})

		Convey("Sending a request, while in battle", func() {
			t1 := model.Breeder{ID: 1}
			t2 := model.Breeder{ID: 3}

			tb := &testBattle{participants: battle.Participants{{t1}, {t2}}, field: NewTestBattleField()}
			bs.Create(tb)

			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 2}`),
			}
			err := handleBattleRequest(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("There should be no new request in the db", func() {
				req, err := battleDB.LoadRequest(1)
				So(err, ShouldNotBeNil)
				So(req, ShouldResemble, model.Request{})
			})

			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, playerBusy)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})

		Convey("Sending a request to a breeder, who is in a battle", func() {
			t1 := model.Breeder{ID: 3}
			t2 := model.Breeder{ID: 2}

			tb := &testBattle{participants: battle.Participants{{t1}, {t2}}, field: NewTestBattleField()}
			bs.Create(tb)

			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 2}`),
			}
			err := handleBattleRequest(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("There should be no new request in the db", func() {
				req, err := battleDB.LoadRequest(1)
				So(err, ShouldNotBeNil)
				So(req, ShouldResemble, model.Request{})
			})

			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, playerBusy)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})

		Convey("The breeder ID is unparsable", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": -1}`),
			}
			err := handleBattleRequest(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("There should be no new request in the db", func() {
				req, err := battleDB.LoadRequest(1)
				So(err, ShouldNotBeNil)
				So(req, ShouldResemble, model.Request{})
			})

			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})

		})

	})
}

func TestRequestBattleAccept(t *testing.T) {
	Convey("Given a session and a connection", t, func() {
		ctx := context.TODO()
		sessions, conns := prepare()
		session := sessions[1]
		conn := NewTestWriter()
		battleDB := conns.BattleDB
		monsterDB := conns.MonsterDB
		bs := conns.BattleServer.(battle.Server)
		bs.Start()
		go bs.Run()
		Reset(func() {
			bs.Stop()
		})

		// Create request
		msg := Message{
			ID:   1,
			Data: []byte(`{"breederID": 2}`),
		}
		if err := handleBattleRequest(ctx, conns, msg, conn, sessions[0]); err != nil {
			panic(err)
		}

		Convey("Sending invalid JSON", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{:}`),
			}
			err := handleBattleRequestAccept(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The request should not be accepted", func() {
				req, _ := battleDB.LoadRequest(1)
				So(req.Status, ShouldResemble, model.ReqStatus_Pending)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})
		Convey("Leaving out the data", func() {
			msg := Message{
				ID:   1,
				Data: []byte("{}"),
			}
			err := handleBattleRequestAccept(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should be notified", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})
		Convey("Sending a valid accept message", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"requestID": 1}`),
			}
			err := handleBattleRequestAccept(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The request should be accepted", func() {
				req, _ := battleDB.LoadRequest(1)
				So(req.Status, ShouldEqual, model.ReqStatus_Ok)
			})

			// battle should be created
			Convey("The battle should be created", func() {
				b, err := bs.Get(1)
				So(err, ShouldBeNil)
				So(b, ShouldNotBeNil)

			})

			Convey("The teams should be registered to the battle", func() {

				b, err := bs.Get(1)
				So(err, ShouldBeNil)
				So(b.Field().Teams(), ShouldNotBeEmpty)

				for _, breeder := range b.Participants().Flatten() {
					So(b.Field().Teams()[breeder.ID], ShouldNotBeEmpty)
				}
			})

			Convey("All participants should be notified", func() {
				req, _ := battleDB.LoadRequest(1)
				connTo := connectionPool[req.To].(*TestWriter)
				connFrom := connectionPool[req.From].(*TestWriter)

				tmplTeam := `[{"species":"Hoarfox","level":5,"name":"Hoarfox","status":[],"id":%d,"currentHP":100,"currentStamina":100}]`
				tmplLocation := `[{"side":%d,"position":0}, {"side": %d,"position":1}]`

				team1, _ := monsterDB.LoadTeamByBreederId(req.From)
				strTeam1 := fmt.Sprintf(tmplTeam, team1[0].ID)
				loc1 := fmt.Sprintf(tmplLocation, 0, 0)

				team2, _ := monsterDB.LoadTeamByBreederId(req.To)
				strTeam2 := fmt.Sprintf(tmplTeam, team2[0].ID)
				loc2 := fmt.Sprintf(tmplLocation, 1, 1)

				resMsg := fmt.Sprintf(`{"type":"%s","id":0,"data":{"battleID":%d,"teams":{"%d":%s,"%d":%s},"locations":{"%d":%s,"%d":%s}}}`, messages.Type_BattleCreation, 1, req.From, strTeam1, req.To, strTeam2, req.From, loc1, req.To, loc2)

				So(string(connTo.data), ShouldResembleJSON, resMsg)
				So(string(connFrom.data), ShouldResembleJSON, resMsg)
			})

			//TODO: Test randomly fails, because the result of the second breeder is returned first
			SkipConvey("The battle should have a listener for each participant", func() {

				req, _ := battleDB.LoadRequest(1)
				connTo := connectionPool[req.To].(*TestWriter)
				connFrom := connectionPool[req.From].(*TestWriter)

				for i := 0; i < 2; i++ {
					var id model.ID
					if i == 0 {
						id = req.To
					} else {
						id = req.From
					}

					side := i

					msg := Message{
						ID:   1,
						Data: []byte(fmt.Sprintf(`{"breederID": %d,"teamIndex":%d,"location":{"side":%d,"position":0}}`, id, 0, side)),
					}
					handleSendMonster(ctx, conns, msg, connTo, session)
				}
				<-connTo.dataChannel
				for i := 0; i < 3; i++ {
					<-connTo.dataChannel
					<-connFrom.dataChannel
				}

				resMsg := fmt.Sprintf(`{"type":"%s","id":0,"data":{"id":%d,"breeder":%d,"resultType":"sendMonster","result":{"spawnLocation":{"side":%d,"position":%d},"teamIndex":%d}}`, messages.Type_BattleResult, 0, req.From, 1, 0, 0)
				toMsg := string(<-connTo.dataChannel)
				fromMsg := string(<-connFrom.dataChannel)

				So(toMsg, ShouldStartWith, resMsg)
				So(strings.Compare(toMsg, fromMsg), ShouldEqual, 0)

			})
		})
		Convey("Accepting a non existent request", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"requestID": 3000}`),
			}
			err := handleBattleRequestAccept(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, notFound)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})
		Convey("Accepting a request to another breeder", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"requestID": 1}`),
			}
			err := handleBattleRequestAccept(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, notAuthorized)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})
		Convey("If the breeder has already send an invitation", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1}`),
			}
			_ = handleBattleRequest(ctx, conns, msg, conn, session)
			Convey("The breeder should not be able to accept the invitation", func() {
				msg := Message{
					ID:   1,
					Data: []byte(`{"requestID": 1}`),
				}
				err := handleBattleRequestAccept(ctx, conns, msg, conn, session)
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The user should receive an error", func() {
					errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, playerBusy)
					So(string(conn.data), ShouldResembleJSON, errMsg)
				})

			})

		})
	})
}

func TestRequestWildBattle(t *testing.T) {
	Convey("Given a session and a connection", t, func() {
		ctx := context.TODO()
		sessions, conns := prepare()
		session := sessions[0]
		conn := NewTestWriter()
		bs := conns.BattleServer.(battle.Server)
		bs.Start()
		breederDB := conns.BreederDB
		//go bs.Run()
		Convey("Requesting a wild battle", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"area": "testing"}`),
			}

			err := handleRequestWildBattle(ctx, conns, msg, conn, session)
			Convey("err should be nil", func() {
				So(err, ShouldBeNil)
			})
			battle, err := bs.GetByBreeder(session.BreederID)
			Convey("A battle should be created", func() {
				So(err, ShouldBeNil)
				So(battle, ShouldNotBeNil)
			})
			Convey("The participants should be the pseudo-breeder 'WildBattle' and the requesting breeder", func() {
				So(battle.Participants().Flatten(), ShouldContain, model.Breeder{
					ID:   0,
					Name: "WildBattle",
				})
				b, _ := breederDB.Load(session.BreederID)
				So(battle.Participants().Flatten(), ShouldContain, b)
				So(battle.Participants().TotalSize(), ShouldEqual, 2)
			})
			Convey("The pseudo trainer should have a 'Hoarfox' at level 5", func() {

				mon := battle.Field().Teams()[0][0]
				So(mon.Species.Name, ShouldEqual, "Hoarfox")
				So(mon.Level, ShouldEqual, 5)
			})
		})
		Reset(func() {
			bs.Stop()
		})
	})
}

func TestRequestBattleCancel(t *testing.T) {
	Convey("Given a session and a connection", t, func() {
		ctx := context.TODO()
		sessions, conns := prepare()
		session := sessions[0]
		conn := NewTestWriter()
		battleDB := conns.BattleDB
		bs := conns.BattleServer.(battle.Server)
		bs.Start()
		go bs.Run()
		Reset(func() {
			bs.Stop()
		})

		// Create request
		msg := Message{
			ID:   1,
			Data: []byte(`{"breederID": 2}`),
		}
		if err := handleBattleRequest(ctx, conns, msg, conn, sessions[0]); err != nil {
			panic(err)
		}

		Convey("Sending invalid JSON", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{:}`),
			}
			err := handleBattleRequestCancel(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The request should not be accepted", func() {
				req, _ := battleDB.LoadRequest(1)
				So(req.Status, ShouldResemble, model.ReqStatus_Pending)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})
		Convey("Leaving out the data", func() {
			msg := Message{
				ID:   1,
				Data: []byte("{}"),
			}
			err := handleBattleRequestCancel(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should be notified", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})
		Convey("Sending a valid cancel message", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"requestID": 1}`),
			}
			Convey("as the creator", func() {
				err := handleBattleRequestCancel(ctx, conns, msg, conn, session)
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The request should be canceled", func() {
					req, _ := battleDB.LoadRequest(1)
					So(req.Status, ShouldEqual, model.ReqStatus_Canceled)
				})

				Convey("All participants should be notified", func() {
					req, _ := battleDB.LoadRequest(1)
					connTo := connectionPool[req.To].(*TestWriter)

					resFrom := fmt.Sprintf(`{"type":"%s","id":0,"responseID":1,"data":{"requestID":%d}}`, messages.Type_BattleRequestCancelResponse, 1)
					resTo := fmt.Sprintf(`{"type":"%s","id":0,"data":{"requestID":%d}}`, messages.Type_BattleRequestCancelResponse, 1)

					So(string(conn.data), ShouldResembleJSON, resFrom)
					So(string(connTo.data), ShouldResembleJSON, resTo)
				})

			})

			Convey("as the challenged breeder", func() {
				err := handleBattleRequestCancel(ctx, conns, msg, conn, sessions[1])
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The request should be canceled", func() {
					req, _ := battleDB.LoadRequest(1)
					So(req.Status, ShouldEqual, model.ReqStatus_Canceled)
				})

				Convey("All participants should be notified", func() {
					req, _ := battleDB.LoadRequest(1)
					connFrom := connectionPool[req.From].(*TestWriter)

					resFrom := fmt.Sprintf(`{"type":"%s","id":0,"data":{"requestID":%d}}`, messages.Type_BattleRequestCancelResponse, 1)
					resTo := fmt.Sprintf(`{"type":"%s","id":0,"responseID":1,"data":{"requestID":%d}}`, messages.Type_BattleRequestCancelResponse, 1)

					So(string(connFrom.data), ShouldResembleJSON, resFrom)
					So(string(conn.data), ShouldResembleJSON, resTo)
				})

			})

		})
		Convey("Canceling a non existent request", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"requestID": 3000}`),
			}
			err := handleBattleRequestCancel(ctx, conns, msg, conn, session)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, notFound)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})
		Convey("Canceling a request of another breeder", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 4}`),
			}
			if err := handleBattleRequest(ctx, conns, msg, conn, sessions[2]); err != nil {
				panic(err)
			}
			msg = Message{
				ID:   1,
				Data: []byte(`{"requestID": 2}`),
			}
			err := handleBattleRequestCancel(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, notAuthorized)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})

		})
	})
}

func TestSendMonster(t *testing.T) {
	Convey("Given an existing battle", t, func() {
		ctx := context.TODO()
		sessions, conns := prepare()
		conn := NewTestWriter()
		breederDB := conns.BreederDB
		bs := &testServer{}
		conns.BattleServer = bs
		t1 := model.Breeder{ID: 1}
		t2 := model.Breeder{ID: 2}
		tb := &testBattle{participants: battle.Participants{{t1}, {t2}}, field: NewTestBattleField()}
		bs.Create(tb)

		Convey("Sending a SendMonsterAction", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "teamIndex": 0, "location":{"side":0,"position":0}}`),
			}
			err := handleSendMonster(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})

			Convey("So the send action is a SendMonsterAction", func() {
				So(tb.sendAction, ShouldHaveSameTypeAs, &battle.SendMonsterAction{})

				breeder, _ := breederDB.Load(1)
				action := &battle.SendMonsterAction{
					Breeder:   &breeder,
					TeamIndex: 0,
					Position:  0,
					Side:      0,
					IssuedAction: battle.IssuedAction{
						Breeder: &breeder,
					},
					LocationAction: battle.LocationAction{
						Location: battle.Location{Side: 0, Position: 0},
					},
					ActionType: battle.BattleActionType_Player,
				}
				So(tb.sendAction, ShouldResemble, action)

			})
		})
		Convey("Sending invalid json", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{:}`),
			}
			err := handleSendMonster(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})

		Convey("Sending invalid data: BreederID", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": -1, "teamIndex": 0, "location":{"side":0,"position":0}}`),
			}
			err := handleSendMonster(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Sending invalid data: teamIndex", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "teamIndex": -1, "location":{"side":0,"position":0}}`),
			}
			err := handleSendMonster(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Sending invalid data: side", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "teamIndex": 0, "location":{"side":-1,"position":0}}`),
			}
			err := handleSendMonster(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Sending invalid data: position", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "teamIndex": 1, "location":{"side":0,"position":-1}}`),
			}
			err := handleSendMonster(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Trying to send as another breeder", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 3, "teamIndex": 1, "location":{"side":0,"position":0}}`),
			}
			err := handleSendMonster(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, unauthorized)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})

		Convey("Sending an invalid SendMonsterAction", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "teamIndex": 3, "location":{"side":0,"position":0}}`),
			}
			err := handleSendMonster(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

	})
}

func TestCapture(t *testing.T) {
	Convey("Given an existing battle", t, func() {
		ctx := context.TODO()
		sessions, conns := prepare()
		conn := NewTestWriter()
		bs := &testServer{}
		conns.BattleServer = bs
		t1 := model.Breeder{ID: 1}
		t2 := model.Breeder{ID: 2}
		tb := &testBattle{participants: battle.Participants{{t1}, {t2}}, field: NewTestBattleField()}
		bs.Create(tb)
		tb.Field().Objects()[0][0].Moves[0].IncreaseCooldown()
		tb.Field().Objects()[1][0].Moves[0].IncreaseCooldown()
		Convey("Sending a CaptureAction", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "item": "Capture Device", "actionFor":{"side":0,"position":0}, "target":{"side": 1, "position": 0}}`),
			}
			err := handleCapture(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})

			Convey("So the send action is a CaptureAction", func() {
				So(tb.sendAction, ShouldHaveSameTypeAs, &battle.CaptureAction{})
			})
		})
		Convey("Sending invalid json", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{:}`),
			}
			err := handleCapture(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})
		Convey("Sending invalid data: BreederID", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": -1, "item": "Capture Device", "actionFor":{"side":0,"position":0}, "target":{"side": 1, "position": 0}}`),
			}
			err := handleCapture(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Sending invalid data: item", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "item": "", "actionFor":{"side":0,"position":0}, "target":{"side": 1, "position": 0}}`),
			}
			err := handleCapture(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Sending invalid data: attacker side", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "item": "Capture Device", "actionFor":{"side":-1,"position":0}, "target":{"side": 1, "position": 0}}`),
			}
			err := handleCapture(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Sending invalid data: attacker position", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "item": "Capture Device", "actionFor":{"side":0,"position":-1}, "target":{"side": 1, "position": 0}}`),
			}
			err := handleCapture(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Sending invalid data: defender side", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "item": "Capture Device", "actionFor":{"side":0,"position":0}, "target":{"side": -1, "position": 0}}`),
			}
			err := handleCapture(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Sending invalid data: defender position", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "item": "Capture Device", "actionFor":{"side":0,"position":0}, "target":{"side": 1, "position": -1}}`),
			}
			err := handleCapture(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Trying to send as another breeder", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 3, "item": "Capture Device", "actionFor":{"side":0,"position":0}, "target":{"side": 1, "position": 0}}`),
			}
			err := handleCapture(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, unauthorized)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})

		Convey("Failing IsOK", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "item": "Potion", "actionFor":{"side":0,"position":0}, "target":{"side": 1, "position": 0}}`),
			}
			err := handleCapture(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})
	})
}

func TestAttack(t *testing.T) {
	Convey("Given an existing battle", t, func() {
		ctx := context.TODO()
		sessions, conns := prepare()
		conn := NewTestWriter()
		conns.BattleServer = &testServer{}
		bs := conns.BattleServer.(battle.Server)
		t1 := model.Breeder{ID: 1}
		t2 := model.Breeder{ID: 2}
		tb := &testBattle{participants: battle.Participants{{t1}, {t2}}, field: NewTestBattleField()}
		bs.Create(tb)
		tb.Field().Objects()[0][0].Moves[0].IncreaseCooldown()
		tb.Field().Objects()[1][0].Moves[0].IncreaseCooldown()
		Convey("Sending a AttackAction", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "moveIndex": 0, "attacker":{"side":0,"position":0}, "targets":[{"side": 1, "position": 0}]}`),
			}
			err := handleAttack(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})

			Convey("So the send action is a AttackAction", func() {
				So(tb.sendAction, ShouldHaveSameTypeAs, &battle.AttackAction{})
			})
		})
		Convey("Sending invalid json", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{:}`),
			}
			err := handleAttack(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})
		Convey("Sending invalid data: BreederID", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": -1, "moveIndex": 0, "attacker":{"side":0,"position":0}, "targets":[{"side": 1, "position": 0}]}`),
			}
			err := handleAttack(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Sending invalid data: moveIndex", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "moveIndex": -1, "attacker":{"side":0,"position":0}, "targets":[{"side": 1, "position": 0}]}`),
			}
			err := handleAttack(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Sending invalid data: attacker side", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "moveIndex": 0, "attacker":{"side":-1,"position":0}, "targets":[{"side": 1, "position": 0}]}`),
			}
			err := handleAttack(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Sending invalid data: attacker position", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "moveIndex": 0, "attacker":{"side":0,"position":-1}, "targets":[{"side": 1, "position": 0}]}`),
			}
			err := handleAttack(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Sending invalid data: empty targets", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "moveIndex": 0, "attacker":{"side":0,"position":0}, "targets":[]}`),
			}
			err := handleAttack(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, malformedRequest)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})

		Convey("Sending invalid data: defender side", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "moveIndex": 0, "attacker":{"side":0,"position":0}, "targets":[{"side": -1, "position": 0}]}`),
			}
			err := handleAttack(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Sending invalid data: defender position", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "moveIndex": 0, "attacker":{"side":0,"position":0}, "targets":[{"side": 1, "position": -1}]}`),
			}
			err := handleAttack(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})

		Convey("Trying to send as another breeder", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 3, "moveIndex": 0, "attacker":{"side":0,"position":0}, "targets":[{"side": 1, "position": 0}]}`),
			}
			err := handleAttack(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s"}`, messages.Type_Error, msg.ID, unauthorized)
				So(string(conn.data), ShouldResembleJSON, errMsg)
			})
		})

		Convey("Failing IsOK", func() {
			msg := Message{
				ID:   1,
				Data: []byte(`{"breederID": 1, "moveIndex": 2, "attacker":{"side":0,"position":0}, "targets":[{"side": 1, "position": 0}]}`),
			}
			err := handleAttack(ctx, conns, msg, conn, sessions[0])
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("So the send action is nil", func() {
				So(tb.sendAction, ShouldBeNil)
			})
			Convey("The user should receive an error", func() {
				errMsg := fmt.Sprintf(`{"type":"%s","id":0,"responseID":%d,"data":"%s`, messages.Type_Error, msg.ID, invalidData)
				So(string(conn.data), ShouldStartWith, errMsg)
			})
		})
	})
}

func prepare() ([]*Session, util.Connections) {
	conns := prepareConnections()
	_, s1 := breeder("test1", conns)
	_, s2 := breeder("test2", conns)
	_, s3 := breeder("test3", conns)
	_, s4 := breeder("test4", conns)

	return []*Session{s1, s2, s3, s4}, conns
}

func prepareConnections() util.Connections {
	return util.Connections{
		BreederDB:    memory.NewBreederDB(),
		MonsterDB:    memory.NewMonsterDB(),
		BattleDB:     memory.NewBattleDB(),
		BattleServer: battle.NewServer(),
		Overworld:    &testOverworld{},
		ItemDB:       memory.NewItemDB(),
	}

	//ctx = context.WithValue(ctx, contextKey("speciesLoader"), &json.SpeciesLoader{})

}

type testOverworld struct{}

func (t *testOverworld) AddPlayer(breederID model.ID, commands chan overworld.Command, updates chan overworld.Update) (uint64, error) {
	return 5, nil
}

func breeder(name string, conns util.Connections) (model.Breeder, *Session) {
	breeder := model.Breeder{
		Name: name,
	}

	breederDB := conns.BreederDB
	id, _ := breederDB.Create(breeder)
	breeder, _ = breederDB.Load(id)

	msg := Message{
		ID:   1,
		Data: []byte(fmt.Sprintf(`{"breederID":%d}`, id)),
	}

	session := &Session{}
	prepareSession(session)

	tw := NewTestWriter()
	handleLogin(context.TODO(), conns, msg, tw, session)

	monster := *model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 5, 5 }, func() (model.AttributeStat, model.AttributeStat) { return 0, 0 })

	monsterDB := conns.MonsterDB
	monsterId, _ := monsterDB.Create(monster)

	monster, _ = monsterDB.Load(monsterId)

	monsterDB.SaveTeamByBreederId(breeder.ID, []model.Monster{monster})
	return breeder, session
}

type testServer struct {
	battle.Server
	battle battle.Battle
}

func (ts *testServer) Create(battle battle.Battle) model.ID {
	ts.battle = battle
	ts.battle.SetID(1)
	return 1
}

func (ts *testServer) AddListener(id model.ID, ch chan battle.BattleResult) error {
	return nil
}

func (ts *testServer) GetByBreeder(id model.ID) (battle.Battle, error) {
	if ts.battle == nil {
		return nil, db.NotFoundError{}
	}

	for _, part := range ts.battle.Participants().Flatten() {
		if part.ID == id {
			return ts.battle, nil
		}

	}

	return nil, db.NotFoundError{}

}

type testBattle struct {
	battle.Battle
	sendAction   battle.BattleAction
	field        battle.BattleField
	participants battle.Participants
	id           model.ID
}

func (tb *testBattle) SetID(id model.ID) {
	tb.id = id
}

func (tb *testBattle) ID() model.ID {
	return tb.id
}

func (tb *testBattle) SendAction(action battle.BattleAction) {
	tb.sendAction = action
}

func (tb *testBattle) Field() battle.BattleField {
	return tb.field
}

func (tb *testBattle) Participants() battle.Participants {
	return tb.participants
}

type testBattleField struct {
	battle.BattleField
	field [][]*model.Monster
}

func NewTestBattleField() *testBattleField {

	mon := func() *model.Monster {
		return model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 5, 5 }, func() (model.AttributeStat, model.AttributeStat) { return 0, 0 })
	}

	tbf := &testBattleField{}

	tbf.field = [][]*model.Monster{
		[]*model.Monster{mon()},
		[]*model.Monster{mon()},
	}

	return tbf
}

func (tbf *testBattleField) Breeder() battle.Participants {
	breeder := make(battle.Participants, 2)
	breeder[0] = append(breeder[0], model.Breeder{ID: 1})
	breeder[1] = append(breeder[1], model.Breeder{ID: 2})

	return breeder
}

func (tbf *testBattleField) Teams() (teams map[model.ID][]*model.Monster) {
	teams = make(map[model.ID][]*model.Monster)
	teams[0] = make([]*model.Monster, 2)
	teams[1] = make([]*model.Monster, 2)

	for i := model.ID(0); i < 2; i++ {
		for j := 0; j < 2; j++ {
			teams[i][j] = model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 5, 5 }, func() (model.AttributeStat, model.AttributeStat) { return 0, 0 })
		}
	}

	return teams
}

func (tbf *testBattleField) Objects() [][]*model.Monster {
	return tbf.field

}

func (tbf *testBattleField) Inventories() map[model.ID][]*item.Item {
	m := map[model.ID][]*item.Item{
		1: []*item.Item{&item.Item{Base: model.ItemCache["Capture Device"], Amount: 10}},
	}

	return m
}

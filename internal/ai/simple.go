//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package ai

import (
	"encoding/json"

	"gitlab.com/kori-irrlicht/project-open-monster/internal/battle"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/server/messages"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

const pseudoID = 0

var pseudoBreeder = &model.Breeder{
	ID:   0,
	Name: "WildBattle",
}

type Battle interface {
	SendAction(battle.BattleAction)
}

type Simple struct {
	battle    Battle
	teams     map[int][]messages.MonsterView
	locations map[int][]messages.BattleLocation

	opponentIds []int
}

func NewSimple(battle Battle) *Container {
	return &Container{ai: &Simple{battle: battle}}

}

func (s *Simple) createSendMonster(teamIndex, side, position uint) battle.SendMonsterAction {
	return battle.SendMonsterAction{
		Breeder:   pseudoBreeder,
		TeamIndex: teamIndex,
		Position:  position,
		Side:      side,
		IssuedAction: battle.IssuedAction{
			Breeder: pseudoBreeder,
		},
		LocationAction: battle.LocationAction{
			Location: battle.Location{
				Position: position,
				Side:     side,
			},
		},
		ActionType: battle.BattleActionType_Player,
	}

}

func (s *Simple) createAttack(moveIndex, side, position uint, attacker int) battle.AttackAction {
	targetLocation := battle.Location{
		Side:     side,
		Position: position,
	}
	atkLocation := battle.Location{
		Side:     uint(s.locations[pseudoID][attacker].Side),
		Position: uint(s.locations[pseudoID][attacker].Position),
	}

	return battle.AttackAction{
		Move:             moveIndex,
		AttackerLocation: atkLocation,
		DefenderLocation: targetLocation, // TODO: Should allow multiple locations
		PlayerAction: battle.PlayerAction{
			IssuedAction: battle.IssuedAction{
				Breeder: pseudoBreeder,
			},
		},
		LocationAction: battle.LocationAction{
			Location: atkLocation,
		},
	}

}

func (s *Simple) handleCreation(m messages.BattleCreation) {
	s.teams = m.Teams
	s.locations = m.Locations

	for playerId, locations := range m.Locations {
		if playerId == pseudoID {
			continue
		}
		if s.locations[pseudoID][0].Side != locations[0].Side {
			s.opponentIds = append(s.opponentIds, playerId)

		}
	}

	action := s.createSendMonster(0, uint(s.locations[pseudoID][0].Side), uint(s.locations[pseudoID][0].Position))
	s.battle.SendAction(&action)
}

func (s *Simple) handleNextTurn() {

	targetId := s.opponentIds[0]

	action := s.createAttack(0, uint(s.locations[targetId][0].Side), uint(s.locations[targetId][0].Position), 0)
	s.battle.SendAction(&action)
}

type ai interface {
	handleCreation(messages.BattleCreation)
	handleNextTurn()
}

type Container struct {
	ai ai
}

// Implements MessageWriter from package internal/server
func (s *Container) Write(data []byte) (int, error) {
	var msg struct {
		Type       string          `json:"type"`
		ID         int             `json:"id"`
		ResponseID int             `json:"responseID,omitempty"`
		Data       json.RawMessage `json:"data"`
	}

	if err := json.Unmarshal(data, &msg); err != nil {
		return 0, err
	}

	switch msg.Type {
	case messages.Type_BattleCreation:
		var m messages.BattleCreation
		if err := json.Unmarshal(msg.Data, &m); err != nil {
			return 0, err
		}
		s.ai.handleCreation(m)

	case messages.Type_BattleResult:
		var m messages.BattleResult
		if err := json.Unmarshal(msg.Data, &m); err != nil {
			return 0, err
		}
		switch battle.ResultType(m.ResultType) {
		case battle.BattleResult_NextTurn:
			s.ai.handleNextTurn()
		case battle.BattleResult_Attack:
		case battle.BattleResult_Capture:
		case battle.BattleResult_Finished:
		case battle.BattleResult_SendMonster:
		case battle.BattleResult_Spawn:
		case battle.BattleResult_Item:
		default:
		}

	}

	return len(data), nil
}

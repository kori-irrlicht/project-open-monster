//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld_test

import (
	"testing"

	"github.com/EngoEngine/ecs"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
)

func TestControl(t *testing.T) {
	Convey("Given a new control system", t, func() {
		sys := overworld.ControlSystem{}
		Convey("Given a player", func() {

			player := overworld.Player{
				BasicEntity: ecs.NewBasic(),
			}
			player.Position.X = 0
			player.Position.Y = 0
			player.SpaceComponent.Height = 32
			player.SpaceComponent.Width = 32
			player.BreederID = 2

			inputChan := make(chan overworld.Command, 1)
			player.Input = inputChan

			sys.AddByInterface(&player)
			Convey("Removing the player", func() {
				sys.Remove(player.BasicEntity)
				Convey("The player is not updated", func() {
					player.Direction.X = 1
					player.Direction.Y = 1
					player.Speed = 16

					sys.Update(1)

					Convey("The player should not have moved on the x-axis", func() {
						So(player.Position.X, ShouldEqual, 0)
					})
					Convey("The player should not have moved on the y-axis", func() {
						So(player.Position.Y, ShouldEqual, 0)

					})

				})
			})
			Convey("The player is active", func() {
				player.IsActive = true

				Convey("The player is moving on the x-axis with a speed of 16", func() {
					player.Direction.X = 1
					player.Direction.Y = 0
					player.Speed = 16

					sys.Update(1)

					Convey("The player should be 16 units on the x-axis", func() {
						So(player.Position.X, ShouldEqual, 16)
					})
					Convey("The player should not have moved on the y-axis", func() {
						So(player.Position.Y, ShouldEqual, 0)

					})

				})

				Convey("The player is moving on the y-axis with a speed of 16", func() {
					player.Direction.X = 0
					player.Direction.Y = 1
					player.Speed = 16

					sys.Update(1)

					Convey("The player should be 16 units on the y-axis", func() {
						So(player.Position.Y, ShouldEqual, 16)
					})
					Convey("The player should not have moved on the x-axis", func() {
						So(player.Position.X, ShouldEqual, 0)

					})

				})

				Convey("The player is moving on the x-axis and the y-axis with a speed of 16", func() {
					player.Direction.X = 1
					player.Direction.Y = 1
					player.Speed = 16

					sys.Update(1)

					Convey("The player should be 16 units on the x-axis", func() {
						So(player.Position.X, ShouldEqual, 16)
					})
					Convey("The player should be 16 units on the y-axis", func() {
						So(player.Position.Y, ShouldEqual, 16)
					})

				})
			})
			Convey("The player is not active", func() {
				player.IsActive = false

				Convey("The player is moving on the x-axis with a speed of 16", func() {
					player.Direction.X = 1
					player.Direction.Y = 0
					player.Speed = 16

					sys.Update(1)

					Convey("The player should be 0 units on the x-axis", func() {
						So(player.Position.X, ShouldEqual, 0)
					})
					Convey("The player should not have moved on the y-axis", func() {
						So(player.Position.Y, ShouldEqual, 0)

					})

				})

				Convey("The player is moving on the y-axis with a speed of 16", func() {
					player.Direction.X = 0
					player.Direction.Y = 1
					player.Speed = 16

					sys.Update(1)

					Convey("The player should be 0 units on the y-axis", func() {
						So(player.Position.Y, ShouldEqual, 0)
					})
					Convey("The player should not have moved on the x-axis", func() {
						So(player.Position.X, ShouldEqual, 0)

					})

				})

				Convey("The player is moving on the x-axis and the y-axis with a speed of 16", func() {
					player.Direction.X = 1
					player.Direction.Y = 1
					player.Speed = 16

					sys.Update(1)

					Convey("The player should be 0 units on the x-axis", func() {
						So(player.Position.X, ShouldEqual, 0)
					})
					Convey("The player should be 0 units on the y-axis", func() {
						So(player.Position.Y, ShouldEqual, 0)
					})

				})
			})

		})
	})

}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld_test

import (
	"testing"

	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo"
	"github.com/EngoEngine/engo/common"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
)

func TestEvent(t *testing.T) {
	Convey("Given a new event system", t, func() {
		engo.Mailbox = &engo.MessageManager{}
		sys := &overworld.EventSystem{}
		sys.New(nil)

		e := eventEntity{}
		b := ecs.NewBasic()
		e.BasicEntity = &b
		sys.AddByInterface(e)

		Convey("Updating the system", func() {
			var dispatched bool
			engo.Mailbox.Listen("EventOutMessage", func(message engo.Message) {
				dispatched = true
			})
			sys.Update(1)
			Convey("No message should have been dispatched", func() {
				So(dispatched, ShouldBeFalse)
			})
		})

		Convey("Sending an InteractionMessage with an unknown entity", func() {
			var dispatched bool
			engo.Mailbox.Listen("EventOutMessage", func(message engo.Message) {
				dispatched = true
			})

			e1 := &interactionEntity{}
			e1.BasicEntity = e.BasicEntity
			e2 := &interactionEntity{}
			b := ecs.NewBasic()
			e2.BasicEntity = &b
			engo.Mailbox.Dispatch(overworld.InteractionMessage{
				Entity: e1,
				With:   e2,
			})
			sys.Update(1)
			Convey("No message should have been dispatched", func() {
				So(dispatched, ShouldBeFalse)
			})
		})
		Convey("Sending an InteractionMessage with an known entity", func() {
			var dispatched bool
			engo.Mailbox.Listen("EventOutMessage", func(message engo.Message) {
				dispatched = true
			})

			e1 := &interactionEntity{}
			e1.BasicEntity = e.BasicEntity
			e2 := &interactionEntity{}
			e2.BasicEntity = e.BasicEntity
			engo.Mailbox.Dispatch(overworld.InteractionMessage{
				Entity: e1,
				With:   e2,
			})
			sys.Update(1)
			Convey("A message should have been dispatched", func() {
				So(dispatched, ShouldBeTrue)
			})
		})
	})
}

type interactionEntity struct {
	*ecs.BasicEntity
	*common.SpaceComponent
	*overworld.WorldComponent
	*overworld.DirectionComponent
	*overworld.InteractionComponent
}

type eventEntity struct {
	*ecs.BasicEntity
	*overworld.EventComponent
}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld_test

import (
	"testing"

	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo"
	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/data"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
)

func init() {
	logrus.SetLevel(logrus.WarnLevel)

	err := data.Load("../../assets/data", "file")

	if err != nil {
		panic(err)
	}

}

func TestOverworld(t *testing.T) {
	Convey("Setting up the overworld", t, func() {
		engo.Mailbox = &engo.MessageManager{}

		ov := overworld.Overworld{}
		w := world{World: &ecs.World{}}
		ov.Setup(&w)

		Convey("The world should contain the InputSystem", func() {
			So(w.Systems()[0], ShouldHaveSameTypeAs, &overworld.InputSystem{})
		})
		Convey("The world should contain the ControlSystem", func() {
			So(w.Systems()[1], ShouldHaveSameTypeAs, &overworld.ControlSystem{})
		})
		Convey("The world should contain the CollisionSystem", func() {
			So(w.Systems()[2], ShouldHaveSameTypeAs, &overworld.CollisionSystem{})
			Convey("Walls should be added to the game", func() {
				wall := w.entities[0]
				So(wall, ShouldHaveSameTypeAs, &overworld.Wall{})
			})
		})
		Convey("The world should contain the EncounterSystem", func() {
			So(w.Systems()[3], ShouldHaveSameTypeAs, &overworld.EncounterSystem{})
			Convey("The system should contain encounter areas", func() {
				sys := w.Systems()[3].(*overworld.EncounterSystem)
				So(len(sys.Areas), ShouldBeGreaterThan, 0)
				So(sys.Areas["overworld"][0].GetActivityComponent().IsActive, ShouldBeTrue)
				So(sys.Areas["overworld"][0].GetEncounterComponent().IsArea, ShouldBeTrue)
				So(sys.Areas["overworld"][0].GetEncounterComponent().Area, ShouldEqual, "testing")
			})
		})
		Convey("The world should contain the InteractionSystem", func() {
			So(w.Systems()[4], ShouldHaveSameTypeAs, &overworld.InteractionSystem{})
		})
		Convey("The world should contain the WarpSystem", func() {
			So(w.Systems()[5], ShouldHaveSameTypeAs, &overworld.WarpSystem{})
		})
		Convey("The world should contain the PickupSystem", func() {
			So(w.Systems()[6], ShouldHaveSameTypeAs, &overworld.EventSystem{})
		})
		Convey("The world should contain the OutputSystem", func() {
			So(w.Systems()[7], ShouldHaveSameTypeAs, &overworld.OutputSystem{})
		})

	})
}

type world struct {
	*ecs.World
	entities []ecs.Identifier
}

func (w *world) AddEntity(e ecs.Identifier) {
	w.entities = append(w.entities, e)
	w.World.AddEntity(e)
}

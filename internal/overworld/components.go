//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld

import (
	"time"

	"github.com/EngoEngine/engo"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type WorldComponent struct {
	World string
	Area  engo.Point
}

func (c *WorldComponent) GetWorldComponent() *WorldComponent {
	return c
}

type WorldFace interface {
	GetWorldComponent() *WorldComponent
}

type DirectionComponent struct {
	Direction engo.Point
	Looking   engo.Point
	Speed     float32
}

func (c *DirectionComponent) GetDirectionComponent() *DirectionComponent {
	return c
}

type DirectionFace interface {
	GetDirectionComponent() *DirectionComponent
}

type InputComponent struct {
	Input chan Command
}

func (c *InputComponent) GetInputComponent() *InputComponent {
	return c
}

type InputFace interface {
	GetInputComponent() *InputComponent
}

type OutputComponent struct {
	Output chan Update
}

func (c *OutputComponent) GetOutputComponent() *OutputComponent {
	return c
}

type OutputFace interface {
	GetOutputComponent() *OutputComponent
}

type EncounterComponent struct {
	Area string

	// IsArea is false, if the component belongs to a player
	IsArea bool
}

func (c *EncounterComponent) GetEncounterComponent() *EncounterComponent {
	return c
}

type EncounterFace interface {
	GetEncounterComponent() *EncounterComponent
}

type ActivityComponent struct {
	// If true, this entity should be processed
	IsActive bool
}

func (c *ActivityComponent) GetActivityComponent() *ActivityComponent {
	return c
}

type ActivityFace interface {
	GetActivityComponent() *ActivityComponent
}

// PlayerComponent does nothing, but mark an entity as a player.
// If a system should only act on players, this can be used as a filter
type PlayerComponent struct {
	BreederID model.ID
}

func (c *PlayerComponent) GetPlayerComponent() *PlayerComponent {
	return c
}

type PlayerFace interface {
	GetPlayerComponent() *PlayerComponent
}

type NameComponent struct {
	Name string
}

func (c *NameComponent) GetNameComponent() *NameComponent {
	return c
}

type NameFace interface {
	GetNameComponent() *NameComponent
}

type WarpTargetComponent struct {
	World string
	To    string
}

func (c *WarpTargetComponent) GetWarpTargetComponent() *WarpTargetComponent {
	return c
}

type WarpTargetFace interface {
	GetWarpTargetComponent() *WarpTargetComponent
}

type InteractionComponent struct {
	// State determines if the interaction is active, passive or both
	State uint

	// Mask determines with which groups the entity actively interacts
	Mask uint

	// Group determines to which group the interaction belongs
	Group uint

	// The interaction range
	Range float32

	// The interaction area
	Area engo.AABB
}

func (c *InteractionComponent) GetInteractionComponent() *InteractionComponent {
	return c
}

type InteractionFace interface {
	GetInteractionComponent() *InteractionComponent
}

type DialogComponent struct {
	Name string
}

func (c *DialogComponent) GetDialogComponent() *DialogComponent {
	return c
}

type DialogFace interface {
	GetDialogComponent() *DialogComponent
}

type EncounterTrackerComponent struct {
	lastEncounter time.Time
}

func (c *EncounterTrackerComponent) GetEncounterTrackerComponent() *EncounterTrackerComponent {
	return c
}

type EncounterTrackerFace interface {
	GetEncounterTrackerComponent() *EncounterTrackerComponent
}

type ItemComponent struct {
	Item   string
	Amount int
}

func (c *ItemComponent) GetItemComponent() *ItemComponent {
	return c
}

type ItemFace interface {
	GetItemComponent() *ItemComponent
}

type IdComponent struct {
	Id string
}

func (c *IdComponent) GetIdComponent() *IdComponent {
	return c
}

type IdFace interface {
	GetIdComponent() *IdComponent
}

type EventComponent struct {
	Event string
}

func (c *EventComponent) GetEventComponent() *EventComponent {
	return c
}

type EventFace interface {
	GetEventComponent() *EventComponent
}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld

import (
	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo"
	"github.com/EngoEngine/engo/common"
)

type interactionEntity struct {
	*ecs.BasicEntity
	*common.SpaceComponent
	*WorldComponent
	*DirectionComponent
	*InteractionComponent
}

type Interactable interface {
	common.BasicFace
	common.SpaceFace
	WorldFace
	DirectionFace
	InteractionFace
}

// InteractionMessage is sent whenever a interaction is detected by the InteractionSystem.
type InteractionMessage struct {
	Entity Interactable
	With   Interactable
}

// Type implements the engo.Message interface
func (InteractionMessage) Type() string { return "InteractionMessage" }

type InteractionSystem struct {
	entities []interactionEntity
	Active   uint
	Passive  uint
}

func (c *InteractionSystem) Add(basic *ecs.BasicEntity, space *common.SpaceComponent, world *WorldComponent, direction *DirectionComponent, interaction *InteractionComponent) {
	c.entities = append(c.entities, interactionEntity{basic, space, world, direction, interaction})
}

func (c *InteractionSystem) AddByInterface(o ecs.Identifier) {
	obj := o.(Interactable)
	c.Add(obj.GetBasicEntity(), obj.GetSpaceComponent(), obj.GetWorldComponent(), obj.GetDirectionComponent(), obj.GetInteractionComponent())
}

func (c *InteractionSystem) Remove(basic ecs.BasicEntity) {
	delete := -1
	for index, e := range c.entities {
		if e.BasicEntity.ID() == basic.ID() {
			delete = index
			break
		}
	}
	if delete >= 0 {
		c.entities = append(c.entities[:delete], c.entities[delete+1:]...)
	}
}

func (c *InteractionSystem) Update(dt float32) {
	for _, e1 := range c.entities {
		// Only Active entities should be considered
		if e1.InteractionComponent.State&c.Active == 0 {
			continue
		}
		for _, e2 := range c.entities {

			// Skip itself
			if e1.ID() == e2.ID() {
				continue
			}

			// Only Passive entities should be considered
			if e2.InteractionComponent.State&c.Passive == 0 {
				continue
			}

			// They can't interact if they are in different worlds
			if e1.WorldComponent.World != e2.WorldComponent.World {
				continue
			}

			group := e2.Group & e1.Mask
			// They can't interact if their groups don't match
			if group == 0 {
				continue
			}

			to := e1.DirectionComponent.Looking
			to.MultiplyScalar(e1.InteractionComponent.Range)
			to.Add(e1.Center())

			l := Line{From: e1.Center(), To: to}
			if l.CollidesAABB(e2.AABB()) || common.IsIntersecting(e1.InteractionComponent.Area, e2.GetSpaceComponent().AABB()) {

				engo.Mailbox.Dispatch(InteractionMessage{
					Entity: e1,
					With:   e2,
				})
			}

		}
	}
}

type Line struct {
	From engo.Point
	To   engo.Point
}

func (l Line) Intersects(l2 Line) bool {
	xdiff := engo.Point{X: l.From.X - l.To.X, Y: l2.From.X - l2.To.X}
	ydiff := engo.Point{X: l.From.Y - l.To.Y, Y: l2.From.Y - l2.To.Y}

	return Determinant(xdiff, ydiff) != 0
}

func Determinant(p1, p2 engo.Point) float32 {
	return p1.X*p2.Y - p1.Y*p2.X
}

func (l Line) CollidesAABB(aabb engo.AABB) bool {
	lines := []Line{
		{
			From: aabb.Min,
			To:   engo.Point{X: aabb.Min.X, Y: aabb.Max.Y},
		},
		{
			From: aabb.Min,
			To:   engo.Point{X: aabb.Max.X, Y: aabb.Min.Y},
		},
		{
			From: aabb.Max,
			To:   engo.Point{X: aabb.Min.X, Y: aabb.Max.Y},
		},
		{
			From: aabb.Max,
			To:   engo.Point{X: aabb.Max.X, Y: aabb.Min.Y},
		},
	}

	// check if the aabb of the line is intersecting the aabb
	// must be checked, since the line might be inside the aabb
	// also, it is faster than the line-line checks, if there is no way that the line collides with the aabb, we can return faster

	laabb := engo.AABB{Min: l.From, Max: l.To}

	// Fix the min and max points, if their order is wrong
	if laabb.Max.X < laabb.Min.X && laabb.Max.Y < laabb.Min.Y {
		laabb.Min, laabb.Max = laabb.Max, laabb.Min
	} else if laabb.Max.X < laabb.Min.X {
		laabb.Min.X, laabb.Max.X = laabb.Max.X, laabb.Min.X
	} else if laabb.Max.Y < laabb.Min.Y {
		laabb.Min.Y, laabb.Max.Y = laabb.Max.Y, laabb.Min.Y
	}

	if !common.IsIntersecting(laabb, aabb) {
		// If the AABBs are not colliding, the line won't collide with the aabb
		return false
	}

	// If a line is intersected, the line intersects the aabb
	for _, line := range lines {
		if l.Intersects(line) {
			return true
		}
	}

	return false

}

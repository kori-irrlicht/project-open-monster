//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld_test

import (
	"math/rand"
	"testing"

	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo"
	"github.com/EngoEngine/engo/common"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
)

func TestEncounter(t *testing.T) {

	Convey("Given a new encounter system", t, func() {
		engo.Mailbox = &engo.MessageManager{}
		sys := overworld.EncounterSystem{}
		Convey("Adding some encounter areas", func() {
			areas := []overworld.Encounter{}
			areas = append(areas, overworld.Encounter{
				BasicEntity: ecs.NewBasic(),
				SpaceComponent: common.SpaceComponent{
					Position: engo.Point{},
					Height:   32,
					Width:    32,
				},
				ActivityComponent: overworld.ActivityComponent{
					IsActive: true,
				},
				EncounterComponent: overworld.EncounterComponent{
					IsArea: true,
					Area:   "testing",
				},
				WorldComponent: overworld.WorldComponent{
					World: "overworld",
				},
			})
			sys.SetAreas(map[string][]overworld.Encounter{"overworld": areas})

			Convey("Adding a player", func() {
				player := overworld.Player{
					BasicEntity: ecs.NewBasic(),
				}
				player.SpaceComponent.Height = 32
				player.SpaceComponent.Width = 32
				player.BreederID = 2
				player.IsActive = true
				player.WorldComponent.World = "overworld"

				sys.AddByInterface(&player)

				Convey("Removing the player", func() {
					sys.Remove(player.BasicEntity)
					Convey("The player is not updated", func() {
						player.Position = engo.Point{}
						Convey("An encounter message should not be dispatched", func() {
							dispatched := false
							engo.Mailbox.Listen("EncounterMessage", func(message engo.Message) {
								dispatched = true
							})
							sys.Update(1)
							So(dispatched, ShouldBeFalse)
						})

					})

				})

				Convey("Moving the player into the encounter area", func() {
					player.Position = engo.Point{}
					Convey("An encounter message should be dispatched", func() {
						dispatched := false
						var msg overworld.EncounterMessage
						engo.Mailbox.Listen("EncounterMessage", func(message engo.Message) {
							dispatched = true
							msg, _ = message.(overworld.EncounterMessage)
						})

						overworld.Random = rand.New(&fakeSource{i: 1})
						sys.Update(1)
						So(dispatched, ShouldBeTrue)
						Convey("The entity id should match the player", func() {
							So(msg.ID, ShouldEqual, player.ID())
						})
						Convey("The msg should contain the area name", func() {
							So(msg.Area, ShouldEqual, areas[0].EncounterComponent.Area)
						})
					})

				})

				Convey("Keeping the player out of the encounter area", func() {
					player.Position = engo.Point{X: 100, Y: 100}
					Convey("An encounter message should be dispatched", func() {
						dispatched := false
						engo.Mailbox.Listen("EncounterMessage", func(message engo.Message) {
							dispatched = true
						})
						sys.Update(1)
						So(dispatched, ShouldBeFalse)
					})
				})
			})
		})
	})

}

type fakeSource struct{ i int64 }

func (f *fakeSource) Int63() int64 {
	return f.i
}
func (f *fakeSource) Seed(i int64) {}

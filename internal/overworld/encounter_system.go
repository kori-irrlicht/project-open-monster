//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld

import (
	"math/rand"
	"time"

	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo"
	"github.com/EngoEngine/engo/common"
)

/* #nosec */
var Random = rand.New(rand.NewSource(time.Now().UnixNano()))

type encounterEntity struct {
	*ecs.BasicEntity
	*common.SpaceComponent
	*PlayerComponent
	*ActivityComponent
	*WorldComponent
	*EncounterTrackerComponent
}

type Encounterable interface {
	common.BasicFace
	common.SpaceFace
	PlayerFace
	ActivityFace
	WorldFace
	EncounterTrackerFace
}

type EncounterSystem struct {
	// areas are static components and must be added on creation
	Areas    map[string][]Encounter
	entities []encounterEntity
}

func (c *EncounterSystem) Add(basic *ecs.BasicEntity, space *common.SpaceComponent, player *PlayerComponent, activity *ActivityComponent, world *WorldComponent, enc *EncounterTrackerComponent) {
	c.entities = append(c.entities, encounterEntity{basic, space, player, activity, world, enc})
}

func (c *EncounterSystem) AddByInterface(o ecs.Identifier) {
	obj := o.(Encounterable)
	c.Add(obj.GetBasicEntity(), obj.GetSpaceComponent(), obj.GetPlayerComponent(), obj.GetActivityComponent(), obj.GetWorldComponent(), obj.GetEncounterTrackerComponent())
}

func (c *EncounterSystem) Remove(basic ecs.BasicEntity) {
	delete := -1
	for index, e := range c.entities {
		if e.BasicEntity.ID() == basic.ID() {
			delete = index
			break
		}
	}
	if delete >= 0 {
		c.entities = append(c.entities[:delete], c.entities[delete+1:]...)
	}
}

func (c *EncounterSystem) Update(dt float32) {
	for _, e := range c.entities {
		if !e.IsActive {
			continue
		}
		for _, a := range c.Areas[e.World] {
			if !a.IsActive {
				continue
			}

			if e.SpaceComponent.Center().Within(a) {

				if time.Since(e.EncounterTrackerComponent.lastEncounter) > time.Second*5 && Random.Intn(600) < 15 {
					engo.Mailbox.Dispatch(EncounterMessage{e.BasicEntity.ID(), a.GetEncounterComponent().Area})
					e.IsActive = false
				}
			}
		}
	}
}

func (c *EncounterSystem) SetAreas(areas map[string][]Encounter) {
	c.Areas = areas
}

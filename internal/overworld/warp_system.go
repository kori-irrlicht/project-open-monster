//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld

import (
	"fmt"

	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo"
	"github.com/EngoEngine/engo/common"
)

type warpEntity struct {
	*ecs.BasicEntity
	*common.SpaceComponent
	*WorldComponent
}

type Warpable interface {
	common.BasicFace
	common.SpaceFace
	WorldFace
}

type WarpMessage struct {
	ID    uint64
	World string
	To    string
}

// Type implements the engo.Message interface
func (WarpMessage) Type() string { return "WarpMessage" }

type WarpSystem struct {
	entities    map[uint64]warpEntity
	WarpTargets map[string]map[string]*WarpTarget
	WarpPoints  []*WarpPoint
}

func (c *WarpSystem) New(w *ecs.World) {
	c.entities = make(map[uint64]warpEntity)
	c.WarpTargets = map[string]map[string]*WarpTarget{
		"meta": {},
	}

	engo.Mailbox.Listen("WarpMessage", func(message engo.Message) {
		if m, ok := message.(WarpMessage); ok {

			if e, ok2 := c.entities[m.ID]; ok2 {

				fmt.Println(m)
				if target, ok3 := c.WarpTargets[m.World][m.To]; ok3 {

					fmt.Printf("%+v\n", e.WorldComponent)
					e.WorldComponent.World = target.World
					e.WorldComponent.Area = target.Area

					e.SpaceComponent.Position.X = target.Position.X
					e.SpaceComponent.Position.Y = target.Position.Y

					engo.Mailbox.Dispatch(AreaChangeMessage{
						ID:    e.BasicEntity.ID(),
						World: e.WorldComponent.World,
						Area:  e.WorldComponent.Area,
					})
				}
			}
		}
	})
}

func (c *WarpSystem) AddTarget(target *WarpTarget) {
	if _, ok := c.WarpTargets[target.World]; !ok {
		c.WarpTargets[target.World] = make(map[string]*WarpTarget)
	}
	c.WarpTargets[target.World][target.Name] = target

	fmt.Println(target)

	// TODO: Use the type attribute set by Tiled instead of the name
	if target.Name == "startPoint" {
		c.WarpTargets["meta"][target.Name] = target
	}
}

func (c *WarpSystem) AddPoint(point *WarpPoint) {
	c.WarpPoints = append(c.WarpPoints, point)
}

func (c *WarpSystem) Add(basic *ecs.BasicEntity, space *common.SpaceComponent, world *WorldComponent) {
	c.entities[basic.ID()] = warpEntity{basic, space, world}
}

func (c *WarpSystem) AddByInterface(o ecs.Identifier) {
	obj := o.(Warpable)
	c.Add(obj.GetBasicEntity(), obj.GetSpaceComponent(), obj.GetWorldComponent())
}

func (c *WarpSystem) Remove(basic ecs.BasicEntity) {
	delete(c.entities, basic.ID())
}

func (c *WarpSystem) Update(dt float32) {
	for _, e := range c.entities {

		for _, p := range c.WarpPoints {
			if e.Center().Within(p) && e.WorldComponent.World == p.WorldComponent.World {
				engo.Mailbox.Dispatch(WarpMessage{
					ID:    e.BasicEntity.ID(),
					World: p.GetWarpTargetComponent().World,
					To:    p.GetWarpTargetComponent().To,
				})
			}
		}
	}
}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld

import (
	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo"
	"github.com/EngoEngine/engo/common"
)

type eventEntity struct {
	*ecs.BasicEntity
	*EventComponent
}

type Eventable interface {
	common.BasicFace
	EventFace
}

type EventSystem struct {
	entities map[uint64]eventEntity
	current  map[uint64]bool
	toHandle []InteractionMessage
}

type EventOutMessage struct {
	Entity Interactable
	Event  Eventable
}

// Type implements the engo.Message interface
func (EventOutMessage) Type() string { return "EventOutMessage" }

func (s *EventSystem) New(*ecs.World) {
	s.entities = make(map[uint64]eventEntity)
	s.current = make(map[uint64]bool)

	engo.Mailbox.Listen("InteractionMessage", func(message engo.Message) {
		event, ok := message.(InteractionMessage)
		if !ok {
			return
		}
		s.toHandle = append(s.toHandle, event)
	})
}

func (c *EventSystem) Add(basic *ecs.BasicEntity, event *EventComponent) {
	c.entities[basic.ID()] = eventEntity{basic, event}
}

func (c *EventSystem) AddByInterface(o ecs.Identifier) {
	obj := o.(Eventable)
	c.Add(obj.GetBasicEntity(), obj.GetEventComponent())
}

func (c *EventSystem) Remove(basic ecs.BasicEntity) {
	delete(c.entities, basic.ID())
}

func (c *EventSystem) Update(dt float32) {
	current := make(map[uint64]bool)

	toHandle := c.toHandle
	c.toHandle = []InteractionMessage{}

	for _, event := range toHandle {

		if e, ok := c.entities[event.With.ID()]; ok {

			if _, ok := c.current[event.Entity.ID()]; !ok {

				dm := EventOutMessage{
					Entity: event.Entity,
					Event:  e,
				}
				engo.Mailbox.Dispatch(dm)

			}

			current[event.Entity.ID()] = true

		}
	}

	c.current = current
}

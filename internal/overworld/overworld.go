//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld

import (
	"fmt"
	"strconv"

	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo"
	"github.com/Noofbiz/tmx"
	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

const PlayerSize = 16

var Maps = make(map[string]*tmx.Map)

func Start(db OverworldDB) *Overworld {
	opts := engo.RunOptions{
		HeadlessMode: true,
		FPSLimit:     30,
	}

	world := &Overworld{db: db}
	go engo.Run(opts, world)

	return world
}

func Stop() {
	engo.Exit()
}

type Overworld struct {
	db OverworldDB

	world gameWorld
	tiles map[string]map[engo.Point]WorldCoordinates
}

// Preload implements engo.Scene
func (g *Overworld) Preload() {}

func (g *Overworld) addSystems() {
	w := g.world
	// Create encounter areas
	{
		var inputable *Inputable
		w.AddSystemInterface(&InputSystem{world: w}, inputable, nil)
	}
	{
		var controllable *Controllable
		w.AddSystemInterface(&ControlSystem{}, controllable, nil)
	}

	{
		var collisionable *Collisionable
		w.AddSystemInterface(&CollisionSystem{Solids: wallCollision | npcCollision | itemCollision}, collisionable, nil)
	}
	{
		var encounterable *Encounterable
		system := &EncounterSystem{Areas: map[string][]Encounter{}}
		w.AddSystemInterface(system, encounterable, nil)
	}
	{
		var interactable *Interactable
		w.AddSystemInterface(&InteractionSystem{
			Active:  InteractionStateActive,
			Passive: InteractionStatePassive,
		}, interactable, nil)
	}
	{
		var warpable *Warpable
		w.AddSystemInterface(&WarpSystem{}, warpable, nil)
	}
	{
		var eventable *Eventable
		w.AddSystemInterface(&EventSystem{}, eventable, nil)
	}
	{
		var outputable *Outputable
		var renderable *Renderable
		w.AddSystemInterface(&OutputSystem{tiles: &g.tiles}, []interface{}{outputable, renderable}, nil)
	}

}

// Setup implements engo.Scene
func (g *Overworld) Setup(u engo.Updater) {
	w, _ := u.(gameWorld)
	g.world = w
	g.tiles = make(map[string]map[engo.Point]WorldCoordinates)

	g.addSystems()
	g.parseWorldData()

}

func (g *Overworld) parseWorldData() {

	parseProperties := func(props []tmx.Property) (offset, area engo.Point, world, areaName string) {
		for _, prop := range props {
			switch prop.Name {
			case "offsetX":
				x, _ := strconv.Atoi(prop.Value)
				offset.X = float32(x)
			case "offsetY":
				y, _ := strconv.Atoi(prop.Value)
				offset.Y = float32(y)
			case "x":
				x, _ := strconv.Atoi(prop.Value)
				area.X = float32(x)
			case "y":
				y, _ := strconv.Atoi(prop.Value)
				area.Y = float32(y)
			case "world":
				world = prop.Value
			case "area":
				areaName = prop.Value
			default:
				logrus.WithField("name", prop.Name).Warnln("Unknown property")
			}
		}
		return

	}

	createWarpPoints := func(warpGroup tmx.ObjectGroup, offset engo.Point, world string, area engo.Point) (warpPoints []WarpPoint) {
		parseWarpProperties := func(props []tmx.Property) (world, to string) {
			for _, prop := range props {
				switch prop.Name {
				case "world":
					world = prop.Value
				case "to":
					to = prop.Value
				default:
					logrus.WithField("name", prop.Name).Warnln("Unknown property")
				}
			}
			return

		}
		for _, obj := range warpGroup.Objects {
			point := WarpPoint{}
			point.SpaceComponent.Width = float32(obj.Width)
			point.SpaceComponent.Height = float32(obj.Height)
			point.SpaceComponent.Position.X = float32(obj.X) + offset.X
			point.SpaceComponent.Position.Y = float32(obj.Y) + offset.Y

			point.WorldComponent.World = world
			point.WorldComponent.Area = area

			w, n := parseWarpProperties(obj.Properties)
			point.WarpTargetComponent.World = w
			point.WarpTargetComponent.To = n

			warpPoints = append(warpPoints, point)
		}

		return
	}

	createWarpTargets := func(warpGroup tmx.ObjectGroup, offset engo.Point, world string, area engo.Point) (warpTargets []WarpTarget) {
		for _, obj := range warpGroup.Objects {
			target := WarpTarget{}
			target.SpaceComponent.Position.X = float32(obj.X) + offset.X
			target.SpaceComponent.Position.Y = float32(obj.Y) + offset.Y

			target.WorldComponent.World = world
			target.WorldComponent.Area = area

			target.Name = obj.Name

			warpTargets = append(warpTargets, target)
		}
		return
	}

	createEncounterAreas := func(encounterGroup tmx.ObjectGroup, offset engo.Point, world string) (encounterAreas []Encounter) {

		for _, obj := range encounterGroup.Objects {
			area := Encounter{BasicEntity: ecs.NewBasic()}
			area.SpaceComponent.Width = float32(obj.Width)
			area.SpaceComponent.Height = float32(obj.Height)
			area.SpaceComponent.Position.X = float32(obj.X) + offset.X
			area.SpaceComponent.Position.Y = float32(obj.Y) + offset.Y

			area.WorldComponent.World = world

			area.EncounterComponent.IsArea = true
			for _, prop := range obj.Properties {
				if prop.Name == "area" {
					area.EncounterComponent.Area = prop.Value
					area.ActivityComponent.IsActive = true
				}
			}
			encounterAreas = append(encounterAreas, area)
		}
		return
	}

	createNpcs := func(npcGroup tmx.ObjectGroup, offset engo.Point, world string, tileWidth, tileHeight int) (npcs []NPC) {
		parseNpcProperties := func(props []tmx.Property) (dialog string) {
			for _, prop := range props {
				switch prop.Name {
				case "dialog":
					dialog = prop.Value
				default:
					logrus.WithField("name", prop.Name).Warnln("Unknown property")
				}
			}
			return

		}
		for _, obj := range npcGroup.Objects {
			dialog := parseNpcProperties(obj.Properties)

			npc := NPC{BasicEntity: ecs.NewBasic()}
			npc.SpaceComponent.Width = float32(tileWidth)
			npc.SpaceComponent.Height = float32(tileHeight)
			npc.SpaceComponent.Position.X = float32(obj.X) + offset.X
			npc.SpaceComponent.Position.Y = float32(obj.Y) + offset.Y

			npc.WorldComponent.World = world

			npc.InteractionComponent.State = InteractionStatePassive
			npc.InteractionComponent.Group = InteractionDialog

			npc.DialogComponent.Name = dialog

			npc.CollisionComponent.Group = npcCollision

			npcs = append(npcs, npc)

		}
		return

	}

	createItems := func(itemGroup tmx.ObjectGroup, offset engo.Point, world string, tileWidth, tileHeight int) (items []Item) {
		parseItemProperties := func(props []tmx.Property) (amount int, id string) {
			for _, prop := range props {
				switch prop.Name {
				case "amount":
					var err error
					amount, err = strconv.Atoi(prop.Value)
					if err != nil {
						logrus.WithField("amount", prop.Value).Errorln("Can't parse item amount")
					}

				case "id":
					id = prop.Value

				default:
					logrus.WithField("name", prop.Name).Warnln("Unknown property")
				}
			}
			return

		}
		for _, obj := range itemGroup.Objects {
			amount, id := parseItemProperties(obj.Properties)

			item := Item{BasicEntity: ecs.NewBasic()}
			item.SpaceComponent.Width = float32(tileWidth)
			item.SpaceComponent.Height = float32(tileHeight)
			item.SpaceComponent.Position.X = float32(obj.X) + offset.X
			item.SpaceComponent.Position.Y = float32(obj.Y) + offset.Y

			item.WorldComponent.World = world

			item.InteractionComponent.State = InteractionStatePassive
			item.InteractionComponent.Group = InteractionPickup

			item.CollisionComponent.Group = itemCollision

			item.ItemComponent.Amount = amount
			item.ItemComponent.Item = obj.Name

			item.IdComponent.Id = id

			items = append(items, item)

		}
		return

	}

	createInteractable := func(objGroup tmx.ObjectGroup, offset engo.Point, world string, tileWidth, tileHeight int) (items []Item, npcs []NPC) {
		parseProperties := func(props []tmx.Property) (lua string) {
			for _, prop := range props {
				switch prop.Name {
				case "lua":
					lua = prop.Value

				default:
					logrus.WithField("name", prop.Name).Warnln("Unknown property")
				}
			}
			return

		}
		for _, obj := range objGroup.Objects {
			lua := parseProperties(obj.Properties)
			switch obj.Type {
			case "item":
				item := Item{BasicEntity: ecs.NewBasic()}
				item.SpaceComponent.Width = float32(tileWidth)
				item.SpaceComponent.Height = float32(tileHeight)
				item.SpaceComponent.Position.X = float32(obj.X) + offset.X
				item.SpaceComponent.Position.Y = float32(obj.Y) + offset.Y

				item.WorldComponent.World = world

				item.InteractionComponent.State = InteractionStatePassive
				item.InteractionComponent.Group = InteractionPickup

				item.CollisionComponent.Group = itemCollision

				item.EventComponent.Event = lua

				items = append(items, item)
			case "npc":
				npc := NPC{BasicEntity: ecs.NewBasic()}
				npc.SpaceComponent.Width = float32(tileWidth)
				npc.SpaceComponent.Height = float32(tileHeight)
				npc.SpaceComponent.Position.X = float32(obj.X) + offset.X
				npc.SpaceComponent.Position.Y = float32(obj.Y) + offset.Y

				npc.WorldComponent.World = world

				npc.InteractionComponent.State = InteractionStatePassive
				npc.InteractionComponent.Group = InteractionDialog

				npc.CollisionComponent.Group = npcCollision
				npc.EventComponent.Event = lua

				npcs = append(npcs, npc)

			}

		}
		return

	}

	createWalls := func(layers []tmx.Layer, tilesets []tmx.Tileset, offset engo.Point, mapWidth, tileWidth, tileHeight int) (walls []Wall, gids []int) {
		// Collect wall tiles
		objects := make(map[uint32][]tmx.ObjectGroup)
		for _, tileset := range tilesets {
			for _, tile := range tileset.Tiles {
				objects[tile.ID+1] = tile.ObjectGroup // +1 because somehow the IDs are one off in the exported tmx file
			}
		}

		// Create walls
		for _, layer := range layers {
			for _, data := range layer.Data {
				for i, tile := range data.Tiles {
					obj := objects[tile.GID]
					gids = append(gids, int(tile.GID))
					if len(obj) == 0 {
						continue
					}

					wall := Wall{BasicEntity: ecs.NewBasic()}
					wallObj := obj[0].Objects[0]

					col := i % mapWidth
					row := i / mapWidth

					wall.SpaceComponent.Position.X = float32(col*tileWidth) + float32(wallObj.X) + offset.X
					wall.SpaceComponent.Position.Y = float32(row*tileHeight) + float32(wallObj.Y) + offset.Y
					wall.SpaceComponent.Width = float32(wallObj.Width)
					wall.SpaceComponent.Height = float32(wallObj.Height)

					wall.CollisionComponent.Group = wallCollision

					walls = append(walls, wall)

				}
			}

		}
		return

	}

	logger := logrus.WithField("method", "Overworld.parseWorldData")

	var encounterSystem *EncounterSystem
	var warpSystem *WarpSystem
	for _, s := range g.world.Systems() {
		switch t := s.(type) {
		case *EncounterSystem:
			encounterSystem = t
		case *WarpSystem:
			warpSystem = t
		}
	}

	for _, m := range Maps {

		offset, area, world, areaName := parseProperties(m.Properties)
		logger.WithFields(logrus.Fields{
			"offset":   offset,
			"area":     area,
			"world":    world,
			"areaName": areaName,
		}).Infoln("parsed properties")

		for _, objGroup := range m.ObjectGroups {
			switch objGroup.Name {
			case "Battle":
				encounterAreas := createEncounterAreas(objGroup, offset, world)
				encounterSystem.Areas[world] = append(encounterSystem.Areas[world], encounterAreas...)
			case "Warp":
				warpPoints := createWarpPoints(objGroup, offset, world, area)
				for _, point := range warpPoints {
					point := point
					warpSystem.AddPoint(&point)
				}
			case "WarpTarget":
				warpTargets := createWarpTargets(objGroup, offset, world, area)
				for _, target := range warpTargets {
					target := target
					warpSystem.AddTarget(&target)
				}

			case "npc":
				npcs := createNpcs(objGroup, offset, world, m.TileWidth, m.TileHeight)
				for _, npc := range npcs {
					npc := npc
					g.world.AddEntity(&npc)
				}

			case "Items":
				items := createItems(objGroup, offset, world, m.TileWidth, m.TileHeight)
				for _, item := range items {
					item := item
					g.world.AddEntity(&item)
				}

			case "Interactable":
				items, npcs := createInteractable(objGroup, offset, world, m.TileWidth, m.TileHeight)
				for _, item := range items {
					item := item
					g.world.AddEntity(&item)
				}
				for _, npc := range npcs {
					npc := npc
					g.world.AddEntity(&npc)
				}

			}
		}

		walls, ids := createWalls(m.Layers, m.Tilesets, offset, m.Width, m.TileWidth, m.TileHeight)
		for _, w := range walls {
			w := w
			w.World = world
			g.world.AddEntity(&w)
		}

		if _, ok := g.tiles[world]; !ok {
			g.tiles[world] = make(map[engo.Point]WorldCoordinates)
		}

		g.tiles[world][area] = WorldCoordinates{
			World:    world,
			AreaName: areaName,
			Tiles:    ids,
			Offset:   offset,
			Area:     area,
			Size:     engo.Point{X: float32(m.Width), Y: float32(m.Height)},
		}

	}
}

// Type implements engo.Scene
func (g *Overworld) Type() string { return "overworld" }

// AddPlayer registers a player to the ingame world. It should be called, after a player has logged in.
//
// commands channel is the input channel to control the player character.
// updates channel is the output channel and contains the updated actions of the players.
func (g *Overworld) AddPlayer(breederID model.ID, commands chan Command, updates chan Update) (uint64, error) {
	logger := logrus.WithField("method", "AddPlayer")
	logger.Infof("Adding player with id: %d", breederID)
	player := Player{
		BasicEntity: ecs.NewBasic(),
		InputComponent: InputComponent{
			Input: commands,
		},
		OutputComponent: OutputComponent{
			Output: updates,
		},
	}
	pos, err := g.db.LoadPosition(breederID)
	if err != nil {
		return 0, fmt.Errorf("could not load position: %w", err)
	}
	player.SpaceComponent.Position.X = pos.X
	player.SpaceComponent.Position.Y = pos.Y
	player.SpaceComponent.Width = 16
	player.SpaceComponent.Height = 16

	player.WorldComponent.World = "overworld"
	player.WorldComponent.Area = engo.Point{}

	player.CollisionComponent.Main = wallCollision | npcCollision | playerCollision | itemCollision
	player.CollisionComponent.Group = playerCollision

	player.ActivityComponent.IsActive = true

	player.InteractionComponent.State = InteractionStatePassive
	player.InteractionComponent.Mask = InteractionDialog | InteractionPickup
	player.InteractionComponent.Range = 24

	player.PlayerComponent.BreederID = breederID

	g.world.AddEntity(&player)

	engo.Mailbox.Dispatch(WarpMessage{
		ID:    player.BasicEntity.ID(),
		World: "meta",
		To:    "startPoint",
	})

	return player.ID(), nil

}

const (
	_ = iota
	wallCollision
	npcCollision
	playerCollision
	itemCollision
)

// Interface is represents ecs.World
type gameWorld interface {
	AddSystem(system ecs.System)

	// AddSystemInterface adds a system to the world, but also adds a filter that allows
	// automatic adding of entities that match the provided in interface, and excludes any
	// that match the provided ex interface, even if they also match in. in and ex must be
	// pointers to the interface or else this panics.
	AddSystemInterface(sys ecs.SystemAddByInterfacer, in interface{}, ex interface{})

	// AddEntity adds the entity to all systems that have been added via
	// AddSystemInterface. If the system was added via AddSystem the entity will not be
	// added to it.
	AddEntity(e ecs.Identifier)

	// Systems returns the list of Systems managed by the World.
	Systems() []ecs.System

	// Update updates each System managed by the World. It is invoked by the engine
	// once every frame, with dt being the duration since the previous update.
	Update(dt float32)

	// RemoveEntity removes the entity across all systems.
	RemoveEntity(e ecs.BasicEntity)
}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld

import (
	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo/common"
)

type controlEntity struct {
	*ecs.BasicEntity
	*common.SpaceComponent
	*DirectionComponent
	*ActivityComponent
}

type Controllable interface {
	common.BasicFace
	common.SpaceFace
	DirectionFace
	ActivityFace
}

type ControlSystem struct {
	entities []controlEntity
}

func (c *ControlSystem) Add(basic *ecs.BasicEntity, space *common.SpaceComponent, direction *DirectionComponent, active *ActivityComponent) {
	c.entities = append(c.entities, controlEntity{basic, space, direction, active})
}

func (c *ControlSystem) AddByInterface(o ecs.Identifier) {
	obj := o.(Controllable)
	c.Add(obj.GetBasicEntity(), obj.GetSpaceComponent(), obj.GetDirectionComponent(), obj.GetActivityComponent())
}

func (c *ControlSystem) Remove(basic ecs.BasicEntity) {
	delete := -1
	for index, e := range c.entities {
		if e.BasicEntity.ID() == basic.ID() {
			delete = index
			break
		}
	}
	if delete >= 0 {
		c.entities = append(c.entities[:delete], c.entities[delete+1:]...)
	}
}

func (c *ControlSystem) Update(dt float32) {
	for _, e := range c.entities {
		if !e.ActivityComponent.IsActive {
			continue
		}

		speed := e.Speed * dt

		e.SpaceComponent.Position.X += speed * e.Direction.X
		e.SpaceComponent.Position.Y += speed * e.Direction.Y

		if e.Direction.X != 0 || e.Direction.Y != 0 {
			e.DirectionComponent.Looking = e.Direction
		}

	}
}

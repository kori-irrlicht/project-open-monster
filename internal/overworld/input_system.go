//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld

import (
	"time"

	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo"
	"github.com/EngoEngine/engo/common"
)

type inputEntity struct {
	*ecs.BasicEntity
	*DirectionComponent
	*InputComponent
	*InteractionComponent
	*ActivityComponent
	*EncounterTrackerComponent
}

type Inputable interface {
	common.BasicFace
	DirectionFace
	InputFace
	InteractionFace
	ActivityFace
	EncounterTrackerFace
}

type InputSystem struct {
	entities []inputEntity
	world    gameWorld
}

func (c *InputSystem) Add(basic *ecs.BasicEntity, direction *DirectionComponent, input *InputComponent, interaction *InteractionComponent, activity *ActivityComponent, enc *EncounterTrackerComponent) {
	c.entities = append(c.entities, inputEntity{basic, direction, input, interaction, activity, enc})
}

func (c *InputSystem) AddByInterface(o ecs.Identifier) {
	obj := o.(Inputable)
	c.Add(obj.GetBasicEntity(), obj.GetDirectionComponent(), obj.GetInputComponent(), obj.GetInteractionComponent(), obj.GetActivityComponent(), obj.GetEncounterTrackerComponent())
}

func (c *InputSystem) Remove(basic ecs.BasicEntity) {
	delete := -1
	for index, e := range c.entities {
		if e.BasicEntity.ID() == basic.ID() {
			delete = index
			break
		}
	}
	if delete >= 0 {
		c.entities = append(c.entities[:delete], c.entities[delete+1:]...)
	}
}

func (c *InputSystem) Update(dt float32) {
	for _, e := range c.entities {
		for i := 0; i < len(e.InputComponent.Input); i++ {

			switch t := (<-e.InputComponent.Input).(type) {
			case BattleFinishedCommand:
				e.ActivityComponent.IsActive = true
				e.EncounterTrackerComponent.lastEncounter = time.Now()
			case MoveCommand:

				e.Direction.X = t.Direction.X
				e.Direction.Y = t.Direction.Y
				e.Direction, _ = e.Direction.Normalize()

				e.Speed = 32

			case InteractionCommand:
				if t.Interact {
					e.InteractionComponent.State |= InteractionStateActive
				} else {
					// Clear the state bit
					e.InteractionComponent.State &= ^InteractionStateActive

				}

			case DisconnectCommand:
				engo.Mailbox.Dispatch(DisconnectMessage{
					ID: e.ID(),
				})
				c.world.RemoveEntity(*e.BasicEntity)

			}
		}

	}
}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld_test

import (
	"testing"

	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo"
	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/data"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

func init() {
	logrus.SetLevel(logrus.WarnLevel)

	err := data.Load("../../assets/data", "file")

	if err != nil {
		panic(err)
	}

}

func TestOutput(t *testing.T) {
	Convey("Given a new output system", t, func() {
		engo.Mailbox = &engo.MessageManager{}

		var sys *overworld.OutputSystem

		ow := overworld.Overworld{}
		w := ecs.World{}
		ow.Setup(&w)

		for _, v := range w.Systems() {
			switch t := v.(type) {
			case *overworld.OutputSystem:
				sys = t
			}
		}

		Convey("Given a player", func() {

			player := createPlayer(1)
			sys.AddByInterface(&player)

			player2 := createPlayer(2)
			sys.AddByInterface(&player2)

			Convey("If an encounter message is send for the player", func() {
				engo.Mailbox.Dispatch(overworld.EncounterMessage{
					ID:   player.ID(),
					Area: "testing",
				})

				Convey("The player should receive a battle update in the output channel", func() {
					So(len(player.Output), ShouldBeGreaterThan, 0)

					msg := <-player.Output
					So(msg, ShouldHaveSameTypeAs, overworld.BattleUpdate{})
					enc, ok := msg.(overworld.BattleUpdate)
					So(ok, ShouldBeTrue)
					So(enc.Area, ShouldEqual, "testing")

				})

				Convey("Another player should not be notified", func() {
					So(len(player2.Output), ShouldEqual, 0)
				})
			})

			Convey("If an AreaChangeMessage is send for the player", func() {
				engo.Mailbox.Dispatch(overworld.AreaChangeMessage{
					ID:    player.ID(),
					World: "overworld",
					Area:  engo.Point{0, 0},
				})

				Convey("The player should receive a world update in the output channel", func() {
					So(len(player.Output), ShouldBeGreaterThan, 0)

					msg := <-player.Output
					So(msg, ShouldHaveSameTypeAs, overworld.WorldUpdate{})
					enc, ok := msg.(overworld.WorldUpdate)
					So(ok, ShouldBeTrue)

					Convey("The update should contain the surrounding areas", func() {
						So(enc.Coordinates, ShouldHaveLength, 2)
						So(enc.Coordinates[0], ShouldNotResemble, enc.Coordinates[1])

						So(enc.Coordinates[0].World, ShouldEqual, player.World)
						So(enc.Coordinates[0].Area, ShouldResemble, engo.Point{-1, 0})

						So(enc.Coordinates[1].World, ShouldEqual, player.World)
						So(enc.Coordinates[1].Area, ShouldResemble, engo.Point{0, 0})

					})

				})

				Convey("Another player should not be notified", func() {
					So(len(player2.Output), ShouldEqual, 0)
				})
			})

			Convey("Every update, a VelocityUpdate is send to all players", func() {
				player.Position.X = 5
				player.Position.Y = 6

				player.Direction.X = 2
				player.Direction.Y = 2
				player.Speed = 3

				sys.Update(1)
				up1 := <-player.Output
				up2 := <-player2.Output
				So(up1, ShouldResemble, up2)

				enc, ok := up1.(overworld.VelocityUpdateBatch)
				So(ok, ShouldBeTrue)

				Convey("The player position should be (5,6)", func() {
					So(enc[0].Position, ShouldResemble, model.Vector2D{5, 6})
					So(enc[0].Velocity, ShouldResemble, model.Vector2D{6, 6})
				})
			})

		})
	})
}

func createPlayer(id model.ID) overworld.Player {
	player := overworld.Player{
		BasicEntity: ecs.NewBasic(),
	}
	player.SpaceComponent.Height = 32
	player.SpaceComponent.Width = 32
	player.BreederID = id
	player.IsActive = true

	outputChan := make(chan overworld.Update, 1)
	player.Output = outputChan

	player.World = "overworld"

	return player
}

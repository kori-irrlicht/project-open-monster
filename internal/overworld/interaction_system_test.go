//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld_test

import (
	"testing"

	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo"
	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/data"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
)

func init() {
	logrus.SetLevel(logrus.WarnLevel)

	err := data.Load("../../assets/data", "file")

	if err != nil {
		panic(err)
	}

}

func TestInteraction(t *testing.T) {
	Convey("Given an InteractionSystem and a player", t, func() {
		engo.Mailbox = &engo.MessageManager{}
		sys := overworld.InteractionSystem{Active: 1, Passive: 2}
		player := overworld.Player{
			BasicEntity: ecs.NewBasic(),
		}
		player.SpaceComponent.Height = 32
		player.SpaceComponent.Width = 32
		player.DirectionComponent.Looking = engo.Point{X: 1, Y: 0}
		player.BreederID = 2
		player.IsActive = true

		player.InteractionComponent.Group = overworld.InteractionPlayer
		player.InteractionComponent.Mask = overworld.InteractionChest
		player.InteractionComponent.State = overworld.InteractionStatePassive
		player.InteractionComponent.Range = 32

		sys.AddByInterface(&player)

		Convey("Adding a chest in front of the player", func() {
			chest := overworld.Chest{
				BasicEntity: ecs.NewBasic(),
			}
			chest.SpaceComponent.Height = 32
			chest.SpaceComponent.Width = 32
			chest.SpaceComponent.Position = engo.Point{X: 32, Y: 0}

			chest.InteractionComponent.Group = overworld.InteractionChest
			chest.InteractionComponent.State = overworld.InteractionStatePassive

			sys.AddByInterface(&chest)

			Convey("The chest is in the same world as the player", func() {
				chest.World = player.World
				var m *engo.Message
				engo.Mailbox.Listen("InteractionMessage", func(msg engo.Message) {
					m = &msg
				})

				Convey("No interaction is triggered", func() {
					sys.Update(1)
					So(m, ShouldBeNil)

				})

				Convey("And setting the player to active", func() {
					player.GetInteractionComponent().State |= overworld.InteractionStateActive

					Convey("The chest is not passive", func() {
						chest.State = 0
						Convey("No interaction is triggered", func() {
							sys.Update(1)
							So(m, ShouldBeNil)
						})
					})

					Convey("The chest has the wrong group", func() {
						chest.InteractionComponent.Group = 0
						Convey("No interaction is triggered", func() {
							sys.Update(1)
							So(m, ShouldBeNil)
						})
					})

					Convey("An interaction is triggered", func() {
						sys.Update(1)
						So(m, ShouldNotBeNil)
						msg := (*m).(overworld.InteractionMessage)
						So(msg.Entity.ID(), ShouldEqual, player.ID())
						So(msg.With.ID(), ShouldEqual, chest.ID())
					})
				})
			})
			Convey("The chest is in another world as the player", func() {
				chest.World = player.World + "foo"
				var m *engo.Message
				engo.Mailbox.Listen("InteractionMessage", func(msg engo.Message) {
					m = &msg
				})

				Convey("No interaction is triggered", func() {
					sys.Update(1)
					So(m, ShouldBeNil)

				})

				Convey("And setting the player to active", func() {
					player.GetInteractionComponent().State |= overworld.InteractionStateActive
					Convey("No interaction is triggered", func() {
						sys.Update(1)
						So(m, ShouldBeNil)

					})

				})
			})

		})
		Convey("Adding a chest behind the player", func() {
			chest := overworld.Chest{
				BasicEntity: ecs.NewBasic(),
			}
			chest.SpaceComponent.Height = 32
			chest.SpaceComponent.Width = 32
			chest.SpaceComponent.Position = engo.Point{X: -32, Y: 0}

			chest.InteractionComponent.Group = overworld.InteractionChest
			chest.InteractionComponent.State = overworld.InteractionStatePassive

			sys.AddByInterface(&chest)

			var m *engo.Message
			engo.Mailbox.Listen("InteractionMessage", func(msg engo.Message) {
				m = &msg
			})

			Convey("No interaction is triggered", func() {
				sys.Update(1)
				So(m, ShouldBeNil)

			})

			Convey("And setting the player to active", func() {
				player.GetInteractionComponent().State |= overworld.InteractionStateActive

				Convey("No interaction is triggered", func() {
					sys.Update(1)
					So(m, ShouldBeNil)
				})
			})

		})

	})
}

func TestLine(t *testing.T) {
	Convey("Given a line from (0,0) to (1,0)", t, func() {
		l1 := overworld.Line{From: engo.Point{X: 0, Y: 0}, To: engo.Point{X: 1, Y: 0}}

		Convey("Given a line from (0.5,-1) to (0.5,1)", func() {
			l2 := overworld.Line{From: engo.Point{X: 0.5, Y: -1}, To: engo.Point{X: 0.5, Y: 1}}
			Convey("They should intersect", func() {
				So(l1.Intersects(l2), ShouldBeTrue)
				So(l2.Intersects(l1), ShouldBeTrue)
			})
		})

		Convey("Given a line from (0,1) to (1,1,)", func() {
			l2 := overworld.Line{From: engo.Point{X: 0, Y: 1}, To: engo.Point{X: 1, Y: 1}}
			Convey("They should not intersect", func() {
				So(l1.Intersects(l2), ShouldBeFalse)
				So(l2.Intersects(l1), ShouldBeFalse)
			})
		})

		Convey("Given a AABB from (-1,-1) to (1,1)", func() {
			aabb := engo.AABB{Min: engo.Point{X: -1, Y: -1}, Max: engo.Point{X: 1, Y: 1}}
			Convey("The line should intersect the aabb", func() {
				So(l1.CollidesAABB(aabb), ShouldBeTrue)
			})
		})

		Convey("Given a AABB from (-1,-1) to (0.5,1)", func() {
			aabb := engo.AABB{Min: engo.Point{X: -1, Y: -1}, Max: engo.Point{X: 0.5, Y: 1}}
			Convey("The line should intersect the aabb", func() {
				So(l1.CollidesAABB(aabb), ShouldBeTrue)
			})
		})

		Convey("Given a AABB from (-1,-1) to (-2,-2)", func() {
			aabb := engo.AABB{Min: engo.Point{X: -1, Y: -1}, Max: engo.Point{X: -2, Y: -2}}
			Convey("The line should not intersect the aabb", func() {
				So(l1.CollidesAABB(aabb), ShouldBeFalse)
			})
		})

	})
}

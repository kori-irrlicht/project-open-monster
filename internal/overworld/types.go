//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld

import (
	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo"
	"github.com/EngoEngine/engo/common"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type Command interface{}
type Update interface{}

type OverworldDB interface {
	LoadPosition(breederID model.ID) (position model.Vector2D, err error)
	SavePosition(breederID model.ID, position model.Vector2D) (err error)
}

type MoveCommand struct {
	Direction model.Vector2D
}

type InteractionCommand struct {
	Interact bool
}

type DisconnectCommand struct{}
type DisconnectUpdate struct {
	ID uint64
}

type BattleFinishedCommand struct{}

type VelocityUpdateBatch []VelocityUpdate

type VelocityUpdate struct {
	ID       uint64
	Position model.Vector2D
	Velocity model.Vector2D
}

type WorldUpdate struct {
	Coordinates []WorldCoordinates
}

type BattleUpdate struct {
	Area string
}

type EventUpdate struct {
	Lua string
}

type EncounterMessage struct {
	ID   uint64
	Area string
}

func (e EncounterMessage) Type() string {
	return "EncounterMessage"
}

type AreaChangeMessage struct {
	ID    uint64
	World string
	Area  engo.Point
}

func (e AreaChangeMessage) Type() string {
	return "AreaChangeMessage"
}

type DisconnectMessage struct {
	ID uint64
}

func (e DisconnectMessage) Type() string {
	return "DisconnectMessage"
}

type WorldCoordinates struct {
	World    string
	AreaName string
	Tiles    []int
	Offset   engo.Point
	Area     engo.Point
	Size     engo.Point
}

type TMXWorld struct {
	Maps                 []TMXMap `json:"maps"`
	OnlyShowAdjacentMaps bool     `json:"onlyShowAdjacentMaps"`
	Type                 string   `json:"type"`
}

type TMXMap struct {
	FileName string `json:"fileName"`
	Height   int    `json:"height"`
	Width    int    `json:"width"`
	X        int    `json:"x"`
	Y        int    `json:"y"`
}

type Player struct {
	ecs.BasicEntity
	common.SpaceComponent
	WorldComponent
	DirectionComponent
	common.CollisionComponent

	InputComponent
	OutputComponent

	InteractionComponent

	ActivityComponent
	PlayerComponent
	EncounterTrackerComponent
}

type Wall struct {
	ecs.BasicEntity
	common.SpaceComponent
	common.CollisionComponent
	WorldComponent
}

type Encounter struct {
	ecs.BasicEntity
	common.SpaceComponent
	EncounterComponent
	ActivityComponent
	WorldComponent
}

type WarpPoint struct {
	common.SpaceComponent
	WorldComponent
	WarpTargetComponent
}

type WarpTarget struct {
	common.SpaceComponent
	WorldComponent
	NameComponent
}

type Chest struct {
	ecs.BasicEntity
	common.SpaceComponent
	WorldComponent
	DirectionComponent
	common.CollisionComponent

	InteractionComponent
}

type NPC struct {
	ecs.BasicEntity
	common.SpaceComponent
	WorldComponent
	DirectionComponent
	common.CollisionComponent

	InteractionComponent
	DialogComponent
	EventComponent
}

type Item struct {
	ecs.BasicEntity
	common.SpaceComponent
	WorldComponent
	DirectionComponent
	common.CollisionComponent
	IdComponent

	InteractionComponent
	ItemComponent
	EventComponent
}

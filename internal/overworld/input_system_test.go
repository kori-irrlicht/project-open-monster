//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld_test

import (
	"testing"

	"github.com/EngoEngine/ecs"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

func TestInput(t *testing.T) {
	Convey("Given a new input system", t, func() {
		sys := overworld.InputSystem{}
		Convey("Given a player", func() {

			player := overworld.Player{
				BasicEntity: ecs.NewBasic(),
			}
			player.SpaceComponent.Height = 32
			player.SpaceComponent.Width = 32
			player.BreederID = 2
			player.IsActive = true

			inputChan := make(chan overworld.Command, 1)
			player.Input = inputChan

			sys.AddByInterface(&player)
			Convey("Sending a move command: X = 1", func() {
				mc := overworld.MoveCommand{Direction: model.Vector2D{X: 1, Y: 0}}
				inputChan <- mc
				sys.Update(1)
				Convey("The player should move in that direction", func() {
					So(player.Direction.X, ShouldEqual, 1)
					So(player.Direction.Y, ShouldEqual, 0)
				})
			})
			Convey("Sending a move command: Y = 1", func() {
				mc := overworld.MoveCommand{Direction: model.Vector2D{X: 0, Y: 1}}
				inputChan <- mc
				sys.Update(1)
				Convey("The player should move in that direction", func() {
					So(player.Direction.X, ShouldEqual, 0)
					So(player.Direction.Y, ShouldEqual, 1)
				})
			})
			Convey("Sending a move command: X = 1 & Y = 1", func() {
				mc := overworld.MoveCommand{Direction: model.Vector2D{X: 1, Y: 1}}
				inputChan <- mc
				sys.Update(1)
				Convey("The player should move in that direction by a normalized vector", func() {
					So(player.Direction.X, ShouldAlmostEqual, 0.7071067, 0.0000001)
					So(player.Direction.Y, ShouldAlmostEqual, 0.7071067, 0.0000001)
				})
			})
		})

	})
}

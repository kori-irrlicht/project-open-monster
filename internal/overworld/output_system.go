//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package overworld

import (
	"fmt"

	"github.com/EngoEngine/ecs"
	"github.com/EngoEngine/engo"
	"github.com/EngoEngine/engo/common"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type outputEntity struct {
	*ecs.BasicEntity
	*OutputComponent
}

type renderEntity struct {
	*ecs.BasicEntity
	*DirectionComponent
	*common.SpaceComponent
	*WorldComponent
}

type Renderable interface {
	common.BasicFace
	common.SpaceFace
	DirectionFace
	WorldFace
}

type Outputable interface {
	common.BasicFace
	OutputFace
}

type OutputSystem struct {
	entities       map[uint64]renderEntity
	outputEntities map[uint64]outputEntity
	tiles          *map[string]map[engo.Point]WorldCoordinates
}

func (s *OutputSystem) New(*ecs.World) {
	s.entities = make(map[uint64]renderEntity)
	s.outputEntities = make(map[uint64]outputEntity)

	engo.Mailbox.Listen("EncounterMessage", func(message engo.Message) {

		encounter, isEncounter := message.(EncounterMessage)
		if isEncounter {
			if e, ok := s.outputEntities[encounter.ID]; ok {
				e.OutputComponent.Output <- BattleUpdate{encounter.Area}
			}
		}
	})

	engo.Mailbox.Listen("CollisionMessage", func(message engo.Message) {
		m := message.(CollisionMessage)
		fmt.Printf("%+v :: %+v\n", m.Entity.SpaceComponent, m.To.SpaceComponent)
	})

	engo.Mailbox.Listen("EventOutMessage", func(message engo.Message) {
		m, ok := message.(EventOutMessage)
		if !ok {
			return
		}
		if e, ok := s.outputEntities[m.Entity.ID()]; ok {
			upd := EventUpdate{
				Lua: m.Event.GetEventComponent().Event,
			}
			e.OutputComponent.Output <- upd
		}

	})

	engo.Mailbox.Listen("DisconnectMessage", func(message engo.Message) {
		m, ok := message.(DisconnectMessage)
		if !ok {
			return
		}
		for _, e := range s.outputEntities {
			e.Output <- DisconnectUpdate(m)
		}

	})

	engo.Mailbox.Listen("AreaChangeMessage", func(message engo.Message) {
		areaChange, isAreaChange := message.(AreaChangeMessage)
		if isAreaChange {
			if e, ok := s.entities[areaChange.ID]; ok {
				if o, ok2 := s.outputEntities[areaChange.ID]; ok2 {
					upd := WorldUpdate{
						Coordinates: []WorldCoordinates{},
					}
					for i := -1; i <= 1; i++ {
						for j := -1; j <= 1; j++ {
							p := engo.Point{X: float32(i), Y: float32(j)}
							if coord, ok := (*s.tiles)[e.World][*p.Add(e.WorldComponent.Area)]; ok {
								upd.Coordinates = append(upd.Coordinates, coord)
							}
						}
					}

					o.OutputComponent.Output <- upd
				}
			}
		}
	})
}

func (c *OutputSystem) AddRender(basic *ecs.BasicEntity, space *common.SpaceComponent, direction *DirectionComponent, world *WorldComponent) {
	c.entities[basic.ID()] = renderEntity{basic, direction, space, world}
}

func (c *OutputSystem) AddOutput(basic *ecs.BasicEntity, output *OutputComponent) {
	c.outputEntities[basic.ID()] = outputEntity{basic, output}
}

func (c *OutputSystem) AddByInterface(o ecs.Identifier) {

	if obj, ok := o.(Outputable); ok {
		c.AddOutput(obj.GetBasicEntity(), obj.GetOutputComponent())
	}

	if obj, ok := o.(Renderable); ok {
		c.AddRender(obj.GetBasicEntity(), obj.GetSpaceComponent(), obj.GetDirectionComponent(), obj.GetWorldComponent())

	}
}

func (c *OutputSystem) Remove(basic ecs.BasicEntity) {
	delete(c.entities, basic.ID())
	delete(c.outputEntities, basic.ID())
}

func (c *OutputSystem) Update(dt float32) {
	var batch VelocityUpdateBatch
	for _, e := range c.entities {

		batch = append(batch, VelocityUpdate{
			ID: e.BasicEntity.ID(),
			Position: model.Vector2D{
				X: e.Position.X,
				Y: e.Position.Y,
			},
			Velocity: model.Vector2D{
				X: e.Direction.X * e.Speed,
				Y: e.Direction.Y * e.Speed,
			},
		})
	}
	for _, e := range c.outputEntities {
		e.Output <- batch
	}
}

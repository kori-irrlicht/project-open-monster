//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package data

import (
	"fmt"
	"io"

	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/file"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/json"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/tgz"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/habitat"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/status"
)

func Load(location, archiveType string) error {
	logger := logrus.WithField("method", "Load")
	logger.WithFields(logrus.Fields{
		"location": location,
		"archive":  archiveType,
	}).Infoln("Start loading the data")

	var provider db.FileProvider
	switch t := archiveType; t {
	case "file":
		provider = &file.Provider{}
	case "tgz":
		provider = &tgz.Provider{}
	default:
		return fmt.Errorf("Unknown archive type: '%s'", t)
	}

	reader, err := provider.ProvideFiles(location)

	if err != nil {
		return err
	}

	if err = LoadSpecies(reader["monster"]); err != nil {
		return err
	}
	if err = LoadMoves(reader["moves"]); err != nil {
		return err
	}

	if err = LoadItems(reader["items"]); err != nil {
		return err
	}

	if err = LoadHabitat(reader["habitat"]); err != nil {
		return err
	}

	if err = LoadStatus(reader["status"]); err != nil {
		return err
	}

	if err = LoadTraits(reader["traits"]); err != nil {
		return err
	}

	if err = LoadWorld(reader["maps"]); err != nil {
		return err
	}

	logger.Infoln("Finished loading the data")

	return nil
}

func LoadSpecies(reader map[string]io.Reader) error {
	l := logrus.WithField("method", "LoadSpecies")
	l.Infoln("Loading")

	loader := json.NewSpeciesLoader(reader)
	species, err := loader.Load()
	if err != nil {
		return fmt.Errorf("could not load species: %w", err)
	}
	model.SpeciesCache = species
	l.Infoln("done")
	return nil
}

func LoadMoves(reader map[string]io.Reader) error {
	l := logrus.WithField("method", "LoadMoves")
	l.Infoln("Loading")

	loader := json.NewMoveLoader(reader)
	moves, err := loader.Load()
	if err != nil {
		return fmt.Errorf("could not load moves: %w", err)
	}
	model.MoveBaseCache = moves
	l.Infoln("done")
	return nil
}

func LoadItems(reader map[string]io.Reader) error {
	l := logrus.WithField("method", "LoadItems")
	l.Infoln("Loading")

	loader := json.NewItemLoader(reader)
	items, err := loader.Load()
	if err != nil {
		return fmt.Errorf("could not load items: %w", err)
	}
	model.ItemCache = items
	l.Infoln("done")
	return nil
}

func LoadHabitat(reader map[string]io.Reader) error {
	l := logrus.WithField("method", "LoadHabitat")
	l.Infoln("Loading")

	loader := json.NewHabitatLoader(reader)
	hab, err := loader.Load()
	if err != nil {
		return fmt.Errorf("could not load habitat: %w", err)
	}

	for area, entries := range hab {
		for _, entry := range entries {
			if err := habitat.Add(area, *entry); err != nil {
				return fmt.Errorf("could not add entry to habitat '%s': %w", area, err)
			}
		}
	}
	l.Infoln("done")
	return nil
}

func LoadStatus(reader map[string]io.Reader) error {
	l := logrus.WithField("method", "LoadStatus")
	l.Infoln("Loading")

	loader := json.NewStatusLoader(reader)
	stat, err := loader.Load()
	if err != nil {
		return fmt.Errorf("could not load status: %w", err)
	}
	for _, v := range stat {
		status.Register(*v)
	}
	l.Infoln("done")
	return nil
}

func LoadTraits(reader map[string]io.Reader) error {
	l := logrus.WithField("method", "LoadTraits")
	l.Infoln("Loading")

	loader := json.NewTraitLoader(reader)
	traits, err := loader.Load()
	if err != nil {
		return fmt.Errorf("could not load traits: %w", err)
	}
	for k, v := range traits {
		model.TraitCache[k] = v
	}
	l.Infoln("done")
	return nil
}

func LoadWorld(reader map[string]io.Reader) error {
	l := logrus.WithField("method", "LoadWorlds")
	l.Infoln("Loading")

	loader := json.NewWorldLoader(reader)
	worlds, err := loader.Load()
	if err != nil {
		return fmt.Errorf("could not load worlds: %w", err)
	}

	overworld.Maps = worlds

	l.Infoln("done")
	return nil
}

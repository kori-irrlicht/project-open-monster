//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package util

import (
	"gitlab.com/kori-irrlicht/project-open-monster/internal/battle"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/overworld"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

// Connections contains the different subsystems and DB connections
type Connections struct {
	BattleServer BattleServer
	Overworld    Overworld

	BreederDB BreederDB
	MonsterDB MonsterDB
	BattleDB  BattleDB
	ItemDB    ItemDB
}

type Overworld interface {
	AddPlayer(breederID model.ID, commands chan overworld.Command, updates chan overworld.Update) (uint64, error)
}

type BattleServer interface {
	Create(b battle.Battle) model.ID
	AddListener(id model.ID, ch chan battle.BattleResult) error
	Get(id model.ID) (battle.Battle, error)
	GetByBreeder(id model.ID) (battle.Battle, error)
}

type BreederDB interface {
	Create(model.Breeder) (model.ID, error)
	Load(model.ID) (model.Breeder, error)
	Save(model.Breeder) error
	LoadAll() ([]model.Breeder, error)
	Delete(model.ID) error
	SavePosition(breederID model.ID, position model.Vector2D) (err error)
}

type MonsterDB interface {
	Create(model.Monster) (model.ID, error)
	Load(model.ID) (model.Monster, error)
	Save(model.Monster) error
	LoadTeamByBreederId(model.ID) ([]model.Monster, error)
	SaveTeamByBreederId(model.ID, []model.Monster) error
	Delete(model.ID) error
}

type BattleDB interface {
	CreateRequest(model.Request) (model.ID, error)
	LoadRequest(model.ID) (model.Request, error)
	SaveRequest(model.Request) error
	LoadRequestByBreeder(model.ID) ([]model.Request, error)
}

type ItemDB interface {
	Add(breederID model.ID, itemName string, amount int) error
	Remove(breederID model.ID, itemName string, amount int) error

	//Returns every item and amount in the inventory of the breeder
	Inventory(breederID model.ID) (map[string]int, error)

	// Checks if a breeder has already collected a certain item.
	// itemID is a unique ID and must not be confused with the itemName.
	HasCollected(breederID model.ID, itemID string) (bool, error)

	// Marks an item as collected by a breeder and adds it to their inventory
	Collect(breederID model.ID, itemID string, itemName string, amount int) error
}

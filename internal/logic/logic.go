//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package logic

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

// BreederCreator creates a new breeder in the database with a monster of the given species as a starter.
// The breeder ID will be generated in the process of this function.
type BreederCreator interface {
	CreateBreeder(model.Breeder, *model.Species) (model.Breeder, error)
}

type breederCreator struct {
	monsterDB db.MonsterDB
	breederDB db.BreederDB
}

func NewBreederCreator(breederDB db.BreederDB, monsterDB db.MonsterDB) BreederCreator {
	return breederCreator{
		breederDB: breederDB,
		monsterDB: monsterDB,
	}
}

// Implements BreederCreator
func (bc breederCreator) CreateBreeder(newBreeder model.Breeder, starter *model.Species) (breeder model.Breeder, err error) {

	id, err := bc.breederDB.Create(newBreeder)
	if err != nil {
		return model.Breeder{}, fmt.Errorf("could not create breeder: %w", err)
	}

	// Delete the breeder, if an error occurs after this point
	defer (func() {
		if err != nil {
			if innerErr := bc.breederDB.Delete(id); innerErr != nil {
				logrus.WithField("id", id).WithError(innerErr).Errorln("Could not delete breeder")
			}
		}
	})()

	// Reload breeder by ID to get the complete object
	breeder, err = bc.breederDB.Load(id)
	if err != nil {
		return model.Breeder{}, fmt.Errorf("could not load breeder: %w", err)
	}

	monster := *model.CreateMonster(starter, func() (model.Level, model.Level) { return 5, 5 }, model.DefaultGeneticModifierRange)
	monster.OwnerID = breeder.ID

	monsterId, err := bc.monsterDB.Create(monster)
	if err != nil {
		return model.Breeder{}, fmt.Errorf("could not create monster: %w", err)
	}

	// Delete the monster, if an error occurs after this point
	defer (func() {
		if err != nil {
			if innerErr := bc.monsterDB.Delete(id); innerErr != nil {
				logrus.WithField("id", id).WithError(innerErr).Errorln("Could not delete monster")
			}
		}
	})()

	// Reload monster by ID to get the complete object
	monster, err = bc.monsterDB.Load(monsterId)
	if err != nil {
		return model.Breeder{}, fmt.Errorf("could not load monster: %w", err)
	}

	if err = bc.monsterDB.SaveTeamByBreederId(breeder.ID, []model.Monster{monster}); err != nil {
		return model.Breeder{}, fmt.Errorf("could not save team: %w", err)
	}

	if err = bc.breederDB.SavePosition(id, model.Vector2D{}); err != nil {
		return breeder, err
	}

	return breeder, nil
}

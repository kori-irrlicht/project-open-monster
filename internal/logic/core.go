//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package logic

import (
	lua "github.com/yuin/gopher-lua"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/util"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/status"
	luar "layeh.com/gopher-luar"
)

func PrepareLua(conn util.Connections) *lua.LState {
	api := API{Conns: conn}
	monster := OverworldMonsterAPI{Conns: conn}

	L := lua.NewState(lua.Options{SkipOpenLibs: false})
	L.SetGlobal("API", luar.New(L, api))
	L.SetGlobal("MonsterAPI", luar.New(L, monster))

	return L
}

type API struct {
	Conns util.Connections
}

func (a API) AddItem(breeder model.ID, item string, amount int) error {
	return a.Conns.ItemDB.Add(breeder, item, amount)
}

func (a API) RemoveItem(breeder model.ID, item string, amount int) error {
	return a.Conns.ItemDB.Remove(breeder, item, amount)
}

func (a API) PickupItem(breederID model.ID, itemID string, itemName string, amount int) error {

	if ok, err := a.Conns.ItemDB.HasCollected(breederID, itemID); err != nil {
		return err
	} else if ok {
		return nil
	}
	return a.Conns.ItemDB.Collect(breederID, itemID, itemName, amount)
}

type MonsterAPI interface {
	HealMonster(model.ID) error

	GetTeam(model.ID) ([]model.Monster, error)
	HealTeam(model.ID) error
}

type OverworldMonsterAPI struct {
	Conns util.Connections
}

func (a OverworldMonsterAPI) GetTeam(breederID model.ID) ([]model.Monster, error) {
	return a.Conns.MonsterDB.LoadTeamByBreederId(breederID)
}

func (a OverworldMonsterAPI) HealMonster(monsterID model.ID) error {
	mon, err := a.Conns.MonsterDB.Load(monsterID)
	if err != nil {
		return err
	}
	mon.CurrentHP = int(mon.Attributes().Health)
	mon.Status = []status.Ailment{}
	return a.Conns.MonsterDB.Save(mon)
}

func (a OverworldMonsterAPI) HealTeam(breederID model.ID) error {
	team, err := a.GetTeam(breederID)
	if err != nil {
		return err
	}
	for _, mon := range team {
		if err := a.HealMonster(mon.ID); err != nil {
			return err
		}
	}
	return nil
}

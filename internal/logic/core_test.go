package logic_test

import (
	"errors"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/data"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/logic"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/util"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/status"
)

func init() {
	if err := data.Load("../../assets/data", "file"); err != nil {
		panic(err)
	}
}

func TestAPI(t *testing.T) {
	Convey("Given a new API", t, func() {
		item := &ItemDB{}
		monster := &MonsterDB{}
		c := util.Connections{ItemDB: item, MonsterDB: monster}
		api := logic.API{Conns: c}

		Convey("Setting the db to fail", func() {
			item.shouldFail = []bool{true}
			monster.shouldFail = []bool{true}

			Convey("Adding an item", func() {
				err := api.AddItem(1, "test", 5)
				So(err, ShouldResemble, errors.New("Foo"))
			})
			Convey("Removing an item", func() {
				err := api.AddItem(1, "test", 5)
				So(err, ShouldResemble, errors.New("Foo"))
			})
			Convey("Collecting an item", func() {
				Convey("HasCollected fails", func() {
					item.shouldFail = []bool{true, true}
					err := api.PickupItem(1, "id", "test", 5)
					So(err, ShouldResemble, errors.New("Bar"))

				})
				Convey("Collect fails", func() {
					item.shouldFail = []bool{false, true}
					err := api.PickupItem(1, "id", "test", 5)
					So(err, ShouldResemble, errors.New("Foo"))

				})
			})

		})

		Convey("Calling the db successfully", func() {
			item.shouldFail = []bool{false, false}
			monster.shouldFail = []bool{false, false, false, false, false, false}
			Convey("Adding an item", func() {
				err := api.AddItem(1, "test", 5)
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The parameters should be passed to the db", func() {
					So(item.lastAdded.id, ShouldEqual, 1)
					So(item.lastAdded.item, ShouldResemble, "test")
					So(item.lastAdded.amount, ShouldEqual, 5)
				})

			})
			Convey("Removing an item", func() {
				err := api.RemoveItem(1, "test", 5)
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The parameters should be passed to the db", func() {
					So(item.lastRemoved.id, ShouldEqual, 1)
					So(item.lastRemoved.item, ShouldResemble, "test")
					So(item.lastRemoved.amount, ShouldEqual, 5)
				})

			})
			Convey("Collecting an item", func() {
				Convey("If the item has not been collected", func() {

					err := api.PickupItem(1, "id", "test", 5)
					Convey("There should be no error", func() {
						So(err, ShouldBeNil)
					})
					Convey("The parameters should be passed to the db", func() {
						So(item.lastHasCollected.id, ShouldEqual, 1)
						So(item.lastHasCollected.itemID, ShouldResemble, "id")
						So(item.lastCollected.id, ShouldEqual, 1)
						So(item.lastCollected.item, ShouldResemble, "test")
						So(item.lastCollected.itemID, ShouldResemble, "id")
						So(item.lastCollected.amount, ShouldEqual, 5)
					})
				})
				Convey("If the item has been collected", func() {
					item.hasBeenCollected = true
					err := api.PickupItem(1, "id", "test", 5)
					Convey("There should be no error", func() {
						So(err, ShouldBeNil)
					})
					Convey("The parameters should be passed to the db", func() {
						So(item.lastHasCollected.id, ShouldEqual, 1)
						So(item.lastHasCollected.itemID, ShouldResemble, "id")
						So(item.lastCollected.id, ShouldEqual, 0)
						So(item.lastCollected.item, ShouldResemble, "")
						So(item.lastCollected.itemID, ShouldResemble, "")
						So(item.lastCollected.amount, ShouldEqual, 0)
					})

				})

			})

		})

	})

}
func TestMonsterAPI(t *testing.T) {
	Convey("Given a new API", t, func() {
		item := &ItemDB{}
		monster := &MonsterDB{}
		c := util.Connections{ItemDB: item, MonsterDB: monster}
		api := logic.OverworldMonsterAPI{Conns: c}

		Convey("Setting the db to fail", func() {
			item.shouldFail = []bool{true}
			monster.shouldFail = []bool{true}

			Convey("Loading the team", func() {
				team, err := api.GetTeam(1)
				So(err, ShouldResemble, errors.New("Foo"))
				So(team, ShouldHaveLength, 0)
			})
			Convey("Healing a monster", func() {
				err := api.HealMonster(1)
				So(err, ShouldResemble, errors.New("Foo"))
			})
			Convey("Healing the team", func() {
				Convey("Errors of GetTeam should be returned", func() {
					monster.shouldFail = []bool{true, true}
					err := api.HealTeam(1)
					So(err, ShouldResemble, errors.New("Foo"))

				})
				Convey("Errors of HealMonster should be returned", func() {
					monster.shouldFail = []bool{false, false, true}
					err := api.HealTeam(1)
					So(err, ShouldResemble, errors.New("Save"))

				})

			})

		})

		Convey("Calling the db successfully", func() {
			item.shouldFail = []bool{false, false}
			monster.shouldFail = []bool{false, false, false, false, false, false}
			Convey("Loading the team", func() {
				team, err := api.GetTeam(1)
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The team should be returned", func() {
					So(team, ShouldHaveLength, 2)
					So(team[0].Species.Name, ShouldEqual, "Hoarfox")
					So(team[1].Species.Name, ShouldEqual, "Hoarfox")
				})
				So(monster.lastLoadedTeam.id, ShouldEqual, 1)
			})
			Convey("Healing a monster", func() {
				err := api.HealMonster(1)
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The monster should be healed", func() {
					So(monster.lastLoadedMonster.ID, ShouldEqual, monster.lastSavedMonster[0].ID)
					So(monster.lastLoadedMonster.CurrentHP, ShouldNotEqual, monster.lastSavedMonster[0].CurrentHP)
					So(monster.lastSavedMonster[0].CurrentHP, ShouldEqual, monster.lastSavedMonster[0].Attributes().Health)
					So(monster.lastLoadedMonster.Status, ShouldNotBeEmpty)
					So(monster.lastSavedMonster[0].Status, ShouldBeEmpty)
				})

			})
			Convey("Healing the team", func() {
				err := api.HealTeam(1)
				Convey("There should be no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("The monsters should be healed", func() {
					for _, mon := range monster.lastSavedMonster {

						So(mon.CurrentHP, ShouldEqual, mon.Attributes().Health)
						So(mon.Status, ShouldBeEmpty)
					}
				})
			})

		})

	})

}

type ItemDB struct {
	shouldFail []bool

	lastAdded struct {
		id     model.ID
		item   string
		amount int
	}

	lastRemoved struct {
		id     model.ID
		item   string
		amount int
	}
	lastHasCollected struct {
		id     model.ID
		itemID string
	}
	lastCollected struct {
		id     model.ID
		item   string
		itemID string
		amount int
	}
	hasBeenCollected bool
}

func (i *ItemDB) ShouldFail() bool {
	b := i.shouldFail[0]
	i.shouldFail = i.shouldFail[1:]
	return b
}

func (i *ItemDB) Add(id model.ID, item string, amount int) error {
	i.lastAdded.id = id
	i.lastAdded.item = item
	i.lastAdded.amount = amount
	if i.ShouldFail() {
		return errors.New("Foo")
	}
	return nil

}

func (i *ItemDB) Remove(id model.ID, item string, amount int) error {
	i.lastRemoved.id = id
	i.lastRemoved.item = item
	i.lastRemoved.amount = amount
	if i.ShouldFail() {
		return errors.New("Foo")
	}
	return nil
}

func (i *ItemDB) Inventory(breederID model.ID) (map[string]int, error) {
	return nil, nil
}

func (i *ItemDB) HasCollected(breederID model.ID, itemID string) (bool, error) {
	i.lastHasCollected.id = breederID
	i.lastHasCollected.itemID = itemID
	if i.ShouldFail() {
		return false, errors.New("Bar")
	}
	return i.hasBeenCollected, nil
}

func (i *ItemDB) Collect(breederID model.ID, itemID string, itemName string, amount int) error {
	i.lastCollected.id = breederID
	i.lastCollected.item = itemName
	i.lastCollected.itemID = itemID
	i.lastCollected.amount = amount
	if i.ShouldFail() {
		return errors.New("Foo")
	}
	return nil
}

type MonsterDB struct {
	util.MonsterDB
	shouldFail []bool

	lastLoadedTeam struct {
		id model.ID
	}
	lastLoadedMonster model.Monster
	lastSavedMonster  []model.Monster
}

func (i *MonsterDB) ShouldFail() bool {
	b := i.shouldFail[0]
	i.shouldFail = i.shouldFail[1:]
	return b
}

func (i *MonsterDB) LoadTeamByBreederId(id model.ID) ([]model.Monster, error) {
	i.lastLoadedTeam.id = id
	if i.ShouldFail() {
		return nil, errors.New("Foo")
	}

	mon1 := model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 5, 5 }, func() (model.AttributeStat, model.AttributeStat) { return 5, 5 })
	mon1.ID = 1
	mon2 := model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 5, 5 }, func() (model.AttributeStat, model.AttributeStat) { return 5, 5 })
	mon2.ID = 2

	return []model.Monster{*mon1, *mon2}, nil
}

func (i *MonsterDB) Load(id model.ID) (model.Monster, error) {
	if i.ShouldFail() {
		return model.Monster{}, errors.New("Foo")
	}
	mon := model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 5, 5 }, func() (model.AttributeStat, model.AttributeStat) { return 5, 5 })
	mon.CurrentHP = 0
	mon.ID = id
	mon.Status = []status.Ailment{status.New("Freeze")}
	i.lastLoadedMonster = *mon
	return *mon, nil
}

func (i *MonsterDB) Save(mon model.Monster) error {
	i.lastSavedMonster = append(i.lastSavedMonster, mon)
	if i.ShouldFail() {
		return errors.New("Save")
	}
	return nil
}

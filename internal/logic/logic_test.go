//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package logic_test

import (
	"testing"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/spf13/viper"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/data"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/logic"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/memory"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

func init() {
	logrus.SetLevel(logrus.WarnLevel)
	basepath_monster := "../../assets/data/monster"
	basepath_moves := "../../assets/data/moves"
	viper.SetDefault("data.dir.monster", basepath_monster)
	viper.SetDefault("data.dir.move", basepath_moves)

	if err := data.Load("../../assets/data", "file"); err != nil {
		panic(err)
	}

}

func TestCreateBreeder(t *testing.T) {
	Convey("Given some working db connections", t, func() {
		breederDB := memory.NewBreederDB()
		monsterDB := memory.NewMonsterDB()
		bc := logic.NewBreederCreator(breederDB, monsterDB)

		Convey("Creating a new breeder", func() {
			breeder := model.Breeder{
				Name: "Test",
			}
			species := model.SpeciesCache["Hoarfox"]
			newBreeder, err := bc.CreateBreeder(breeder, species)
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The new breeder has the same fields", func() {
				So(newBreeder.Name, ShouldEqual, breeder.Name)
			})
			Convey("The new breeder should have the ID 1", func() {
				So(newBreeder.ID, ShouldEqual, 1)
			})
			Convey("The new breeder should be in the DB", func() {
				dbBreeder, err := breederDB.Load(newBreeder.ID)
				So(err, ShouldBeNil)
				So(dbBreeder, ShouldResemble, newBreeder)
			})

			Convey("The breeder should have a monster of the given species", func() {
				team, err := monsterDB.LoadTeamByBreederId(newBreeder.ID)
				So(err, ShouldBeNil)
				So(team[0].Species, ShouldEqual, species)

				Convey("The monster should be at level 5", func() {
					So(team[0].Level, ShouldEqual, 5)
				})

				Convey("The monster should have the breeder as an owner", func() {
					So(team[0].OwnerID, ShouldEqual, newBreeder.ID)
				})
			})
		})
	})
	Convey("Given some borked db connections", t, func() {
		breederDB := &TestBreederDB{BreederDB: memory.NewBreederDB()}
		monsterDB := &TestMonsterDB{MonsterDB: memory.NewMonsterDB()}
		bc := logic.NewBreederCreator(breederDB, monsterDB)
		breeder := model.Breeder{
			Name: "Test",
		}
		species := model.SpeciesCache["Hoarfox"]
		Convey("BreederDB can't create a breeder", func() {
			breederDB.CreateError = errors.New("")
			newBreeder, err := bc.CreateBreeder(breeder, species)
			So(err, ShouldNotBeNil)
			So(newBreeder.ID, ShouldEqual, 0)

			Convey("There should be no new breeder", func() {
				b, err := breederDB.Load(1)
				So(err, ShouldNotBeNil)
				So(b.ID, ShouldEqual, 0)
			})
		})

		Convey("BreederDB can't load a breeder", func() {
			breederDB.LoadError = errors.New("")
			newBreeder, err := bc.CreateBreeder(breeder, species)
			So(err, ShouldNotBeNil)
			So(newBreeder.ID, ShouldEqual, 0)

			Convey("There should be no new breeder", func() {
				breederDB.LoadError = nil
				b, err := breederDB.Load(1)
				So(err, ShouldNotBeNil)
				So(b.ID, ShouldEqual, 0)
			})
		})

		Convey("MonsterDB can't create a monster", func() {
			monsterDB.CreateError = errors.New("")
			newBreeder, err := bc.CreateBreeder(breeder, species)
			So(err, ShouldNotBeNil)
			So(newBreeder.ID, ShouldEqual, 0)

			// DB does currently not support rerolling
			Convey("There should be no new breeder", func() {
				b, err := breederDB.Load(1)
				So(err, ShouldNotBeNil)
				So(b.ID, ShouldEqual, 0)
			})
		})
		Convey("MonsterDB can't load a monster", func() {
			monsterDB.LoadError = errors.New("")
			newBreeder, err := bc.CreateBreeder(breeder, species)
			So(err, ShouldNotBeNil)
			So(newBreeder.ID, ShouldEqual, 0)

			// DB does currently not support rerolling
			Convey("There should be no new breeder", func() {
				b, err := breederDB.Load(1)
				So(err, ShouldNotBeNil)
				So(b.ID, ShouldEqual, 0)
			})

			Convey("There should be no new monster", func() {
				monsterDB.LoadError = nil
				b, err := monsterDB.Load(1)
				So(err, ShouldNotBeNil)
				So(b.ID, ShouldEqual, 0)

			})
		})
		Convey("MonsterDB can't save a team", func() {
			monsterDB.SaveTeamByBreederIDError = errors.New("")
			newBreeder, err := bc.CreateBreeder(breeder, species)
			So(err, ShouldNotBeNil)
			So(newBreeder.ID, ShouldEqual, 0)

			// DB does currently not support rerolling
			Convey("There should be no new breeder", func() {
				b, err := breederDB.Load(1)
				So(err, ShouldNotBeNil)
				So(b.ID, ShouldEqual, 0)
			})

			Convey("There should be no new monster", func() {
				b, err := monsterDB.Load(1)
				So(err, ShouldNotBeNil)
				So(b.ID, ShouldEqual, 0)

			})
		})
	})
}

type TestBreederDB struct {
	BreederDB    db.BreederDB
	CreateError  error
	LoadError    error
	LoadAllError error
	SaveError    error
}

func (tdb *TestBreederDB) Create(breeder model.Breeder) (model.ID, error) {
	if tdb.CreateError == nil {
		return tdb.BreederDB.Create(breeder)
	}
	return 0, tdb.CreateError
}

func (tdb *TestBreederDB) Load(id model.ID) (model.Breeder, error) {
	if tdb.LoadError == nil {
		return tdb.BreederDB.Load(id)
	}
	return model.Breeder{}, tdb.LoadError
}

func (tdb *TestBreederDB) LoadAll() ([]model.Breeder, error) {
	if tdb.LoadAllError == nil {
		return tdb.BreederDB.LoadAll()
	}
	return nil, tdb.LoadAllError
}

func (tdb *TestBreederDB) Save(breeder model.Breeder) error {
	if tdb.SaveError == nil {
		return tdb.BreederDB.Save(breeder)
	}
	return tdb.SaveError
}

func (tdb *TestBreederDB) Delete(id model.ID) error {
	return tdb.BreederDB.Delete(id)
}

func (tdb *TestBreederDB) SavePosition(id model.ID, position model.Vector2D) error {
	return nil

}

type TestMonsterDB struct {
	MonsterDB                db.MonsterDB
	CreateError              error
	LoadError                error
	SaveError                error
	LoadTeamByBreederIDError error
	SaveTeamByBreederIDError error
}

func (mdb *TestMonsterDB) Create(monster model.Monster) (model.ID, error) {
	if mdb.CreateError == nil {
		return mdb.MonsterDB.Create(monster)
	}
	return 0, mdb.CreateError
}

func (mdb *TestMonsterDB) Load(id model.ID) (model.Monster, error) {
	if mdb.LoadError == nil {
		return mdb.MonsterDB.Load(id)
	}
	return model.Monster{}, mdb.LoadError
}

func (mdb *TestMonsterDB) Save(monster model.Monster) error {
	if mdb.SaveError == nil {
		return mdb.MonsterDB.Save(monster)
	}
	return mdb.SaveError
}

func (mdb *TestMonsterDB) LoadTeamByBreederId(id model.ID) (team []model.Monster, err error) {
	if mdb.LoadTeamByBreederIDError == nil {
		return mdb.MonsterDB.LoadTeamByBreederId(id)
	}
	return team, mdb.LoadTeamByBreederIDError
}

func (mdb *TestMonsterDB) SaveTeamByBreederId(id model.ID, team []model.Monster) error {
	if mdb.SaveTeamByBreederIDError == nil {
		return mdb.MonsterDB.SaveTeamByBreederId(id, team)
	}
	return mdb.SaveTeamByBreederIDError
}

func (mdb *TestMonsterDB) Delete(id model.ID) error {
	return mdb.MonsterDB.Delete(id)
}

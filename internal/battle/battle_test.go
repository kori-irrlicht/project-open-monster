//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package battle_test

import (
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/spf13/viper"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/battle"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/data"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/json"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/monster"
)

func init() {
	logrus.SetLevel(logrus.WarnLevel)
	basepath := "../../assets/data/"
	viper.SetDefault("data.dir.monster", basepath+"monster")
	viper.SetDefault("data.dir.move", basepath+"moves")
	if err := data.Load("../../assets/data", "file"); err != nil {
		panic(err)
	}
}

func TestBattle(t *testing.T) {
	Convey("Given two breeders", t, func() {
		t1 := model.Breeder{1, "Test1", []model.Monster{}}
		t2 := model.Breeder{2, "Test2", []model.Monster{}}

		teams := make(map[model.ID][]model.Monster)

		moveLoader := json.MoveLoader{}
		moveBases, err := moveLoader.Load()
		if err != nil {
			panic(err)
		}

		mon1 := *model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 100, 100 }, func() (model.AttributeStat, model.AttributeStat) { return 0, 0 })
		mon2 := *model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 100, 100 }, func() (model.AttributeStat, model.AttributeStat) { return 0, 0 })
		teams[t1.ID] = []model.Monster{mon1}
		teams[t2.ID] = []model.Monster{mon2}

		Convey("Those will participate in the battle", func() {
			btl := battle.CreateBattle(battle.Participants{{t1}, {t2}}, teams, 1, nil, &resultSaver{})
			Convey("So they are participants", func() {
				So(btl.Participants()[0], ShouldResemble, []model.Breeder{t1})
				So(btl.Participants()[1], ShouldResemble, []model.Breeder{t2})
				So(btl.Field().Breeder()[0], ShouldResemble, []model.Breeder{t1})
				So(btl.Field().Breeder()[1], ShouldResemble, []model.Breeder{t2})
			})
			Convey("The btl is initialized before the first round", func() {
				So(btl.CurrentRound(), ShouldEqual, 0)
			})

			Convey("The battle should have the teams", func() {
				So(btl.Field().Teams(), ShouldNotBeEmpty)
				So(btl.Field().Teams()[t1.ID], ShouldNotBeEmpty)
				So(btl.Field().Teams()[t2.ID], ShouldNotBeEmpty)
				for id, team := range teams {
					for i, mon := range team {
						So(btl.Field().Teams()[id][i], ShouldResemble, &mon)
					}
				}
			})

			Convey("Sending a SystemAction, the action should be resolved immediatly and only once", func() {
				dummy := &DummySystemAction{
					IssuedAction: battle.IssuedAction{
						Breeder: &t1,
					},
				}
				btl.SendAction(dummy)
				btl.ProcessActions()
				So(dummy.count, ShouldEqual, 1)
			})
			Convey("Sending a PlayerControlAction, the action should be resolved immediatly and only once", func() {
				dummy := &DummyPlayerControlAction{
					IssuedAction: battle.IssuedAction{
						Breeder: &t1,
					},
				}
				btl.SendAction(dummy)
				btl.ProcessActions()
				So(dummy.count, ShouldEqual, 1)
			})

			Convey("Sending a SurrenderAction", func() {
				sa := battle.NewSurrenderAction(battle.Location{0, 0}, &t1)
				sa.Apply(btl)
				Convey("The battle should be finished", func() {
					So(btl.IsFinished(), ShouldBeTrue)
				})
				Convey("The other team should have won", func() {
					btl.SendResult()
					res := (<-btl.Replay()).Result.(battle.FinishedResult)
					So(res.WinnerIndex, ShouldEqual, 1)
					So(res.IsDraw, ShouldBeFalse)
				})
			})

			Convey("The battle should not be finished", func() {
				So(btl.IsFinished(), ShouldBeFalse)
			})

			Convey("If one breeder has no remaining monster left", func() {
				id := btl.Participants()[0][0].ID
				for _, mon := range btl.Field().Teams()[id] {
					mon.CurrentHP = 0
				}
				Convey("The battle should be finished", func() {
					So(btl.IsFinished(), ShouldBeTrue)
				})
				Convey("Sending the battle result", func() {
					btl.SendResult()
					Convey("The replay contains, that team 1 has won", func() {
						res := (<-btl.Replay()).Result.(battle.FinishedResult)
						So(res.IsDraw, ShouldBeFalse)
						So(res.WinnerIndex, ShouldEqual, 1)
					})

				})
			})

			Convey("If both breeder have no remaining monster left", func() {
				for _, mon := range btl.Field().Teams()[1] {
					mon.CurrentHP = 0
				}
				for _, mon := range btl.Field().Teams()[2] {
					mon.CurrentHP = 0
				}
				Convey("The battle should be finished", func() {
					So(btl.IsFinished(), ShouldBeTrue)
				})
				Convey("Sending the battle result", func() {
					btl.SendResult()
					Convey("The replay contains a draw", func() {
						res := (<-btl.Replay()).Result.(battle.FinishedResult)
						So(res.IsDraw, ShouldBeTrue)
					})
				})
			})

			Convey("Sending a spawn command while no monster is on the field", func() {
				mon := createMonster(moveBases)
				spawnMonster := &battle.SpawnBattleObjectAction{
					Object:   mon,
					Side:     1,
					Position: 1,
					IssuedAction: battle.IssuedAction{
						Breeder: &t1,
					},
					ActionType: battle.BattleActionType_PlayerControl,
				}
				btl.SendAction(spawnMonster)
				btl.ProcessActions()

				Convey("The replay should contain the spawn result", func() {
					ch := btl.Replay()
					res := <-ch
					So(res.Result, ShouldHaveSameTypeAs, battle.SpawnResult{})
					sres := res.Result.(battle.SpawnResult)

					monObj := sres.Object
					So(monObj.ID, ShouldEqual, mon.ID)
					So(sres.SpawnLocation, ShouldResemble, battle.Location{
						Side:     1,
						Position: 1,
					})
				})

			})
			Convey("If at least one field is empty and another monster could be spawned, ", func() {
				mon1 := createMonster(moveBases)
				mon1.GeneticModifier.Initiative = 10
				spawnMonster := &battle.SpawnBattleObjectAction{
					Object:   mon1,
					Side:     0,
					Position: 0,
				}
				spawnMonster.Apply(btl)
				Convey("Sending an AttackAction", func() {
					sendMonster := battle.NewAttackAction(0, battle.Location{0, 0}, battle.Location{1, 0}, &t2)
					btl.SendAction(sendMonster)
					btl.ProcessActions()
					btl.ProcessPlayerActions()
					Convey("It is not processed", func() {
						So(btl.Replay(), ShouldBeEmpty)
					})
				})
				Convey("Sending a SendMonsterActions", func() {
					sendMonster := battle.NewSendMonsterAction(0, 1, 0, &t2)
					btl.SendAction(sendMonster)
					btl.ProcessActions()
					btl.ProcessPlayerActions()
					Convey("It is processed", func() {
						res := (<-btl.Replay()).Result //.(battle.SendMonsterResult)

						So(res, ShouldNotBeNil)
						So(res, ShouldHaveSameTypeAs, battle.SendMonsterResult{})
					})
				})

			})

			Convey("Spawning a monster on each side of the btl field", func() {
				mon1 := createMonster(moveBases)
				mon1.GeneticModifier.Initiative = 10
				mon2 := createMonster(moveBases)
				spawnMonster := &battle.SpawnBattleObjectAction{
					Object:   mon1,
					Side:     0,
					Position: 0,
				}
				spawnMonster.Apply(btl)
				spawnMonster = &battle.SpawnBattleObjectAction{
					Object:   mon2,
					Side:     1,
					Position: 0,
				}
				spawnMonster.Apply(btl)

				Convey("Sending a PlayerAction, it won't be resolved immediatly", func() {

					dummy := &DummyPlayerAction{
						IssuedAction: battle.IssuedAction{
							Breeder: &t1,
						},
						LocationAction: battle.LocationAction{
							Location: battle.Location{
								Position: 0,
								Side:     0,
							},
						},
						priority: 100}
					btl.SendAction(dummy)
					btl.ProcessActions()
					btl.ProcessPlayerActions()
					So(dummy.count, ShouldEqual, 0)
					Convey("Sending another PlayerAction", func() {
						dummy2 := &DummyPlayerAction{
							IssuedAction: battle.IssuedAction{
								Breeder: &t2,
							},
							LocationAction: battle.LocationAction{
								Location: battle.Location{
									Position: 0,
									Side:     1,
								},
							},
							priority: 10}
						btl.SendAction(dummy2)
						btl.ProcessActions()
						btl.ProcessPlayerActions()
						So(dummy.count, ShouldEqual, 1)
						So(dummy2.count, ShouldEqual, 1)
						So(dummy2.time, ShouldHappenBefore, dummy.time)
						Convey("The replay should contain the actions in order of execution", func() {
							So((<-btl.Replay()).Result, ShouldResemble, DummyResult{10})
							So((<-btl.Replay()).Result, ShouldResemble, DummyResult{100})

						})

					})
				})

				Convey("Both monster will attack", func() {
					btl.SendAction(&battle.AttackAction{
						PlayerAction: battle.PlayerAction{
							IssuedAction: battle.IssuedAction{
								Breeder: &t1,
							},
						},
						LocationAction: battle.LocationAction{
							Location: battle.Location{
								Side:     0,
								Position: 0,
							},
						},
						AttackerLocation: battle.Location{
							Side:     0,
							Position: 0,
						},
						DefenderLocation: battle.Location{
							Side:     1,
							Position: 0,
						},
						Move: 0,
					})
					btl.SendAction(&battle.AttackAction{
						PlayerAction: battle.PlayerAction{
							IssuedAction: battle.IssuedAction{
								Breeder: &t2,
							},
						},
						LocationAction: battle.LocationAction{
							Location: battle.Location{
								Side:     1,
								Position: 0,
							},
						},
						AttackerLocation: battle.Location{
							Side:     1,
							Position: 0,
						},
						DefenderLocation: battle.Location{
							Side:     0,
							Position: 0,
						},
						Move: 0,
					})
					btl.ProcessActions()
					btl.ProcessPlayerActions()
					btl.ProcessActions()
					btl.ProcessPlayerActions()

					firstResult := <-btl.Replay()
					//firstResult := <-btl.Replay()
					Convey("So the first monster has damaged the second monster", func() {
						res, ok := firstResult.Result.(*battle.AttackResult)
						So(ok, ShouldBeTrue)
						Convey("So the stamina of the attacker should be reduced", func() {
							var ch battle.ChangeEntry
							loc := battle.Location{
								Side:     0,
								Position: 0,
							}
							for _, change := range res.Changes {
								if change.Location == loc {
									ch = change
								}
							}

							// 20 is the stamina usage of 'White Frost'
							expected := float64(mon2.Attributes().Stamina-20) / float64(mon2.Attributes().Stamina) * 100

							So(ch.Change, ShouldEqual, battle.Change_Stamina)
							So(ch.Value, ShouldEqual, expected)
						})
						Convey("So the defending monster should have reduced hp", func() {
							var ch battle.ChangeEntry
							loc := battle.Location{
								Side:     1,
								Position: 0,
							}
							for _, change := range res.Changes {
								if change.Location == loc && change.Change == battle.Change_Health {
									ch = change
								}
							}

							// 120 is the calculated damage
							expected := float64(mon2.Attributes().Health-120) / float64(mon2.Attributes().Health) * 100
							So(ch.Value, ShouldEqual, expected)
							So(res.Move, ShouldEqual, model.MoveBaseCache["White Frost"])
							_ = res
						})

					})
				})
			})
		})
	})
}

func createMonster(moveBases map[string]model.MoveBase) *model.Monster {
	monster := model.CreateMonster(monster.Hoarfox, func() (model.Level, model.Level) { return 100, 100 }, func() (model.AttributeStat, model.AttributeStat) { return 0, 0 })
	monster.Moves[0] = model.NewMove(model.MoveBaseCache["White Frost"])
	return monster
}

type DummySystemAction struct {
	battle.ResultAction
	battle.IssuedAction
	battle.LocationAction
	count int
}

func (dummy *DummySystemAction) Type() battle.BattleActionType {
	return battle.BattleActionType_System
}
func (dummy *DummySystemAction) Priority(battle.Battle) int { return 0 }
func (dummy *DummySystemAction) Apply(battle.Battle) {
	dummy.count += 1
	dummy.ResultAction.Result_ = DummyResult{1}
}

type DummyPlayerControlAction struct {
	battle.ResultAction
	battle.IssuedAction
	battle.LocationAction
	count int
}

func (dummy *DummyPlayerControlAction) Type() battle.BattleActionType {
	return battle.BattleActionType_PlayerControl
}
func (dummy *DummyPlayerControlAction) Priority(battle.Battle) int { return 0 }
func (dummy *DummyPlayerControlAction) Apply(battle.Battle) {
	dummy.count += 1
	dummy.ResultAction.Result_ = DummyResult{2}
}

type DummyPlayerAction struct {
	battle.ResultAction
	battle.IssuedAction
	battle.LocationAction
	count    int
	priority int
	time     time.Time
}

func (dummy *DummyPlayerAction) Type() battle.BattleActionType {
	return battle.BattleActionType_Player
}
func (dummy *DummyPlayerAction) Priority(battle.Battle) int { return dummy.priority }
func (dummy *DummyPlayerAction) Apply(battle.Battle) {
	dummy.count += 1
	dummy.time = time.Now()
	dummy.ResultAction.Result_ = DummyResult{dummy.priority}
}

type DummyResult struct {
	value int
}

func (d DummyResult) Type() battle.ResultType {
	return "dummyResult"
}

type resultSaver struct {
}

func (rs *resultSaver) CaptureMonster(breederId model.ID, monster model.Monster) {

}

func (rs *resultSaver) SaveMonster(monster model.Monster) {

}

func (rs *resultSaver) SaveUsedItem(breeder model.ID, item string, amount int) {}

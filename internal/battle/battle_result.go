//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package battle

import "gitlab.com/kori-irrlicht/project-open-monster/pkg/model"

type Location struct {
	Side     uint `json:"side"`
	Position uint `json:"position"`
}

type Change string

const (
	Change_Health      Change = "health"
	Change_Status      Change = "status"
	Change_AttrPhysAtk Change = "physAtk"
	Change_AttrPhysDef Change = "physDef"
	Change_AttrSpecAtk Change = "specAtk"
	Change_AttrSpecDef Change = "specDef"
	Change_AttrInit    Change = "init"
	Change_Stamina     Change = "stamina"

	// Items-only
	Change_Capture Change = "capture"
)

type ChangeEntry struct {
	Change       Change                 `json:"change"`
	Value        interface{}            `json:"value"`
	Additional   map[string]interface{} `json:"additional,omitempty"`
	Location     Location               `json:"location"`
	TeamIndex    uint                   `json:"teamIndex"`
	UseTeamIndex bool                   `json:"useTeamIndex,omitempty"`
}

type Changes []ChangeEntry

type Result interface {
	Type() ResultType
}

type AttackResult struct {
	AttackerLocation Location       `json:"attacker"`
	Move             model.MoveBase `json:"move"`
	Changes          Changes        `json:"changes"`
}

func (a AttackResult) Type() ResultType { return BattleResult_Attack }

type SpawnResult struct {
	SpawnLocation Location `json:"spawnLocation"`

	//Type defines, whether a monster or another object was spawned here
	//	Type   string         `json:"type"`
	Object *model.Monster `json:"object"`
}

func (a SpawnResult) Type() ResultType { return BattleResult_Spawn }

type SendMonsterResult struct {
	SpawnLocation Location `json:"spawnLocation"`
	TeamIndex     uint     `json:"teamIndex"`
}

func (a SendMonsterResult) Type() ResultType { return BattleResult_SendMonster }

type FinishedResult struct {
	IsDraw      bool `json:"isDraw"`
	WinnerIndex int  `json:"winnerIndex"`
}

func (a FinishedResult) Type() ResultType { return BattleResult_Finished }

type CaptureResult struct {
	Captured   Location `json:"captured"`
	Successful bool     `json:"successful"`
}

func (a CaptureResult) Type() ResultType { return BattleResult_Capture }

type ItemResult struct {
	Item    string  `json:"item"`
	Changes Changes `json:"changes"`
}

func (a ItemResult) Type() ResultType { return BattleResult_Item }

// Result is the base for the results, which are created after an BattleAction was evaluated
// TODO: Rename to BattleResult
type BattleResult struct {
	// ID is in relation to the current battle. Also Results are ordered by ascending ID to ensure the correct recreation on the client-side
	ID model.ID `json:"id"`

	// Breeder is the breeder, who initiated the action
	Breeder model.ID `json:"breeder"`

	// ResultType contains the type of the result stored in Result
	ResultType ResultType `json:"resultType"`

	Result interface{} `json:"result"`
}

type ResultType string

const (
	BattleResult_Spawn       ResultType = "spawn"
	BattleResult_SendMonster ResultType = "sendMonster"
	BattleResult_Attack      ResultType = "attack"
	BattleResult_Finished    ResultType = "finished"
	BattleResult_NextTurn    ResultType = "nextTurn"
	BattleResult_Capture     ResultType = "capture"
	BattleResult_Item        ResultType = "item"
)

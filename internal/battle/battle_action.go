//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package battle

import (
	"fmt"
	"math"

	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/item"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/status"
	luar "layeh.com/gopher-luar"
)

type BattleActionType string

const (
	BattleActionType_System        BattleActionType = "system"
	BattleActionType_Player        BattleActionType = "player"
	BattleActionType_PlayerControl BattleActionType = "player_control"
	BattleActionType_Environment   BattleActionType = "environment"
)

type BattleAction interface {
	Type() BattleActionType
	Priority(Battle) int
	Apply(Battle)
	IssuedBy() *model.Breeder
	Result() Result
	Field() Location
}

// TODO: Rewrite this
// Maybe use a wrapper for Type, Prio, IssuedBy, Result and Location?

type IssuedAction struct {
	Breeder *model.Breeder
}

func (action *IssuedAction) IssuedBy() *model.Breeder {
	return action.Breeder
}

type LocationAction struct {
	Location Location
}

func (action LocationAction) Field() Location {
	return action.Location
}

type ResultAction struct {
	Result_ Result
}

func (action *ResultAction) Result() Result {
	return action.Result_
}

type PlayerAction struct {
	IssuedAction
	ResultAction
	LocationAction
}

func (action *PlayerAction) Type() BattleActionType {
	return BattleActionType_Player
}

type SpawnBattleObjectAction struct {
	IssuedAction
	ResultAction
	LocationAction
	Object     *model.Monster
	Position   uint
	Side       uint
	Breeder    *model.Breeder
	ActionType BattleActionType
}

func (action *SpawnBattleObjectAction) Priority(Battle) int {
	return 0
}

func (action *SpawnBattleObjectAction) Type() BattleActionType {
	return action.ActionType
}

// Deprecated?
func (action *SpawnBattleObjectAction) Apply(b Battle) {
	objs := b.Field().Objects()
	objs[action.Side][action.Position] = action.Object

	for sideIndex, fieldSide := range b.Field().Objects() {
		if uint(sideIndex) == action.Side {
			continue
		}
		for _, mon := range fieldSide {
			if mon != nil {
				b.Matchups().Add(mon.ID, action.Object.ID)
			}
		}
	}

	action.Result_ = SpawnResult{
		SpawnLocation: Location{
			Side:     action.Side,
			Position: action.Position,
		},
		//Type:   *model.MonsterType_Monster,
		Object: action.Object,
	}

}

func (action *SpawnBattleObjectAction) String() string {
	return fmt.Sprintf("SpawnBattleObjectAction{position: %d, side: %d, actionType: %v}", action.Position, action.Side, action.ActionType)
}

type SendMonsterAction struct {
	IssuedAction
	ResultAction
	LocationAction
	TeamIndex  uint
	Position   uint
	Side       uint
	Breeder    *model.Breeder
	ActionType BattleActionType
}

func NewSendMonsterAction(teamIndex, side, position uint, by *model.Breeder) *SendMonsterAction {
	return &SendMonsterAction{
		TeamIndex: teamIndex,
		Position:  position,
		Side:      side,
		Breeder:   by,
		IssuedAction: IssuedAction{
			Breeder: by,
		},
		LocationAction: LocationAction{
			Location: Location{
				Position: position,
				Side:     side,
			},
		},
		ActionType: BattleActionType_Player,
	}

}

func (action *SendMonsterAction) Priority(Battle) int {
	return 0
}

func (action *SendMonsterAction) Type() BattleActionType {
	return action.ActionType
}

func (action *SendMonsterAction) Apply(b Battle) {
	logger := logrus.WithField("method", "SendMonsterAction.Apply")
	spoa := SpawnBattleObjectAction{
		Position:   action.Position,
		Side:       action.Side,
		Breeder:    action.Breeder,
		ActionType: action.ActionType,
		Object:     b.Field().Teams()[action.Breeder.ID][action.TeamIndex],
	}

	spoa.Apply(b)

	logger.WithField("monster", spoa.Object).Debugln("Spawned monster")

	action.Result_ = SendMonsterResult{
		SpawnLocation: Location{
			Side:     action.Side,
			Position: action.Position,
		},
		TeamIndex: action.TeamIndex,
	}

}

func (action *SendMonsterAction) String() string {
	return fmt.Sprintf("SendMonsterAction{position: %d, side: %d, actionType: %v}", action.Position, action.Side, action.ActionType)
}

func (action *SendMonsterAction) IsOK(b Battle) error {
	team := b.Field().Teams()[action.Breeder.ID]
	if action.TeamIndex >= uint(len(team)) {
		return fmt.Errorf("teamIndex out of bounds: %d", action.TeamIndex)
	}
	monster := b.Field().Teams()[action.Breeder.ID][action.TeamIndex]

	if monster.IsKO() {
		return fmt.Errorf("monster is unable to fight: %d", action.TeamIndex)
	}

	isValidPosition := false
	for _, breeder := range b.Field().Breeder()[action.Side] {
		isValidPosition = isValidPosition || (breeder.ID == action.Breeder.ID)
	}
	if !isValidPosition {
		return fmt.Errorf("can't send monster to another players side: %d", action.Side)
	}

	return nil
}

type AttackAction struct {
	PlayerAction
	LocationAction
	AttackerLocation Location
	DefenderLocation Location
	Move             uint
}

func NewAttackAction(moveIndex uint, attackerLocation, defenderLocation Location, by *model.Breeder) *AttackAction {
	return &AttackAction{
		Move:             moveIndex,
		AttackerLocation: attackerLocation,
		DefenderLocation: defenderLocation,
		PlayerAction: PlayerAction{
			IssuedAction: IssuedAction{
				Breeder: by,
			},
			LocationAction: LocationAction{
				Location: attackerLocation,
			},
		},
	}
}

func (action *AttackAction) Priority(b Battle) int {
	objs := b.Field().Objects()
	attacker := objs[action.AttackerLocation.Side][action.AttackerLocation.Position]
	return -int(attacker.Attributes().Initiative)
}

func (action *AttackAction) Apply(b Battle) {
	objs := b.Field().Objects()

	attacker := objs[action.AttackerLocation.Side][action.AttackerLocation.Position]
	attackerLocation := action.AttackerLocation
	defenderLocation := action.DefenderLocation
	defender := objs[action.DefenderLocation.Side][action.DefenderLocation.Position]

	if attacker == nil {
		return
	}

	// Can't attack if the attacker is knocked out
	if attacker.IsKO() {
		logrus.Infoln("Attacker is K.O.")
		return
	}

	if defender == nil {
		logrus.Infoln("Defender is nil")
		// TODO: Find other target, if there is no defender (?)
		return
	}

	var changes Changes

	//TODO: Move the damage calculation to the attack, so there can be attacks, which use a different formula
	chosenMove := attacker.Moves[action.Move]

	for _, mod := range chosenMove.Base().Modifier() {
		switch t := mod.(type) {
		case *model.DamageModifier:
			changes = append(changes, action.applyDamageModifier(defender, defenderLocation, *t, *attacker, *chosenMove)...)
		case *model.StatusModifier:
			changes = append(changes, action.applyStatusModifier(defender, defenderLocation, *t)...)
		}
		/*
			if dmgMod, ok := mod.(*model.DamageModifier); ok {
				dmgModifier = dmgMod
			}
		*/
	}

	changes = append(changes, action.updateStamina(attacker, attackerLocation, chosenMove.Base().StaminaUsage())...)

	// Remove defeated monster from the battle field
	if defender.IsKO() {
		handleMonsterDefeated(b, defender)
	}

	if attacker.IsKO() {
		handleMonsterDefeated(b, attacker)
	}

	chosenMove.ResetCooldown()

	action.Result_ = &AttackResult{
		AttackerLocation: attackerLocation,
		Move:             chosenMove.Base(),
		Changes: append(Changes{
			ChangeEntry{
				Change:   Change_Stamina,
				Value:    attacker.CurrentStaminaPercent(),
				Location: attackerLocation,
			},
		}, changes...),
	}
	logrus.WithField("result", action.Result_).Infoln("Created replay result")
}

func handleMonsterDefeated(b Battle, monster *model.Monster) {
	// Remove monster from the battle field
	for sideIndex, sides := range b.Field().Objects() {
		for positionIndex, mon := range sides {
			if mon != nil && mon.ID == monster.ID {
				b.Field().Objects()[sideIndex][positionIndex] = nil
			}
		}
	}

	countMatchup := len(b.Matchups()[monster.ID])

	// distribute experience points to its opponents
	for _, team := range b.Field().Teams() {
		for _, mon := range team {
			if _, ok := b.Matchups()[monster.ID][mon.ID]; ok && !mon.IsKO() {
				levelDiff := float64(mon.Level) / float64(monster.Level)
				exp := float64(monster.Species.BaseExp) / float64(countMatchup)
				mon.Exp += model.Exp(levelDiff*exp + 0.5) // 0.5 is necessary to round x.75 to x+1 and not x

				mon.TrainingModifier = mon.TrainingModifier.Add(monster.Species.TrainingModifier)
			}
		}

	}

}

func (action *AttackAction) updateStamina(attacker *model.Monster, attackerLocation Location, stamina int) Changes {
	var changes Changes
	staminaDiff := attacker.CurrentStamina - stamina
	if staminaDiff < 0 {
		attacker.ReduceStamina(attacker.CurrentStamina)
		c := reduceHP(attacker, attackerLocation, -staminaDiff)
		changes = append(changes, c...)
	} else {
		attacker.ReduceStamina(stamina)
	}

	return changes

}

func nameToAttribute(name string, monster model.Monster) model.AttributeStat {
	switch name {
	case "Health":
		return monster.Attributes().Health
	case "Stamina":
		return monster.Attributes().Stamina
	case "Physical Attack":
		return monster.Attributes().PhysAttack
	case "Physical Defense":
		return monster.Attributes().PhysDefense
	case "Special Attack":
		return monster.Attributes().SpecAttack
	case "Special Defense":
		return monster.Attributes().SpecDefense
	case "Initiative":
		return monster.Attributes().Initiative
	}
	return 0

}

func (action *AttackAction) applyDamageModifier(defender *model.Monster, defenderLocation Location, dmgModifier model.DamageModifier, attacker model.Monster, chosenMove model.Move) Changes {
	var changes Changes

	atkStat := nameToAttribute(dmgModifier.AttackStat, attacker)
	defStat := nameToAttribute(dmgModifier.DefenseStat, *defender)

	atkDefRatio := float64(atkStat) / float64(defStat)
	levelRatio := float64(attacker.Level) * 2 / (float64(defender.Level) + 50)

	dmg := atkDefRatio * levelRatio * float64(dmgModifier.Power) * model.GetTypeEffectivity(chosenMove.Base().Type(), defender.Types()...)

	// Not part of the damage formula itself, but used to round the value to next number
	dmg += 0.5
	logrus.WithFields(logrus.Fields{
		"atkDefRatio": atkDefRatio,
		"levelRatio":  levelRatio,
		"dmg":         dmg,
	}).Debugln("Calculated dmg")

	changes = append(changes, reduceHP(defender, defenderLocation, int(dmg))...)
	logrus.WithFields(logrus.Fields{
		"Attack":   atkStat,
		"Defense":  defStat,
		"Power":    dmgModifier.Power,
		"Damage":   dmg,
		"attacker": attacker.Species.Name,
		"move":     chosenMove.Base().Name,
	}).Debugln("Applied damage modifier")
	return changes
}

func (action *AttackAction) applyStatusModifier(defender *model.Monster, defenderLocation Location, statusModifier model.StatusModifier) Changes {
	var changes Changes

	ok := defender.ApplyStatus(statusModifier.Status, statusModifier.Power)
	if ok {
		s := []string{}
		for _, v := range defender.Status {
			s = append(s, v.Base.Name)
		}

		changes = append(changes, ChangeEntry{
			Change:   Change_Status,
			Value:    s,
			Location: defenderLocation,
		})
	}
	logrus.WithFields(logrus.Fields{
		"Applied Status": ok,
		"Status":         statusModifier.Status,
		"Power":          statusModifier.Power,
	}).Debugln("Applied status modifier")

	return changes
}

func (action *AttackAction) IsOK(b Battle) error {

	isValidPosition := false
	for _, breeder := range b.Field().Breeder()[action.AttackerLocation.Side] {
		isValidPosition = isValidPosition || (breeder.ID == action.Breeder.ID)
	}
	if !isValidPosition {
		return fmt.Errorf("can't attack with another players monster: %d", action.AttackerLocation.Side)
	}

	obj := b.Field().Objects()[action.AttackerLocation.Side][action.AttackerLocation.Position]

	if obj == nil {
		return fmt.Errorf("no monster at the given location: %v", action.AttackerLocation)
	}

	monster := obj

	if monster.IsKO() {
		return fmt.Errorf("monster is unable to fight: %v", action.AttackerLocation)
	}

	if action.Move >= uint(len(monster.Moves)) {
		return fmt.Errorf("moveIndex out of bounds: %d", action.Move)
	}

	move := monster.Moves[action.Move]
	if move.Base() == model.NilMoveBase {
		return fmt.Errorf("moveIndex out of bounds: %d", action.Move)
	}

	if !move.IsReady() {
		return fmt.Errorf("move is not ready: %d", action.Move)
	}

	return nil
}

func reduceHP(obj *model.Monster, location Location, amount int) Changes {
	var changes Changes
	obj.Damage(amount)
	changes = append(changes, ChangeEntry{
		Change:   Change_Health,
		Value:    obj.CurrentHPPercent(),
		Location: location,
	})
	if obj.CurrentHP <= 0 {
		changes = append(changes, ChangeEntry{
			Change:   Change_Status,
			Value:    status.KO,
			Location: location,
		})
	}
	return changes

}

type CaptureAction struct {
	PlayerAction
	TargetLocation Location
	Item           string
}

func (action *CaptureAction) Priority(b Battle) int {
	// Capture usage should act first most of the times, but let's leave some leeway
	return math.MinInt64 + 100
}

func (action *CaptureAction) Apply(b Battle) {
	issuerID := action.IssuedBy().ID

	inventory := b.Field().Inventories()[issuerID]
	for _, item := range inventory {
		if item.Base.Name == action.Item {
			item.Amount -= 1
		}
	}

	capturedMon := b.Field().Objects()[action.TargetLocation.Side][action.TargetLocation.Position]

	b.Field().Captured()[issuerID] = append(b.Field().Captured()[issuerID], capturedMon)

	b.Field().Objects()[action.TargetLocation.Side][action.TargetLocation.Position] = nil

	for breeder, team := range b.Field().Teams() {
		for index, mon := range team {
			if mon == capturedMon {

				b.Field().Teams()[breeder][index] = nil
			}
		}
	}

	action.Result_ = CaptureResult{
		Captured:   action.TargetLocation,
		Successful: true,
	}

}

func (action *CaptureAction) IsOK(b Battle) error {
	side := action.TargetLocation.Side
	position := action.TargetLocation.Position
	if side > uint(len(b.Field().Objects())) {
		return fmt.Errorf("Invalid target side: %d", side)
	}
	if position > uint(len(b.Field().Objects()[side])) {
		return fmt.Errorf("Invalid target position: %d", position)
	}
	obj := b.Field().Objects()[side][position]

	if obj == nil {
		return fmt.Errorf("no monster at the given location: %v", action.TargetLocation)
	}
	if obj.IsKO() {
		return fmt.Errorf("no monster at the given location: %v", action.TargetLocation)
	}

	inventory := b.Field().Inventories()[action.IssuedBy().ID]

	var itemExists *item.Item
	for _, v := range inventory {
		if v.Base.Name == action.Item {
			itemExists = v
		}
	}
	if itemExists == nil {
		return fmt.Errorf("breeder does not have item '%s' in their inventory", action.Item)
	}

	/* TODO: Capturing needs rework
	hasCaptureModifier := false
	for _, v := range itemExists.Base.Modifier {
		hasCaptureModifier = hasCaptureModifier || v.Type() == item.ModifierType_Capture
	}

	if !hasCaptureModifier {
		return fmt.Errorf("item '%s' can not be used to capture a monster", action.Item)
	}
	*/

	return nil
}

type ItemAction struct {
	PlayerAction
	TargetLocation Location
	Item           string
	TeamIndex      int
}

func (action *ItemAction) Priority(b Battle) int {
	// Item usage should act first most of the times, but let's leave some leeway
	return math.MinInt64 + 100
}

func (action *ItemAction) Apply(b Battle) {

	issuerID := action.IssuedBy().ID

	inventory := b.Field().Inventories()[issuerID]
	for _, item := range inventory {
		if item.Base.Name == action.Item {
			item.Amount -= 1
		}
	}

	b.Field().UsedItems()[issuerID][action.Item] += 1

	itemResult := ItemResult{
		Item:    action.Item,
		Changes: make(Changes, 0),
	}

	item := model.ItemCache[action.Item]

	l := prepareLua(b, action, &itemResult)
	l.SetGlobal("targetLocation", luar.New(l, action.TargetLocation))
	l.SetGlobal("teamIndex", luar.New(l, action.TeamIndex))
	if err := l.DoString(item.Effect.Battle); err != nil {
		logrus.WithError(err).Errorln("Error while running the item effect")
	}

	action.Result_ = itemResult
}

func (action *ItemAction) IsOK(b Battle) error {
	side := action.TargetLocation.Side
	position := action.TargetLocation.Position
	if side > uint(len(b.Field().Objects())) {
		return fmt.Errorf("Invalid target side: %d", side)
	}
	if position > uint(len(b.Field().Objects()[side])) {
		return fmt.Errorf("Invalid target position: %d", position)
	}
	obj := b.Field().Objects()[side][position]

	if obj == nil {
		return fmt.Errorf("no monster at the given location: %v", action.TargetLocation)
	}
	if obj.IsKO() {
		return fmt.Errorf("no monster at the given location: %v", action.TargetLocation)
	}

	inventory := b.Field().Inventories()[action.IssuedBy().ID]

	var itemExists *item.Item
	for _, v := range inventory {
		if v.Base.Name == action.Item {
			itemExists = v
		}
	}
	if itemExists == nil {
		return fmt.Errorf("breeder does not have item '%s' in their inventory", action.Item)
	}

	if len(itemExists.Base.Effect.Battle) == 0 {
		return fmt.Errorf("Item '%s' does not have an effect in battle", action.Item)
	}

	/* TODO: Capturing needs rework
	hasCaptureModifier := false
	for _, v := range itemExists.Base.Modifier {
		hasCaptureModifier = hasCaptureModifier || v.Type() == item.ModifierType_Capture
	}

	if !hasCaptureModifier {
		return fmt.Errorf("item '%s' can not be used to capture a monster", action.Item)
	}
	*/

	return nil
}

type SurrenderAction struct {
	PlayerAction
}

func NewSurrenderAction(location Location, by *model.Breeder) *SurrenderAction {
	return &SurrenderAction{
		PlayerAction: PlayerAction{
			LocationAction: LocationAction{
				Location: location,
			},
			IssuedAction: IssuedAction{
				Breeder: by,
			},
		},
	}
}

func (action *SurrenderAction) Priority(b Battle) int {
	// Surrender should act first most of the times, but let's leave some leeway
	return math.MinInt64 + 50
}

func (action *SurrenderAction) Apply(b Battle) {
	b.Surrender(action.Breeder)
}

func (action *SurrenderAction) IsOK(b Battle) error { return nil }

// Enforce interface implementation
// TODO: Move to battle_test?
var (
	_ BattleAction = &SpawnBattleObjectAction{}
	_ BattleAction = &AttackAction{}
	_ BattleAction = &CaptureAction{}
)

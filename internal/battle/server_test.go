//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package battle

import (
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

func init() {
	logrus.SetLevel(logrus.WarnLevel)
}

func TestStartStopServer(t *testing.T) {
	Convey("The server not be started", t, func() {
		s := NewServer()
		So(s.Started(), ShouldBeFalse)
		//So(s.Stopped(), ShouldBeTrue)
	})
	Convey("Starting the server", t, func() {
		s := NewServer()
		s.Start()
		Convey("The server should be started", func() {
			So(s.Started(), ShouldBeTrue)
			So(s.Stopped(), ShouldBeFalse)
		})
		Convey("Stopping the server", func() {
			s.Stop()
			Convey("The server should be stopped", func() {
				So(s.Started(), ShouldBeFalse)
				So(s.Stopped(), ShouldBeTrue)
			})
		})
	})
}

func TestCycle(t *testing.T) {
	Convey("Adding a battle to the server", t, func() {
		s := NewServer().(*defaultServer)
		b := &TestBattle{}
		s.battles[1] = b
		Convey("Calling cycle", func() {
			s.cycle()
			Convey("ProcessActions should be called", func() {
				So(b.processActionsCalled, ShouldEqual, 1)
			})
			Convey("ProcessPlayerActions should be called", func() {
				So(b.processPlayerActionsCalled, ShouldEqual, 1)
			})
			Convey("ProcessActions should be called before ProcessPlayerActions", func() {
				So(b.processActionsCalledTime, ShouldHappenBefore, b.processPlayerActionsCalledTime)
			})
		})

	})
	Convey("Adding multiple battle to the server", t, func() {
		s := NewServer().(*defaultServer)
		b1 := &TestBattle{}
		s.battles[1] = b1
		b2 := &TestBattle{}
		s.battles[2] = b2
		Convey("Calling cycle", func() {
			s.cycle()
			Convey("ProcessActions should be called", func() {
				So(b1.processActionsCalled, ShouldEqual, 1)
				So(b2.processActionsCalled, ShouldEqual, 1)
			})
			Convey("ProcessPlayerActions should be called", func() {
				So(b1.processPlayerActionsCalled, ShouldEqual, 1)
				So(b2.processPlayerActionsCalled, ShouldEqual, 1)
			})
		})
	})
}

func TestCreate(t *testing.T) {
	Convey("The server is not started", t, func() {
		s := NewServer()
		Convey("Creating a new battle should panic", func() {
			So(func() { s.Create(&TestBattle{}) }, ShouldPanic)
		})
	})
	Convey("Starting the server", t, func() {
		s := NewServer().(*defaultServer)
		s.Start()
		defer s.Stop()
		Convey("And creating a new battle", func() {
			b1 := &TestBattle{}
			id := s.Create(b1)
			Convey("The id should be 1", func() {
				So(id, ShouldEqual, 1)
			})
			Convey("battles should contain the battle", func() {
				bTest, ok := s.battles[1]
				So(ok, ShouldBeTrue)
				So(bTest, ShouldEqual, b1)
			})
			Convey("Creating another new battle", func() {
				b2 := &TestBattle{}
				id := s.Create(b2)
				Convey("The id should be 2", func() {
					So(id, ShouldEqual, 2)
				})
				Convey("battles should contain the battle", func() {
					bTest, ok := s.battles[2]
					So(ok, ShouldBeTrue)
					So(bTest, ShouldEqual, b2)
				})
			})
		})
	})
}

func TestGet(t *testing.T) {
	Convey("The server is not started", t, func() {
		s := NewServer()
		Convey("Trying to get a battle should panic", func() {
			So(func() { s.Get(1) }, ShouldPanic)
		})
	})
	Convey("The server is started", t, func() {
		s := NewServer().(*defaultServer)
		s.Start()
		defer s.Stop()
		Convey("And it already has a battle", func() {
			b1 := &TestBattle{}
			id := s.Create(b1)
			Convey("Asking for the battle should return it", func() {
				b, err := s.Get(id)
				So(err, ShouldBeNil)
				So(b, ShouldEqual, b1)
			})
			Convey("Asking for an unknown id should return an error", func() {
				b, err := s.Get(id + 1000)
				So(b, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

		})
	})
}

func TestGetByBreeder(t *testing.T) {
	Convey("The server is not started", t, func() {
		s := NewServer()
		Convey("Trying to get a battle should panic", func() {
			So(func() { s.GetByBreeder(1) }, ShouldPanic)
		})
	})
	Convey("The server is started", t, func() {
		s := NewServer().(*defaultServer)
		s.Start()
		defer s.Stop()
		Convey("And it already has a battle", func() {
			t1 := model.Breeder{ID: 1}
			t2 := model.Breeder{ID: 2}
			b1 := &TestBattle{participants: Participants{[]model.Breeder{t1}, []model.Breeder{t2}}}
			_ = s.Create(b1)
			Convey("Asking for the battle by the first breeder should return it", func() {
				b, err := s.GetByBreeder(t1.ID)
				So(err, ShouldBeNil)
				So(b, ShouldEqual, b1)
			})
			Convey("Asking for the battle by the second breeder should return it", func() {
				b, err := s.GetByBreeder(t2.ID)
				So(err, ShouldBeNil)
				So(b, ShouldEqual, b1)
			})
			Convey("Asking for an unknown id should return an error", func() {
				b, err := s.GetByBreeder(t1.ID + 1000)
				So(b, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})

		})
	})
}

func TestAddListener(t *testing.T) {
	Convey("The server is not started", t, func() {
		s := NewServer()
		Convey("Adding a new listener should panic", func() {
			So(func() { s.AddListener(1, make(chan BattleResult, 1)) }, ShouldPanic)
		})
	})
	Convey("Starting the server with a battle", t, func() {
		s := NewServer().(*defaultServer)
		s.Start()
		defer s.Stop()
		b1 := &TestBattle{ch: make(chan BattleResult, 1)}
		id := s.Create(b1)
		Convey("And adding a listener to an existing battle", func() {
			ch := make(chan BattleResult, 10)
			err := s.AddListener(id, ch)
			Convey("The server should have a listener", func() {
				So(s.listener[id], ShouldHaveLength, 1)
			})
			Convey("There should be no error", func() {
				So(err, ShouldBeNil)
			})
			Convey("The battle sends a BattleResult", func() {
				result := BattleResult{
					ID: 5,
				}
				b1.ch <- result
				Convey("The listener should contain the BattleResult", func() {
					timeout := time.After(time.Second)
					select {
					case res := <-ch:
						So(res, ShouldResemble, result)
					case <-timeout:
						So("this should not be called", ShouldBeNil)
					}
				})
			})
		})
		Convey("Add a listener to a non existent battle", func() {
			ch := make(chan BattleResult, 10)
			err := s.AddListener(id+1, ch)
			Convey("There should be an error", func() {
				So(err, ShouldNotBeNil)
			})
			Convey("The server should not have a listener", func() {
				So(s.listener[id+1], ShouldHaveLength, 0)
			})
		})
	})
}

type TestBattle struct {
	processActionsCalled       int
	processPlayerActionsCalled int

	processActionsCalledTime       time.Time
	processPlayerActionsCalledTime time.Time

	ch chan BattleResult

	id model.ID

	participants Participants
}

func (tb *TestBattle) ID() model.ID {
	return tb.id
}

func (tb *TestBattle) SetID(id model.ID) {
	tb.id = id
}

func (tb *TestBattle) Replay() chan BattleResult {
	return tb.ch
}

func (tb *TestBattle) SendAction(ba BattleAction) {}
func (tb *TestBattle) ProcessActions() {
	tb.processActionsCalled++
	tb.processActionsCalledTime = time.Now()
}
func (tb *TestBattle) ProcessPlayerActions() {
	tb.processPlayerActionsCalled++
	tb.processPlayerActionsCalledTime = time.Now()

}

func (tb *TestBattle) Participants() Participants {
	return tb.participants
}

func (tb *TestBattle) Field() BattleField { return battleField{} }
func (tb *TestBattle) CurrentRound() uint { return 0 }

func (tb *TestBattle) IsFinished() bool { return false }

func (tb *TestBattle) SendResult() {}

func (tb *TestBattle) PostTurnProcessing()      {}
func (tb *TestBattle) Surrender(*model.Breeder) {}
func (tb *TestBattle) Matchups() Matchups {
	return Matchups{}
}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package battle

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/item"
)

type Battle interface {
	ID() model.ID
	SetID(model.ID)
	Replay() chan BattleResult
	SendAction(ba BattleAction)
	ProcessActions()
	ProcessPlayerActions()
	Participants() Participants
	Field() BattleField
	CurrentRound() uint

	// IsFinished checks if the battle is finished
	// A battle is finished if at least one team has no remaining monsters left.
	IsFinished() (finished bool)

	// SendResult sends a message with the battle result to the replay channel
	SendResult()

	PostTurnProcessing()

	Surrender(by *model.Breeder)

	// Keeps track of which monster fought against which monster.
	// For exp purpose
	Matchups() Matchups
}

/*
Battle
*/
type battle struct {
	//TODO: can the participants be removed, since this is already in the battlefield?
	participants Participants
	currentRound uint
	field        BattleField

	// playerActions are actions directly issued by a player
	// These actions need to be sorted by priority, like who is faster
	playerActions chan BattleAction

	playerActionMapping map[model.ID]BattleAction

	// playerControlActions are actions directly issued by a player
	// These actions need to be executed immediatly
	playerControlActions chan BattleAction

	// systemActions are actions, which should be executed immediatly
	systemActions chan BattleAction

	// environmentActions are executed after the playerActions
	// These actions may contain effects which can't be evaluated in the player phase
	// They are fixed and will be executed after every round
	environmentActions []BattleAction

	// replay contains the actions in order of execution
	replay chan BattleResult

	id model.ID

	// Every turn, an action is executed for each field
	fieldActionMapping map[Location]BattleAction

	positionsPerSide uint

	resultSaver ResultSaver

	surrenderedBreeder *model.Breeder

	matchups Matchups
}

func (b *battle) Matchups() Matchups {
	return b.matchups
}

func (b battle) Participants() Participants {
	return b.participants
}

func (b battle) CurrentRound() uint {
	return b.currentRound
}

func (b battle) Field() BattleField {
	return b.field
}

func (b *battle) Replay() chan BattleResult {
	return b.replay
}

/*
SendAction accepts BattleActions, which should be applied to the current battle.

If a participant tries to send the same action multiple times per round, every action after the first one should be discarded.
*/
func (b *battle) SendAction(ba BattleAction) {
	logrus.WithField("action", ba).Debugln("Sending action...")
	switch ba.Type() {
	case BattleActionType_System:
		b.systemActions <- ba
	case BattleActionType_PlayerControl:
		b.playerControlActions <- ba
	case BattleActionType_Player:
		b.playerActions <- ba
	case BattleActionType_Environment:
	}

}

func (b *battle) ProcessActions() {
	logrus.Traceln("waiting for battle action...")
	select {
	case ba := <-b.systemActions:
		b.applyActions(ba)
	case ba := <-b.playerControlActions:
		b.applyActions(ba)
	case ba := <-b.playerActions:
		b.fieldActionMapping[ba.Field()] = ba
	default:
	}
}

func (b *battle) ProcessPlayerActions() {
	var locations []Location
	var mustSendMonster []Location
	for side := uint(0); side < 2; side++ {

		for position := uint(0); position < b.positionsPerSide; position++ {
			location := Location{
				Side:     side,
				Position: position,
			}
			mon := b.Field().Objects()[location.Side][location.Position]

			if mon == nil && b.canSendMonster(location) {
				mustSendMonster = append(mustSendMonster, location)
			}

			locations = append(locations, location)
		}
	}

	mustSendMonsterMode := len(mustSendMonster) > 0
	if mustSendMonsterMode {
		locations = mustSendMonster
	}

	var filteredActions []BattleAction

	for _, location := range locations {
		action, ok := b.fieldActionMapping[location]

		mon := b.Field().Objects()[location.Side][location.Position]

		if !ok {
			// No monster available for this location
			// Doesn't need an action
			if mon == nil && !b.canSendMonster(location) {
				continue

			} else {

				logrus.WithFields(logrus.Fields{
					"actions queued":    len(b.fieldActionMapping),
					"actions necessary": b.positionsPerSide * 2,
				}).Traceln("not enough actions")
				return
			}

		}

		if mustSendMonsterMode {
			// Only SendMonsterActions should be processed
			switch action.(type) {
			case *SendMonsterAction:
			default:
				continue
			}
		}

		filteredActions = append(filteredActions, action)
	}

	actions := make([]BattleAction, 0)
	for _, ba := range filteredActions {
		c := len(actions)
		// Insert the new action if its priority is less then the next queued action
		for j, val := range actions {
			if ba.Priority(b) < val.Priority(b) {
				c = j
				break
			}
		}
		// TODO: Clean up insertion sort
		// Something tells me that there might be a more elegant way to do this...
		actions = append(actions[:c], append([]BattleAction{ba}, actions[c:]...)...)
	}

	// Clear map to reset actions
	b.fieldActionMapping = make(map[Location]BattleAction)

	for _, ba := range actions {
		b.applyActions(ba)
	}

	b.PostTurnProcessing()
}

func (b *battle) PostTurnProcessing() {
	logrus.Infoln("Postprocessing turn")
	b.updateCooldowns()

	b.replay <- BattleResult{
		ResultType: BattleResult_NextTurn,
		ID:         0, // TODO: Increase with each send result

	}
}

func (b *battle) updateCooldowns() {

	for _, objs := range b.Field().Objects() {
		for _, monster := range objs {
			if monster == nil {
				continue
			}
			for _, atk := range monster.Moves {
				atk.IncreaseCooldown()
			}
		}
	}
}

func (b *battle) applyActions(ba BattleAction) {
	ba.Apply(b)
	if res := ba.Result(); res != nil {
		bRes := BattleResult{
			Breeder:    ba.IssuedBy().ID,
			Result:     res,
			ResultType: res.Type(),
			ID:         0, // TODO: Increase with each send result
		}
		b.replay <- bRes
	}
}

// IsFinished checks if the battle is finished
// A battle is finished if at least one team has no remaining monsters left.
func (b *battle) IsFinished() (finished bool) {

	if b.surrenderedBreeder != nil {
		return true
	}

	// team as in: a team of breeders
	for _, team := range b.Participants() {

		// Only one team needs to have no monster left
		// If this happens, we can abort
		if !b.hasMonsterLeft(team) {
			return true
		}

	}
	return false
}

func (b *battle) hasMonsterLeft(team []model.Breeder) bool {
	hasMonsterLeft := false
	for _, breeder := range team {

		// this is the team of a breeder, their team of monsters
		for _, monster := range b.field.Teams()[breeder.ID] {
			if monster == nil {
				continue
			}
			hasMonsterLeft = hasMonsterLeft || !monster.IsKO()

			// if this team has at least one monster left, we don't care about the rest
			if hasMonsterLeft {
				return true
			}
		}
	}
	return false

}

// canSendMonster checks if a monster can be send to a given location
func (b *battle) canSendMonster(location Location) bool {
	logger := logrus.WithField("method", "canSendMonster")
	team := b.Field().Breeder()[location.Side]
	var breeder model.Breeder

	// Check if it is a double or single player team
	isSinglePlayerTeam := uint(len(team)) < b.positionsPerSide
	if isSinglePlayerTeam {
		breeder = team[0]
	} else {
		breeder = team[location.Position]
	}

	monstersTeam := b.Field().Teams()[breeder.ID]

	monstersField := b.Field().Objects()[location.Side]

	sendable := 0

	// Count every monster in the team.
	// If it can't be send into battle, we will reduce the number again.
	// If there is at least one monster, which can be send into battle, sendable should be greater than 0
	for _, monTeam := range monstersTeam {
		sendable += 1
		// Can't send a monster if it is K.O.
		if monTeam.IsKO() {
			sendable -= 1
		}
		for _, monField := range monstersField {
			if monField == nil {
				continue
			}
			// Can't send a monster if it is on the battle field
			if monField.ID == monTeam.ID {
				sendable -= 1
			}
		}
	}

	var result bool
	if isSinglePlayerTeam {

		result = uint(sendable) > location.Position
	} else {

		result = sendable > 0
	}

	logger.WithField("sendable", result).Traceln("Exit")

	return result
}

// NotifyWinners sends a message with the battle result to the replay channel
func (b *battle) SendResult() {
	draw := false
	winnerTeamIndex := 0
	if b.surrenderedBreeder != nil {
		for _, v := range b.Field().Breeder()[0] {
			if v.ID == b.surrenderedBreeder.ID {
				winnerTeamIndex = 1
				break
			}
		}

	} else if b.hasMonsterLeft(b.Participants()[0]) {
		winnerTeamIndex = 0
	} else if b.hasMonsterLeft(b.Participants()[1]) {
		winnerTeamIndex = 1
	} else {
		draw = true
	}

	res := FinishedResult{
		IsDraw:      draw,
		WinnerIndex: winnerTeamIndex,
	}
	bRes := BattleResult{

		Result:     res,
		ResultType: res.Type(),
		ID:         0, // TODO: Increase with each send result
	}
	b.replay <- bRes

	for breeder, usedItems := range b.Field().UsedItems() {
		for item, amount := range usedItems {
			b.resultSaver.SaveUsedItem(breeder, item, amount)
		}
	}

	for breeder, cap := range b.Field().Captured() {

		for _, mon := range cap {

			b.resultSaver.CaptureMonster(breeder, *mon)
		}

	}

	for id, team := range b.Field().Teams() {
		if id == 0 {
			continue
		}
		for _, mon := range team {
			if mon != nil {
				b.resultSaver.SaveMonster(*mon)
			}
		}
	}

}

func (b *battle) Surrender(by *model.Breeder) {
	b.surrenderedBreeder = by
}

func (b *battle) ID() model.ID {
	return b.id
}

func (b *battle) SetID(id model.ID) {
	b.id = id
}

type battleField struct {
	breeder Participants
	teams   map[model.ID][]*model.Monster
	// Side, then position
	objects [][]*model.Monster

	inventories map[model.ID][]*item.Item

	usedItems map[model.ID]map[string]int

	captured map[model.ID][]*model.Monster
}

func (bf battleField) Teams() map[model.ID][]*model.Monster {
	return bf.teams
}

func (bf battleField) Inventories() map[model.ID][]*item.Item {
	return bf.inventories
}

func (bf battleField) Breeder() Participants {
	return bf.breeder
}

func (bf battleField) Objects() [][]*model.Monster {
	return bf.objects
}

func (bf battleField) UsedItems() map[model.ID]map[string]int {
	return bf.usedItems
}

func (bf battleField) Captured() map[model.ID][]*model.Monster {
	return bf.captured
}

type BattleType interface{}

/*
CreateBattle creates a new battle with the given participants and a battlefield of the given dimensions
The participants are divided into sides, so all breeder in the same subarray are in the same team
The battle is set to round 1.
*/
func CreateBattle(participants Participants, teams map[model.ID][]model.Monster /*battleType BattleType,*/, positionsPerSide uint, inventories map[model.ID][]item.Item, resultSaver ResultSaver) Battle {

	usedItems := make(map[model.ID]map[string]int)

	b := &battle{
		participants: participants,
		currentRound: 0,
		field: battleField{
			objects:     make([][]*model.Monster, 2),
			breeder:     participants,
			teams:       make(map[model.ID][]*model.Monster),
			inventories: make(map[model.ID][]*item.Item),
			usedItems:   usedItems,
			captured:    make(map[model.ID][]*model.Monster),
		},
		// Replace 20 with estimation of actions per player * amount player
		playerActions:        make(chan BattleAction, 20),
		playerControlActions: make(chan BattleAction, 20),
		systemActions:        make(chan BattleAction, 5),
		environmentActions:   make([]BattleAction, 10),
		replay:               make(chan BattleResult, 20),
		playerActionMapping:  make(map[model.ID]BattleAction),

		//TODO: initialize fieldActionMapping
		fieldActionMapping: make(map[Location]BattleAction),
		positionsPerSide:   positionsPerSide,

		resultSaver: resultSaver,

		matchups: make(Matchups),
	}

	for id, team := range teams {

		for _, mon := range team {
			mon := mon // change scope of mon
			b.field.Teams()[id] = append(b.field.Teams()[id], &mon)
		}
	}

	for id, inventory := range inventories {
		for _, item := range inventory {
			item := item
			b.field.Inventories()[id] = append(b.field.Inventories()[id], &item)
		}
	}

	for y := 0; y < 2; y++ {
		b.field.Objects()[y] = make([]*model.Monster, 2)
	}

	for _, v := range participants.Flatten() {
		usedItems[v.ID] = make(map[string]int)
	}

	return b
}

type Participants [][]model.Breeder

/*
TotalSize returns the total amount of participants
*/
func (p Participants) TotalSize() int {
	c := 0
	for i := 0; i < len(p); i++ {
		c += len(p[i])
	}
	return c
}

func (p Participants) Flatten() (breeder []model.Breeder) {
	for _, tr := range p {
		breeder = append(breeder, tr...)
	}
	return breeder

}

type BattleField interface {
	Teams() map[model.ID][]*model.Monster
	Breeder() Participants
	Objects() [][]*model.Monster
	Inventories() map[model.ID][]*item.Item

	UsedItems() map[model.ID]map[string]int

	// Captured contains a list of all the captured monsters in the battle.
	//
	// Note: Only if fighting against wild monsters.
	Captured() map[model.ID][]*model.Monster
}

type ResultSaver interface {
	CaptureMonster(breeder model.ID, monster model.Monster)
	SaveMonster(monster model.Monster)
	SaveUsedItem(breeder model.ID, item string, amount int)
}

type Matchups map[model.ID]map[model.ID]struct{}

func (m Matchups) Add(x, y model.ID) {
	_, ok := m[x]
	if !ok {
		m[x] = map[model.ID]struct{}{}
	}
	m[x][y] = struct{}{}

	_, ok = m[y]
	if !ok {
		m[y] = map[model.ID]struct{}{}
	}
	m[y][x] = struct{}{}

}

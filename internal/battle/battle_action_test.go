//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package battle_test

import (
	"testing"

	"github.com/sirupsen/logrus"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/spf13/viper"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/battle"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/data"
	"gitlab.com/kori-irrlicht/project-open-monster/internal/logic"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/memory"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/item"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/status"
)

func init() {
	logrus.SetLevel(logrus.WarnLevel)
	basepath_monster := "../../assets/data/monster"
	basepath_moves := "../../assets/data/moves"
	viper.SetDefault("data.dir.monster", basepath_monster)
	viper.SetDefault("data.dir.move", basepath_moves)

	if err := data.Load("../../assets/data", "file"); err != nil {
		panic(err)
	}

}

func TestItemAction(t *testing.T) {
	Convey("Given a battle", t, func() {
		t1 := model.Breeder{1, "Test1", []model.Monster{}}
		t2 := model.Breeder{2, "Test2", []model.Monster{}}

		teams := make(map[model.ID][]model.Monster)
		mon1 := *model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 5, 5 }, GMRange)
		mon2 := *model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 6, 6 }, GMRange)
		teams[t1.ID] = []model.Monster{mon1}
		teams[t2.ID] = []model.Monster{mon2}

		inventories := map[model.ID][]item.Item{
			t1.ID: []item.Item{
				item.Item{
					Base:   model.ItemCache["Potion"],
					Amount: 10,
				},
				item.Item{
					Base:   model.ItemCache["Capture Device"],
					Amount: 10,
				},
			},
			t2.ID: []item.Item{},
		}

		btl := battle.CreateBattle(battle.Participants{{t1}, {t2}}, teams, 1, inventories, nil)
		spoa := &battle.SendMonsterAction{
			TeamIndex: 0,
			Breeder:   &t1,
			Side:      0,
			Position:  0,
		}
		spoa.Apply(btl)
		spoa = &battle.SendMonsterAction{
			TeamIndex: 0,
			Breeder:   &t2,
			Side:      1,
			Position:  0,
		}
		spoa.Apply(btl)
		btl.Field().Teams()[t1.ID][0].Moves[0].IncreaseCooldown()

		Convey("Checking if the action can be executed", func() {
			Convey("A valid Action was issued", func() {
				atkA := battle.ItemAction{
					PlayerAction: battle.PlayerAction{
						IssuedAction: battle.IssuedAction{
							Breeder: &t1,
						},
						LocationAction: battle.LocationAction{
							Location: battle.Location{
								Side:     0,
								Position: 0,
							},
						},
					},
					TargetLocation: battle.Location{
						Side:     1,
						Position: 0,
					},
					Item: "Capture Device",
				}
				Convey("The action can be executed", func() {
					So(atkA.IsOK(btl), ShouldBeNil)
				})
				Convey("Capturing a monster", func() {
					atkA.Apply(btl)
					Convey("The monster should be captured", func() {
						So(btl.Field().Captured()[t1.ID], ShouldContain, &mon2)
					})
					Convey("The captured monster should not be on the battlefield anymore", func() {
						So(btl.Field().Objects()[1], ShouldNotContain, &mon2)
					})

					Convey("The captured monster should not be in the opponents team", func() {
						So(btl.Field().Teams()[t2.ID], ShouldNotContain, &mon2)
					})

					Convey("A capture device should be used", func() {
						So(btl.Field().Inventories()[t1.ID][1].Amount, ShouldEqual, 9)
					})
				})
			})
			Convey("There is no monster at the given location", func() {
				atkA := battle.ItemAction{
					PlayerAction: battle.PlayerAction{
						IssuedAction: battle.IssuedAction{
							Breeder: &t1,
						},
						LocationAction: battle.LocationAction{
							Location: battle.Location{
								Side:     0,
								Position: 0,
							},
						},
					},
					TargetLocation: battle.Location{
						Side:     1,
						Position: 1,
					},
					Item: "Capture Device",
				}
				Convey("The action can't be executed", func() {
					So(atkA.IsOK(btl), ShouldNotBeNil)
				})
			})
			Convey("If the monster is defeated", func() {
				btl.Field().Teams()[t2.ID][0].CurrentHP = 0
				atkA := battle.ItemAction{
					PlayerAction: battle.PlayerAction{
						IssuedAction: battle.IssuedAction{
							Breeder: &t1,
						},
						LocationAction: battle.LocationAction{
							Location: battle.Location{
								Side:     0,
								Position: 0,
							},
						},
					},
					TargetLocation: battle.Location{
						Side:     1,
						Position: 0,
					},
					Item: "Capture Device",
				}
				Convey("The action can't be executed", func() {
					So(atkA.IsOK(btl), ShouldNotBeNil)
				})
			})
			Convey("The itemName is incorrect: Unknown", func() {
				atkA := battle.ItemAction{
					PlayerAction: battle.PlayerAction{
						IssuedAction: battle.IssuedAction{
							Breeder: &t1,
						},
						LocationAction: battle.LocationAction{
							Location: battle.Location{
								Side:     0,
								Position: 0,
							},
						},
					},
					TargetLocation: battle.Location{
						Side:     1,
						Position: 0,
					},
					Item: "aaaCapture Device",
				}
				Convey("The action can't be executed", func() {
					So(atkA.IsOK(btl), ShouldNotBeNil)
				})
			})
			Convey("The itemName is incorrect: No battle effect", func() {
				atkA := battle.ItemAction{
					PlayerAction: battle.PlayerAction{
						IssuedAction: battle.IssuedAction{
							Breeder: &t1,
						},
						LocationAction: battle.LocationAction{
							Location: battle.Location{
								Side:     0,
								Position: 0,
							},
						},
					},
					TargetLocation: battle.Location{
						Side:     1,
						Position: 0,
					},
					Item: "Potion",
				}
				Convey("The action can't be executed", func() {
					So(atkA.IsOK(btl), ShouldNotBeNil)
				})
			})
		})
	})

}

func TestAttackAction(t *testing.T) {
	Convey("Given a battle", t, func() {
		t1 := model.Breeder{1, "Test1", []model.Monster{}}
		t2 := model.Breeder{2, "Test2", []model.Monster{}}

		teams := make(map[model.ID][]model.Monster)
		mon1 := *model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 5, 5 }, GMRange)
		mon2 := *model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 6, 6 }, GMRange)
		teams[t1.ID] = []model.Monster{mon1}
		teams[t2.ID] = []model.Monster{mon2}

		btl := battle.CreateBattle(battle.Participants{{t1}, {t2}}, teams, 1, nil, nil)
		spoa := &battle.SendMonsterAction{
			TeamIndex: 0,
			Breeder:   &t1,
			Side:      0,
			Position:  0,
		}
		spoa.Apply(btl)
		spoa = &battle.SendMonsterAction{
			TeamIndex: 0,
			Breeder:   &t2,
			Side:      1,
			Position:  0,
		}
		spoa.Apply(btl)
		btl.Field().Teams()[t1.ID][0].Moves[0].IncreaseCooldown()

		Convey("Checking if the action can be executed", func() {
			Convey("A valid Action was issued", func() {
				atkA := battle.AttackAction{
					PlayerAction: battle.PlayerAction{
						IssuedAction: battle.IssuedAction{
							Breeder: &t1,
						},
					},
					LocationAction: battle.LocationAction{
						Location: battle.Location{
							Side:     0,
							Position: 0,
						},
					},
					AttackerLocation: battle.Location{
						Side:     0,
						Position: 0,
					},
					DefenderLocation: battle.Location{
						Side:     1,
						Position: 0,
					},
					Move: 0,
				}
				Convey("The action can be executed", func() {
					So(atkA.IsOK(btl), ShouldBeNil)
				})
			})
			Convey("There is no monster at the given location", func() {
				atkA := battle.AttackAction{
					PlayerAction: battle.PlayerAction{
						IssuedAction: battle.IssuedAction{
							Breeder: &t1,
						},
					},
					LocationAction: battle.LocationAction{
						Location: battle.Location{
							Side:     0,
							Position: 1,
						},
					},
					AttackerLocation: battle.Location{
						Side:     0,
						Position: 1,
					},
					DefenderLocation: battle.Location{
						Side:     1,
						Position: 0,
					},
					Move: 0,
				}
				Convey("The action can't be executed", func() {
					So(atkA.IsOK(btl), ShouldNotBeNil)
				})
			})
			Convey("The monster does not belong to the breeder", func() {
				atkA := battle.AttackAction{
					PlayerAction: battle.PlayerAction{
						IssuedAction: battle.IssuedAction{
							Breeder: &t1,
						},
					},
					LocationAction: battle.LocationAction{
						Location: battle.Location{
							Side:     1,
							Position: 0,
						},
					},
					AttackerLocation: battle.Location{
						Side:     1,
						Position: 0,
					},
					DefenderLocation: battle.Location{
						Side:     1,
						Position: 0,
					},
					Move: 0,
				}
				Convey("The action can't be executed", func() {
					So(atkA.IsOK(btl), ShouldNotBeNil)
				})
			})
			Convey("If the monster is defeated", func() {
				btl.Field().Teams()[t1.ID][0].CurrentHP = 0
				atkA := battle.AttackAction{
					PlayerAction: battle.PlayerAction{
						IssuedAction: battle.IssuedAction{
							Breeder: &t1,
						},
					},
					LocationAction: battle.LocationAction{
						Location: battle.Location{
							Side:     0,
							Position: 0,
						},
					},
					AttackerLocation: battle.Location{
						Side:     0,
						Position: 0,
					},
					DefenderLocation: battle.Location{
						Side:     1,
						Position: 0,
					},
					Move: 0,
				}
				Convey("The action can't be executed", func() {
					So(atkA.IsOK(btl), ShouldNotBeNil)
				})
			})
			Convey("The attack is not ready", func() {
				btl.Field().Teams()[t1.ID][0].Moves[0].ResetCooldown()
				atkA := battle.AttackAction{
					PlayerAction: battle.PlayerAction{
						IssuedAction: battle.IssuedAction{
							Breeder: &t1,
						},
					},
					LocationAction: battle.LocationAction{
						Location: battle.Location{
							Side:     0,
							Position: 0,
						},
					},
					AttackerLocation: battle.Location{
						Side:     0,
						Position: 0,
					},
					DefenderLocation: battle.Location{
						Side:     1,
						Position: 0,
					},
					Move: 0,
				}
				Convey("The action can't be executed", func() {
					So(atkA.IsOK(btl), ShouldNotBeNil)
				})
			})
			Convey("The moveIndex is incorrect: Out of bounds", func() {
				atkA := battle.AttackAction{
					PlayerAction: battle.PlayerAction{
						IssuedAction: battle.IssuedAction{
							Breeder: &t1,
						},
					},
					LocationAction: battle.LocationAction{
						Location: battle.Location{
							Side:     0,
							Position: 0,
						},
					},
					AttackerLocation: battle.Location{
						Side:     0,
						Position: 0,
					},
					DefenderLocation: battle.Location{
						Side:     1,
						Position: 0,
					},
					Move: 6,
				}
				Convey("The action can't be executed", func() {
					So(atkA.IsOK(btl), ShouldNotBeNil)
				})
			})
			Convey("The moveIndex is incorrect: NilMoveBase", func() {
				atkA := battle.AttackAction{
					PlayerAction: battle.PlayerAction{
						IssuedAction: battle.IssuedAction{
							Breeder: &t1,
						},
					},
					LocationAction: battle.LocationAction{
						Location: battle.Location{
							Side:     0,
							Position: 0,
						},
					},
					AttackerLocation: battle.Location{
						Side:     0,
						Position: 0,
					},
					DefenderLocation: battle.Location{
						Side:     1,
						Position: 0,
					},
					Move: 3,
				}
				Convey("The action can't be executed", func() {
					So(atkA.IsOK(btl), ShouldNotBeNil)
				})
			})
		})
	})

}

func TestSendMonsterAction(t *testing.T) {
	Convey("Given a battle", t, func() {
		t1 := model.Breeder{1, "Test1", []model.Monster{}}
		t2 := model.Breeder{2, "Test2", []model.Monster{}}

		teams := make(map[model.ID][]model.Monster)
		mon1 := *model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 5, 5 }, GMRange)
		mon2 := *model.CreateMonster(model.SpeciesCache["Hoarfox"], func() (model.Level, model.Level) { return 6, 6 }, GMRange)
		mon1.ID = 1
		mon2.ID = 2

		teams[t1.ID] = []model.Monster{mon1}
		teams[t2.ID] = []model.Monster{mon2}

		btl := battle.CreateBattle(battle.Participants{{t1}, {t2}}, teams, 1, nil, nil)

		Convey("Requesting to spawn an object on the first side, second slot ", func() {

			spoa := &battle.SendMonsterAction{
				TeamIndex: 0,
				Breeder:   &t1,
				Side:      0,
				Position:  1,
			}
			spoa.Apply(btl)
			Convey("The object spawns at 1x2", func() {
				So(btl.Field().Objects()[0][1], ShouldResemble, &mon1)
			})
			Convey("Trying to spawn another object at the same location", func() {

				spoa := &battle.SendMonsterAction{
					TeamIndex: 0,
					Breeder:   &t2,
					Side:      0,
					Position:  1,
				}
				spoa.Apply(btl)
				Convey("Should overwrite the first object", func() {
					So(btl.Field().Objects()[0][1], ShouldNotResemble, &mon1)
					So(btl.Field().Objects()[0][1], ShouldResemble, &mon2)
				})

			})
			Convey("Spawning a monster on the other side", func() {

				spoa := &battle.SendMonsterAction{
					TeamIndex: 0,
					Breeder:   &t2,
					Side:      1,
					Position:  0,
				}
				spoa.Apply(btl)
				Convey("It should be spawned on the other side", func() {
					So(btl.Field().Objects()[1][0], ShouldResemble, &mon2)
				})
				Convey("The battle should contain the matchup of these monsters", func() {
					_, ok := btl.Matchups()[mon1.ID][mon2.ID]
					So(ok, ShouldBeTrue)
					_, ok = btl.Matchups()[mon2.ID][mon1.ID]
					So(ok, ShouldBeTrue)
				})

			})
		})

		Convey("Checking if the action can be executed", func() {
			Convey("A valid Action was issued", func() {
				spoa := &battle.SendMonsterAction{
					TeamIndex: 0,
					Breeder:   &t1,
					Side:      0,
					Position:  1,
				}
				Convey("The action can be executed", func() {
					So(spoa.IsOK(btl), ShouldBeNil)
				})
			})
			Convey("An invalid teamIndex was given", func() {
				spoa := &battle.SendMonsterAction{
					TeamIndex: 2,
					Breeder:   &t1,
					Side:      0,
					Position:  1,
				}
				Convey("The action can't be executed", func() {
					So(spoa.IsOK(btl), ShouldNotBeNil)
				})
			})
			Convey("If the monster is defeated", func() {
				btl.Field().Teams()[t1.ID][0].CurrentHP = 0
				spoa := &battle.SendMonsterAction{
					TeamIndex: 0,
					Breeder:   &t1,
					Side:      0,
					Position:  1,
				}
				Convey("The action can't be executed", func() {
					So(spoa.IsOK(btl), ShouldNotBeNil)
				})
			})
			Convey("Sending the monster to another players field", func() {
				spoa := &battle.SendMonsterAction{
					TeamIndex: 0,
					Breeder:   &t1,
					Side:      1,
					Position:  1,
				}
				Convey("The action can't be executed", func() {
					So(spoa.IsOK(btl), ShouldNotBeNil)
				})
			})
		})
	})

}

func TestBattleAction(t *testing.T) {
	Convey("Given a battle", t, func() {

		monsterDB := memory.NewMonsterDB()

		bc := logic.NewBreederCreator(memory.NewBreederDB(), monsterDB)
		t1 := model.Breeder{1, "Test1", []model.Monster{}}
		t2 := model.Breeder{2, "Test2", []model.Monster{}}
		t1, _ = bc.CreateBreeder(t1, model.SpeciesCache["Hoarfox"])
		t2, _ = bc.CreateBreeder(t2, model.SpeciesCache["Hoarfox"])

		team1, _ := monsterDB.LoadTeamByBreederId(t1.ID)

		dummy := model.CreateMonster(model.SpeciesCache["Dummy"], func() (model.Level, model.Level) { return 100, 100 }, GMRange)
		dummy.ID = 1

		monster := model.CreateMonster(model.SpeciesCache["Dummy"], func() (model.Level, model.Level) { return 100, 100 }, GMRange)
		monster.ID = 2

		btl := battle.CreateBattle(battle.Participants{{t1}, {t2}}, map[model.ID][]model.Monster{
			1: []model.Monster{*monster},
			2: []model.Monster{*dummy},
		}, 1, nil, nil)

		model.AddTypeMatchup("Ice", "Forest", 2)
		model.AddTypeMatchup("Ice", "Fire", 0.5)

		Convey("Requesting to spawn an object on the first side, second slot ", func() {

			spoa := &battle.SpawnBattleObjectAction{
				Object:   dummy,
				Side:     0,
				Position: 1,
			}
			spoa.Apply(btl)
			Convey("The object spawns at 1x2", func() {
				So(btl.Field().Objects()[0][1], ShouldEqual, dummy)
			})
			Convey("Trying to spawn another object at the same location", func() {
				dummy2 := model.CreateMonster(model.SpeciesCache["Dummy"], func() (model.Level, model.Level) { return 10, 10 }, GMRange)

				spoa := &battle.SpawnBattleObjectAction{
					Object:   dummy2,
					Side:     0,
					Position: 1,
				}
				spoa.Apply(btl)
				Convey("Should overwrite the first object", func() {
					So(btl.Field().Objects()[0][1], ShouldNotEqual, dummy)
					So(btl.Field().Objects()[0][1], ShouldEqual, dummy2)
				})

			})
		})
		Convey("Spawning a battle object", func() {

			spoa := &battle.SpawnBattleObjectAction{
				Object:   dummy,
				Side:     1,
				Position: 1,
			}
			spoa.Apply(btl)
			Convey("Spawning a monster", func() {

				monster.CurrentHP = 1
				spawnMonster := &battle.SpawnBattleObjectAction{
					Object:   monster,
					Side:     0,
					Position: 1,
				}
				spawnMonster.Apply(btl)
				Convey("with 160 attack", func() {
					monster.Species = &model.Species{
						Base: model.Attributes{PhysAttack: 165},
					}

					Convey("The monster attacks a battle object", func() {
						attackAction := &battle.AttackAction{
							PlayerAction: battle.PlayerAction{
								IssuedAction: battle.IssuedAction{
									Breeder: &t1,
								},
							},
							AttackerLocation: battle.Location{
								Side:     0,
								Position: 1,
							},
							DefenderLocation: battle.Location{
								Side:     1,
								Position: 1,
							},
							Move: 1,
						}
						attackAction.Apply(btl)
						Convey("So the battle object took 42 damage", func() {

							So(dummy.CurrentHP, ShouldEqual, dummy.Attributes().Health-42)
						})

					})

				})
				Convey("with 160 special attack", func() {
					monster.Species = &model.Species{
						Base: model.Attributes{SpecAttack: 165},
					}

					Convey("The monster attacks a battle object", func() {
						attackAction := &battle.AttackAction{
							PlayerAction: battle.PlayerAction{
								IssuedAction: battle.IssuedAction{
									Breeder: &t1,
								},
							},
							AttackerLocation: battle.Location{
								Side:     0,
								Position: 1,
							},
							DefenderLocation: battle.Location{
								Side:     1,
								Position: 1,
							},
							Move: 0,
						}
						attackAction.Apply(btl)
						Convey("So the battle object took 42 damage", func() {

							So(dummy.CurrentHP, ShouldEqual, dummy.Attributes().Health-42)
						})

					})

				})
				Convey("with 118 attack", func() {
					monster.Species = &model.Species{
						Base: model.Attributes{PhysAttack: 112},
					}

					Convey("The monster attacks a battle object", func() {
						attackAction := &battle.AttackAction{
							PlayerAction: battle.PlayerAction{
								IssuedAction: battle.IssuedAction{
									Breeder: &t1,
								},
							},
							AttackerLocation: battle.Location{
								Side:     0,
								Position: 1,
							},
							DefenderLocation: battle.Location{
								Side:     1,
								Position: 1,
							},
							Move: 1,
						}
						attackAction.Apply(btl)
						Convey("So the battle object took 30 damage", func() {
							So(dummy.CurrentHP, ShouldEqual, dummy.Attributes().Health-30)
						})

					})

				})
				Convey("with 119 attack", func() {
					monster.Species = &model.Species{
						Base: model.Attributes{PhysAttack: 114},
					}

					Convey("The monster attacks a battle object", func() {
						attackAction := &battle.AttackAction{
							PlayerAction: battle.PlayerAction{
								IssuedAction: battle.IssuedAction{
									Breeder: &t1,
								},
							},
							AttackerLocation: battle.Location{
								Side:     0,
								Position: 1,
							},
							DefenderLocation: battle.Location{
								Side:     1,
								Position: 1,
							},
							Move: 1,
						}
						attackAction.Apply(btl)
						Convey("So the battle object took 30 damage", func() {
							So(dummy.CurrentHP, ShouldEqual, dummy.Attributes().Health-30)
						})

					})

				})
				Convey("with status K.O.", func() {
					monster.Species = &model.Species{
						Base: model.Attributes{PhysAttack: 119},
					}
					monster.CurrentHP = 0

					Convey("The monster does not attack the battle object", func() {
						attackAction := &battle.AttackAction{
							PlayerAction: battle.PlayerAction{
								IssuedAction: battle.IssuedAction{
									Breeder: &t1,
								},
							},
							AttackerLocation: battle.Location{
								Side:     0,
								Position: 1,
							},
							DefenderLocation: battle.Location{
								Side:     1,
								Position: 1,
							},
						}
						attackAction.Apply(btl)
						Convey("So the battle object took no damage", func() {
							So(dummy.CurrentHP, ShouldEqual, dummy.Attributes().Health)
						})

					})

				})
				Convey("Reducing the enemies hp to zero", func() {
					monster.Species = &model.Species{
						Base: model.Attributes{PhysAttack: 360},
					}
					dummy.Damage(int(dummy.Attributes().Health - 20))
					//dummy.health = 20

					Convey("The monster does not attack the battle object", func() {
						attackAction := &battle.AttackAction{
							PlayerAction: battle.PlayerAction{
								IssuedAction: battle.IssuedAction{
									Breeder: &t1,
								},
							},
							AttackerLocation: battle.Location{
								Side:     0,
								Position: 1,
							},
							DefenderLocation: battle.Location{
								Side:     1,
								Position: 1,
							},
							Move: 1,
						}
						attackAction.Apply(btl)
						Convey("So the battle object should be K.O.", func() {
							So(dummy.CurrentHP, ShouldBeLessThanOrEqualTo, 0)
							So(dummy.IsKO(), ShouldBeTrue)
						})
						Convey("The monster should gain exp", func() {
							So(btl.Field().Teams()[1][0].Exp, ShouldEqual, 100)
						})
						Convey("The monster should gain training modifier", func() {
							So(btl.Field().Teams()[1][0].TrainingModifier, ShouldResemble, model.Attributes{
								Health:      1,
								Initiative:  1,
								PhysAttack:  1,
								PhysDefense: 1,
								SpecAttack:  1,
								SpecDefense: 1,
								Stamina:     1,
							})
						})

					})

				})
			})
			Convey("Spawning a monster with a typed attack", func() {

				monster := &team1[0]
				monster.Level = 100
				monster.GeneticModifier = model.Attributes{}
				spawnMonster := &battle.SpawnBattleObjectAction{
					Object:   monster,
					Side:     0,
					Position: 1,
				}
				spawnMonster.Apply(btl)

				Convey("The monster attacks a battle object of type \"Forest\" with an \"ice\" attack", func() {
					spec := *dummy.Species
					spec.Types[0] = "Forest"
					dummy.Species = &spec
					attackAction := &battle.AttackAction{
						PlayerAction: battle.PlayerAction{
							IssuedAction: battle.IssuedAction{
								Breeder: &t1,
							},
						},
						AttackerLocation: battle.Location{
							Side:     0,
							Position: 1,
						},
						DefenderLocation: battle.Location{
							Side:     1,
							Position: 1,
						},
						Move: 0,
					}
					Convey("And the target had no 'Freeze' damage", func() {

						attackAction.Apply(btl)
						Convey("So the battle object took 53 damage", func() {
							So(dummy.CurrentHP, ShouldEqual, dummy.Attributes().Health-53)
						})
						Convey("So the target took 'Freeze' damage", func() {
							So(dummy.StatusThreshold["Freeze"], ShouldEqual, 20)
						})
						Convey("So the target is not frozen", func() {
							So(dummy.Status, ShouldNotContain, status.New("Freeze"))
						})
						Convey("So the result should not contain a notification that the monster is frozen", func() {

							res := attackAction.Result().(*battle.AttackResult)
							So(res.Changes, ShouldNotContain, battle.ChangeEntry{
								Change:   battle.Change_Status,
								Value:    []status.Status{"Freeze"},
								Location: attackAction.DefenderLocation,
							})
						})
					})
					Convey("And the target had enough 'Freeze' damage", func() {
						dummy.ApplyStatus("Freeze", 99)
						attackAction.Apply(btl)
						Convey("So the battle object took 53 damage", func() {
							So(dummy.CurrentHP, ShouldEqual, dummy.Attributes().Health-53)
						})
						Convey("So the target took 'Freeze' damage", func() {
							So(dummy.StatusThreshold["Freeze"], ShouldEqual, 119)
						})
						Convey("So the target is frozen", func() {
							So(dummy.Status, ShouldContain, status.New("Freeze"))
						})
						Convey("So the result should contain a notification that the monster is frozen", func() {

							res := attackAction.Result().(*battle.AttackResult)
							So(res.Changes, ShouldContain, battle.ChangeEntry{
								Change:   battle.Change_Status,
								Value:    []string{"Freeze"},
								Location: attackAction.DefenderLocation,
							})
						})
					})

				})
				Convey("The monster attacks a battle object of type \"Fire\" with an \"ice\" attack", func() {
					spec := *dummy.Species
					spec.Types[0] = "Fire"
					dummy.Species = &spec
					attackAction := &battle.AttackAction{
						PlayerAction: battle.PlayerAction{
							IssuedAction: battle.IssuedAction{
								Breeder: &t1,
							},
						},
						AttackerLocation: battle.Location{
							Side:     0,
							Position: 1,
						},
						DefenderLocation: battle.Location{
							Side:     1,
							Position: 1,
						},
						Move: 0,
					}
					attackAction.Apply(btl)
					Convey("So the battle object took 13 damage", func() {
						So(dummy.CurrentHP, ShouldEqual, dummy.Attributes().Health-13)
					})

				})

			})
		})
	})
}

func GMRange() (model.AttributeStat, model.AttributeStat) {
	return 0, 0
}

//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.
package battle

import (
	"fmt"
	"sync"

	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

type Server interface {
	Start()
	Stop()
	Started() bool
	Stopped() bool

	// Run processes the each battle once.
	// Panics if the server is not started.
	// Must be called continuously to keep the server running
	Run()

	// Create adds the battle to the battle server.
	// If the server is started, the battle will be processed by the Run method
	// Panics if the server is not started.
	Create(b Battle) model.ID

	// AddListener adds a listener to the battle.
	// BattleResults will be written into the given channel.
	// Panics if the server is not started.
	// Returns a NotFoundError if no such battle exists
	AddListener(id model.ID, ch chan BattleResult) error

	// Get returns the Battle with the given ID.
	// Panics if the server is not started.
	// Returns a NotFoundError if no such battle exists
	Get(id model.ID) (Battle, error)

	// GetByBreeder returns the Battle in which the given breeder participates.
	// Panics if the server is not started.
	// Returns a NotFoundError if no such battle exists
	GetByBreeder(id model.ID) (Battle, error)
}

type defaultServer struct {
	started    bool
	battles    map[model.ID]Battle
	idProvider chan model.ID
	listener   map[model.ID][]chan BattleResult
	stopped    chan bool
	lock       sync.RWMutex
}

func NewServer() Server {
	return &defaultServer{
		started:    false,
		battles:    make(map[model.ID]Battle),
		idProvider: make(chan model.ID, 10),
		listener:   make(map[model.ID][]chan BattleResult),
		stopped:    make(chan bool, 2),
	}
}

// Implements Server interface
func (ds *defaultServer) Start() {
	logrus.Info("Starting battle server")
	go ds.generateIds()
	ds.started = true
	logrus.Info("Started battle server")
}

// handleListener waits for a new BattleResult from the given battle and sends it to all listeners.
func (ds *defaultServer) handleListener(battle Battle) {
	logrus.Infoln("Started to handle listener")
	ch := battle.Replay()
	for !ds.Stopped() {
		result := <-ch
		for _, v := range ds.listener[battle.ID()] {
			v <- result
		}

	}
	logrus.Infoln("Stopped to handle listener")
}

// generateIds continuously writes sequential numbers to the idProvider.
// Should be called in a goroutine, since the function only returns if the server is shutdown
func (ds *defaultServer) generateIds() {
	logrus.Infoln("Started generating ids")
	var i model.ID = 1
	for !ds.Stopped() {
		ds.idProvider <- i
		i++
	}
	logrus.Infoln("Stopped generating ids")
}

// cycle iterates over all battles and processes the queued actions
func (ds *defaultServer) cycle() {
	ds.lock.Lock()
	defer ds.lock.Unlock()
	for k, v := range ds.battles {
		v.ProcessActions()
		v.ProcessPlayerActions()

		if v.IsFinished() {
			// Notify and remove the battle
			v.SendResult()

			logrus.Debugln("Battle finished. Removing...")
			delete(ds.battles, k)
		}
	}
}

// Implements Server interface
func (ds *defaultServer) Run() {
	for !ds.Stopped() {
		ds.cycle()
	}
}

// Implements Server interface
func (ds *defaultServer) Started() bool { return ds.started }

// Implements Server interface
func (ds *defaultServer) Stop() {
	logrus.Info("Stopping battle server")
	ds.started = false
	ds.stopped <- true

	//drain id channel, so generateIds can exit
	if len(ds.idProvider) > 0 {
		<-ds.idProvider
	}

	logrus.Info("Stopped battle server")
}

// Implements Server interface
func (ds *defaultServer) Stopped() bool {
	select {
	case <-ds.stopped:
		ds.stopped <- true
		return true
	default:
		return false
	}
}

// Implements Server interface
func (ds *defaultServer) Get(id model.ID) (Battle, error) {
	ds.serverMustBeStarted()
	if b, ok := ds.battles[id]; ok {
		return b, nil
	}
	return nil, fmt.Errorf("could not find battle with id: %d", id)
}

// Implements Server interface
func (ds *defaultServer) Create(b Battle) model.ID {
	logger := logrus.WithField("method", "battleServer.Create")
	logger.Traceln("Entering")
	ds.lock.Lock()
	defer ds.lock.Unlock()
	ds.serverMustBeStarted()

	logger.Traceln("Retrieve id")
	var id model.ID = <-ds.idProvider
	logger.WithField("id", id).Traceln("Got id")

	ds.battles[id] = b
	b.SetID(id)
	go ds.handleListener(b)
	logger.Traceln("Leaving")
	return id
}

// Implements Server interface
func (ds *defaultServer) AddListener(id model.ID, ch chan BattleResult) error {
	ds.serverMustBeStarted()
	_, ok := ds.battles[id]
	if !ok {
		return fmt.Errorf("no battle with id: %d", id)
	}
	ds.listener[id] = append(ds.listener[id], ch)
	return nil
}

// serverMustBeStarted panics if the server is not started
func (ds *defaultServer) serverMustBeStarted() {
	if !ds.Started() {
		panic("Server not started")
	}

}

// Implements Server interface
func (ds *defaultServer) GetByBreeder(id model.ID) (Battle, error) {
	ds.serverMustBeStarted()
	for _, battle := range ds.battles {
		for _, team := range battle.Participants() {
			for _, participant := range team {
				if participant.ID == id {
					return battle, nil
				}
			}
		}
	}
	return nil, db.NotFoundError{Msg: fmt.Sprintf("no battle has a participant with id %d", id)}
}

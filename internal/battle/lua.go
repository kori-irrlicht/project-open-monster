//   Project OpenMonster
//   Copyright (C) 2020  Kori
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published
//   by the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU Affero General Public License for more details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/\>.

package battle

import (
	"fmt"

	lua "github.com/yuin/gopher-lua"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	luar "layeh.com/gopher-luar"
)

type API struct {
	battle Battle
	action *ItemAction
	result *ItemResult
}

//var _ logic.API = API{}

func (a API) GetTeam(breederID model.ID) (mons []model.Monster, err error) {
	team, ok := a.battle.Field().Teams()[breederID]
	if !ok {
		return []model.Monster{}, fmt.Errorf("No breeder with id %d on the field", breederID)
	}
	for _, mon := range team {
		mons = append(mons, *mon)
	}
	return

}

func (a API) HealMonster(monsterID model.ID) error {
	for _, team := range a.battle.Field().Teams() {
		for _, monster := range team {
			if monster.ID == monsterID {
				monster.CurrentHP = int(monster.Attributes().Health)
				return nil
			}

		}
	}
	return fmt.Errorf("no monster with ID %d in the battle", monsterID)
}

func (a API) HealTeam(breederID model.ID) error {

	team, ok := a.battle.Field().Teams()[breederID]
	if !ok {
		return fmt.Errorf("No breeder with id %d on the field", breederID)
	}
	for _, monster := range team {
		monster.CurrentHP = int(monster.Attributes().Health)
	}

	return nil
}

func (a API) Capture(targetLocation Location) {
	capturerID := a.action.IssuedBy().ID

	capturedMon := a.battle.Field().Objects()[targetLocation.Side][targetLocation.Position]

	a.battle.Field().Captured()[capturerID] = append(a.battle.Field().Captured()[capturerID], capturedMon)

	a.battle.Field().Objects()[targetLocation.Side][targetLocation.Position] = nil

	for breeder, team := range a.battle.Field().Teams() {
		for index, mon := range team {
			if mon == capturedMon {

				a.battle.Field().Teams()[breeder][index] = nil
			}
		}
	}

	a.result.Changes = append(a.result.Changes, ChangeEntry{
		Change:   Change_Capture,
		Location: targetLocation,
		Value:    true,
	})
}

func prepareLua(b Battle, ia *ItemAction, ir *ItemResult) *lua.LState {
	api := API{battle: b, action: ia, result: ir}

	L := lua.NewState(lua.Options{SkipOpenLibs: false})
	L.SetGlobal("api", luar.New(L, api))

	return L
}

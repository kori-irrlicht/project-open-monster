<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.1" name="BlackWhiteTiles" tilewidth="16" tileheight="16" tilecount="2" columns="2">
 <image source="../../../../../../../../../Pictures/BlackWhiteTiles.png" width="32" height="16"/>
 <tile id="0">
  <properties>
   <property name="group" value="floor"/>
   <property name="type" value="neutral"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="group" value="wall"/>
   <property name="type" value="neutral"/>
  </properties>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
</tileset>

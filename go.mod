module gitlab.com/kori-irrlicht/project-open-monster

go 1.13

require (
	github.com/EngoEngine/ecs v1.0.4
	github.com/EngoEngine/engo v1.0.5
	github.com/Noofbiz/tmx v0.2.0
	github.com/asdine/storm/v3 v3.2.1
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20200707082815-5321531c36a2 // indirect
	github.com/hajimehoshi/go-mp3 v0.3.1 // indirect
	github.com/hajimehoshi/oto v0.6.4 // indirect
	github.com/magiconair/properties v1.8.2 // indirect
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.6.0
	github.com/smartystreets/goconvey v1.6.4
	github.com/spf13/afero v1.3.4 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v1.0.0
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.7.1
	github.com/yuin/gopher-lua v0.0.0-20200816102855-ee81675732da
	gitlab.com/kori-irrlicht/goconvey-assertions v0.0.0-20200602154717-77de3927e64c
	gitlab.com/kori-irrlicht/hekate v0.1.4
	go.etcd.io/bbolt v1.3.5
	golang.org/x/exp v0.0.0-20200821190819-94841d0725da // indirect
	golang.org/x/image v0.0.0-20200801110659-972c09e46d76 // indirect
	golang.org/x/mobile v0.0.0-20200801112145-973feb4309de // indirect
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	golang.org/x/sys v0.0.0-20200905004654-be1d3432aa8f // indirect
	gopkg.in/ini.v1 v1.60.2 // indirect
	layeh.com/gopher-luar v1.0.8
)

replace github.com/spf13/viper => github.com/kori-irrlicht/viper v0.0.0-20200604163519-2710ee5ba441

replace github.com/Noofbiz/tmx => github.com/kori-irrlicht/tmx v0.0.0-20200912134043-c68ab98ff38f

replace github.com/EngoEngine/engo => github.com/kori-irrlicht/engo v0.0.0-20200806150508-d3e25cd8af97

replace github.com/hajimehoshi/oto => github.com/kori-irrlicht/oto v0.0.0-20200806154908-17584273695c
